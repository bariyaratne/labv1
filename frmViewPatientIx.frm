VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmViewPatientIx 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "View Request Bills"
   ClientHeight    =   8385
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11760
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8385
   ScaleWidth      =   11760
   Begin VB.TextBox txtBillId 
      Height          =   375
      Left            =   8400
      TabIndex        =   57
      Top             =   1080
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtPhone 
      Height          =   360
      Left            =   5760
      TabIndex        =   54
      Top             =   1080
      Width           =   2055
   End
   Begin VB.TextBox txtUserID 
      Height          =   375
      Left            =   7920
      TabIndex        =   51
      Top             =   2040
      Visible         =   0   'False
      Width           =   375
   End
   Begin MSComCtl2.DTPicker dtpTime 
      Height          =   375
      Left            =   9000
      TabIndex        =   50
      Top             =   600
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   661
      _Version        =   393216
      Format          =   101187586
      CurrentDate     =   40447
   End
   Begin MSComCtl2.DTPicker dtpDate 
      Height          =   375
      Left            =   9000
      TabIndex        =   49
      Top             =   120
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "yyyy MMMM dd"
      Format          =   101187587
      CurrentDate     =   40447
   End
   Begin VB.TextBox txtPatientIxBillID 
      Height          =   375
      Left            =   960
      TabIndex        =   46
      Top             =   600
      Visible         =   0   'False
      Width           =   975
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2775
      Left            =   7920
      TabIndex        =   29
      Top             =   4560
      Width           =   3645
      _ExtentX        =   6429
      _ExtentY        =   4895
      _Version        =   393216
      Tab             =   1
      TabHeight       =   520
      TabCaption(0)   =   "Cash"
      TabPicture(0)   =   "frmViewPatientIx.frx":0000
      Tab(0).ControlEnabled=   0   'False
      Tab(0).Control(0)=   "txtDiscountPercent"
      Tab(0).Control(1)=   "txtGTotal"
      Tab(0).Control(2)=   "txtDiscount"
      Tab(0).Control(3)=   "txtNTotal"
      Tab(0).Control(4)=   "Label2"
      Tab(0).Control(5)=   "Label4"
      Tab(0).Control(6)=   "Label5"
      Tab(0).Control(7)=   "Label1"
      Tab(0).ControlCount=   8
      TabCaption(1)   =   "Credit"
      TabPicture(1)   =   "frmViewPatientIx.frx":001C
      Tab(1).ControlEnabled=   -1  'True
      Tab(1).Control(0)=   "Label15"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Label21"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "cmbCompany"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "txtCardNo"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Ward"
      TabPicture(2)   =   "frmViewPatientIx.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "txtBHT"
      Tab(2).Control(1)=   "cmbBHT"
      Tab(2).Control(2)=   "cmbRoom"
      Tab(2).Control(3)=   "Label18"
      Tab(2).Control(4)=   "Label17"
      Tab(2).Control(5)=   "Label16"
      Tab(2).ControlCount=   6
      Begin VB.TextBox txtCardNo 
         Height          =   375
         Left            =   240
         TabIndex        =   55
         Top             =   1800
         Width           =   3255
      End
      Begin VB.TextBox txtBHT 
         Height          =   375
         Left            =   -74760
         TabIndex        =   44
         Top             =   600
         Width           =   3255
      End
      Begin VB.TextBox txtDiscountPercent 
         Height          =   375
         Left            =   -73680
         TabIndex        =   33
         Top             =   1320
         Width           =   495
      End
      Begin VB.TextBox txtGTotal 
         Height          =   375
         Left            =   -73680
         TabIndex        =   32
         Top             =   600
         Width           =   2055
      End
      Begin VB.TextBox txtDiscount 
         Height          =   360
         Left            =   -72840
         TabIndex        =   31
         Top             =   1320
         Width           =   1215
      End
      Begin VB.TextBox txtNTotal 
         Height          =   375
         Left            =   -73680
         TabIndex        =   30
         Top             =   2040
         Width           =   2055
      End
      Begin MSDataListLib.DataCombo cmbCompany 
         Height          =   360
         Left            =   240
         TabIndex        =   38
         Top             =   960
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
      Begin MSDataListLib.DataCombo cmbBHT 
         Height          =   360
         Left            =   -74760
         TabIndex        =   41
         Top             =   1440
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
      Begin MSDataListLib.DataCombo cmbRoom 
         Height          =   360
         Left            =   -74760
         TabIndex        =   43
         Top             =   2160
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
      Begin VB.Label Label21 
         AutoSize        =   -1  'True
         Caption         =   "Card No"
         Height          =   240
         Left            =   240
         TabIndex        =   56
         Top             =   1560
         Width           =   690
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "&New BHT"
         Height          =   240
         Left            =   -74760
         TabIndex        =   45
         Top             =   360
         Width           =   780
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Room"
         Height          =   240
         Left            =   -74760
         TabIndex        =   42
         Top             =   1920
         Width           =   495
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Previous BHT"
         Height          =   240
         Left            =   -74760
         TabIndex        =   40
         Top             =   1200
         Width           =   1125
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Company"
         Height          =   240
         Left            =   240
         TabIndex        =   39
         Top             =   600
         Width           =   795
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "&Total"
         Height          =   240
         Left            =   -74760
         TabIndex        =   37
         Top             =   600
         Width           =   435
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Dis&count"
         Height          =   240
         Left            =   -74760
         TabIndex        =   36
         Top             =   1320
         Width           =   720
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "N&et Total"
         Height          =   240
         Left            =   -74760
         TabIndex        =   35
         Top             =   2040
         Width           =   780
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "%"
         Height          =   240
         Left            =   -73080
         TabIndex        =   34
         Top             =   1320
         Width           =   180
      End
   End
   Begin VB.TextBox txtTotalCharge 
      Height          =   375
      Left            =   7920
      TabIndex        =   28
      Top             =   3840
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtOtherCOst 
      Height          =   375
      Left            =   7920
      TabIndex        =   27
      Top             =   1080
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtExpenceCost 
      Height          =   375
      Left            =   7920
      TabIndex        =   26
      Top             =   600
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtMaterialCost 
      Height          =   375
      Left            =   7920
      TabIndex        =   25
      Top             =   120
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtOtherCharge 
      Height          =   375
      Left            =   7920
      TabIndex        =   24
      Top             =   2400
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtStaffCharge 
      Height          =   375
      Left            =   7920
      TabIndex        =   23
      Top             =   2880
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtHospitalCharge 
      Height          =   375
      Left            =   7920
      TabIndex        =   22
      Top             =   3360
      Visible         =   0   'False
      Width           =   375
   End
   Begin VB.TextBox txtName 
      Height          =   360
      Left            =   2520
      TabIndex        =   1
      Top             =   120
      Width           =   5295
   End
   Begin VB.TextBox txtY 
      Height          =   375
      Left            =   2520
      TabIndex        =   8
      Top             =   1080
      Width           =   2295
   End
   Begin VB.TextBox txtM 
      Height          =   375
      Left            =   3600
      TabIndex        =   10
      Top             =   1080
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox txtD 
      Height          =   375
      Left            =   4800
      TabIndex        =   12
      Top             =   1560
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox txtCost 
      Height          =   375
      Left            =   7920
      TabIndex        =   21
      Top             =   1560
      Visible         =   0   'False
      Width           =   375
   End
   Begin btButtonEx.ButtonEx btnReprint 
      Height          =   375
      Left            =   1560
      TabIndex        =   19
      Top             =   7920
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Reprint"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   10320
      TabIndex        =   20
      Top             =   7800
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid GridIx 
      Height          =   4815
      Left            =   120
      TabIndex        =   18
      Top             =   2520
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   8493
      _Version        =   393216
      SelectionMode   =   1
   End
   Begin MSDataListLib.DataCombo cmbDoc 
      Height          =   360
      Left            =   2520
      TabIndex        =   15
      Top             =   1560
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmbIns 
      Height          =   360
      Left            =   2520
      TabIndex        =   17
      Top             =   2040
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDOB 
      Height          =   375
      Left            =   5760
      TabIndex        =   13
      Top             =   1560
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   95158275
      CurrentDate     =   39773
   End
   Begin MSDataListLib.DataCombo cmbTitle 
      Height          =   360
      Left            =   2520
      TabIndex        =   3
      Top             =   600
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbSex 
      Height          =   360
      Left            =   4800
      TabIndex        =   5
      Top             =   600
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx btnCancel 
      Height          =   375
      Left            =   120
      TabIndex        =   52
      Top             =   7920
      Visible         =   0   'False
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Cancel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label20 
      AutoSize        =   -1  'True
      Caption         =   "Phone"
      Height          =   240
      Left            =   5040
      TabIndex        =   53
      Top             =   1080
      Width           =   525
   End
   Begin VB.Label Label19 
      AutoSize        =   -1  'True
      Caption         =   "Time"
      Height          =   240
      Left            =   8400
      TabIndex        =   48
      Top             =   600
      Width           =   435
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Date"
      Height          =   240
      Left            =   8400
      TabIndex        =   47
      Top             =   120
      Width           =   390
   End
   Begin VB.Label Label14 
      AutoSize        =   -1  'True
      Caption         =   "Age"
      Height          =   240
      Left            =   120
      TabIndex        =   6
      Top             =   1080
      Width           =   330
   End
   Begin VB.Label Label13 
      AutoSize        =   -1  'True
      Caption         =   "&Patient Name"
      Height          =   240
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1140
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      Caption         =   "Title"
      Height          =   240
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   375
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      Caption         =   "Sex"
      Height          =   240
      Left            =   4440
      TabIndex        =   4
      Top             =   600
      Width           =   315
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      Caption         =   "Years"
      Height          =   240
      Left            =   2520
      TabIndex        =   7
      Top             =   1080
      Width           =   480
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Caption         =   "Months"
      Height          =   240
      Left            =   3600
      TabIndex        =   9
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      Caption         =   "Days"
      Height          =   240
      Left            =   4800
      TabIndex        =   11
      Top             =   1560
      Width           =   405
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Referring Doctor"
      Height          =   240
      Left            =   120
      TabIndex        =   14
      Top             =   1560
      Width           =   1410
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "Referring Institution"
      Height          =   240
      Left            =   120
      TabIndex        =   16
      Top             =   2040
      Width           =   1695
   End
End
Attribute VB_Name = "frmViewPatientIx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'    Dim rsViewPatient As New ADODB.Recordset
    Dim rsViewIx As New ADODB.Recordset
    Dim rsPatientIxBill As New ADODB.Recordset
    Dim rsPatientIx As New ADODB.Recordset
    Dim rsPatient As New ADODB.Recordset
    Dim rsIx As New ADODB.Recordset
    Dim rsBHT As New ADODB.Recordset

    Public temPatientIxBillID As Long
    Dim MyPatient As New clsPatient


    Dim temSQL As String
    Dim cSetDfltPrinter As New cSetDfltPrinter
    Dim temtubeID() As Long
    Dim temTubeVol() As Double
    Dim temTube() As String
    Dim temTubeCount As Long

Private Sub FillGrid()
    Dim i As Integer
    With rsPatientIx
        If .State = 1 Then .Close
        temSQL = "SELECT tblIx.*, tblPatientIx.* FROM tblPatientIx LEFT JOIN tblIx ON tblPatientIx.IxID = tblIx.IxID where tblPatientIx.Deleted = False AND PatientIxBillID = " & temPatientIxBillID
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
                gridIx.Rows = gridIx.Rows + 1
                gridIx.Row = gridIx.Rows - 1
                gridIx.col = 0
                gridIx.text = gridIx.Row
                gridIx.col = 1
                gridIx.text = ![Ix]
                gridIx.col = 2
                gridIx.text = Format(![tblPatientIx.Value], "0.00")
            .MoveNext
        Wend
    End With
End Sub


Private Sub btnCancel_Click()
    Dim i As Integer
    i = MsgBox("Are you sure you want to cancel this bill?", vbYesNo)
    If i = vbNo Then Exit Sub
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblPatientIxBill where PatientIxBillID = " & temPatientIxBillID
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Cancelled = True
            !CancelledUserID = UserID
            !CancelledDate = Date
            !CancelledTime = time
            .Update
        End If
        .Close
    End With
    MsgBox "Bill Cancelled"
    Dim newViewForm As New frmViewPatientIx
    newViewForm.txtPatientIxBillID = temPatientIxBillID
    newViewForm.Show
    Unload Me
End Sub

Private Sub btnReprint_Click()
    printLabBillPos Val(txtPatientIxBillID.text), BillPrinterName, BillPaperName, Me.hdc, Me, 1
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub




Private Sub DisplayPatientDetails()
    txtPatientIxBillID.text = temPatientIxBillID
    With rsPatientIxBill
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblPatientIxBill where PatientIxBillID = " & temPatientIxBillID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            dtpDate.Value = !Date
            dtpTime.Value = !time
            txtUserID.text = !UserID

            txtGTotal.text = Format(!Value, "0.00")
            txtDiscount.text = Format(!Discount, "0.00")
            txtNTotal.text = Format(!NetValue, "0.00")

            If !Cancelled = True Then
                btnReprint.Enabled = False
                btnCancel.Enabled = False
            End If

            If UserID <> !UserID Then
                btnReprint.Enabled = False
                btnCancel.Enabled = False
            End If

            If Date <> !Date Then
                btnReprint.Enabled = False
                btnCancel.Enabled = False
            End If
            MyPatient.ID = !PatientID

            cmbTitle.text = MyPatient.Title
            txtName.text = MyPatient.Name
            txtY.text = MyPatient.AgeInWords
            cmbSex.text = MyPatient.Sex
            txtPhone.text = MyPatient.Phone

            If Not IsNull(!DoctorID) Then
                cmbDoc.BoundText = !DoctorID
            Else
                cmbDoc.BoundText = 0
            End If
            
            If Not IsNull(!InstitutionID) Then
                cmbIns.BoundText = !InstitutionID
            Else
                cmbIns.BoundText = 0
            End If
            
            If !CompanyID = 0 And !BHTID = 0 And !RoomID = 0 Then
                SSTab1.Tab = 0
            ElseIf !CompanyID <> 0 Then
                SSTab1.Tab = 1
                cmbCompany.BoundText = !CompanyID
                If Not IsNull(!CardNo) Then
                    txtCardNo.text = !CardNo
                End If
            ElseIf !BHTID <> 0 Then
                SSTab1.Tab = 2
                cmbBHT.BoundText = !BHTID
                cmbRoom.BoundText = !RoomID
            End If
        End If
        .Close
    End With
End Sub

Private Sub Form_Load()
    Call FillCombos
    Call FormatGrid
    Call GetSettings
    Call LockMyControls
End Sub


Private Sub LockMyControls()
    Dim MyCtrl As VB.Control
    For Each MyCtrl In Controls
        If TypeOf MyCtrl Is ComboBox Or TypeOf MyCtrl Is TextBox Then
            MyCtrl.Locked = True
        End If
    Next
    For Each MyCtrl In Controls
        If TypeOf MyCtrl Is DTPicker Then
            MyCtrl.Enabled = False
        End If
    Next
    
    For Each MyCtrl In Controls
        If TypeOf MyCtrl Is SSTab Then
            MyCtrl.Enabled = False
        End If
    Next
End Sub




Private Sub GetSettings()
End Sub

Private Sub SaveSettings()

End Sub


Private Sub FormatGrid()
    With gridIx
        .Rows = 1
        .Cols = 3
        .Row = 0
        .col = 0
        .CellAlignment = 4
        .text = "No."
        .col = 1
        .CellAlignment = 4
        .text = "Investigation"
        .col = 2
        .CellAlignment = 4
        .text = "Value"
    End With

End Sub

Public Sub FillCombos()
    Dim RDoc As New clsFillCombos
    Dim RIns As New clsFillCombos
    Dim Sex As New clsFillCombos
    Dim Title As New clsFillCombos

    Dim Room As New clsFillCombos
    Dim Company As New clsFillCombos

    Company.FillAnyCombo cmbCompany, "Company", True

    Room.FillAnyCombo cmbRoom, "Room", True


    Sex.FillAnyCombo cmbSex, "Sex", False
    Title.FillAnyCombo cmbTitle, "Title", False

    RDoc.FillAnyCombo cmbDoc, "Doctor", True
    RIns.FillAnyCombo cmbIns, "Institution", True

    With rsBHT
        temSQL = "Select * from tblBHT where Discharged = false AND Admitted = true order by BHT"
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With cmbBHT
        Set .RowSource = rsBHT
        .ListField = "BHT"
        .BoundColumn = "BHTID"
        .text = Empty
    End With
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSettings
End Sub


Private Sub eLabPrint(PatientID As Long)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyPatient As New clsPatient
    MyPatient.ID = PatientID
    
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    If SelectForm(BillPaperName, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer



        Printer.Font = "Arial Black"
'        Printer.Print
'        Printer.Print
'        Printer.Print
'
        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(HospitalName) / 2)
        Printer.Print "" ' HospitalName
        
        Printer.FontSize = 11
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(HospitalAddress) / 2)
        Printer.Print "" 'HospitalAddress
        
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(HospitalDescreption) / 2)
        Printer.Print "" ' HospitalDescreption
        
    Tab1 = 8
    Tab2 = 13
    Tab3 = 52
    Tab4 = 46
    Tab5 = 46
    tab6 = 7
    tab7 = 9
    tab8 = 6
    tab9 = 43

    Printer.FontName = "Courier"
    Printer.FontSize = 9
    Printer.Print
'    Printer.Print
'    Printer.Print
'    Printer.Print
    Printer.Print Tab(Tab1); "Patient Name : " & MyPatient.Title & " " & MyPatient.Name; Tab(Tab3); "Date : " & Format(Date, "yyyy MM dd")
    Printer.Print Tab(Tab1); "Patient Age  : " & MyPatient.AgeInWords; Tab(Tab3); "Time : " & Format(time, "hh:mm AMPM")
    
    If SSTab1.Tab = 0 Then
        Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone; Tab(Tab3); "Payment : Cash"
        Printer.Print Tab(Tab1); "Referral  : " & cmbDoc.text;
    ElseIf SSTab1.Tab = 1 Then
        Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone; Tab(Tab3); "Payment : Credit(Company)"
        Printer.Print Tab(Tab1); "Referral  : " & cmbDoc.text; Tab(Tab3); "Company : " & cmbCompany.text & " (" & txtCardNo.text & ")"
    ElseIf SSTab1.Tab = 2 Then
        Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone; Tab(Tab3); "Payment : Credit(Inward)"
        Printer.Print Tab(Tab1); "Referral  : " & cmbDoc.text; Tab(Tab3); "BHT : " & cmbBHT.text & " (" & cmbRoom.text & ")"
    End If
    
    Printer.FontName = "Courier"
    Printer.FontSize = 11
    Printer.FontBold = True
    Printer.Print Tab(Tab1); "Bill No.     : " & temPatientIxBillID
    
    
    Printer.FontName = "Courier"
    Printer.FontSize = 9
    Printer.FontBold = False
    
    Dim MyLine As String
    MyLine = "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8
    Tab2 = 13
    Tab3 = 60
    Tab4 = 46
    
    
    Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    Printer.Print Tab(Tab1); MyLine

    With gridIx
        For i = 1 To .Rows - 1
'            Printer.Print Tab(Tab1); .TextMatrix(i, 0); Tab(Tab2); Left(.TextMatrix(i, 1), 22); Tab(Tab3); Right((Space(10)) & .TextMatrix(i, 2), 10)
            Printer.Print Tab(Tab1); Left(.TextMatrix(i, 1), 22); Tab(Tab3); Right((Space(10)) & .TextMatrix(i, 2), 10)
        Next i
    End With
    Tab1 = 8
    Tab2 = 13
    Tab3 = 60
    Tab4 = 46
'    Printer.Print
    With Printer
        Printer.Print Tab(Tab1); MyLine
        
        
    Printer.FontName = "Courier"
    Printer.FontSize = 11
    Printer.FontBold = True
        
        
        
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3 - 15); Right((Space(10)) & (txtGTotal.text), 10)
        
    Printer.FontName = "Courier"
    Printer.FontSize = 9
    Printer.FontBold = False
        
        
        
        If Val(txtDiscount.text) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (txtDiscount.text), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (txtNTotal.text), 10)
        End If
        Printer.Print Tab(Tab1); MyLine
        Printer.Print
        Printer.Print
        Printer.Print
        Printer.Print
'        Printer.Print Tab(Tab1); UserName
'        Printer.Print Tab(Tab1); Format(time, "hh:mm AMPM")
'        Printer.Print Tab(Tab1); Format(Date, "dd MMM yyyy")
        
'        DrawBorder
        
        .EndDoc
    End With
End Sub



Private Sub txtPatientIxBillID_Change()
    temPatientIxBillID = Val(txtPatientIxBillID.text)
    Call DisplayPatientDetails
    Call FillGrid
End Sub
