VERSION 5.00
Begin VB.Form frmActivate 
   Caption         =   "Activation"
   ClientHeight    =   5595
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   8850
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   5595
   ScaleWidth      =   8850
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton btnActivate 
      Caption         =   "&Activate"
      Height          =   495
      Left            =   6480
      TabIndex        =   4
      Top             =   5040
      Width           =   1815
   End
   Begin VB.TextBox txtA 
      Height          =   1935
      Left            =   240
      MultiLine       =   -1  'True
      TabIndex        =   3
      Top             =   2880
      Width           =   8055
   End
   Begin VB.TextBox txtR 
      Height          =   1935
      Left            =   240
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      TabIndex        =   1
      Top             =   480
      Width           =   8055
   End
   Begin VB.Label Label2 
      Caption         =   "Registration Key"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   120
      Width           =   2055
   End
   Begin VB.Label Label1 
      Caption         =   "Activation Key"
      Height          =   255
      Left            =   240
      TabIndex        =   0
      Top             =   2520
      Width           =   1215
   End
End
Attribute VB_Name = "frmActivate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim myActi As New clsActivation
    

Private Sub btnActivate_Click()
    If Trim(txtR.text) = "" Or Trim(txtA.text) = "" Then
        MsgBox "Can not activate"
        Exit Sub
    End If
    If txtA.text = myActi.getActivationKey(txtR.text) Then
        myActi.activate
        MsgBox "Activated. Please restart the program"
        End
    Else
        MsgBox "Wrong Activation Key. Please retry"
        End
    End If
End Sub

Private Sub Form_Load()
    If myActi.isActivated = True Then
        MsgBox "Already Activated"
        End
    End If
    txtR.text = myActi.registrationKey
End Sub
