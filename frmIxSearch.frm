VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmIxSearch 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Investigation Lists"
   ClientHeight    =   7860
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14445
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7860
   ScaleWidth      =   14445
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   6
      Top             =   6960
      Width           =   3255
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   1920
      Style           =   2  'Dropdown List
      TabIndex        =   5
      Top             =   7440
      Width           =   3255
   End
   Begin VB.TextBox txtBillID 
      Alignment       =   1  'Right Justify
      Height          =   360
      Left            =   1560
      TabIndex        =   1
      Top             =   120
      Width           =   1935
   End
   Begin MSFlexGridLib.MSFlexGrid gridIx 
      Height          =   6255
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   14055
      _ExtentX        =   24791
      _ExtentY        =   11033
      _Version        =   393216
      WordWrap        =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   15.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   13200
      TabIndex        =   4
      Top             =   6960
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnBillID 
      Height          =   375
      Left            =   3720
      TabIndex        =   2
      Top             =   120
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "Search Bill No"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnPrintAll 
      Height          =   495
      Left            =   8040
      TabIndex        =   9
      Top             =   6960
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "Print &All"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnPrintIx 
      Height          =   495
      Left            =   10320
      TabIndex        =   10
      Top             =   6960
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "Print &Single Report"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnViewReport 
      Height          =   495
      Left            =   5760
      TabIndex        =   11
      Top             =   6960
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&View Report"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label12 
      Caption         =   "Printer"
      Height          =   375
      Left            =   240
      TabIndex        =   8
      Top             =   6960
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "Paper"
      Height          =   255
      Left            =   240
      TabIndex        =   7
      Top             =   7440
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "Bill No."
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmIxSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim NumForms As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim SuppliedWord As String
    Dim FSys As New Scripting.FileSystemObject
    Private csetPrinter As New cSetDfltPrinter


    Dim temSQL As String
    Dim rsIxList As New ADODB.Recordset
    Dim i As Integer
    Public temReportID As Long
    Public temPatientIxID As Long
    
    
    
    Dim MyPatient As New clsPatient
    Dim MyIx As New clsIx
    Dim MyBill As New clsBill
    Dim MyPatientIx As New clsPatientIx
        
    Dim LeftX As Long
    Dim RightX As Long
    Dim TopY As Long
    Dim BottomY As Long
    Dim InbetweenY As Long
    Dim NowY As Long
    Dim NowX As Long
    Dim PatientDOB As Date
    Dim Line1X As Long
    Dim Line2X As Long
    Dim Line3X As Long
    Dim TemResponce As Byte
    Dim temPatientID As Long
    Dim Frows As Long
    Dim NowROw As Long
    
    Dim TextList As String
    Dim ResultsList As String
    Dim UnitsList As String
    Dim ReferancesList As String
    

    Dim temPatientIxBillID As Long
    

    Dim rsViewPatient As New ADODB.Recordset


    Dim rsIx As New ADODB.Recordset
    
    

Private Sub Process()
    
    
End Sub

Private Sub FillCombos()

End Sub

Private Sub btnBillID_Click()
    On Error Resume Next
    Call FormatGrid
    FillGridByBillID (Val(txtBillID.Text))
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub GotoIx()
    Dim temRow As Integer
    Dim temPatientID As Long
    Dim temIxID As Long
    Dim temDoctorID As Long
    Dim temInstitutionID As Long

    Unload frmReport

    With gridIx
        If .Rows <= 1 Then Exit Sub
        If .Row < 1 Then Exit Sub
        temRow = .Row
        If IsNumeric(.TextMatrix(temRow, 6)) = False Then
            MsgBox "Please select one test"
            Exit Sub
        End If
        temPatientIxID = Val(.TextMatrix(temRow, 6))
    End With
    With rsIxList
        If .State = 1 Then .Close
        temSQL = "Select * from tblPatientIx where PatientIxID = " & temPatientIxID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            temPatientID = !PatientID
            MyPatient.ID = !PatientID
            temIxID = !IxID
            If IsNull(!DoctorID) = False Then
                temDoctorID = !DoctorID
            Else
                temDoctorID = 0
            End If
            If IsNull(!InstitutionID) = False Then
                temInstitutionID = !InstitutionID
            Else
                temInstitutionID = 0
            End If
        Else
            MsgBox "Error"
            Exit Sub
        End If
    End With

    frmReport.Show
    frmReport.WindowState = 2
    On Error Resume Next

'    Call Process

End Sub

Private Sub PrintIx(PatientIxID As Long)
    Dim rsIx As New ADODB.Recordset
    With rsIx
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblPatientIx where PatientIxID = " & PatientIxID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        temPatientIxBillID = !PatientIxBillID
        MyPatient.ID = !PatientID
        MyIx.ID = !IxID
    End With
    If MyBill.BillID <> temPatientIxBillID Then
        MsgBox "Bill Mismatch error"
        Exit Sub
    End If
    
    PrintReportFormat
    PrintIxFormat
    Printer.EndDoc
End Sub



Private Sub PrintIxFormat()
    Dim rstemReportItems As New ADODB.Recordset
    Dim TemX1 As Long
    Dim TemY1 As Long
    Dim TemX2 As Long
    Dim TemY2 As Long
    Dim temRadius As Long
    
    Dim temText As String
    
    Printer.FillStyle = vbFSTransparent
    
    With rstemReportItems
        If .State = 1 Then .Close
        temSQL = "Select * from tblIxItem  where Deleted = false  AND IxID = " & MyIx.IxID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            TemX1 = Printer.ScaleWidth * !X1
            TemX2 = Printer.ScaleWidth * !X2
            TemY1 = Printer.ScaleHeight * !Y1
            TemY2 = Printer.ScaleHeight * !Y2
            If IsNull(!CircleRadius) = False Then
                temRadius = Printer.ScaleWidth * !CircleRadius
            End If
            If !IsLine = True Then
                Printer.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour
            ElseIf !IsRectangle = True Then
                Printer.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour, B
            ElseIf !IsCircle = True Then
                Printer.Circle (TemX1, TemY1), temRadius, !ForeColour
            ElseIf !IsLabel = True Then
                temText = !IxItem
                PrintReportText temText, TemX1, TemX2, TemY1, TemY2, !FontName, !FontSize, !FontBold, !FontItalic, !FontUnderline, !FONTSTRIKETHROUGH, !TextAlignment
            ElseIf !IsValue = True Then
                temText = FindIxValue(!IxItemID)
                PrintReportText temText, TemX1, TemX2, TemY1, TemY2, !FontName, !FontSize, !FontBold, !FontItalic, !FontUnderline, !FONTSTRIKETHROUGH, !TextAlignment
            ElseIf !IsCalc = True Then
                temText = FindCalcValue(!IxItemID)
                PrintReportText temText, TemX1, TemX2, TemY1, TemY2, !FontName, !FontSize, !FontBold, !FontItalic, !FontUnderline, !FONTSTRIKETHROUGH, !TextAlignment
            End If
            .MoveNext
        Wend
        .Close
    End With
End Sub

Private Function FindCalcValue(ByVal temIxItemID As Long) As String
    Dim rsTem As New ADODB.Recordset
    Dim temCalc As Double
    Dim temFunction As String
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblIxItemCalc Where IxItemID = " & temIxItemID & " AND Deleted = false Order by ItemOrderNo"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            If !ItemOrderNo = 1 Then
                If !FunctionType = "Value" Then
                    If IsNumeric(FindIxValue(!ValueIxItemID)) = True Then
                        temCalc = CDbl(FindIxValue(!ValueIxItemID))
                    Else
                        temCalc = 0
                    End If
                ElseIf !FunctionType = "Constant" Then
                    If IsNumeric(!ConstantValue) = True Then
                        temCalc = CDbl(!ConstantValue)
                    Else
                        temCalc = 0
                    End If
                End If
            Else
                Select Case !FunctionType
                    Case "Constant":
                        If IsNumeric(!ConstantValue) = True Then
                            temCalc = CalculateValue(temFunction, temCalc, !ConstantValue)
                        Else
                            temCalc = 0
                        End If
                        
                    Case "Value":
                        If IsNumeric(FindIxValue(!ValueIxItemID)) = True Then
                            temCalc = CalculateValue(temFunction, temCalc, FindIxValue(!ValueIxItemID))
                        Else
                            temCalc = 0
                        End If
                    Case Else:
                        temFunction = !FunctionType
                End Select
            End If
            .MoveNext
        Wend
        FindCalcValue = temCalc
        If .State = 1 Then .Close
        temSQL = "Select * from tblIxItem where IxItemID = " & temIxItemID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!ItemFormat) = False Then
                If !ItemFormat <> "" Then
                    FindCalcValue = Format(FindCalcValue, !ItemFormat)
                End If
            End If
            If IsNull(!ItemPrefix) = False Then
                If !ItemPrefix <> "" Then
                    FindCalcValue = !ItemPrefix & FindCalcValue
                End If
            End If
            If IsNull(!ItemSuffix) = False Then
                If !ItemSuffix <> "" Then
                    FindCalcValue = FindCalcValue & !ItemSuffix
                End If
            End If
        End If
        .Close
    End With
End Function

Private Function CalculateValue(ByVal FunctionType As String, ByVal StartingValue As Double, ByVal NewValue As Double) As Double
    Select Case FunctionType
        Case "Add": CalculateValue = StartingValue + NewValue
        Case "Subtract": CalculateValue = StartingValue - NewValue
        Case "Multiply": CalculateValue = StartingValue * NewValue
        Case "Divide": CalculateValue = StartingValue / NewValue
        Case Else:
    End Select
End Function


Private Function FindIxValue(ByVal temIxItemID As Long) As String
    With rsIx
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblPatientIxItem where PatientIxId = " & MyPatientIx.PatientIxID & " AND IxItemID = " & temIxItemID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            FindIxValue = !Value
        Else
            FindIxValue = Empty
        End If
    End With
End Function



Private Sub PrintReportFormat()
    Dim rstemReportItems As New ADODB.Recordset
    Dim TemX1 As Long
    Dim TemY1 As Long
    Dim TemX2 As Long
    Dim TemY2 As Long
    Dim temRadius As Long
    Dim temText As String
    
    Printer.FillStyle = vbFSTransparent
    
    With rstemReportItems
        If .State = 1 Then .Close
        temSQL = "Select * from tblReportItem  where Deleted = false"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            TemX1 = Printer.ScaleWidth * !X1
            TemX2 = Printer.ScaleWidth * !X2
            TemY1 = Printer.ScaleHeight * !Y1
            TemY2 = Printer.ScaleHeight * !Y2
            If IsNull(!CircleRadius) = False Then
                temRadius = Printer.ScaleWidth * !CircleRadius
            End If
            If !IsLine = True Then
                Printer.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour
            ElseIf !IsRectangle = True Then
                Printer.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour, B
            ElseIf !IsCircle = True Then
                Printer.Circle (TemX1, TemY1), temRadius, !ForeColour
            ElseIf !IsLabel = True Then
                temText = !LabelText
                PrintReportText temText, TemX1, TemX2, TemY1, TemY2, !FontName, !FontSize, !FontBold, !FontItalic, !FontUnderline, !FONTSTRIKETHROUGH, !TextAlignment
            ElseIf !IsText = True Then
                Select Case !TextText
                    Case "Patient Name": temText = MyPatient.NameWithTitle
                    Case "Patient ID": temText = MyPatient.ID
                    Case "Patient Age in Words": temText = MyPatient.AgeInWords
                    Case "Patient Data of Birth": temText = MyPatient.DateOfBirth
                    Case "Patient Sex": temText = MyPatient.Sex
                    Case "Patient Civil Status": temText = ""
                    Case "Bill No": temText = MyBill.BillID
                    Case "Patient Address": temText = MyPatient.Address
                    Case "Patient Telephone": temText = MyPatient.Phone
                    Case "Investigation": temText = MyIx.Ix
                    Case "Investigation ID": temText = MyIx.IxID
                    Case "Investigation Comments": temText = MyIx.Comments
                    Case "Department": temText = MyIx.Department
                    Case "Speciman": temText = MyIx.Speciman
                    Case "Speciman No.": temText = ""
                    Case "Speciman Comments": temText = ""
                    Case "Referring Doctor": temText = MyBill.Doctor
                    Case "Referring Institution": temText = MyBill.Institution
                    Case "Date": temText = Format(MyPatientIx.SavedDate, "dd MMMM yyyy")
                    Case "Time": temText = Format(MyPatientIx.SavedTime, "hh:mm AMPM")
                    

                    Case "Room No.": temText = MyBill.Room
                    Case "Company":
                    
                    
                        If MyBill.CardNo = 0 Then
                            temText = MyBill.Company
                        Else
                            temText = MyBill.Company & " (" & MyBill.CardNo & ")"
                        End If
                    
                    Case "BHT": temText = MyBill.BHT
                    
                    Case Else
                        temText = ""
                End Select
                PrintReportText temText, TemX1, TemX2, TemY1, TemY2, !FontName, !FontSize, !FontBold, !FontItalic, !FontUnderline, !FONTSTRIKETHROUGH, !TextAlignment
            End If
            .MoveNext
        Wend
        .Close
    End With
End Sub

Private Sub btnPrintAll_Click()
    MyBill.BillID = Val(txtBillID.Text)
    
    With gridIx
        If .Rows <= 1 Then
            MsgBox "No Tests to Print"
            Exit Sub
        End If
    End With
    
    Dim temRow As Integer
    Dim sendingID As Long
    
    For temRow = 1 To gridIx.Rows - 1
        sendingID = Val(gridIx.TextMatrix(temRow, 6))
        MyPatientIx.PatientIxID = sendingID
        PrintIx (sendingID)
    Next temRow
End Sub

Private Sub btnPrintIx_Click()
    Dim sendingID As Long
    With gridIx
        If .Rows <= 1 Then Exit Sub
        If .Row < 1 Then Exit Sub
        If IsNumeric(.TextMatrix(.Row, 6)) = False Then
            MsgBox "Please select one test"
            Exit Sub
        End If
        temPatientIxID = Val(.TextMatrix(.Row, 6))
    End With
    MyBill.BillID = Val(txtBillID.Text)
    sendingID = Val(gridIx.TextMatrix(gridIx.Row, 6))
    MyPatientIx.PatientIxID = sendingID
    PrintIx (sendingID)
End Sub

Private Sub btnViewReport_Click()
    Call GotoIx
End Sub

Private Sub Form_Load()
    Call FormatGrid
    Call FillPrinters
    On Error Resume Next
    cmbPrinter.Text = GetSetting(App.EXEName, Me.Name, "Printer", "")
    cmbPrinter_Click
    cmbPaper.Text = GetSetting(App.EXEName, Me.Name, "Paper", "")
    GetCommonSettings Me
End Sub


Private Sub FormatGrid()
    With gridIx
        .Clear
        .Rows = 1
        .Cols = 9
        For i = 0 To .Cols - 1
            Select Case i
                Case 0:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 600
                    .Text = "No."
                Case 1:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 3000
                    .Text = "Patient"
                Case 2:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 3000
                    .Text = "Investigation"
                Case 3:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 2600
                    .Text = "Billing"
                Case 4:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 2600
                    .Text = "Printed"
                Case 5:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 2600
                    .Text = "Issued"
                Case 6:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 0
                    .Text = "ID"
                Case 7:

                    If UserPositionID = 9 Then
                        .col = i
                        .CellAlignment = 4
                        .ColWidth(i) = 1000
                        .Text = "Price"
                    Else
                        .ColWidth(i) = 0
                    End If

                Case 8:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 0
                    .Text = ""
                Case Else:
                    .ColWidth(i) = 0
            End Select
        Next
    End With
End Sub

Private Sub FillGridByBillID(BillID As Long)
    Dim TotalValue As Double

    With rsIxList

        If .State = 1 Then .Close
        temSQL = "SELECT  tblPatient.Name, tblIx.Ix, tblIx.Value, tblPatientIx.* , tblPatientIxBill.PatientID "
        temSQL = temSQL & "FROM (((tblPatient RIGHT JOIN tblPatientIx ON tblPatient.PatientID = tblPatientIx.PatientID) LEFT JOIN tblTitle ON tblPatient.TitleID = tblTitle.TitleID) LEFT JOIN tblIx ON tblPatientIx.IxID = tblIx.IxID) RIGHT JOIN tblPatientIxBill ON tblPatientIx.PatientIxBillID = tblPatientIxBill.PatientIxBillID "
        temSQL = temSQL & "WHERE tblPatientIx.Deleted = False And tblPatientIxBill.PatientIxBillID = " & BillID & " "
        temSQL = temSQL & "ORDER BY tblPatientIx.PatientIxID "
      
        
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        
        Dim ColourCol As Integer
        
        If .RecordCount > 0 Then
            .MoveLast
            .MoveFirst
            gridIx.Rows = .RecordCount + 1
            For i = 1 To .RecordCount
                gridIx.TextMatrix(i, 0) = i
                gridIx.TextMatrix(i, 1) = Format(!Name, "")
                gridIx.TextMatrix(i, 2) = !Ix
                gridIx.TextMatrix(i, 3) = Format(!BilledDate, "dd MM yy") & " - " & Format(!BilledTime, "HH:MM")
                gridIx.TextMatrix(i, 4) = Format(!PrintedDate, "dd MM yy") & " - " & Format(!PrintedTime, "HH:MM")
                gridIx.TextMatrix(i, 5) = Format(!IssuedDate, "dd MM yy") & " - " & Format(!IssuedTime, "HH:MM")
                gridIx.TextMatrix(i, 6) = !PatientIxID
                gridIx.TextMatrix(i, 8) = ![tblPatientIxBill.PatientID]
                If IsNull(![tblIx.Value]) = False Then
                    gridIx.TextMatrix(i, 7) = Format(![tblIx.Value], "0.00")
                    TotalValue = TotalValue + ![tblIx.Value]
                End If
                
                gridIx.Row = i
                
                For ColourCol = 0 To gridIx.Cols - 1
                    gridIx.col = ColourCol
                    If !Saved = True Then
                        gridIx.CellBackColor = RGB(50, 205, 50)
                    Else
                        gridIx.CellBackColor = RGB(205, 50, 50)
                    End If
                    
                Next ColourCol
                
                
                .MoveNext
            Next
        End If
        .Close

    End With


End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSetting App.EXEName, Me.Name, "Printer", cmbPrinter.Text
    SaveSetting App.EXEName, Me.Name, "Paper", cmbPaper.Text
    
    SaveCommonSettings Me
End Sub



Private Sub gridIx_DblClick()
    Call GotoIx
End Sub

Private Sub txtBillID_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        btnBillID_Click
    End If
End Sub

Private Sub FillPrinters()
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        cmbPrinter.AddItem MyPrinter.DeviceName
    Next
End Sub

Private Sub cmbPrinter_Change()
    cmbPrinter_Click
End Sub

Private Sub cmbPrinter_Click()
    cmbPaper.Clear
    csetPrinter.SetPrinterAsDefault (cmbPrinter.Text)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        With FormSize
            .cx = BillPaperHeight
            .cy = BillPaperWidth
        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                cmbPaper.AddItem PtrCtoVbString(.pName)
            End With
        Next i
        ClosePrinter (PrinterHandle)
    End If
End Sub

