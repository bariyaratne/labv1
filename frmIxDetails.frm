VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmIxDetails 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Investigation Details"
   ClientHeight    =   7545
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13170
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7545
   ScaleWidth      =   13170
   Begin btButtonEx.ButtonEx bttnCLose 
      Height          =   495
      Left            =   11880
      TabIndex        =   47
      Top             =   6960
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame1 
      Height          =   6855
      Left            =   3840
      TabIndex        =   31
      Top             =   0
      Width           =   9255
      Begin TabDlg.SSTab SSTab1 
         Height          =   6375
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   9015
         _ExtentX        =   15901
         _ExtentY        =   11245
         _Version        =   393216
         Tabs            =   2
         Tab             =   1
         TabHeight       =   520
         TabCaption(0)   =   "&Income"
         TabPicture(0)   =   "frmIxDetails.frx":0000
         Tab(0).ControlEnabled=   0   'False
         Tab(0).Control(0)=   "Frame2"
         Tab(0).Control(1)=   "Frame3"
         Tab(0).Control(2)=   "Frame5"
         Tab(0).ControlCount=   3
         TabCaption(1)   =   "&Expences"
         TabPicture(1)   =   "frmIxDetails.frx":001C
         Tab(1).ControlEnabled=   -1  'True
         Tab(1).Control(0)=   "Frame7"
         Tab(1).Control(0).Enabled=   0   'False
         Tab(1).Control(1)=   "Frame8"
         Tab(1).Control(1).Enabled=   0   'False
         Tab(1).Control(2)=   "Frame6"
         Tab(1).Control(2).Enabled=   0   'False
         Tab(1).ControlCount=   3
         Begin VB.Frame Frame5 
            Caption         =   "Staff"
            Height          =   2175
            Left            =   -74760
            TabIndex        =   46
            Top             =   360
            Width           =   8655
            Begin VB.TextBox txtStaffFee 
               Alignment       =   1  'Right Justify
               Enabled         =   0   'False
               Height          =   435
               Left            =   7440
               TabIndex        =   51
               Top             =   1560
               Width           =   1095
            End
            Begin VB.TextBox txtIxStaffValue 
               Alignment       =   1  'Right Justify
               Height          =   375
               Left            =   5760
               TabIndex        =   3
               Top             =   240
               Width           =   1455
            End
            Begin MSFlexGridLib.MSFlexGrid GridStaff 
               Height          =   1335
               Left            =   120
               TabIndex        =   5
               Top             =   720
               Width           =   7215
               _ExtentX        =   12726
               _ExtentY        =   2355
               _Version        =   393216
               SelectionMode   =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx bttnDeleteIxStaff 
               Height          =   375
               Left            =   7560
               TabIndex        =   6
               Top             =   720
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   661
               Appearance      =   3
               Enabled         =   0   'False
               Caption         =   "Delete"
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx bttnAddIxStaff 
               Height          =   375
               Left            =   7560
               TabIndex        =   4
               Top             =   240
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   661
               Appearance      =   3
               Enabled         =   0   'False
               Caption         =   "Add"
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin MSDataListLib.DataCombo dtcStaff 
               Height          =   360
               Left            =   120
               TabIndex        =   2
               Top             =   240
               Width           =   3855
               _ExtentX        =   6800
               _ExtentY        =   635
               _Version        =   393216
               Style           =   2
               Text            =   ""
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin VB.Frame Frame6 
            Caption         =   "Material"
            Height          =   2895
            Left            =   240
            TabIndex        =   45
            Top             =   360
            Width           =   8655
            Begin VB.TextBox txtMaterialCost 
               Alignment       =   1  'Right Justify
               Enabled         =   0   'False
               Height          =   435
               Left            =   7440
               TabIndex        =   49
               Top             =   2160
               Width           =   1095
            End
            Begin VB.TextBox txtMQty 
               Alignment       =   1  'Right Justify
               Height          =   375
               Left            =   1800
               TabIndex        =   17
               Top             =   840
               Width           =   1455
            End
            Begin VB.TextBox txtIxMaterialValue 
               Alignment       =   1  'Right Justify
               Height          =   375
               Left            =   5880
               TabIndex        =   19
               Top             =   840
               Width           =   1455
            End
            Begin btButtonEx.ButtonEx bttnDeleteIxMaterial 
               Height          =   375
               Left            =   7560
               TabIndex        =   29
               Top             =   1320
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   661
               Appearance      =   3
               Enabled         =   0   'False
               Caption         =   "Delete"
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx bttnAddIxMaterial 
               Height          =   375
               Left            =   7560
               TabIndex        =   28
               Top             =   840
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   661
               Appearance      =   3
               Enabled         =   0   'False
               Caption         =   "Add"
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin MSFlexGridLib.MSFlexGrid GridMeterial 
               Height          =   1335
               Left            =   240
               TabIndex        =   20
               Top             =   1320
               Width           =   7215
               _ExtentX        =   12726
               _ExtentY        =   2355
               _Version        =   393216
               SelectionMode   =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin MSDataListLib.DataCombo dtcMaterial 
               Height          =   360
               Left            =   1800
               TabIndex        =   15
               Top             =   360
               Width           =   5535
               _ExtentX        =   9763
               _ExtentY        =   635
               _Version        =   393216
               Style           =   2
               Text            =   ""
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin VB.Label Label6 
               Caption         =   "Material"
               Height          =   255
               Left            =   360
               TabIndex        =   14
               Top             =   360
               Width           =   2535
            End
            Begin VB.Label Label5 
               Caption         =   "Value"
               Height          =   255
               Left            =   4320
               TabIndex        =   18
               Top             =   840
               Width           =   2535
            End
            Begin VB.Label Label4 
               Caption         =   "Quentity"
               Height          =   255
               Left            =   360
               TabIndex        =   16
               Top             =   840
               Width           =   2535
            End
         End
         Begin VB.Frame Frame3 
            Height          =   855
            Left            =   -74760
            TabIndex        =   44
            Top             =   4920
            Width           =   8655
            Begin VB.TextBox txtTotalIncome 
               Alignment       =   1  'Right Justify
               Enabled         =   0   'False
               Height          =   435
               Left            =   3840
               TabIndex        =   13
               Top             =   240
               Width           =   3375
            End
            Begin VB.Label Label1 
               Caption         =   "Total Value"
               Height          =   255
               Left            =   240
               TabIndex        =   12
               Top             =   360
               Width           =   2055
            End
         End
         Begin VB.TextBox Text3 
            Alignment       =   1  'Right Justify
            Height          =   360
            Left            =   -70680
            TabIndex        =   37
            Top             =   480
            Width           =   1455
         End
         Begin VB.TextBox Text4 
            Height          =   360
            Left            =   -70560
            TabIndex        =   36
            Text            =   "Text1"
            Top             =   2880
            Width           =   1455
         End
         Begin VB.TextBox Text6 
            Height          =   360
            Left            =   -72000
            TabIndex        =   35
            Text            =   "Text5"
            Top             =   5160
            Width           =   3375
         End
         Begin VB.Frame Frame8 
            Height          =   855
            Left            =   240
            TabIndex        =   34
            Top             =   5400
            Width           =   8655
            Begin VB.TextBox txtTotalExpences 
               Alignment       =   1  'Right Justify
               Enabled         =   0   'False
               Height          =   435
               Left            =   3840
               TabIndex        =   27
               Top             =   240
               Width           =   3375
            End
            Begin VB.Label Label3 
               Caption         =   "Total Cost"
               Height          =   255
               Left            =   240
               TabIndex        =   26
               Top             =   360
               Width           =   2535
            End
         End
         Begin VB.Frame Frame7 
            Caption         =   "Expence"
            Height          =   2175
            Left            =   240
            TabIndex        =   33
            Top             =   3240
            Width           =   8655
            Begin VB.TextBox txtExpence 
               Alignment       =   1  'Right Justify
               Enabled         =   0   'False
               Height          =   435
               Left            =   7440
               TabIndex        =   50
               Top             =   1560
               Width           =   1095
            End
            Begin VB.TextBox txtIxExpence 
               Alignment       =   1  'Right Justify
               Height          =   375
               Left            =   5760
               TabIndex        =   22
               Top             =   240
               Width           =   1455
            End
            Begin btButtonEx.ButtonEx bttnDeleteExpence 
               Height          =   375
               Left            =   7560
               TabIndex        =   25
               Top             =   720
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   661
               Appearance      =   3
               Enabled         =   0   'False
               Caption         =   "Delete"
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx bttnAddExpence 
               Height          =   375
               Left            =   7560
               TabIndex        =   23
               Top             =   240
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   661
               Appearance      =   3
               Enabled         =   0   'False
               Caption         =   "Add"
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin MSDataListLib.DataCombo dtcExpence 
               Height          =   360
               Left            =   120
               TabIndex        =   21
               Top             =   240
               Width           =   3855
               _ExtentX        =   6800
               _ExtentY        =   635
               _Version        =   393216
               MatchEntry      =   -1  'True
               Style           =   2
               Text            =   ""
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin MSFlexGridLib.MSFlexGrid GridExpence 
               Height          =   1335
               Left            =   120
               TabIndex        =   24
               Top             =   720
               Width           =   7215
               _ExtentX        =   12726
               _ExtentY        =   2355
               _Version        =   393216
               SelectionMode   =   1
            End
         End
         Begin VB.Frame Frame2 
            Caption         =   "Income"
            Height          =   2175
            Left            =   -74760
            TabIndex        =   32
            Top             =   2640
            Width           =   8655
            Begin VB.TextBox txtHospitalFee 
               Alignment       =   1  'Right Justify
               Enabled         =   0   'False
               Height          =   435
               Left            =   7440
               TabIndex        =   52
               Top             =   1560
               Width           =   1095
            End
            Begin VB.TextBox txtIxIncomeValue 
               Alignment       =   1  'Right Justify
               Height          =   375
               Left            =   5760
               TabIndex        =   8
               Top             =   240
               Width           =   1455
            End
            Begin MSFlexGridLib.MSFlexGrid GridIncome 
               Height          =   1335
               Left            =   120
               TabIndex        =   10
               Top             =   720
               Width           =   7215
               _ExtentX        =   12726
               _ExtentY        =   2355
               _Version        =   393216
               SelectionMode   =   1
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx bttnDeleteIxIncome 
               Height          =   375
               Left            =   7560
               TabIndex        =   11
               Top             =   720
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   661
               Appearance      =   3
               Enabled         =   0   'False
               Caption         =   "Delete"
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin btButtonEx.ButtonEx bttnAddIxIncome 
               Height          =   375
               Left            =   7560
               TabIndex        =   9
               Top             =   240
               Width           =   855
               _ExtentX        =   1508
               _ExtentY        =   661
               Appearance      =   3
               Enabled         =   0   'False
               Caption         =   "Add"
               Enabled         =   0   'False
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
            Begin MSDataListLib.DataCombo dtcIncome 
               Height          =   360
               Left            =   120
               TabIndex        =   7
               Top             =   240
               Width           =   3855
               _ExtentX        =   6800
               _ExtentY        =   635
               _Version        =   393216
               Style           =   2
               Text            =   ""
               BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
                  Name            =   "Tahoma"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
            End
         End
         Begin btButtonEx.ButtonEx ButtonEx5 
            Height          =   255
            Left            =   -69000
            TabIndex        =   38
            Top             =   1200
            Width           =   735
            _ExtentX        =   1296
            _ExtentY        =   450
            Caption         =   "Delete"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx ButtonEx6 
            Height          =   255
            Left            =   -69000
            TabIndex        =   39
            Top             =   840
            Width           =   735
            _ExtentX        =   1296
            _ExtentY        =   450
            Caption         =   "Add"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx ButtonEx7 
            Height          =   255
            Left            =   -69000
            TabIndex        =   40
            Top             =   3600
            Width           =   735
            _ExtentX        =   1296
            _ExtentY        =   450
            Caption         =   "Delete"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin btButtonEx.ButtonEx ButtonEx8 
            Height          =   255
            Left            =   -69000
            TabIndex        =   41
            Top             =   3240
            Width           =   735
            _ExtentX        =   1296
            _ExtentY        =   450
            Caption         =   "Add"
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
         End
         Begin MSDataListLib.DataCombo DataCombo4 
            Height          =   315
            Left            =   -74760
            TabIndex        =   42
            Top             =   2880
            Width           =   3855
            _ExtentX        =   6800
            _ExtentY        =   635
            _Version        =   393216
            Text            =   "DataCombo1"
         End
         Begin VB.Label Label2 
            Caption         =   "Total Cost"
            Height          =   255
            Left            =   -74760
            TabIndex        =   43
            Top             =   5160
            Width           =   2535
         End
      End
   End
   Begin VB.Frame Frame4 
      Height          =   6975
      Left            =   120
      TabIndex        =   30
      Top             =   0
      Width           =   3615
      Begin VB.TextBox txtID 
         Height          =   360
         Left            =   2160
         TabIndex        =   48
         Top             =   6480
         Width           =   1335
      End
      Begin MSDataListLib.DataCombo dtcIx 
         Height          =   6180
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   10901
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   1
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmIxDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    
    Dim temSql As String
    Dim rsIx As New ADODB.Recordset
    Dim rsViewIx As New ADODB.Recordset
    Dim rsViewStaff As New ADODB.Recordset
    Dim rsIxStaff As New ADODB.Recordset
    Dim rsViewIncome As New ADODB.Recordset
    Dim rsIxIncome As New ADODB.Recordset
    Dim rsViewMaterial As New ADODB.Recordset
    Dim rsIxMaterial As New ADODB.Recordset
    Dim rsViewExpence As New ADODB.Recordset
    Dim rsIxExpence As New ADODB.Recordset

Private Sub CalculateTotalCost()
    Dim Total As Double
    Dim MaterialCost As Double
    Dim ExpenceCost As Double
    Dim i As Integer
    With GridMeterial
        For i = 1 To .Rows - 1
            Total = Total + Val(.TextMatrix(i, 2))
            MaterialCost = MaterialCost + Val(.TextMatrix(i, 2))
        Next
    End With
    With GridExpence
        For i = 1 To .Rows - 1
            Total = Total + Val(.TextMatrix(i, 2))
            ExpenceCost = ExpenceCost + Val(.TextMatrix(i, 2))
        Next
    End With
    txtTotalExpences.Text = Format(Total, "0.00")
    txtMaterialCost.Text = Format(MaterialCost, "0.00")
    txtExpence.Text = Format(ExpenceCost, "0.00")
End Sub

Private Sub CalculateTotalValue()
    Dim Total As Double
    Dim HospitalFee As Double
    Dim StaffFee As Double
    Dim i As Integer
    With GridIncome
        For i = 1 To .Rows - 1
            Total = Total + Val(.TextMatrix(i, 2))
            HospitalFee = HospitalFee + Val(.TextMatrix(i, 2))
        Next
    End With
    With GridStaff
        For i = 1 To .Rows - 1
            Total = Total + Val(.TextMatrix(i, 2))
            StaffFee = StaffFee + Val(.TextMatrix(i, 2))
        Next
    End With
    txtTotalIncome.Text = Format(Total, "0.00")
    txtStaffFee.Text = Format(StaffFee, "0.00")
    txtHospitalFee.Text = Format(HospitalFee, "0.00")
End Sub

Private Sub bttnAddExpence_Click()
    Dim tr As Integer
    If IsNumeric(dtcIx.BoundText) = False Then
        tr = MsgBox("No Investigation Selected", vbCritical, "Investigation?")
        dtcIx.SetFocus
        Exit Sub
    End If
    If IsNumeric(dtcExpence.BoundText) = False Then
        tr = MsgBox("No Expence Selected", vbCritical, "Expence?")
        dtcExpence.SetFocus
        Exit Sub
    End If
    If Val(txtIxExpence.Text) = 0 Then
        tr = MsgBox("No Value", vbCritical, "Value?")
        txtIxExpence.SetFocus
        Exit Sub
    End If
    With rsIxExpence
        If .State = 1 Then .Close
        temSql = "Select * from tblIxExpence"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !IxID = Val(dtcIx.BoundText)
        !ExpenceID = Val(dtcExpence.BoundText)
        !Value = Val(txtIxExpence.Text)
        .Update
        .Close
    End With
    Call FillIxExpence
    Call CalculateTotalCost
    Call UpdateIX
    txtIxExpence.Text = Empty
    dtcExpence.Text = Empty
    dtcExpence.SetFocus
End Sub

Private Sub bttnAddIxIncome_Click()
    Dim tr As Integer
    If IsNumeric(dtcIx.BoundText) = False Then
        tr = MsgBox("No Investigation Selected", vbCritical, "Investigation?")
        dtcIx.SetFocus
        Exit Sub
    End If
    If IsNumeric(dtcIncome.BoundText) = False Then
        tr = MsgBox("No Income Selected", vbCritical, "Income?")
        dtcIncome.SetFocus
        Exit Sub
    End If
    If Val(txtIxIncomeValue.Text) = 0 Then
        tr = MsgBox("No Value", vbCritical, "Value?")
        txtIxIncomeValue.SetFocus
        Exit Sub
    End If
    With rsIxIncome
        If .State = 1 Then .Close
        temSql = "Select * from tblIxIncome"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !IxID = Val(dtcIx.BoundText)
        !IncomeID = Val(dtcIncome.BoundText)
        !Value = Val(txtIxIncomeValue.Text)
        .Update
        .Close
    End With
    Call FillIxIncome
    Call CalculateTotalValue
    Call UpdateIX
    txtIxIncomeValue.Text = Empty
    dtcIncome.Text = Empty
    dtcIncome.SetFocus
End Sub

Private Sub bttnAddIxMaterial_Click()
    Dim tr As Integer
    If IsNumeric(dtcIx.BoundText) = False Then
        tr = MsgBox("No Investigation Selected", vbCritical, "Investigation?")
        dtcIx.SetFocus
        Exit Sub
    End If
    If IsNumeric(dtcMaterial.BoundText) = False Then
        tr = MsgBox("No Material Selected", vbCritical, "Material?")
        dtcMaterial.SetFocus
        Exit Sub
    End If
    If Val(txtIxMaterialValue.Text) = 0 Then
        tr = MsgBox("No Value", vbCritical, "Value?")
        txtIxMaterialValue.SetFocus
        Exit Sub
    End If
    With rsIxMaterial
        If .State = 1 Then .Close
        temSql = "Select * from tblIxMaterial"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !IxID = Val(dtcIx.BoundText)
        !MaterialID = Val(dtcMaterial.BoundText)
        !Value = Val(txtIxMaterialValue.Text)
        !Quentity = Val(txtMQty.Text)
        .Update
        .Close
    End With
    Call FillIxMaterial
    Call CalculateTotalCost
    Call UpdateIX
    txtIxMaterialValue.Text = Empty
    dtcMaterial.Text = Empty
    dtcMaterial.SetFocus
End Sub

Private Sub UpdateIX()
    With rsIx
        If .State = 1 Then .Close
        temSql = "SELECT * from tblIx where IxID = " & Val(dtcIx.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Value = Val(txtTotalIncome.Text)
            !StaffCharge = Val(txtStaffFee.Text)
            !HospitalCharge = Val(txtHospitalFee.Text)
            !OtherCHarge = 0
            !Charge = !StaffCharge + !HospitalCharge + !OtherCHarge
            !MaterialCost = Val(txtMaterialCost.Text)
            !ExpenceCost = Val(txtExpence.Text)
            !OtherCost = 0
            !Cost = Val(txtTotalExpences.Text)
            .Update
        End If
        .Close
    End With
End Sub

Private Sub bttnAddIxStaff_Click()
    Dim tr As Integer
    If IsNumeric(dtcIx.BoundText) = False Then
        tr = MsgBox("No Investigation Selected", vbCritical, "Investigation?")
        dtcIx.SetFocus
        Exit Sub
    End If
    If IsNumeric(dtcStaff.BoundText) = False Then
        tr = MsgBox("No Staff Name Selected", vbCritical, "Satff?")
        dtcStaff.SetFocus
        Exit Sub
    End If
    If Val(txtIxStaffValue.Text) = 0 Then
        tr = MsgBox("No Value", vbCritical, "Value?")
        txtIxStaffValue.SetFocus
        Exit Sub
    End If
    With rsIxStaff
        If .State = 1 Then .Close
        temSql = "Select * from tblIxStaff"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !IxID = Val(dtcIx.BoundText)
        !StaffID = Val(dtcStaff.BoundText)
        !Value = Val(txtIxStaffValue.Text)
        .Update
        .Close
    End With
    Call FillIxStaff
    Call CalculateTotalValue
    Call UpdateIX
    txtIxStaffValue.Text = Empty
    dtcStaff.Text = Empty
    dtcStaff.SetFocus
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnDeleteExpence_Click()
    Dim tr As Integer
    If IsNumeric(dtcIx.BoundText) = False Then
        tr = MsgBox("No Investigation Selected", vbCritical, "Investigation?")
        dtcIx.SetFocus
        Exit Sub
    End If
    If IsNumeric(GridExpence.TextMatrix(GridExpence.Row, 3)) = False Then
        tr = MsgBox("Nothing to Delete", vbCritical, "Error?")
        dtcIx.SetFocus
        Exit Sub
    End If
    With rsIxExpence
        If .State = 1 Then .Close
        temSql = "DELETE * from tblIxExpence where ID = " & (GridExpence.TextMatrix(GridExpence.Row, 3))
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Close
    End With
    Call FillIxExpence
    Call CalculateTotalCost
    Call UpdateIX
    txtIxExpence.Text = Empty
    dtcExpence.Text = Empty
    dtcExpence.SetFocus
End Sub

Private Sub bttnDeleteIxIncome_Click()
    Dim tr As Integer
    If IsNumeric(dtcIx.BoundText) = False Then
        tr = MsgBox("No Investigation Selected", vbCritical, "Investigation?")
        dtcIx.SetFocus
        Exit Sub
    End If
    If IsNumeric(GridIncome.TextMatrix(GridIncome.Row, 3)) = False Then
        tr = MsgBox("Nothing to Delete", vbCritical, "Error?")
        dtcIx.SetFocus
        Exit Sub
    End If
    With rsIxStaff
        If .State = 1 Then .Close
        temSql = "DELETE * from tblIxIncome where ID = " & (GridIncome.TextMatrix(GridIncome.Row, 3))
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Close
    End With
    Call FillIxIncome
    Call CalculateTotalValue
    Call UpdateIX
    txtIxIncomeValue.Text = Empty
    dtcIncome.Text = Empty
    dtcIncome.SetFocus
End Sub

Private Sub bttnDeleteIxMaterial_Click()
    Dim tr As Integer
    If IsNumeric(dtcIx.BoundText) = False Then
        tr = MsgBox("No Investigation Selected", vbCritical, "Investigation?")
        dtcIx.SetFocus
        Exit Sub
    End If
    If IsNumeric(GridMeterial.TextMatrix(GridMeterial.Row, 3)) = False Then
        tr = MsgBox("Nothing to Delete", vbCritical, "Error?")
        dtcIx.SetFocus
        Exit Sub
    End If
    With rsIxMaterial
        If .State = 1 Then .Close
        temSql = "DELETE * from tblIxMaterial where ID = " & (GridMeterial.TextMatrix(GridMeterial.Row, 3))
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Close
    End With
    Call FillIxMaterial
    Call CalculateTotalCost
    Call UpdateIX
    txtIxMaterialValue.Text = Empty
    dtcMaterial.Text = Empty
    dtcMaterial.SetFocus
End Sub

Private Sub bttnDeleteIxStaff_Click()
    Dim tr As Integer
    If IsNumeric(dtcIx.BoundText) = False Then
        tr = MsgBox("No Investigation Selected", vbCritical, "Investigation?")
        dtcIx.SetFocus
        Exit Sub
    End If
    If IsNumeric(GridStaff.TextMatrix(GridStaff.Row, 3)) = False Then
        tr = MsgBox("Nothing to Delete", vbCritical, "Error?")
        dtcIx.SetFocus
        Exit Sub
    End If
    With rsIxStaff
        If .State = 1 Then .Close
        temSql = "DELETE * from tblIxStaff where ID = " & (GridStaff.TextMatrix(GridStaff.Row, 3))
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Close
    End With
    Call FillIxStaff
    Call CalculateTotalValue
    Call UpdateIX
    txtIxStaffValue.Text = Empty
    dtcStaff.Text = Empty
    dtcStaff.SetFocus
End Sub

Private Sub dtcIx_Change()
    If IsNumeric(dtcIx.BoundText) = False Then Exit Sub
    Call FillIxStaff
    Call FillIxIncome
    Call FillIxMaterial
    Call FillIxExpence
    Call CalculateTotalCost
    Call CalculateTotalValue
    Call After
    
    'Call UpdateIX
    

End Sub

Private Sub dtcIx_Click(Area As Integer)
    dtcIx_Change
End Sub

Private Sub Form_Load()
    Call FillCombos
    Call Before
    SSTab1.Tab = 0
End Sub

Private Sub FillCombos()
    With rsViewIx
        If .State = 1 Then .Close
        temSql = "SELECT * from tblIx where Deleted = false order by Ix"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcIx
        Set .RowSource = rsViewIx
        .ListField = "Ix"
        .BoundColumn = "IxID"
    End With
    With rsViewStaff
        If .State = 1 Then .Close
        temSql = "SELECT * FROM tblStaff order by Name"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcStaff
        Set .RowSource = rsViewStaff
        .ListField = "Name"
        .BoundColumn = "StaffID"
    End With
    With rsViewIncome
        If .State = 1 Then .Close
        temSql = "SELECT * FROM tblIncome order by Income"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcIncome
        Set .RowSource = rsViewIncome
        .ListField = "Income"
        .BoundColumn = "IncomeID"
    End With
    With rsViewMaterial
        If .State = 1 Then .Close
        temSql = "SELECT * FROM tblMaterial order by Material"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcMaterial
        Set .RowSource = rsViewMaterial
        .ListField = "Material"
        .BoundColumn = "MaterialID"
        End With
    With rsViewExpence
        If .State = 1 Then .Close
        temSql = "SELECT * FROM tblExpence order by Expence"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcExpence
        Set .RowSource = rsViewExpence
        .ListField = "Expence"
        .BoundColumn = "ExpenceID"
    End With
End Sub

Private Sub FillIxStaff()
    With GridStaff
        .Clear
        
        .Rows = 1
        
        .Cols = 4
        
        .Row = 0
        
        .Col = 0
        .Text = "No."
        
        .Col = 1
        .Text = "Staff"
        
        .Col = 2
        .Text = "Value"
        
        
        .ColWidth(0) = 900
        .ColWidth(1) = 4400
        .ColWidth(2) = 1550
        .ColWidth(3) = 1
    End With
    With rsIxStaff
        If .State = 1 Then .Close
        temSql = "SELECT tblStaff.Name, tblIxStaff.Value, tblIxStaff.ID FROM tblStaff RIGHT JOIN tblIxStaff ON tblStaff.StaffID = tblIxStaff.StaffID WHERE (((tblIxStaff.IxID)=" & Val(dtcIx.BoundText) & "))"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                GridStaff.Rows = GridStaff.Rows + 1
                GridStaff.Row = GridStaff.Rows - 1
                GridStaff.Col = 0
                GridStaff.Text = GridStaff.Row
                GridStaff.Col = 1
                If Not IsNull(!Name) Then GridStaff.Text = !Name
                GridStaff.Col = 2
                GridStaff.Text = Format(!Value, "0.00")
                GridStaff.Col = 3
                GridStaff.Text = !ID
                .MoveNext
            Wend
        End If
        .Close
    End With
        With GridStaff
        If .Rows > 2 Then
            .TopRow = .Rows - 2
        End If
    End With
End Sub
Private Sub FillIxIncome()
    With GridIncome
        .Clear
        
        .Rows = 1
        
        .Cols = 4
        
        .Row = 0
        
        .Col = 0
        .Text = "No."
        
        .Col = 1
        .Text = "Income"
        
        .Col = 2
        .Text = "Value"
        
        .ColWidth(0) = 900
        .ColWidth(1) = 4400
        .ColWidth(2) = 1550
        .ColWidth(3) = 1
    End With
    With rsIxIncome
        If .State = 1 Then .Close
        temSql = "SELECT tblIncome.Income, tblIxIncome.Value, tblIxIncome.ID FROM tblIncome RIGHT JOIN tblIxIncome ON tblIncome.IncomeID = tblIxIncome.IncomeID WHERE (((tblIxIncome.IxID)=" & Val(dtcIx.BoundText) & "))"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                GridIncome.Rows = GridIncome.Rows + 1
                GridIncome.Row = GridIncome.Rows - 1
                GridIncome.Col = 0
                GridIncome.Text = GridIncome.Row
                GridIncome.Col = 1
                If Not IsNull(!Income) Then GridIncome.Text = !Income
                GridIncome.Col = 2
                GridIncome.Text = Format(!Value, "0.00")
                GridIncome.Col = 3
                GridIncome.Text = !ID
                .MoveNext
            Wend
        End If
        .Close
    End With
    With GridIncome
        If .Rows > 2 Then
            .TopRow = .Rows - 2
        End If
    End With
End Sub
Private Sub FillIxMaterial()
    With GridMeterial
        .Clear
        
        .Rows = 1
        
        .Cols = 5
        
        .Row = 0
        
        .Col = 0
        .Text = "No."
        
        .Col = 1
        .Text = "Material"
        
        .Col = 2
        .Text = "Value"
        
        .Col = 4
        .Text = "Quentity"
        
        .ColWidth(0) = 900
        .ColWidth(1) = 3800
        .ColWidth(2) = 1150
        .ColWidth(3) = 1
        
        .ColWidth(4) = 1000
        
    End With
    With rsIxMaterial
        If .State = 1 Then .Close
        temSql = "SELECT tblMaterial.Material, tblIxMaterial.Value, tblIxMaterial.Quentity, tblIxMaterial.ID FROM tblMaterial RIGHT JOIN tblIxMaterial ON tblMaterial.MaterialID = tblIxMaterial.MaterialID WHERE (((tblIxMaterial.IxID)=" & Val(dtcIx.BoundText) & "))"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                GridMeterial.Rows = GridMeterial.Rows + 1
                GridMeterial.Row = GridMeterial.Rows - 1
                GridMeterial.Col = 0
                GridMeterial.Text = GridMeterial.Row
                GridMeterial.Col = 1
                If Not IsNull(!Material) Then GridMeterial.Text = !Material
                GridMeterial.Col = 2
                GridMeterial.Text = Format(!Value, "0.00")
                GridMeterial.Col = 3
                GridMeterial.Text = !ID
                GridMeterial.Col = 4
                If Not IsNull(!Quentity) Then GridMeterial.Text = !Quentity
                .MoveNext
            Wend
        End If
        .Close
    End With
        With GridMeterial
        If .Rows > 2 Then
            .TopRow = .Rows - 2
        End If
    End With
End Sub

Private Sub FillIxExpence()
    With GridExpence
        .Clear
        
        .Rows = 1
        
        .Cols = 4
        
        .Row = 0
        
        .Col = 0
        .Text = "No."
        
        .Col = 1
        .Text = "Expence"
        
        .Col = 2
        .Text = "Value"
        
        
        .ColWidth(0) = 900
        .ColWidth(1) = 4400
        .ColWidth(2) = 1550
        .ColWidth(3) = 1
    End With
    With rsIxExpence
        If .State = 1 Then .Close
        temSql = "SELECT tblExpence.Expence, tblIxExpence.Value, tblIxExpence.ID FROM tblExpence RIGHT JOIN tblIxExpence ON tblExpence.ExpenceID = tblIxExpence.ExpenceID WHERE (((tblIxExpence.IxID)=" & Val(dtcIx.BoundText) & "))"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                GridExpence.Rows = GridExpence.Rows + 1
                GridExpence.Row = GridExpence.Rows - 1
                GridExpence.Col = 0
                GridExpence.Text = GridExpence.Row
                GridExpence.Col = 1
                If Not IsNull(!Expence) Then GridExpence.Text = !Expence
                GridExpence.Col = 2
                GridExpence.Text = Format(!Value, "0.00")
                GridExpence.Col = 3
                GridExpence.Text = !ID
                .MoveNext
            Wend
        End If
        .Close
    End With
        With GridExpence
        If .Rows > 2 Then
            .TopRow = .Rows - 2
        End If
    End With
End Sub

Private Sub Before()
    dtcStaff.Enabled = False
    dtcIncome.Enabled = False
    dtcMaterial.Enabled = False
    dtcExpence.Enabled = False
    
    txtIxStaffValue.Enabled = False
    txtIxIncomeValue.Enabled = False
    txtIxMaterialValue.Enabled = False
    txtIxExpence.Enabled = False
    
    GridStaff.Enabled = False
    GridIncome.Enabled = False
    GridMeterial.Enabled = False
    GridExpence.Enabled = False
    
    bttnAddIxStaff.Enabled = False
    bttnAddIxIncome.Enabled = False
    bttnAddIxMaterial.Enabled = False
    bttnAddExpence.Enabled = False
    
    bttnDeleteIxStaff.Enabled = False
    bttnDeleteIxIncome.Enabled = False
    bttnDeleteIxMaterial.Enabled = False
    bttnDeleteExpence.Enabled = False
    
    bttnAddIxStaff.Visible = False
    bttnAddIxIncome.Visible = False
    bttnAddIxMaterial.Visible = False
    bttnAddExpence.Visible = False
    
    bttnDeleteIxStaff.Visible = False
    bttnDeleteIxIncome.Visible = False
    bttnDeleteIxMaterial.Visible = False
    bttnDeleteExpence.Visible = False
End Sub

Private Sub After()
    dtcStaff.Enabled = True
    dtcIncome.Enabled = True
    dtcMaterial.Enabled = True
    dtcExpence.Enabled = True
    
    txtIxStaffValue.Enabled = True
    txtIxIncomeValue.Enabled = True
    txtIxMaterialValue.Enabled = True
    txtIxExpence.Enabled = True
    
    GridStaff.Enabled = True
    GridIncome.Enabled = True
    GridMeterial.Enabled = True
    GridExpence.Enabled = True
    
    bttnAddIxStaff.Enabled = True
    bttnAddIxIncome.Enabled = True
    bttnAddIxMaterial.Enabled = True
    bttnAddExpence.Enabled = True
    
    bttnDeleteIxStaff.Enabled = True
    bttnDeleteIxIncome.Enabled = True
    bttnDeleteIxMaterial.Enabled = True
    bttnDeleteExpence.Enabled = True
    
    bttnAddIxStaff.Visible = True
    bttnAddIxIncome.Visible = True
    bttnAddIxMaterial.Visible = True
    bttnAddExpence.Visible = True
    
    bttnDeleteIxStaff.Visible = True
    bttnDeleteIxIncome.Visible = True
    bttnDeleteIxMaterial.Visible = True
    bttnDeleteExpence.Visible = True
End Sub

Private Sub Clearvalues()
    txtTotalExpences.Text = Empty
    txtTotalIncome.Text = Empty
    txtIxExpence.Text = Empty
    txtIxMaterialValue.Text = Empty
    txtIxIncomeValue.Text = Empty
    txtIxStaffValue.Text = Empty
    
    txtMaterialCost.Text = Empty
    txtExpence.Text = Empty
    txtStaffFee.Text = Empty
    txtHospitalFee.Text = Empty
End Sub

Private Sub txtID_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        dtcIx.BoundText = Val(txtID.Text)
    End If
End Sub
