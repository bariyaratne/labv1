VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientIxItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varPatientIxItemID As Long
    Private varPatientID As Long
    Private varIxID As Long
    Private varPatientIxID As Long
    Private varIxItemID As Long
    Private varValue As String
    Private varDeleted As Boolean
    Private varDeletedDate As Date
    Private varDeletedTime As Date
    Private varDeletedUserID As Long
    Private varHospitalFee As Double
    Private varStaffFee As Double
    Private varOtherFee As Double

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientIxItem Where PatientIxItemID = " & varPatientIxItemID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !PatientID = varPatientID
        !IxID = varIxID
        !PatientIxID = varPatientIxID
        !IxItemID = varIxItemID
        !Value = varValue
        !Deleted = varDeleted
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !DeletedUserID = varDeletedUserID
        !HospitalFee = varHospitalFee
        !StaffFee = varStaffFee
        !OtherFee = varOtherFee
        .Update
        varPatientIxItemID = !PatientIxItemID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientIxItem WHERE PatientIxItemID = " & varPatientIxItemID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!PatientIxItemID) Then
               varPatientIxItemID = !PatientIxItemID
            End If
            If Not IsNull(!PatientID) Then
               varPatientID = !PatientID
            End If
            If Not IsNull(!IxID) Then
               varIxID = !IxID
            End If
            If Not IsNull(!PatientIxID) Then
               varPatientIxID = !PatientIxID
            End If
            If Not IsNull(!IxItemID) Then
               varIxItemID = !IxItemID
            End If
            If Not IsNull(!Value) Then
               varValue = !Value
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!HospitalFee) Then
               varHospitalFee = !HospitalFee
            End If
            If Not IsNull(!StaffFee) Then
               varStaffFee = !StaffFee
            End If
            If Not IsNull(!OtherFee) Then
               varOtherFee = !OtherFee
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varPatientIxItemID = 0
    varPatientID = 0
    varIxID = 0
    varPatientIxID = 0
    varIxItemID = 0
    varValue = Empty
    varDeleted = False
    varDeletedDate = Empty
    varDeletedTime = Empty
    varDeletedUserID = 0
    varHospitalFee = 0
    varStaffFee = 0
    varOtherFee = 0
End Sub

Public Property Let PatientIxItemID(ByVal vPatientIxItemID As Long)
    Call clearData
    varPatientIxItemID = vPatientIxItemID
    Call loadData
End Property

Public Property Get PatientIxItemID() As Long
    PatientIxItemID = varPatientIxItemID
End Property

Public Property Let PatientID(ByVal vPatientID As Long)
    varPatientID = vPatientID
End Property

Public Property Get PatientID() As Long
    PatientID = varPatientID
End Property

Public Property Let IxID(ByVal vIxID As Long)
    varIxID = vIxID
End Property

Public Property Get IxID() As Long
    IxID = varIxID
End Property

Public Property Let PatientIxID(ByVal vPatientIxID As Long)
    varPatientIxID = vPatientIxID
End Property

Public Property Get PatientIxID() As Long
    PatientIxID = varPatientIxID
End Property

Public Property Let IxItemID(ByVal vIxItemID As Long)
    varIxItemID = vIxItemID
End Property

Public Property Get IxItemID() As Long
    IxItemID = varIxItemID
End Property

Public Property Let Value(ByVal vValue As String)
    varValue = vValue
End Property

Public Property Get Value() As String
    Value = varValue
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedDate(ByVal vDeletedDate As Date)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate() As Date
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime As Date)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime() As Date
    DeletedTime = varDeletedTime
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let HospitalFee(ByVal vHospitalFee As Double)
    varHospitalFee = vHospitalFee
End Property

Public Property Get HospitalFee() As Double
    HospitalFee = varHospitalFee
End Property

Public Property Let StaffFee(ByVal vStaffFee As Double)
    varStaffFee = vStaffFee
End Property

Public Property Get StaffFee() As Double
    StaffFee = varStaffFee
End Property

Public Property Let OtherFee(ByVal vOtherFee As Double)
    varOtherFee = vOtherFee
End Property

Public Property Get OtherFee() As Double
    OtherFee = varOtherFee
End Property


