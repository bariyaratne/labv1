VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsBill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarBillID As Long 'local copy
Private mvarBillType As String 'local copy
Private mvarBillDate As Date 'local copy
Private mvarBillTime As Date 'local copy
Private mvarCancelled As Boolean 'local copy
Private mvarCompany As String 'local copy
Private mvarCompanyID As Long 'local copy
Private mvarBHTID As Long 'local copy
Private mvarBHT As String 'local copy
Private mvarRoom As String 'local copy
Private mvarRoomID As Long 'local copy
Private mvarPatientID As Long 'local copy


Dim temSQL As String
'local variable(s) to hold property value(s)
Private mvarDoctor As String 'local copy
Private mvarDoctorID As Long 'local copy
Private mvarInstitution As String 'local copy
Private mvarInstitutionID As Long 'local copy
Private mvarTotalCharge As Double 'local copy
Private mvarDiscount As Double 'local copy
Private mvarNetTotal As Double 'local copy
Private mvarHospitalCharge As Double 'local copy
Private mvarMLTCharge As Double 'local copy
Private mvarReagentCharge As Double 'local copy
'local variable(s) to hold property value(s)
Private mvarCardNo As Long 'local copy

Public Property Let CardNo(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.CardNo = 5
    mvarCardNo = vData
End Property


Public Property Get CardNo() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CardNo
    CardNo = mvarCardNo
End Property



Public Property Let ReagentCharge(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ReagentCharge = 5
    mvarReagentCharge = vData
End Property


Public Property Get ReagentCharge() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ReagentCharge
    ReagentCharge = mvarReagentCharge
End Property



Public Property Let MLTCharge(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.MLTCharge = 5
    mvarMLTCharge = vData
End Property


Public Property Get MLTCharge() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.MLTCharge
    MLTCharge = mvarMLTCharge
End Property



Public Property Let HospitalCharge(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.HospitalCharge = 5
    mvarHospitalCharge = vData
End Property


Public Property Get HospitalCharge() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.HospitalCharge
    HospitalCharge = mvarHospitalCharge
End Property



Public Property Let NetTotal(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.NetTotal = 5
    mvarNetTotal = vData
End Property


Public Property Get NetTotal() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.NetTotal
    NetTotal = mvarNetTotal
End Property



Public Property Let Discount(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Discount = 5
    mvarDiscount = vData
End Property


Public Property Get Discount() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Discount
    Discount = mvarDiscount
End Property



Public Property Let TotalCharge(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.TotalCharge = 5
    mvarTotalCharge = vData
End Property


Public Property Get TotalCharge() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.TotalCharge
    TotalCharge = mvarTotalCharge
End Property

Public Property Get InstitutionID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.InstitutionID
    InstitutionID = mvarInstitutionID
End Property

Public Property Let Institution(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Institution = 5
    mvarInstitution = vData
End Property


Public Property Get Institution() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Institution
    Institution = mvarInstitution
End Property



Public Property Let DoctorID(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DoctorID = 5
    mvarDoctorID = vData
End Property


Public Property Get DoctorID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DoctorID
    DoctorID = mvarDoctorID
End Property



Public Property Let Doctor(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Doctor = 5
    mvarDoctor = vData
End Property


Public Property Get Doctor() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Doctor
    Doctor = mvarDoctor
End Property





Private Sub Clearvalues()
 mvarBillType = Empty
 mvarBillDate = Empty
 mvarBillTime = Empty
 mvarCancelled = False
 mvarCompany = Empty
 mvarCompanyID = Empty
 mvarBHTID = Empty
 mvarBHT = Empty
 mvarRoom = Empty
 mvarRoomID = Empty
 mvarPatientID = Empty
 mvarDoctor = Empty
 mvarDoctorID = 0
 mvarInstitution = Empty
 mvarInstitutionID = 0
 mvarTotalCharge = 0
 mvarDiscount = 0
 mvarNetTotal = 0
 mvarHospitalCharge = 0
 mvarMLTCharge = 0
 mvarReagentCharge = 0
 mvarCardNo = 0
End Sub

Public Property Get PatientID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PatientID
    PatientID = mvarPatientID
End Property

Public Property Get RoomID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.RoomID
    RoomID = mvarRoomID
End Property

Public Property Get Room() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Room
    Room = mvarRoom
End Property

Public Property Get BHT() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.BHT
    BHT = mvarBHT
End Property

Public Property Get BHTID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.BHTID
    BHTID = mvarBHTID
End Property

Public Property Get CompanyID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.CompanyID
    CompanyID = mvarCompanyID
End Property


Public Property Get Company() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Company
    Company = mvarCompany
End Property

Public Property Get Cancelled() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Cancelled
    Cancelled = mvarCancelled
End Property


Public Property Get BillTime() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.BillTime
    BillTime = mvarBillTime
End Property


Public Property Get billDate() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.BillDate
    billDate = mvarBillDate
End Property


Public Property Get BillType() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.BillType
    BillType = mvarBillType
End Property



Public Property Let BillID(ByVal vData As Long)
    mvarBillID = vData
    Call Clearvalues
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT tblCompany.Company, tblRoom.Room, tblBHT.BHT, tblPatientIxBill.* " & _
                    "FROM tblRoom RIGHT JOIN (tblBHT RIGHT JOIN (tblCompany RIGHT JOIN tblPatientIxBill ON tblCompany.CompanyID = tblPatientIxBill.CompanyID) ON tblBHT.BHTID = tblPatientIxBill.BHTID) ON tblRoom.RoomID = tblPatientIxBill.RoomID " & _
                    "WHERE (((tblPatientIxBill.PatientIxBillID)=" & mvarBillID & "))"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            mvarBillType = Format(!BillType, "")
            mvarBillDate = !Date
            mvarBillTime = !Time
            mvarCancelled = !Cancelled
            mvarCompany = Format(!Company, "")
            If IsNull(!CompanyID) = False Then
            mvarCompanyID = !CompanyID
            End If
            If IsNull(!BHT) = False Then
            mvarBHTID = !BHTID
            End If
            If IsNull(!BHT) = False Then
                mvarBHT = "BHT : " & Format(!BHT, "")
            Else
                mvarBHT = ""
            End If
            mvarRoom = Format(!Room, "")
            If IsNull(!RoomID) = False Then
                mvarRoomID = !RoomID
            End If
            mvarPatientID = !PatientID
            mvarDoctorID = !DoctorID
            mvarInstitution = Empty
            If IsNull(!InstitutionID) = False Then
            mvarInstitutionID = !InstitutionID
            End If
            On Error Resume Next
            mvarTotalCharge = !Charge
            mvarDiscount = !Discount
            mvarNetTotal = !NetValue
            mvarHospitalCharge = !HospitalCharge
            mvarMLTCharge = !StaffCharge
            mvarReagentCharge = !OtherCharge
            If Not IsNull(!CardNo) Then
                mvarCardNo = !CardNo
            End If
        End If
        .Close
        temSQL = "SELECT Doctor from tblDoctor where DoctorID = " & mvarDoctorID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            mvarDoctor = !Doctor
        End If
        temSQL = "SELECT Institution from tblInstitution where InstitutionID = " & mvarInstitutionID
        .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            mvarInstitution = !Institution
        End If
        .Close
    End With
End Property


Public Property Get BillID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.BillID
    BillID = mvarBillID
End Property



