VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmIssue 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Issue Reports"
   ClientHeight    =   8040
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   13980
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8040
   ScaleWidth      =   13980
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   2160
      TabIndex        =   30
      Top             =   7320
      Width           =   4935
      Begin VB.OptionButton optPotrate 
         Caption         =   "Potrate"
         Height          =   240
         Left            =   2400
         TabIndex        =   32
         Top             =   240
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.OptionButton optLandscape 
         Caption         =   "Landscape"
         Height          =   240
         Left            =   120
         TabIndex        =   31
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.ListBox lstIx1 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5130
      Left            =   9600
      TabIndex        =   29
      Top             =   1920
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ListBox lstPatientID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5130
      Left            =   10320
      TabIndex        =   22
      Top             =   1920
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   3840
      Style           =   2  'Dropdown List
      TabIndex        =   19
      Top             =   6600
      Width           =   3255
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   3840
      Style           =   2  'Dropdown List
      TabIndex        =   18
      Top             =   6960
      Width           =   3255
   End
   Begin VB.CheckBox chkPrint 
      Caption         =   "Print Envelops"
      Height          =   195
      Left            =   120
      TabIndex        =   17
      Top             =   7200
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin VB.CheckBox chkToHandOver 
      Caption         =   "To Hand-Over"
      Height          =   255
      Left            =   6000
      TabIndex        =   16
      Top             =   1200
      Value           =   1  'Checked
      Width           =   3015
   End
   Begin VB.ListBox lstIxID 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5130
      Left            =   11520
      TabIndex        =   15
      Top             =   1920
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.ListBox lstIx 
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4545
      Left            =   120
      Style           =   1  'Checkbox
      TabIndex        =   14
      Top             =   1920
      Width           =   13695
   End
   Begin VB.CheckBox chkToPrint 
      Caption         =   "To Take Printouts"
      Height          =   255
      Left            =   6000
      TabIndex        =   8
      Top             =   120
      Width           =   1815
   End
   Begin VB.CheckBox chkPrinted 
      Caption         =   "Printouts taken"
      Height          =   255
      Left            =   6000
      TabIndex        =   7
      Top             =   480
      Width           =   1815
   End
   Begin VB.CheckBox chkToHadnover 
      Caption         =   "Handed-over to patient"
      Height          =   255
      Left            =   6000
      TabIndex        =   6
      Top             =   840
      Width           =   3015
   End
   Begin VB.OptionButton optByPatient 
      Caption         =   "By Patient"
      Height          =   255
      Left            =   9000
      TabIndex        =   2
      Top             =   120
      Value           =   -1  'True
      Width           =   1695
   End
   Begin VB.OptionButton optByIx 
      Caption         =   "By Investigation"
      Height          =   255
      Left            =   9000
      TabIndex        =   1
      Top             =   480
      Width           =   1695
   End
   Begin VB.OptionButton optByID 
      Caption         =   "By Time"
      Height          =   255
      Left            =   9000
      TabIndex        =   0
      Top             =   840
      Width           =   1815
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   12600
      TabIndex        =   3
      Top             =   7200
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmbDept 
      Height          =   360
      Left            =   1440
      TabIndex        =   4
      Top             =   1080
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1440
      TabIndex        =   5
      Top             =   120
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   52690947
      CurrentDate     =   39812
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   1440
      TabIndex        =   9
      Top             =   600
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   52232195
      CurrentDate     =   39812
   End
   Begin btButtonEx.ButtonEx btnIssue 
      Height          =   495
      Left            =   120
      TabIndex        =   10
      Top             =   6600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Issue"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label9 
      Caption         =   "Issued"
      Height          =   255
      Left            =   11760
      TabIndex        =   28
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Label Label8 
      Caption         =   "Printed"
      Height          =   255
      Left            =   9240
      TabIndex        =   27
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Label Label7 
      Caption         =   "Billed"
      Height          =   255
      Left            =   6600
      TabIndex        =   26
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Label Label6 
      Caption         =   "Investigation"
      Height          =   255
      Left            =   3840
      TabIndex        =   25
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Label Label5 
      Caption         =   "Patient"
      Height          =   255
      Left            =   1080
      TabIndex        =   24
      Top             =   1560
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "No"
      Height          =   255
      Left            =   120
      TabIndex        =   23
      Top             =   1560
      Width           =   615
   End
   Begin VB.Label Label12 
      Caption         =   "Printer"
      Height          =   375
      Left            =   2160
      TabIndex        =   21
      Top             =   6600
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "Paper"
      Height          =   255
      Left            =   2160
      TabIndex        =   20
      Top             =   6960
      Width           =   1575
   End
   Begin VB.Label Label1 
      Caption         =   "From :"
      Height          =   255
      Left            =   120
      TabIndex        =   13
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "To:"
      Height          =   255
      Left            =   120
      TabIndex        =   12
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label Label3 
      Caption         =   "Department :"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   1080
      Width           =   1215
   End
End
Attribute VB_Name = "frmIssue"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim NumForms As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim SuppliedWord As String
    Dim FSys As New Scripting.FileSystemObject
    Private csetPrinter As New cSetDfltPrinter
    
    
    Dim temSQL As String
    Dim rsIxList As New ADODB.Recordset
    Dim i As Integer
    Dim temText As String
    
    Public temReportID As Long
    Public temPatientIxID As Long
    
    
Private Sub FillPrinters()
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        cmbPrinter.AddItem MyPrinter.DeviceName
    Next
End Sub

Private Sub btnIssue_Click()
    Dim ThisPatientID  As Long
    Dim MyPatient As New clsPatient
    ThisPatientID = 0
    For i = 0 To lstIx.ListCount - 1
        If lstIx.Selected(i) = True Then
            If ThisPatientID <> 0 Then
                If ThisPatientID <> Val(lstPatientID.List(i)) Then
                    MsgBox ("Please select one patient at a time when you issue reports")
                    lstIx.SetFocus
                    Exit Sub
                End If
            Else
                ThisPatientID = Val(lstPatientID.List(i))
                MyPatient.ID = ThisPatientID
            End If
        End If
    Next
    If ThisPatientID = 0 Then
        MsgBox "Please select at least one Investigation to issue"
        lstIx.SetFocus
        Exit Sub
    End If
    For i = 0 To lstIx.ListCount - 1
        If lstIx.Selected(i) = True Then
            With rsIxList
                If .State = 1 Then .Close
                temSQL = "Select * from tblPatientIx where PatientIxID = " & Val(lstIxID.List(i))
                .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
                If .RecordCount > 0 Then
                    !Issued = True
                    !IssuedDate = Date
                    !IssuedTime = Time
                    !IssuedUserID = UserID
                    .Update
                End If
                If .State = 1 Then .Close
            End With
        End If
    Next
    
    csetPrinter.SetPrinterAsDefault (cmbPrinter.text)
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = cmbPrinter.text Then
            Set MyPrinter = Printer
        End If
    Next
    If SelectForm(cmbPaper.text, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim tabLabName As Integer
    Dim tabLabDescreption As Integer
    Dim tabLabAddress As Integer
    Dim tabPatientName As Integer
    Dim tabPatientAge As Integer
    Dim tabPatientSex As Integer
    Dim tabIX As Integer
    Dim tabIxList As Integer
    Dim tabTitle As Integer
    Dim tabValue As Integer
    
     tabLabName = 30
     tabLabDescreption = 29
     tabLabAddress = 36
     tabPatientName = 10
     tabPatientAge = 10
     tabPatientSex = 10
     tabIX = 10
     tabIxList = 25
     tabTitle = 35
     tabValue = 25
    
    On Error Resume Next
    
    If optLandscape.Value = True Then
        Printer.Orientation = vbPRORLandscape
    End If
    
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.FontSize = 16
    Printer.FontBold = True
    Printer.Font = "Times New Roman"
    Printer.Print Tab(tabLabName); HospitalName
    Printer.FontBold = False
    Printer.FontSize = 12
    Printer.Font = "Times New Roman"
    Printer.Print Tab(tabLabDescreption); "" 'HospitalDescreption
    Printer.Print Tab(tabLabAddress); HospitalAddress
    Printer.Print
    Printer.FontSize = 12
    Printer.Font = "Bookman Old Style"
    Printer.Print Tab(tabPatientName); "Name : "; Tab(tabValue); MyPatient.Name
    Printer.Print Tab(tabPatientAge); "ID : "; Tab(tabValue); MyPatient.ID
    'Printer.Print Tab(tabPatientSex); "Sex : "; Tab(tabValue); MyPatient.Sex
    Printer.Print
    Printer.Print Tab(tabIX); "Ix"
    For i = 0 To lstIx.ListCount - 1
        If lstIx.Selected(i) = True Then
            Printer.FontSize = 10
            Printer.Font = "Courier New"
            Printer.Print Tab(tabIxList); lstIx1.List(i)
        End If
    Next
    Printer.FontSize = 12
    Printer.Font = "Times New Roman"
    Printer.Print
    Printer.Print Tab(tabTitle); "" ' "...::: WE CARE YOUR HEALTHY LIFE :::..."
    Printer.EndDoc

    Call Process
    

End Sub


Private Sub cmbPrinter_Change()
    cmbPrinter_Click
End Sub

Private Sub cmbPrinter_Click()
    cmbPaper.Clear
    csetPrinter.SetPrinterAsDefault (cmbPrinter.text)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        With FormSize
            .cx = BillPaperHeight
            .cy = BillPaperWidth
        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                cmbPaper.AddItem PtrCtoVbString(.pName)
            End With
        Next i
        ClosePrinter (PrinterHandle)
    End If
End Sub
    
Private Sub Process()
    lstIx.Clear
    lstIxID.Clear
    lstIx1.Clear
    
    lstPatientID.Clear
    FillList
End Sub

Private Sub FillCombos()
    Dim Dept As New clsFillCombos
    Dept.FillAnyCombo cmbDept, "Dept", True
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub chkPrinted_Click()
    Call Process
End Sub

Private Sub chkToHadnover_Click()
    Call Process
End Sub

Private Sub chkToPrint_Click()
    Call Process
End Sub

Private Sub cmbDept_Change()
    Call Process
End Sub

Private Sub cmbDept_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbDept.text = Empty
    End If
End Sub

Private Sub dtpFrom_Change()
    Call Process
End Sub

Private Sub dtpTo_Change()
    Call Process
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSetting App.EXEName, Me.Name, "Printer", cmbPrinter.text
    SaveSetting App.EXEName, Me.Name, "Paper", cmbPaper.text
End Sub


Private Sub Form_Load()
    Call FillCombos
    Call FillPrinters
    dtpFrom.Value = Date
    dtpTo.Value = Date
    Call FillList
    On Error Resume Next
    cmbPrinter.text = GetSetting(App.EXEName, Me.Name, "Printer", "")
    cmbPrinter_Click
    cmbPaper.text = GetSetting(App.EXEName, Me.Name, "Paper", "")
End Sub

Private Sub FillList()
    With rsIxList
        If .State = 1 Then .Close
        temSQL = "SELECT tblPatient.Name, tblIx.Ix, tblPatientIx.* "
        temSQL = temSQL & "FROM tblPatient RIGHT JOIN (tblPatientIx LEFT JOIN tblIx ON tblPatientIx.IxID = tblIx.IxID) ON tblPatient.PatientID = tblPatientIx.PatientID "
        temSQL = temSQL & "WHERE (((tblPatientIx.Deleted)=False) AND ( ((tblPatientIx.Billed)=False) "
        If chkPrinted.Value = 1 Then
            temSQL = temSQL & " OR ((tblPatientIx.Printed)=True) "
        End If
        If chkToHadnover.Value = 1 Then
            temSQL = temSQL & " OR ((tblPatientIx.Issued)=True) "
        End If
        If chkToPrint.Value = 1 Then
            temSQL = temSQL & " OR ((tblPatientIx.Printed)=False) "
        End If
        If chkToHandOver.Value = 1 Then
            temSQL = temSQL & " OR (((tblPatientIx.Printed)=True) AND ((tblPatientIx.Issued)=False)) "
        End If
        temSQL = temSQL & " ) "
        temSQL = temSQL & " AND ((tblPatientIx.BilledDate) Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# And #" & Format(dtpTo.Value, "dd MMMM yyyy") & "#) "
        If IsNumeric(cmbDept.BoundText) = True Then
            temSQL = temSQL & " AND ((tblIx.DeptID)=" & Val(cmbDept.BoundText) & "))"
        Else
            temSQL = temSQL & ")"
        End If
        
        If optByPatient.Value = True Then
            temSQL = temSQL & "ORDER BY tblPatient.Name"
        ElseIf optByIx.Value = True Then
            temSQL = temSQL & "ORDER BY tblIx.Ix"
        ElseIf optByID.Value = True Then
            temSQL = temSQL & "ORDER BY tblPatientIx.PatientIxID"
        End If
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            .MoveLast
            .MoveFirst
            For i = 1 To .RecordCount
                temText = Empty
                temText = temText & Format(i, "00")
                temText = temText & vbTab
                temText = temText & Left(!Name & Space(40), 15)
                temText = temText & vbTab
                temText = temText & Left(!Ix & Space(40), 20)
                temText = temText & vbTab
                temText = temText & Format(!BilledDate, "dd/MMM/yy") & " - " & Format(!BilledTime, "HH:MM")
                temText = temText & vbTab
                temText = temText & Format(!PrintedDate, "dd/MMM/yy") & " - " & Format(!PrintedTime, "HH:MM")
                temText = temText & vbTab
                temText = temText & Format(!IssuedDate, "dd/MMM/yy") & " - " & Format(!IssuedTime, "HH:MM")
                lstIx.AddItem temText
                lstIxID.AddItem !PatientIxID
                lstPatientID.AddItem !PatientID
                lstIx1.AddItem !Ix
                .MoveNext
            Next
        End If
        .Close
        
    End With
End Sub

Private Sub optByID_Click()
    Call Process
End Sub

Private Sub optByIx_Click()
    Call Process
End Sub

Private Sub optByPatient_Click()
    Call Process
End Sub

