VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmDoctor 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Doctor Details"
   ClientHeight    =   7845
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11040
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7845
   ScaleWidth      =   11040
   Begin MSDataListLib.DataCombo cmbDoctor 
      Height          =   5700
      Left            =   120
      TabIndex        =   26
      Top             =   240
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   10054
      _Version        =   393216
      Style           =   1
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx bttnCancel 
      Height          =   495
      Left            =   7440
      TabIndex        =   13
      Top             =   6720
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "Ca&ncel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   495
      Left            =   9480
      TabIndex        =   14
      Top             =   7200
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnSave 
      Height          =   495
      Left            =   6000
      TabIndex        =   12
      Top             =   6720
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "Sa&ve"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame FramDoctor 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6375
      Left            =   4440
      TabIndex        =   1
      Top             =   120
      Width           =   6375
      Begin VB.TextBox txtComments 
         Height          =   375
         Left            =   2400
         TabIndex        =   25
         Top             =   5640
         Width           =   3615
      End
      Begin VB.TextBox txtCredit 
         Height          =   360
         Left            =   2400
         TabIndex        =   11
         Top             =   5160
         Width           =   3615
      End
      Begin VB.TextBox txtAccount 
         Height          =   375
         Left            =   2400
         TabIndex        =   10
         Top             =   4680
         Width           =   3615
      End
      Begin VB.TextBox txtBank 
         Height          =   360
         Left            =   2400
         TabIndex        =   9
         Top             =   4200
         Width           =   3615
      End
      Begin VB.TextBox txtPaymethod 
         Height          =   375
         Left            =   2400
         TabIndex        =   8
         Top             =   3720
         Width           =   3615
      End
      Begin VB.TextBox txtEmail 
         Height          =   375
         Left            =   2400
         TabIndex        =   7
         Top             =   3240
         Width           =   3615
      End
      Begin VB.TextBox txtFax 
         Height          =   375
         Left            =   2400
         TabIndex        =   6
         Top             =   2760
         Width           =   3615
      End
      Begin VB.TextBox txtTel 
         Height          =   375
         Left            =   2400
         TabIndex        =   5
         Top             =   2280
         Width           =   3615
      End
      Begin VB.TextBox txtAddress 
         Height          =   1335
         Left            =   2400
         MultiLine       =   -1  'True
         TabIndex        =   4
         Top             =   840
         Width           =   3615
      End
      Begin VB.TextBox txtName 
         Height          =   375
         Left            =   2400
         TabIndex        =   3
         Top             =   360
         Width           =   3615
      End
      Begin VB.Label Label10 
         Caption         =   "Doctor Comments"
         Height          =   495
         Left            =   240
         TabIndex        =   24
         Top             =   5640
         Width           =   1815
      End
      Begin VB.Label Label9 
         Caption         =   "Doctor Credit"
         Height          =   495
         Left            =   240
         TabIndex        =   23
         Top             =   5160
         Width           =   1215
      End
      Begin VB.Label Label8 
         Caption         =   "Doctor Account"
         Height          =   495
         Left            =   240
         TabIndex        =   22
         Top             =   4680
         Width           =   1575
      End
      Begin VB.Label Label7 
         Caption         =   "Doctor Bank"
         Height          =   495
         Left            =   240
         TabIndex        =   21
         Top             =   4200
         Width           =   1215
      End
      Begin VB.Label Label6 
         Caption         =   "Doctor Payment Method"
         Height          =   495
         Left            =   240
         TabIndex        =   20
         Top             =   3720
         Width           =   2535
      End
      Begin VB.Label Label5 
         Caption         =   "Doctor E-Mail"
         Height          =   495
         Left            =   240
         TabIndex        =   19
         Top             =   3240
         Width           =   1215
      End
      Begin VB.Label Label4 
         Caption         =   "Doctor Fax:"
         Height          =   495
         Left            =   240
         TabIndex        =   18
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Doctor Tel:"
         Height          =   495
         Left            =   240
         TabIndex        =   17
         Top             =   2400
         Width           =   1215
      End
      Begin VB.Label Label2 
         Caption         =   "Doctor Address"
         Height          =   495
         Left            =   240
         TabIndex        =   16
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Doctor Name"
         Height          =   375
         Left            =   240
         TabIndex        =   15
         Top             =   360
         Width           =   2055
      End
   End
   Begin btButtonEx.ButtonEx bttnEdit 
      Height          =   495
      Left            =   1560
      TabIndex        =   2
      Top             =   6000
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "E&dit"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnAdd 
      Height          =   495
      Left            =   120
      TabIndex        =   0
      Top             =   6000
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Add"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnDelete 
      Height          =   495
      Left            =   3000
      TabIndex        =   27
      Top             =   6000
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Delete"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDoctor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Dim rsDoctor As New ADODB.Recordset
    

Private Sub btnDelete_Click()
    Dim i As Integer
    If IsNumeric(cmbDoctor.BoundText) = False Then
        MsgBox "Please select a doctor to delete"
        cmbDoctor.SetFocus
        Exit Sub
    End If
    i = MsgBox("Are you sure?", vbYesNo)
    If i = vbNo Then Exit Sub
    With rsDoctor
        If .State = 1 Then .Close
        temSql = "Select * from tblDoctor where DoctorID = " & Val(cmbDoctor.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Deleted = True
            !DeletedUserID = UserID
            !DeletedDate = Date
            !DeletedTime = Time
            .Update
            MsgBox "Deleted"
            .Close
        End If
    End With
    Call Clearvalues
    Call FillCOmbos
    cmbDoctor.Text = Empty
    cmbDoctor.SetFocus
End Sub

Private Sub bttnAdd_Click()
    Dim temString As String
    If IsNumeric(cmbDoctor.BoundText) = False Then
        temString = cmbDoctor.Text
    Else
        temString = Empty
    End If
    cmbDoctor.Text = Empty

    Call Clearvalues
    Call EditMode
        
    txtName.Text = temString
    txtName.SetFocus
    
On Error Resume Next:     SendKeys "{home}+{end}"
        
End Sub

Private Sub bttnCancel_Click()
    Call Clearvalues
    Call SelectMode
    cmbDoctor.Text = Empty
    cmbDoctor.SetFocus
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnEdit_Click()
    Call EditMode
    txtName.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnSave_Click()
    If IsNumeric(cmbDoctor.BoundText) = True Then
        Call ChangeData
    Else
        Call SaveData
    End If
    Call Clearvalues
    Call SelectMode
    Call FillCOmbos
    cmbDoctor.Text = Empty
    cmbDoctor.SetFocus
End Sub

Private Sub FillCOmbos()
    Dim clsDoc As New clsFillCombos
    clsDoc.FillAnyCombo cmbDoctor, "Doctor", True
End Sub

Private Sub cmbDoctor_Change()
    Call Clearvalues
    If IsNumeric(cmbDoctor.BoundText) = True Then
        Call DisplayDetails
    End If
End Sub

Private Sub Form_Load()
    Call FillCOmbos
    Call SelectMode
End Sub

Private Sub SelectMode()
    bttnEdit.Enabled = True
    bttnAdd.Enabled = True
    btnDelete.Enabled = True
    
    bttnSave.Enabled = False
    bttnCancel.Enabled = False
    
    FramDoctor.Enabled = False
End Sub

Private Sub EditMode()
    bttnEdit.Enabled = False
    bttnAdd.Enabled = False
    btnDelete.Enabled = False
    
    bttnSave.Enabled = True
    bttnCancel.Enabled = True
    
    FramDoctor.Enabled = True
End Sub

Private Sub SaveData()
    Dim A
    If Trim(txtName.Text) = "" Then
        A = MsgBox("Name Empty,Enter ", vbCritical + vbOKOnly, "Error")
        txtName.SetFocus
        Exit Sub
    End If
    
    On Error GoTo ErrorHandler
    
    With rsDoctor
        If .State = 1 Then .Close
        temSql = "Select * from tblDoctor"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !Doctor = txtName.Text
        !DoctorAddress = txtAddress.Text
        !DoctorTelephone = txtTel.Text
        !DoctorFax = txtFax.Text
        !DoctorEmail = txtEmail.Text
        !DoctorPaymentMethod = txtPaymethod.Text
        !DoctorBank = txtBank.Text
        !DoctorAccount = txtAccount.Text
        !DoctorCredit = Val(txtCredit.Text)
        !DoctorComments = txtComments.Text
        .Update
        .Close
    End With
    
Exit Sub
    
ErrorHandler:
    MsgBox Error, vbOKOnly, "Update Error"
End Sub

Private Sub ChangeData()
    Dim A
    If Trim(txtName.Text) = "" Then
        A = MsgBox("Name Empty,Enter ", vbCritical + vbOKOnly, "Error")
        txtName.SetFocus
        Exit Sub
    End If
    
    On Error GoTo ErrorHandler
    
    With rsDoctor
        If .State = 1 Then .Close
        temSql = "Select * from tblDoctor where DoctorID = " & Val(cmbDoctor.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Doctor = txtName.Text
            !DoctorAddress = txtAddress.Text
            !DoctorTelephone = txtTel.Text
            !DoctorFax = txtFax.Text
            !DoctorEmail = txtEmail.Text
            !DoctorPaymentMethod = txtPaymethod.Text
            !DoctorBank = txtBank.Text
            !DoctorAccount = txtAccount.Text
            !DoctorCredit = Val(txtCredit.Text)
            !DoctorComments = txtComments.Text
            .Update
        End If
        .Close
    End With
    
Exit Sub
    
ErrorHandler:
    MsgBox Error, vbOKOnly, "Update Error"
End Sub

Private Sub Clearvalues()
    txtName.Text = Empty
    txtAddress.Text = Empty
    txtTel.Text = Empty
    txtFax.Text = Empty
    txtEmail.Text = Empty
    txtPaymethod.Text = Empty
    txtBank.Text = Empty
    txtAccount.Text = Empty
    txtCredit.Text = Empty
    txtComments.Text = Empty
End Sub

Private Sub DisplayDetails()
    If IsNumeric(cmbDoctor.BoundText) = False Then Exit Sub
    With rsDoctor
        If .State = 1 Then .Close
        temSql = "SELECT * from tblDoctor where DoctorID = " & Val(cmbDoctor.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If Not IsNull(!Doctor) Then
                txtName.Text = !Doctor
            End If
            If Not IsNull(!DoctorAddress) Then
                txtAddress.Text = !DoctorAddress
            End If
            If Not IsNull(!DoctorTelephone) Then
                txtTel.Text = !DoctorTelephone
            End If
            If Not IsNull(!DoctorFax) Then
                txtFax.Text = !DoctorFax
            End If
            If Not IsNull(!DoctorEmail) Then
                txtEmail.Text = !DoctorEmail
            End If
            If Not IsNull(!DoctorPaymentMethod) Then
                txtPaymethod.Text = !DoctorPaymentMethod
            End If
            If Not IsNull(!DoctorBank) Then
                txtBank.Text = !DoctorBank
            End If
            If Not IsNull(!DoctorAccount) Then
                txtAccount.Text = !DoctorAccount
            End If
            If Not IsNull(!DoctorCredit) Then
                txtCredit.Text = !DoctorCredit
            End If
            If Not IsNull(!DoctorComments) Then
                txtComments.Text = !DoctorComments
            End If
        End If
    End With

End Sub

