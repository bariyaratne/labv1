VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmMyDetails 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Staff Details"
   ClientHeight    =   5280
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6435
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5280
   ScaleWidth      =   6435
   Begin VB.Frame fra2 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4575
      Left            =   120
      TabIndex        =   9
      Top             =   120
      Width           =   6135
      Begin MSDataListLib.DataCombo dtcPosition 
         Height          =   360
         Left            =   1440
         TabIndex        =   6
         Top             =   3120
         Visible         =   0   'False
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin VB.TextBox txtName 
         Height          =   375
         Left            =   1440
         TabIndex        =   0
         Top             =   240
         Width           =   4575
      End
      Begin VB.TextBox txtAddress 
         Height          =   375
         Left            =   1440
         TabIndex        =   1
         Top             =   720
         Width           =   4575
      End
      Begin VB.TextBox txtPhone 
         Height          =   375
         Left            =   1440
         TabIndex        =   2
         Top             =   1200
         Width           =   4575
      End
      Begin VB.TextBox txtMobile 
         Height          =   375
         Left            =   1440
         TabIndex        =   3
         Top             =   1680
         Width           =   4575
      End
      Begin VB.TextBox txtUserName 
         Height          =   375
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   4
         Top             =   2160
         Width           =   4575
      End
      Begin VB.TextBox txtPassword 
         Height          =   375
         IMEMode         =   3  'DISABLE
         Left            =   1440
         PasswordChar    =   "*"
         TabIndex        =   5
         Top             =   2640
         Width           =   4575
      End
      Begin btButtonEx.ButtonEx bttnChange 
         Height          =   375
         Left            =   2400
         TabIndex        =   10
         Top             =   3840
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnCancel 
         Height          =   375
         Left            =   4080
         TabIndex        =   7
         Top             =   3840
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Cancel"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         Caption         =   "Position"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   3120
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.Label lblName 
         Caption         =   "Name"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label lblAddress 
         Caption         =   "Address"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label lblPhone 
         Caption         =   "Phone"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Label lblMobile 
         Caption         =   "Mobile"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   1800
         Width           =   1455
      End
      Begin VB.Label lblUserName 
         Caption         =   "User Name"
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   2280
         Width           =   1455
      End
      Begin VB.Label lblPassword 
         Caption         =   "Password"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   2760
         Width           =   1455
      End
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   5280
      TabIndex        =   8
      Top             =   4800
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmMyDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsStaffDetails As New ADODB.Recordset
    Dim rsViewStaffDetails As New ADODB.Recordset
    Dim rsComments As New ADODB.Recordset
    Dim rsViewPosition As New ADODB.Recordset
    
    Dim temSQL As String
    
Private Sub AfterEdit()
    
    
    bttnChange.Enabled = True
    bttnCancel.Enabled = True
    txtName.Enabled = True
    txtAddress.Enabled = True
    txtPhone.Enabled = True
    txtMobile.Enabled = True
    txtUserName.Enabled = True
    txtPassword.Enabled = True
    dtcPosition.Enabled = True
    
    bttnChange.Visible = True
    
End Sub


Private Sub Clearvalues()
    txtName.Text = Empty
    txtAddress.Text = Empty
    txtPhone.Text = Empty
    txtMobile.Text = Empty
    txtUserName.Text = Empty
    txtPassword.Text = Empty
    dtcPosition.Text = Empty
End Sub



Private Sub bttnCancel_Click()
    Call Clearvalues
    Call DisplaySelected
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnChange_Click()
    Dim TemResponce As Integer
    With rsStaffDetails
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select * From tblStaff Where StaffID = " & UserID, cnnLab, adOpenStatic, adLockOptimistic
        !Name = txtName.Text
        If .RecordCount = 0 Then Exit Sub
        !Name = Trim(txtName.Text)
        !Address1 = txtAddress.Text
        !Telephone = txtPhone.Text
        !Mobile = txtMobile.Text
        !UserName = EncreptedWord(txtUserName.Text)
        !Password = EncreptedWord(txtPassword.Text)
        !PositionID = dtcPosition.BoundText
        .Update
        If .State = 1 Then .Close
        Unload Me
        Exit Sub
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        Clearvalues
        If .State = 1 Then .Close
    End With
End Sub
    

Private Sub bttnClose_Click()
    Unload Me
End Sub


Private Sub Form_Load()
    FillCombos
    DisplaySelected
    AfterEdit
    
End Sub
Private Sub FillCombos()

    With rsViewPosition
        If .State = 1 Then .Close
        temSQL = "SELECT tblPosition.* FROM tblPosition"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcPosition
        Set .RowSource = rsViewPosition
        .ListField = "Position"
        .BoundColumn = "PositionID"
    End With
    
End Sub

Private Sub NoName()
    Dim TemResponce As Integer
    TemResponce = MsgBox("You have not entered a Name to save", vbCritical, "No Name")
    txtName.SetFocus
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If rsStaffDetails.State = 1 Then rsStaffDetails.Close: Set rsStaffDetails = Nothing
    If rsViewStaffDetails.State = 1 Then rsViewStaffDetails.Close: Set rsViewStaffDetails = Nothing
End Sub

Private Sub DisplaySelected()
    
    With rsStaffDetails
        If .State = 1 Then .Close
        .Open "Select * From tblStaff Where (StaffID = " & UserID & ")", cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Call Clearvalues
        If Not IsNull(!Name) Then txtName.Text = !Name
        If Not IsNull(!Address1) Then txtAddress.Text = !Address1
        If Not IsNull(!Telephone) Then txtPhone.Text = !Telephone
        If Not IsNull(!Mobile) Then txtMobile.Text = !Mobile
        If Not IsNull(!UserName) Then txtUserName.Text = DecreptedWord(!UserName)
        If Not IsNull(!Password) Then txtPassword.Text = DecreptedWord(!Password)
        If Not IsNull(!PositionID) Then dtcPosition.BoundText = !PositionID
        If .State = 1 Then .Close
    End With
End Sub
