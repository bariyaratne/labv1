VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmStaffIxLists 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Staff Payments"
   ClientHeight    =   8310
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14340
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8310
   ScaleWidth      =   14340
   Begin VB.CheckBox chkPrint 
      Caption         =   "Print"
      Height          =   195
      Left            =   120
      TabIndex        =   18
      Top             =   7560
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   4440
      Style           =   2  'Dropdown List
      TabIndex        =   17
      Top             =   7320
      Width           =   3255
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   4440
      Style           =   2  'Dropdown List
      TabIndex        =   16
      Top             =   6960
      Width           =   3255
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   2760
      TabIndex        =   13
      Top             =   7680
      Width           =   4935
      Begin VB.OptionButton optLandscape 
         Caption         =   "Landscape"
         Height          =   240
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.OptionButton optPotrate 
         Caption         =   "Potrate"
         Height          =   240
         Left            =   2400
         TabIndex        =   14
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.OptionButton optByID 
      Caption         =   "By Time"
      Height          =   255
      Left            =   6960
      TabIndex        =   12
      Top             =   960
      Width           =   1815
   End
   Begin VB.OptionButton optByIx 
      Caption         =   "By Investigation"
      Height          =   255
      Left            =   6960
      TabIndex        =   11
      Top             =   600
      Width           =   1695
   End
   Begin VB.OptionButton optByPatient 
      Caption         =   "By Patient"
      Height          =   255
      Left            =   6960
      TabIndex        =   10
      Top             =   240
      Value           =   -1  'True
      Width           =   1695
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   12960
      TabIndex        =   7
      Top             =   7680
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid gridIx 
      Height          =   5295
      Left            =   120
      TabIndex        =   6
      Top             =   1560
      Width           =   14055
      _ExtentX        =   24791
      _ExtentY        =   9340
      _Version        =   393216
      WordWrap        =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin MSDataListLib.DataCombo cmbDept 
      Height          =   360
      Left            =   1440
      TabIndex        =   5
      Top             =   1080
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1440
      TabIndex        =   3
      Top             =   120
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   46858243
      CurrentDate     =   39812
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   1440
      TabIndex        =   4
      Top             =   600
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   46858243
      CurrentDate     =   39812
   End
   Begin btButtonEx.ButtonEx btnPrintList 
      Height          =   495
      Left            =   120
      TabIndex        =   8
      Top             =   6960
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print List"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnIx 
      Height          =   495
      Left            =   1440
      TabIndex        =   9
      Top             =   6960
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Report"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      Height          =   375
      Left            =   9360
      TabIndex        =   22
      Top             =   6960
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "Total Value"
      Height          =   375
      Left            =   7800
      TabIndex        =   21
      Top             =   6960
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "Paper"
      Height          =   255
      Left            =   2760
      TabIndex        =   20
      Top             =   7320
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "Printer"
      Height          =   375
      Left            =   2760
      TabIndex        =   19
      Top             =   6960
      Width           =   1575
   End
   Begin VB.Label Label3 
      Caption         =   "Department :"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "To:"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "From :"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmStaffIxLists"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim NumForms As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim SuppliedWord As String
    Dim FSys As New Scripting.FileSystemObject
    Private csetPrinter As New cSetDfltPrinter
    Dim rsViewStaff As New ADODB.Recordset
    
    Dim temSql As String
    Dim rsIxList As New ADODB.Recordset
    Dim i As Integer
    Public temReportID As Long
    Public temPatientIxID As Long
    
Private Sub Process()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub FillCombos()
    With rsViewStaff
        If .State = 1 Then .Close
        temSql = "Select * from tblStaff Where Deleted = false order by Name"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With cmbDept
        Set .RowSource = rsViewStaff
        .ListField = "Name"
        .BoundColumn = "StaffID"
        
    End With
    
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnIx_Click()
    Dim temRow As Integer
    Dim temPatientID As Long
    Dim temIxID As Long
    Dim temDoctorID As Long
    Dim temInstitutionID As Long
    
    Unload frmReport
    
    With gridIx
        If .Rows <= 1 Then Exit Sub
        If .Row < 1 Then Exit Sub
        temRow = .Row
        If IsNumeric(.TextMatrix(temRow, 6)) = False Then Exit Sub
        temPatientIxID = Val(.TextMatrix(temRow, 6))
    End With
    With rsIxList
        If .State = 1 Then .Close
        temSql = "Select * from tblPatientIx where PatientIxID = " & temPatientIxID
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            temPatientID = !PatientID
            temIxID = !IxID
            If IsNull(!DoctorID) = False Then
                temDoctorID = !DoctorID
            Else
                temDoctorID = 0
            End If
            If IsNull(!InstitutionID) = False Then
                temInstitutionID = !InstitutionID
            Else
                temInstitutionID = 0
            End If
        Else
            MsgBox "Error"
            Exit Sub
        End If
    End With
    
    frmReport.Show
    frmReport.WindowState = 2
    On Error Resume Next
    
    Call Process
    
End Sub


Private Sub btnPrintList_Click()
    
    Dim tabLabName As Integer
    Dim tabLabDescreption As Integer
    Dim tabLabAddress As Integer
    Dim tabNo As Integer
    Dim tabPatient As Integer
    Dim tabIX As Integer
    Dim tabBilled As Integer
    Dim tabPrinted As Integer
    Dim tabIssued As Integer
    Dim tabSex As Integer
    Dim tabAge As Integer
    Dim tabDept As Integer
    Dim tabDate As Integer
    Dim tabPrice As Integer
    Dim tabAge1 As Integer
    Dim tabIX1 As Integer
    
    
    Dim temText As String
    
     tabLabName = 27
     tabLabDescreption = 18
     tabLabAddress = 18
     tabNo = 5
     tabPatient = 10
     tabIX = 60
     tabBilled = 110
     tabPrinted = 85
     tabIssued = 105
     tabAge = 47
     tabDept = 5
     tabDate = 5
     tabPrice = 96
     tabAge1 = 44
     tabIX1 = 56
    
    csetPrinter.SetPrinterAsDefault (cmbPrinter.Text)
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = cmbPrinter.Text Then
            Set MyPrinter = Printer
        End If
    Next
    If SelectForm(cmbPaper.Text, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    
'    On Error Resume Next
'
'    If optLandscape.Value = True Then
'        Printer.Orientation = vbPRORLandscape
'    End If
    Printer.Font.Name = "Verdana"
    Printer.Font.Size = 8
    'Printer.Print Tab(tabNo); "Software by Lankan Medical Programmers (LakMediPro) - 0715812399"
    Printer.Font.Name = "Verdana"
    Printer.Font.Size = 14
    Printer.FontBold = True
    
    
    Printer.Print
    Printer.Print Tab(tabLabName); HospitalName
    Printer.Font.Name = "Verdana"
    Printer.Font.Size = 12
    Printer.Print Tab(tabLabDescreption); HospitalDescreption
    Printer.Print Tab(tabLabAddress); HospitalAddress
    Printer.Print
    If IsNumeric(cmbDept.BoundText) = True Then
        Printer.Print Tab(tabDept); "Department : " & cmbDept.Text
    Else
        Printer.Print Tab(tabDept); "All Departments"
    End If
    If dtpFrom.Value = dtpTo.Value Then
        Printer.Print Tab(tabDate); "On " & Format(dtpTo.Value, "dd MMMM yyyy")
    Else
        Printer.Print Tab(tabDate); "From " & Format(dtpFrom.Value, "dd MMMM yyyy") & " To " & Format(dtpTo.Value, "dd MMMM yyyy")
    End If
    Printer.Print
    Printer.Font.Name = "Verdana"
    Printer.Font.Size = 10
'    Printer.FontBold = False
    
    
    With gridIx
    Dim PrintPatient As New clsPatient
    
    Printer.Print Tab(tabNo); "No";
    Printer.Print Tab(tabPatient); "Patient";
    Printer.Print Tab(tabAge1); "Age";
    Printer.Print Tab(tabIX1); "Investigation";
    Printer.Print Tab(tabPrice); "Price";
    Printer.Print
    Printer.Print
    
    Printer.Font.Name = "Verdana"
    Printer.Font.Size = 10
    Printer.FontBold = False
    
        For i = 1 To .Rows - 1
            PrintPatient.ID = Val(gridIx.TextMatrix(i, 8))
            Printer.Print Tab(tabNo); .TextMatrix(i, 0);

            If temText <> .TextMatrix(i, 1) Then
                temText = .TextMatrix(i, 1)
                Printer.Print Tab(tabPatient); PrintPatient.NameWithTitle;
                Printer.Print Tab(tabAge); PrintPatient.AgeInWords;
            End If

            Printer.Print Tab(tabIX); .TextMatrix(i, 2);
            If UserPositionID = 9 Then
                Printer.Print Tab(tabBilled - Len(.TextMatrix(i, 7))); .TextMatrix(i, 7);
            End If
            
'            Printer.Print Tab(tabPrinted); .TextMatrix(i, 4);
'            Printer.Print Tab(tabIssued); .TextMatrix(i, 5);
            Printer.Print
        Next
        
    End With
    
    
    
    Dim temDiscount As Double
    Dim ToOut As Double
    
    temDiscount = Val(lblTotal.Caption) / 10
    ToOut = Val(lblTotal.Caption) - temDiscount
    
    Dim temDis As String
    Dim temOut As String
    
    temDis = Format(temDiscount, "0.00")
    temOut = Format(ToOut, "0.00")
    
    Printer.Print
    
    If UserPositionID <> 9 Then
        Printer.Print Tab(tabNo); "Total Amount"; Tab(tabBilled - Len(lblTotal.Caption)); lblTotal.Caption
        Printer.Print Tab(tabNo); "Discount"; Tab(tabBilled - Len(temDis)); temDis
        Printer.Print Tab(tabNo); "Net Total"; Tab(tabBilled - Len(temOut)); temOut
    End If
    
    Printer.EndDoc
    
End Sub

Private Sub cmbPrinter_Change()
    cmbPrinter_Click
End Sub

Private Sub cmbPrinter_Click()
    cmbPaper.Clear
    csetPrinter.SetPrinterAsDefault (cmbPrinter.Text)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        With FormSize
            .cx = BillPaperHeight
            .cy = BillPaperWidth
        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                cmbPaper.AddItem PtrCtoVbString(.pName)
            End With
        Next i
        ClosePrinter (PrinterHandle)
    End If
End Sub


Private Sub chkPrinted_Click()
    Call Process
End Sub

Private Sub chkToHadnover_Click()
    Call Process
End Sub

Private Sub chkToPrint_Click()
    Call Process
End Sub

Private Sub cmbDept_Change()
    Call Process
End Sub

Private Sub cmbDept_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbDept.Text = Empty
    End If
End Sub

Private Sub dtpFrom_Change()
    Call Process
End Sub

Private Sub dtpTo_Change()
    Call Process
End Sub

Private Sub FillPrinters()
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        cmbPrinter.AddItem MyPrinter.DeviceName
    Next
End Sub


Private Sub Form_Load()
    
'    If UserPositionID = 9 Then
'        Label4.Visible = True
'        lblTotal.Visible = True
'    Else
'        Label4.Visible = False
'        lblTotal.Visible = False
'    End If
    
    Call FillCombos
    Call FormatGrid
    Call FillPrinters
    dtpFrom.Value = Date
    dtpTo.Value = Date
    Call FillGrid
    On Error Resume Next
    cmbPrinter.Text = GetSetting(App.EXEName, Me.Name, "Printer", "")
    cmbPrinter_Click
    cmbPaper.Text = GetSetting(App.EXEName, Me.Name, "Paper", "")
End Sub


Private Sub FormatGrid()
    With gridIx
        .Clear
        .Rows = 1
        .Cols = 9
        For i = 0 To .Cols - 1
            Select Case i
                Case 0:
                    .Col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 600
                    .Text = "No."
                Case 1:
                    .Col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 3000
                    .Text = "Patient"
                Case 2:
                    .Col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 3000
                    .Text = "Investigation"
                Case 3:
                    .Col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 2600
                    .Text = "Value"
                Case Else:
                    .ColWidth(i) = 0
            End Select
        Next
    End With
End Sub

Private Sub FillGrid()
    Dim TotalValue As Double
    
    With rsIxList
    
        If .State = 1 Then .Close
        temSql = "SELECT tblPatient.Name, tblIx.Ix, tblIxStaff.Value "
        temSql = temSql & "FROM tblIxStaff RIGHT JOIN (((tblPatient RIGHT JOIN tblPatientIx ON tblPatient.PatientID = tblPatientIx.PatientID) LEFT JOIN tblTitle ON tblPatient.TitleID = tblTitle.TitleID) LEFT JOIN tblIx ON tblPatientIx.IxID = tblIx.IxID) ON tblIxStaff.IxID = tblIx.IxID "
        temSql = temSql & "WHERE tblPatientIx.Deleted = False AND "
        temSql = temSql & " tblPatientIx.BilledDate Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# And #" & Format(dtpTo.Value, "dd MMMM yyyy") & "# "
        If IsNumeric(cmbDept.BoundText) = True Then
            temSql = temSql & " AND tblIxStaff.StaffID = " & Val(cmbDept.BoundText) & " "
        Else
            temSql = temSql & " "
        End If
        
        If optByPatient.Value = True Then
            temSql = temSql & "ORDER BY tblPatient.Name"
        ElseIf optByIx.Value = True Then
            temSql = temSql & "ORDER BY tblIx.Ix"
        ElseIf optByID.Value = True Then
            temSql = temSql & "ORDER BY tblPatientIx.PatientIxID"
        End If
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            .MoveLast
            .MoveFirst
            gridIx.Rows = .RecordCount + 1
            For i = 1 To .RecordCount
                gridIx.TextMatrix(i, 0) = i
                gridIx.TextMatrix(i, 1) = !Name
                gridIx.TextMatrix(i, 2) = !Ix
                If IsNull(![Value]) = False Then
                    gridIx.TextMatrix(i, 3) = Format(!Value, "0.00")
                    TotalValue = TotalValue + !Value
                End If
                .MoveNext
            Next
        End If
        .Close
        
    End With
    
    lblTotal.Caption = Format(TotalValue, "0.00")
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSetting App.EXEName, Me.Name, "Printer", cmbPrinter.Text
    SaveSetting App.EXEName, Me.Name, "Paper", cmbPaper.Text
End Sub

Private Sub optByID_Click()
    Call Process

End Sub

Private Sub optByIx_Click()
    Call Process

End Sub

Private Sub optByPatient_Click()
    Call Process

End Sub
