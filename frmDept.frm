VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmDept 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Department"
   ClientHeight    =   4665
   ClientLeft      =   2130
   ClientTop       =   1635
   ClientWidth     =   10920
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4665
   ScaleWidth      =   10920
   Begin VB.Frame Frame2 
      Height          =   3975
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   3615
      Begin MSDataListLib.DataCombo dtcDept 
         Height          =   2820
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   4974
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   1
         Text            =   ""
      End
      Begin btButtonEx.ButtonEx bttnAdd 
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnEdit 
         Height          =   375
         Left            =   1320
         TabIndex        =   5
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Edit"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnDelete 
         Height          =   375
         Left            =   2520
         TabIndex        =   13
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Delete"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3975
      Left            =   3960
      TabIndex        =   9
      Top             =   120
      Width           =   6855
      Begin VB.TextBox txtReagentFee 
         Height          =   360
         Left            =   1920
         TabIndex        =   18
         Top             =   1920
         Width           =   4455
      End
      Begin VB.TextBox txtMLTFee 
         Height          =   360
         Left            =   1920
         TabIndex        =   16
         Top             =   1440
         Width           =   4455
      End
      Begin VB.TextBox txtHospitalFee 
         Height          =   360
         Left            =   1920
         TabIndex        =   14
         Top             =   960
         Width           =   4455
      End
      Begin VB.TextBox txtName 
         Height          =   360
         Left            =   1920
         TabIndex        =   0
         Top             =   480
         Width           =   4455
      End
      Begin VB.TextBox txtcomment 
         Height          =   735
         Left            =   1920
         MultiLine       =   -1  'True
         TabIndex        =   1
         Top             =   2400
         Width           =   4455
      End
      Begin btButtonEx.ButtonEx bttnSave 
         Height          =   375
         Left            =   2880
         TabIndex        =   2
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnCancel 
         Height          =   375
         Left            =   4440
         TabIndex        =   7
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Cancel"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnChange 
         Height          =   375
         Left            =   2880
         TabIndex        =   6
         Top             =   3360
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label5 
         Caption         =   "&Reagent Fee %"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1920
         Width           =   2055
      End
      Begin VB.Label Label4 
         Caption         =   "&MLT Fee %"
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   1440
         Width           =   2055
      End
      Begin VB.Label Label3 
         Caption         =   "&Hospital Fee %"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   960
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "&Department"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   480
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "C&omments"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   2400
         Width           =   2055
      End
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   9840
      TabIndex        =   8
      Top             =   4200
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmDept"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsDept As New ADODB.Recordset
    Dim rsViewDept As New ADODB.Recordset
    Dim temSQL As String

Private Sub bttnCancel_Click()
    Call BeforeAddEdit
    Call Clearvalues
    dtcDept.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnDelete_Click()
    Dim i As Integer
    i = MsgBox("Are you sure you want to delete", vbYesNo)
    If i = vbNo Then Exit Sub
    With rsDept
        If .State = 1 Then .Close
        temSQL = "Select * from tblDept where DeptID = " & Val(dtcDept.BoundText)
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Deleted = True
            !DeletedDate = Date
            !DeletedTime = time
            !DeletedUserID = UserID
            .Update
        End If
        If .State = 1 Then .Close
    End With
    Call FillCombos
    dtcDept.Text = Empty
End Sub

Private Sub dtcDept_Change()
    If IsNumeric(dtcDept.BoundText) = True Then
        Call DisplaySelected
        bttnEdit.Enabled = True
        bttnAdd.Enabled = False
        bttnDelete.Enabled = True
    Else
        Clearvalues
        bttnAdd.Enabled = True
        bttnEdit.Enabled = False
        bttnDelete.Enabled = False
    End If
End Sub

Private Sub Form_Load()
    FillCombos
    BeforeAddEdit
    Clearvalues
End Sub

Private Sub bttnAdd_Click()
    Clearvalues
    AfterAdd
    txtName.SetFocus
    txtName.Text = dtcDept.Text
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnEdit_Click()
    AfterEdit
    txtName.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnChange_Click()
    Dim TemResponce As Integer
    If txtName.Text = "" Then NoName: Exit Sub
    With rsDept
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select * From tblDept Where DeptID = " & Val(dtcDept.BoundText), cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount = 0 Then Exit Sub
        !Dept = Trim(txtName.Text)
        !HospitalFee = Val(txtHospitalFee.Text)
        !MLTFee = Val(txtMLTFee.Text)
        !ReagentFee = Val(txtReagentFee.Text)
        !Comments = txtcomment.Text
        .Update
        If .State = 1 Then .Close
        FillCombos
        BeforeAddEdit
        Clearvalues
        dtcDept.Text = Empty
        dtcDept.SetFocus
        Exit Sub
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        Clearvalues
        BeforeAddEdit
        dtcDept.Text = Empty
        dtcDept.SetFocus
        If .State = 1 Then .Close
    End With
End Sub

Private Sub bttnSave_Click()
    Dim TemResponce As Integer
    If Trim(txtName.Text) = "" Then NoName: Exit Sub
    With rsDept
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select * From tblDept", cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !Dept = Trim(txtName.Text)
        !Comments = txtcomment.Text
        !HospitalFee = Val(txtHospitalFee.Text)
        !MLTFee = Val(txtMLTFee.Text)
        !ReagentFee = Val(txtReagentFee.Text)
        .Update
        If .State = 1 Then .Close
        FillCombos
        BeforeAddEdit
        Clearvalues
        dtcDept.Text = Empty
        dtcDept.SetFocus
        Exit Sub
    
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        Clearvalues
        BeforeAddEdit
        dtcDept.Text = Empty
        dtcDept.SetFocus
        If .State = 1 Then .Close
    End With
    
End Sub

Private Sub NoName()
    Dim TemResponce As Integer
    TemResponce = MsgBox("You have not entered an Dept to save", vbCritical, "No Name")
    txtName.SetFocus
End Sub

Private Sub FillCombos()
    With rsViewDept
        If .State = 1 Then .Close
        temSQL = "SELECT tblDept.*  FROM tblDept WHERE Deleted = False Order by Dept "
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcDept
        Set .RowSource = rsViewDept
        .ListField = "Dept"
        .BoundColumn = "DeptID"
    End With
End Sub

Private Sub AfterAdd()
    
    bttnAdd.Enabled = False
    bttnEdit.Enabled = False
    dtcDept.Enabled = False
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = True
    bttnChange.Enabled = False
    bttnCancel.Enabled = True
    txtcomment.Enabled = True
    txtName.Enabled = True
    txtHospitalFee.Enabled = True
    txtMLTFee.Enabled = True
    txtReagentFee.Enabled = True
    
    bttnSave.Visible = True
    bttnChange.Visible = False
    
End Sub

Private Sub AfterEdit()
    
    bttnAdd.Enabled = False
    bttnEdit.Enabled = False
    dtcDept.Enabled = False
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = False
    bttnChange.Enabled = True
    bttnCancel.Enabled = True
    txtcomment.Enabled = True
    txtName.Enabled = True
    txtHospitalFee.Enabled = True
    txtMLTFee.Enabled = True
    txtReagentFee.Enabled = True
    
    bttnSave.Visible = False
    bttnChange.Visible = True
    
End Sub

Private Sub BeforeAddEdit()
    
    bttnAdd.Enabled = True
    bttnEdit.Enabled = False
    dtcDept.Enabled = True
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = False
    bttnChange.Enabled = False
    bttnCancel.Enabled = False
    txtcomment.Enabled = False
    txtName.Enabled = False
    txtHospitalFee.Enabled = False
    txtMLTFee.Enabled = False
    txtReagentFee.Enabled = False
    
    bttnSave.Visible = True
    bttnChange.Visible = True
    
    On Error Resume Next
    dtcDept.SetFocus
    
End Sub

Private Sub Clearvalues()
    txtName.Text = Empty
    txtHospitalFee.Text = Empty
    txtMLTFee.Text = Empty
    txtReagentFee.Text = Empty
    txtcomment.Text = Empty
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If rsDept.State = 1 Then rsDept.Close: Set rsDept = Nothing
    If rsViewDept.State = 1 Then rsViewDept.Close: Set rsViewDept = Nothing
End Sub

Private Sub DisplaySelected()
    If Not IsNumeric(dtcDept.BoundText) Then Exit Sub
    With rsDept
        If .State = 1 Then .Close
        .Open "Select * From tblDept Where (DeptID = " & dtcDept.BoundText & ")", cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Call Clearvalues
        txtName.Text = !Dept
        If Not IsNull(!Comments) Then txtcomment.Text = !Comments
        If Not IsNull(!HospitalFee) Then txtHospitalFee.Text = Format(!HospitalFee, "0.00")
        If Not IsNull(!MLTFee) Then txtMLTFee.Text = Format(!MLTFee, "0.00")
        If Not IsNull(!ReagentFee) Then txtReagentFee.Text = Format(!ReagentFee, "0.00")
        If .State = 1 Then .Close
    End With
End Sub

