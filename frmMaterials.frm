VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmMaterials 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Materials"
   ClientHeight    =   5145
   ClientLeft      =   1860
   ClientTop       =   1380
   ClientWidth     =   10665
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   Moveable        =   0   'False
   ScaleHeight     =   5145
   ScaleWidth      =   10665
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   120
      TabIndex        =   14
      Top             =   4320
      Width           =   3735
      Begin btButtonEx.ButtonEx bttnAdd 
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnEdit 
         Height          =   375
         Left            =   1440
         TabIndex        =   2
         Top             =   240
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Edit"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnDelete 
         Height          =   375
         Left            =   2760
         TabIndex        =   21
         Top             =   240
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Delete"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame3 
      Height          =   735
      Left            =   3960
      TabIndex        =   13
      Top             =   3600
      Width           =   6615
      Begin btButtonEx.ButtonEx bttnSave 
         Height          =   375
         Left            =   360
         TabIndex        =   5
         Top             =   240
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnCancel 
         Height          =   375
         Left            =   4680
         TabIndex        =   7
         Top             =   240
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Cancel"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnChange 
         Height          =   375
         Left            =   360
         TabIndex        =   6
         Top             =   240
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame2 
      Height          =   4215
      Left            =   120
      TabIndex        =   12
      Top             =   120
      Width           =   3735
      Begin MSDataListLib.DataCombo dtcMaterial 
         Height          =   3780
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   6668
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   1
         Text            =   ""
      End
   End
   Begin VB.Frame Frame1 
      Height          =   3375
      Left            =   3960
      TabIndex        =   9
      Top             =   120
      Width           =   6615
      Begin MSDataListLib.DataCombo dtcUnit 
         Height          =   360
         Left            =   1800
         TabIndex        =   20
         Top             =   1800
         Width           =   4575
         _ExtentX        =   8070
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin VB.TextBox txtValue 
         Height          =   360
         Left            =   1800
         TabIndex        =   17
         Top             =   1320
         Width           =   4575
      End
      Begin VB.TextBox txtCost 
         Height          =   360
         Left            =   1800
         TabIndex        =   15
         Top             =   840
         Width           =   4575
      End
      Begin VB.TextBox txtComments 
         Height          =   855
         Left            =   1800
         TabIndex        =   4
         Top             =   2280
         Width           =   4575
      End
      Begin VB.TextBox txtMaterial 
         Height          =   360
         Left            =   1800
         TabIndex        =   3
         Top             =   360
         Width           =   4575
      End
      Begin VB.Label Label5 
         Caption         =   "&Unit"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   1800
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "&Value"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   1320
         Width           =   1575
      End
      Begin VB.Label Label2 
         Caption         =   "&Cost"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   840
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "&Meterial"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "C&omments"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   2280
         Width           =   1575
      End
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   8640
      TabIndex        =   8
      Top             =   4560
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmMaterials"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsMaterial As New ADODB.Recordset
    Dim rsViewMaterial As New ADODB.Recordset
    Dim TemMaterialID As Long
    Dim rsUnits As New ADODB.Recordset
    Dim rsViewUnits As New ADODB.Recordset
    Dim TemUnitID As Long
    Dim temSql As String
    

Private Sub bttnCancel_Click()
    Call BeforeAddEdit
    Call Clearvalues
End Sub

Private Sub dtcMaterialName_Click(Area As Integer)
    If IsNumeric(dtcMaterial.BoundText) = False Then Exit Sub
    Call DisplaySelected
End Sub

Private Sub bttnDelete_Click()
    With rsMaterial
        If .State = 1 Then .Close
        temSql = "Select * from tblMaterial where MaterialID = " & Val(dtcMaterial.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Deleted = True
            !DeletedDate = Date
            !DeletedTime = Time
            !DeletedUserID = UserID
            .Update
        End If
        If .State = 1 Then .Close
    End With
    Call FillMaterialName
    dtcMaterial.Text = Empty
End Sub

Private Sub dtcMaterial_Change()
    If IsNumeric(dtcMaterial.BoundText) = True Then
        Call DisplaySelected
        bttnEdit.Enabled = True
        bttnAdd.Enabled = False
        bttnDelete.Enabled = True
    Else
        Clearvalues
        bttnAdd.Enabled = True
        bttnEdit.Enabled = False
        bttnDelete.Enabled = False
    End If
End Sub

Private Sub dtcMaterial_Click(Area As Integer)
    Call DisplaySelected
End Sub

Private Sub Form_Load()
    Call FillMaterialName
    Call FillUnits
    Call BeforeAddEdit
    Call Clearvalues
End Sub

Private Sub FillMaterialName()
    With rsViewMaterial
        If .State = 1 Then .Close
        .Open "Select* From tblMaterial  WHERE Deleted = False Order By Material", cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            Set dtcMaterial.RowSource = rsViewMaterial
            dtcMaterial.BoundColumn = "MaterialID"
            dtcMaterial.ListField = "Material"
        End If
    End With
End Sub
Private Sub FillUnits()
    With rsViewUnits
        If .State = 1 Then .Close
        .Open "Select* From tblUnit Order By Unit", cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            Set dtcUnit.RowSource = rsViewUnits
            dtcUnit.BoundColumn = "UnitID"
            dtcUnit.ListField = "Unit"
        End If
    End With
End Sub


Private Sub bttnAdd_Click()
    Call Clearvalues
    Call AfterAdd
    txtMaterial.Text = dtcMaterial.Text
    txtMaterial.SetFocus
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnEdit_Click()
    Call AfterEdit
    txtMaterial.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnChange_Click()
    Dim TemResponce As Integer
    If Trim(txtMaterial.Text) = "" Then NoName: Exit Sub
    With rsMaterial
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select tblMaterial.* From tblMaterial Where (MaterialID = " & TemMaterialID & ")", cnnLab, 3, 3
        If .RecordCount > 0 Then
            !Material = txtMaterial.Text
            !UnitCost = txtCost.Text
            !UnitValue = txtValue.Text
            !UnitID = Val(dtcUnit.BoundText)
            !Comments = txtComments.Text
            .Update
        End If
        FillMaterialName
        BeforeAddEdit
        Clearvalues
        dtcMaterial.Text = Empty
        dtcMaterial.SetFocus
        If .State = 1 Then .Close
        Exit Sub
    
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Update Error")
        If .State = 1 Then .CancelUpdate
        Clearvalues
        BeforeAddEdit
        If .State = 1 Then .Close
    End With
End Sub

Private Sub bttnSave_Click()
    Dim TemResponce As Integer
    If Trim(txtMaterial.Text) = "" Then NoName: Exit Sub
    With rsMaterial
   ' On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select tblMaterial.* From tblMaterial", cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !Material = txtMaterial.Text
        !UnitCost = Val(txtCost.Text)
        !UnitValue = Val(txtValue.Text)
        !UnitID = Val(dtcUnit.Text)
        !Comments = txtComments.Text
        .Update
        FillMaterialName
        BeforeAddEdit
        Clearvalues
        If .State = 1 Then .Close
        dtcMaterial.Text = Empty
        dtcMaterial.SetFocus
        Exit Sub
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        Clearvalues
        BeforeAddEdit
        If .State = 1 Then .Close
    End With
End Sub

Private Sub NoName()
    Dim TemResponce As Integer
    TemResponce = MsgBox("You must enter a Material name to save", vbCritical, "No Name")
    txtMaterial.SetFocus
End Sub

Private Sub AfterAdd()
    bttnSave.Visible = True
    bttnChange.Visible = False
    bttnCancel.Visible = True
    bttnAdd.Enabled = False
    bttnDelete.Enabled = False
    bttnEdit.Enabled = False
    bttnSave.Enabled = True
    bttnCancel.Enabled = True
    bttnChange.Enabled = False
    Frame1.Enabled = True
    Frame2.Enabled = False
End Sub

Private Sub AfterEdit()
    bttnSave.Visible = False
    bttnChange.Visible = True
    bttnCancel.Visible = True
    bttnDelete.Enabled = False
    bttnAdd.Enabled = False
    bttnEdit.Enabled = False
    bttnSave.Enabled = False
    bttnCancel.Enabled = True
    bttnChange.Enabled = True
    Frame1.Enabled = True
    Frame2.Enabled = False
End Sub



Private Sub BeforeAddEdit()
    bttnAdd.Visible = True
    bttnEdit.Visible = True
    bttnSave.Visible = False
    bttnDelete.Enabled = False
    bttnCancel.Visible = False
    bttnChange.Visible = False
    bttnAdd.Enabled = True
    bttnEdit.Enabled = True
    bttnSave.Enabled = False
    bttnCancel.Enabled = False
    Frame1.Enabled = False
    Frame2.Enabled = True
End Sub

Private Function Clearvalues()
    txtMaterial.Text = Empty
    txtComments.Text = Empty
End Function

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If rsMaterial.State = 1 Then rsMaterial.Close: Set rsMaterial = Nothing
    If rsViewMaterial.State = 1 Then rsViewMaterial.Close: Set rsViewMaterial = Nothing
End Sub

Private Sub DisplaySelected()
    If Not IsNumeric(dtcMaterial.BoundText) Then Exit Sub
    With rsMaterial
        If .State = 1 Then .Close
        .Open "Select tblMaterial.* From tblMaterial Where (MaterialID = " & dtcMaterial.BoundText & ")", cnnLab, adOpenStatic, adLockReadOnly
        Call Clearvalues
        If .RecordCount > 0 Then
            If Not (!Material) = "" Then txtMaterial.Text = !Material
            If Not (!UnitCost) = "" Then txtCost.Text = !UnitCost
            If Not (!UnitValue) = "" Then txtValue.Text = !UnitValue
            If Not (!UnitID) = "" Then dtcUnit.BoundText = !UnitID
            If Not (!Comments) = "" Then txtComments.Text = !Comments
            TemMaterialID = !MaterialID
        Else
            txtMaterial.Text = Empty
            txtCost.Text = Empty
            txtValue.Text = Empty
            dtcUnit.Text = Empty
            txtComments.Text = Empty
            TemMaterialID = 0
        End If
        If .State = 1 Then .Close
    End With
End Sub
