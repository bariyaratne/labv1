VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmFDayEndSummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Day End Summery"
   ClientHeight    =   6120
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5985
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6120
   ScaleWidth      =   5985
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   4800
      TabIndex        =   7
      Top             =   5640
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnPrint 
      Height          =   375
      Left            =   3600
      TabIndex        =   6
      Top             =   5640
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame3 
      Height          =   735
      Left            =   120
      TabIndex        =   11
      Top             =   4800
      Width           =   5775
      Begin VB.TextBox txtTIncome 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   375
         Left            =   1920
         TabIndex        =   5
         Top             =   240
         Width           =   3735
      End
      Begin VB.Label Label3 
         Caption         =   "Total  &Income"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame2 
      Height          =   3615
      Left            =   120
      TabIndex        =   10
      Top             =   1200
      Width           =   5775
      Begin MSFlexGridLib.MSFlexGrid GridBill 
         Height          =   3255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   5535
         _ExtentX        =   9763
         _ExtentY        =   5741
         _Version        =   393216
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   120
      TabIndex        =   8
      Top             =   0
      Width           =   5775
      Begin MSComCtl2.DTPicker dtpFrom 
         Height          =   375
         Left            =   1920
         TabIndex        =   12
         Top             =   240
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd MMMM yyyy"
         Format          =   20905987
         CurrentDate     =   39773
      End
      Begin MSDataListLib.DataCombo dtcUser 
         Height          =   360
         Left            =   1920
         TabIndex        =   2
         Top             =   1200
         Visible         =   0   'False
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   635
         _Version        =   393216
         Enabled         =   0   'False
         Text            =   ""
      End
      Begin MSComCtl2.DTPicker dtpTo 
         Height          =   375
         Left            =   1920
         TabIndex        =   13
         Top             =   720
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd MMMM yyyy"
         Format          =   20905987
         CurrentDate     =   39773
      End
      Begin VB.Label Label2 
         Caption         =   "&User"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   1200
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "&From"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "&To"
         Height          =   255
         Left            =   240
         TabIndex        =   0
         Top             =   720
         Width           =   1575
      End
   End
End
Attribute VB_Name = "frmFDayEndSummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Dim rsBill As New ADODB.Recordset
    Dim csetPrinter As New cSetDfltPrinter
        
Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnPrint_Click()
    Call eLabPrint
End Sub

Private Sub dtpFrom_Change()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub Form_Load()
    dtpFrom.Value = Date
    dtpTo.Value = Date
    Call FormatGrid
    Call FillGrid
    
End Sub

Private Sub FormatGrid()

    '   0   BillID
    '   1   Date
    '   2   Time
    '   3   Value
    With GridBill
        .Rows = 1
        .Cols = 4
        .Row = 0
        .Col = 0
        .CellAlignment = 4
        .Text = "Bill ID."
        .Col = 1
        .CellAlignment = 4
        .Text = "Date"
        .Col = 2
        .CellAlignment = 4
        .Text = "Time"
        .Col = 3
        .Text = "Value"
'        .Col = 4
'        .Text = "IxID"
        .ColWidth(0) = 700
        .ColWidth(1) = 1700
        .ColWidth(2) = 1700
        .ColWidth(3) = 1050
'        .ColWidth(1) = .Width - (.ColWidth(0) + .ColWidth(2) + 100)
    End With
End Sub

Private Sub FillGrid()
    Dim TotalValue As Double
    With rsBill
        If .State = 1 Then .Close
        temSql = "SELECT * FROM tblPatientIxBill "
        temSql = temSql & " Where Date Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# And #" & Format(dtpTo.Value, "dd MMMM yyyy") & "# AND PatientIxBillID MOD 3 = 1  "
        If IsNumeric(dtcUser.BoundText) = True Then
            temSql = temSql & " And UserID = " & Val(dtcUser.BoundText)
        End If
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                GridBill.Rows = GridBill.Rows + 1
                GridBill.Row = GridBill.Rows - 1
                GridBill.Col = 0
                GridBill.Text = GridBill.Row
                GridBill.Col = 1
                GridBill.Text = !Date
                GridBill.Col = 2
                GridBill.Text = !Time
                GridBill.Col = 3
                GridBill.Text = !NetValue
                TotalValue = TotalValue + !NetValue
                .MoveNext
            Wend
        End If
        txtTIncome.Text = Format(TotalValue, "0.00")
    End With
    With GridBill
        If .Rows > 8 Then
            .TopRow = .Rows - 8
        End If
    End With
End Sub

Private Sub GridBill_Click()
'    With GridBill
'        .Col = 0
'        If IsNumeric(.TextMatrix(.Row, 0)) = True Then
'            TxPatientIxBillID = Val(.TextMatrix(.Row, 0))
'            frmViewIx.Show
'        End If
'    End With
End Sub

Private Sub eLabPrint()
    csetPrinter.SetPrinterAsDefault BillPrinterName
    If SelectForm(BillPaperName, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    
    Tab1 = 1
    Tab2 = 4
    Tab3 = 20
    Tab4 = 30
    
    With Printer
        .FontSize = 12
        .Font = "Times New Roman"
        Printer.Print
        Printer.Print Tab(Tab1); HospitalName
        .FontSize = 9
        .Font = "Times New Roman"
        Printer.Print Tab(Tab1); HospitalDescreption
        Printer.Print Tab(Tab1); HospitalAddress
        Printer.Print
        Printer.Print Tab(Tab1); "Date : "; Format(Date, "dd MM yy"); Tab(Tab1 + 25); "Time : "; Time;
        Printer.Print Tab(Tab1); "User Name : "; UserName; 'Tab(Tab1 + 25); "Bill No." & TxPatientIxBillID
        Printer.Print Tab(Tab1); "--------------------------------------"
        .FontSize = 10
        .Font = "Lucida Console"
    End With
    
    Tab1 = 1
    Tab2 = 4
    Tab3 = 26
    Tab4 = 30
    
    With GridBill
        For i = 1 To .Rows - 1
            Printer.Print Tab(Tab1); .TextMatrix(i, 0); Tab(Tab2); Left(.TextMatrix(i, 1), 22); Tab(Tab3); Right((Space(10)) & .TextMatrix(i, 2), 10)
        Next i
    End With
    Tab1 = 1
    Tab2 = 4
    Tab3 = 42
    Tab4 = 40
    With Printer
        .Font = 12
        Printer.Print Tab(Tab1); "--------------------------------------"
        Printer.Print Tab(Tab1); "Total Income"; Tab(Tab4); Right((Space(10)) & (txtTIncome.Text), 10)
'        If Val(txtDiscount.Text) > 0 Then
'            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (txtDiscount.Text), 10)
'            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab4); Right((Space(10)) & (txtNTotal.Text), 10)
'        End If
        Printer.Print Tab(Tab1); "--------------------------------------"
        Printer.Print Tab(Tab1); "THANK YOU"
        Printer.Print Tab(Tab1); "--------------------------------------"
        .EndDoc
    End With
End Sub

