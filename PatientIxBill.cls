VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientIxBill"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varPatientIxBillID As Long
    Private varPatientID As Long
    Private varDate As Date
    Private varTime As Date
    Private varUserID As Date
    Private varDoctorID As Long
    Private varInstitutionID As Long
    Private varStaffCharge As Double
    Private varHospitalCharge As Double
    Private varOtherCharge As Double
    Private varCharge As Double
    Private varValue As Double
    Private varDiscount As Double
    Private varNetValue As Double
    Private varMaterialCost As Double
    Private varExpenceCost As Double
    Private varOtherCost As Double
    Private varCost As Double
    Private varCancelled As Boolean
    Private varCancelledDate As Date
    Private varCancelledTime As Date
    Private varCancelledUserID As Date
    Private varDeleted As Boolean
    Private varDeletedUserID As Long
    Private varDeletedDate As Date
    Private varDeletedTime As Date
    Private varCompanyID As Long
    Private varBHTID As Long
    Private varRoomID As Long
    Private varBillType As String
    Private varCardNo As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientIxBill Where PatientIxBillID = " & varPatientIxBillID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !PatientID = varPatientID
        !Date = varDate
        !Time = varTime
        !UserID = varUserID
        !DoctorID = varDoctorID
        !InstitutionID = varInstitutionID
        !StaffCharge = varStaffCharge
        !HospitalCharge = varHospitalCharge
        !OtherCharge = varOtherCharge
        !Charge = varCharge
        !Value = varValue
        !Discount = varDiscount
        !NetValue = varNetValue
        !MaterialCost = varMaterialCost
        !ExpenceCost = varExpenceCost
        !OtherCost = varOtherCost
        !Cost = varCost
        !Cancelled = varCancelled
        !CancelledDate = varCancelledDate
        !CancelledTime = varCancelledTime
        !CancelledUserID = varCancelledUserID
        !Deleted = varDeleted
        !DeletedUserID = varDeletedUserID
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !CompanyID = varCompanyID
        !BHTID = varBHTID
        !RoomID = varRoomID
        !BillType = varBillType
        !CardNo = varCardNo
        .Update
        varPatientIxBillID = !PatientIxBillID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientIxBill WHERE PatientIxBillID = " & varPatientIxBillID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!PatientIxBillID) Then
               varPatientIxBillID = !PatientIxBillID
            End If
            If Not IsNull(!PatientID) Then
               varPatientID = !PatientID
            End If
            If Not IsNull(!Date) Then
               varDate = !Date
            End If
            If Not IsNull(!Time) Then
               varTime = !Time
            End If
            If Not IsNull(!UserID) Then
               varUserID = !UserID
            End If
            If Not IsNull(!DoctorID) Then
               varDoctorID = !DoctorID
            End If
            If Not IsNull(!InstitutionID) Then
               varInstitutionID = !InstitutionID
            End If
            If Not IsNull(!StaffCharge) Then
               varStaffCharge = !StaffCharge
            End If
            If Not IsNull(!HospitalCharge) Then
               varHospitalCharge = !HospitalCharge
            End If
            If Not IsNull(!OtherCharge) Then
               varOtherCharge = !OtherCharge
            End If
            If Not IsNull(!Charge) Then
               varCharge = !Charge
            End If
            If Not IsNull(!Value) Then
               varValue = !Value
            End If
            If Not IsNull(!Discount) Then
               varDiscount = !Discount
            End If
            If Not IsNull(!NetValue) Then
               varNetValue = !NetValue
            End If
            If Not IsNull(!MaterialCost) Then
               varMaterialCost = !MaterialCost
            End If
            If Not IsNull(!ExpenceCost) Then
               varExpenceCost = !ExpenceCost
            End If
            If Not IsNull(!OtherCost) Then
               varOtherCost = !OtherCost
            End If
            If Not IsNull(!Cost) Then
               varCost = !Cost
            End If
            If Not IsNull(!Cancelled) Then
               varCancelled = !Cancelled
            End If
            If Not IsNull(!CancelledDate) Then
               varCancelledDate = !CancelledDate
            End If
            If Not IsNull(!CancelledTime) Then
               varCancelledTime = !CancelledTime
            End If
            If Not IsNull(!CancelledUserID) Then
               varCancelledUserID = !CancelledUserID
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!CompanyID) Then
               varCompanyID = !CompanyID
            End If
            If Not IsNull(!BHTID) Then
               varBHTID = !BHTID
            End If
            If Not IsNull(!RoomID) Then
               varRoomID = !RoomID
            End If
            If Not IsNull(!BillType) Then
               varBillType = !BillType
            End If
            If Not IsNull(!CardNo) Then
               varCardNo = !CardNo
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varPatientIxBillID = 0
    varPatientID = 0
    varDate = Empty
    varTime = Empty
    varUserID = Empty
    varDoctorID = 0
    varInstitutionID = 0
    varStaffCharge = 0
    varHospitalCharge = 0
    varOtherCharge = 0
    varCharge = 0
    varValue = 0
    varDiscount = 0
    varNetValue = 0
    varMaterialCost = 0
    varExpenceCost = 0
    varOtherCost = 0
    varCost = 0
    varCancelled = False
    varCancelledDate = Empty
    varCancelledTime = Empty
    varCancelledUserID = Empty
    varDeleted = False
    varDeletedUserID = 0
    varDeletedDate = Empty
    varDeletedTime = Empty
    varCompanyID = 0
    varBHTID = 0
    varRoomID = 0
    varBillType = Empty
    varCardNo = 0
End Sub

Public Property Let PatientIxBillID(ByVal vPatientIxBillID As Long)
    Call clearData
    varPatientIxBillID = vPatientIxBillID
    Call loadData
End Property

Public Property Get PatientIxBillID() As Long
    PatientIxBillID = varPatientIxBillID
End Property

Public Property Let PatientID(ByVal vPatientID As Long)
    varPatientID = vPatientID
End Property

Public Property Get PatientID() As Long
    PatientID = varPatientID
End Property

Public Property Let billDate(ByVal vDate As Date)
    varDate = vDate
End Property

Public Property Get billDate() As Date
    billDate = varDate
End Property

Public Property Let Time(ByVal vTime As Date)
    varTime = vTime
End Property

Public Property Get Time() As Date
    Time = varTime
End Property

Public Property Let UserID(ByVal vUserID As Date)
    varUserID = vUserID
End Property

Public Property Get UserID() As Date
    UserID = varUserID
End Property

Public Property Let DoctorID(ByVal vDoctorID As Long)
    varDoctorID = vDoctorID
End Property

Public Property Get DoctorID() As Long
    DoctorID = varDoctorID
End Property

Public Property Let InstitutionID(ByVal vInstitutionID As Long)
    varInstitutionID = vInstitutionID
End Property

Public Property Get InstitutionID() As Long
    InstitutionID = varInstitutionID
End Property

Public Property Let StaffCharge(ByVal vStaffCharge As Double)
    varStaffCharge = vStaffCharge
End Property

Public Property Get StaffCharge() As Double
    StaffCharge = varStaffCharge
End Property

Public Property Let HospitalCharge(ByVal vHospitalCharge As Double)
    varHospitalCharge = vHospitalCharge
End Property

Public Property Get HospitalCharge() As Double
    HospitalCharge = varHospitalCharge
End Property

Public Property Let OtherCharge(ByVal vOtherCharge As Double)
    varOtherCharge = vOtherCharge
End Property

Public Property Get OtherCharge() As Double
    OtherCharge = varOtherCharge
End Property

Public Property Let Charge(ByVal vCharge As Double)
    varCharge = vCharge
End Property

Public Property Get Charge() As Double
    Charge = varCharge
End Property

Public Property Let Value(ByVal vValue As Double)
    varValue = vValue
End Property

Public Property Get Value() As Double
    Value = varValue
End Property

Public Property Let Discount(ByVal vDiscount As Double)
    varDiscount = vDiscount
End Property

Public Property Get Discount() As Double
    Discount = varDiscount
End Property

Public Property Let NetValue(ByVal vNetValue As Double)
    varNetValue = vNetValue
End Property

Public Property Get NetValue() As Double
    NetValue = varNetValue
End Property

Public Property Let MaterialCost(ByVal vMaterialCost As Double)
    varMaterialCost = vMaterialCost
End Property

Public Property Get MaterialCost() As Double
    MaterialCost = varMaterialCost
End Property

Public Property Let ExpenceCost(ByVal vExpenceCost As Double)
    varExpenceCost = vExpenceCost
End Property

Public Property Get ExpenceCost() As Double
    ExpenceCost = varExpenceCost
End Property

Public Property Let OtherCost(ByVal vOtherCost As Double)
    varOtherCost = vOtherCost
End Property

Public Property Get OtherCost() As Double
    OtherCost = varOtherCost
End Property

Public Property Let Cost(ByVal vCost As Double)
    varCost = vCost
End Property

Public Property Get Cost() As Double
    Cost = varCost
End Property

Public Property Let Cancelled(ByVal vCancelled As Boolean)
    varCancelled = vCancelled
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = varCancelled
End Property

Public Property Let CancelledDate(ByVal vCancelledDate As Date)
    varCancelledDate = vCancelledDate
End Property

Public Property Get CancelledDate() As Date
    CancelledDate = varCancelledDate
End Property

Public Property Let CancelledTime(ByVal vCancelledTime As Date)
    varCancelledTime = vCancelledTime
End Property

Public Property Get CancelledTime() As Date
    CancelledTime = varCancelledTime
End Property

Public Property Let CancelledUserID(ByVal vCancelledUserID As Date)
    varCancelledUserID = vCancelledUserID
End Property

Public Property Get CancelledUserID() As Date
    CancelledUserID = varCancelledUserID
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let DeletedDate(ByVal vDeletedDate As Date)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate() As Date
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime As Date)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime() As Date
    DeletedTime = varDeletedTime
End Property

Public Property Let CompanyID(ByVal vCompanyID As Long)
    varCompanyID = vCompanyID
End Property

Public Property Get CompanyID() As Long
    CompanyID = varCompanyID
End Property

Public Property Let BHTID(ByVal vBHTID As Long)
    varBHTID = vBHTID
End Property

Public Property Get BHTID() As Long
    BHTID = varBHTID
End Property

Public Property Let RoomID(ByVal vRoomID As Long)
    varRoomID = vRoomID
End Property

Public Property Get RoomID() As Long
    RoomID = varRoomID
End Property

Public Property Let BillType(ByVal vBillType As String)
    varBillType = vBillType
End Property

Public Property Get BillType() As String
    BillType = varBillType
End Property

Public Property Let CardNo(ByVal vCardNo As Long)
    varCardNo = vCardNo
End Property

Public Property Get CardNo() As Long
    CardNo = varCardNo
End Property


