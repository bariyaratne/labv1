VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Begin VB.Form frmPatientIxNew 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Adding Investigations"
   ClientHeight    =   8385
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11865
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8385
   ScaleWidth      =   11865
   Begin MSDataListLib.DataCombo cmbName 
      Height          =   360
      Left            =   3240
      TabIndex        =   2
      Top             =   120
      Width           =   8295
      _ExtentX        =   14631
      _ExtentY        =   635
      _Version        =   393216
      Text            =   ""
   End
   Begin MSFlexGridLib.MSFlexGrid gridPt 
      Height          =   3615
      Left            =   8040
      TabIndex        =   37
      Top             =   960
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   6376
      _Version        =   393216
   End
   Begin VB.TextBox txtPhone 
      Height          =   360
      Left            =   3840
      TabIndex        =   6
      Top             =   600
      Width           =   3015
   End
   Begin TabDlg.SSTab SSTab1 
      Height          =   2775
      Left            =   8040
      TabIndex        =   21
      Top             =   4680
      Width           =   3645
      _ExtentX        =   6429
      _ExtentY        =   4895
      _Version        =   393216
      TabHeight       =   520
      TabCaption(0)   =   "Cash"
      TabPicture(0)   =   "frmPatientIxNew.frx":0000
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label5"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Label4"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Label2"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "txtNTotal"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).Control(5)=   "txtDiscount"
      Tab(0).Control(5).Enabled=   0   'False
      Tab(0).Control(6)=   "txtGTotal"
      Tab(0).Control(6).Enabled=   0   'False
      Tab(0).Control(7)=   "txtDiscountPercent"
      Tab(0).Control(7).Enabled=   0   'False
      Tab(0).ControlCount=   8
      TabCaption(1)   =   "Credit"
      TabPicture(1)   =   "frmPatientIxNew.frx":001C
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Label15"
      Tab(1).Control(1)=   "Label12"
      Tab(1).Control(2)=   "cmbCompany"
      Tab(1).Control(3)=   "txtCardNo"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Ward"
      TabPicture(2)   =   "frmPatientIxNew.frx":0038
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Label16"
      Tab(2).Control(1)=   "Label17"
      Tab(2).Control(2)=   "Label18"
      Tab(2).Control(3)=   "cmbRoom"
      Tab(2).Control(4)=   "cmbBHT"
      Tab(2).Control(5)=   "txtBHT"
      Tab(2).ControlCount=   6
      Begin VB.TextBox txtCardNo 
         Height          =   375
         Left            =   -74760
         TabIndex        =   25
         Top             =   1800
         Width           =   3255
      End
      Begin VB.TextBox txtBHT 
         Height          =   375
         Left            =   -74760
         TabIndex        =   27
         Top             =   600
         Width           =   3255
      End
      Begin VB.TextBox txtDiscountPercent 
         Height          =   375
         Left            =   1320
         TabIndex        =   50
         Top             =   1320
         Width           =   495
      End
      Begin VB.TextBox txtGTotal 
         Height          =   375
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   49
         Top             =   600
         Width           =   2055
      End
      Begin VB.TextBox txtDiscount 
         Height          =   360
         Left            =   2160
         Locked          =   -1  'True
         TabIndex        =   48
         Top             =   1320
         Width           =   1215
      End
      Begin VB.TextBox txtNTotal 
         Height          =   375
         Left            =   1320
         Locked          =   -1  'True
         TabIndex        =   47
         Top             =   2040
         Width           =   2055
      End
      Begin MSDataListLib.DataCombo cmbCompany 
         Height          =   360
         Left            =   -74760
         TabIndex        =   23
         Top             =   960
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
      Begin MSDataListLib.DataCombo cmbBHT 
         Height          =   360
         Left            =   -74760
         TabIndex        =   29
         Top             =   1440
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
      Begin MSDataListLib.DataCombo cmbRoom 
         Height          =   360
         Left            =   -74760
         TabIndex        =   31
         Top             =   2160
         Width           =   3255
         _ExtentX        =   5741
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Card No"
         Height          =   240
         Left            =   -74760
         TabIndex        =   24
         Top             =   1560
         Width           =   690
      End
      Begin VB.Label Label18 
         AutoSize        =   -1  'True
         Caption         =   "&New BHT"
         Height          =   240
         Left            =   -74760
         TabIndex        =   26
         Top             =   360
         Width           =   780
      End
      Begin VB.Label Label17 
         AutoSize        =   -1  'True
         Caption         =   "Room"
         Height          =   240
         Left            =   -74760
         TabIndex        =   30
         Top             =   1920
         Width           =   495
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Previous BHT"
         Height          =   240
         Left            =   -74760
         TabIndex        =   28
         Top             =   1200
         Width           =   1125
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Company"
         Height          =   240
         Left            =   -74760
         TabIndex        =   22
         Top             =   600
         Width           =   795
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "&Total"
         Height          =   240
         Left            =   240
         TabIndex        =   54
         Top             =   600
         Width           =   435
      End
      Begin VB.Label Label4 
         AutoSize        =   -1  'True
         Caption         =   "Dis&count"
         Height          =   240
         Left            =   240
         TabIndex        =   53
         Top             =   1320
         Width           =   720
      End
      Begin VB.Label Label5 
         AutoSize        =   -1  'True
         Caption         =   "N&et Total"
         Height          =   240
         Left            =   240
         TabIndex        =   52
         Top             =   2040
         Width           =   780
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   "%"
         Height          =   240
         Left            =   1920
         TabIndex        =   51
         Top             =   1320
         Width           =   180
      End
   End
   Begin VB.TextBox txtTotalCharge 
      Height          =   375
      Left            =   7320
      TabIndex        =   46
      Top             =   600
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.TextBox txtOtherCOst 
      Height          =   375
      Left            =   6960
      TabIndex        =   45
      Top             =   600
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.TextBox txtExpenceCost 
      Height          =   375
      Left            =   7320
      TabIndex        =   44
      Top             =   1560
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.TextBox txtMaterialCost 
      Height          =   375
      Left            =   7320
      TabIndex        =   43
      Top             =   2040
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.TextBox txtOtherCharge 
      Height          =   375
      Left            =   6960
      TabIndex        =   42
      Top             =   1080
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.TextBox txtStaffCharge 
      Height          =   375
      Left            =   7320
      TabIndex        =   41
      Top             =   1080
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.TextBox txtHospitalCharge 
      Height          =   375
      Left            =   6960
      TabIndex        =   40
      Top             =   1560
      Visible         =   0   'False
      Width           =   255
   End
   Begin VB.TextBox txtY 
      Height          =   375
      Left            =   1560
      TabIndex        =   9
      Top             =   1320
      Width           =   975
   End
   Begin VB.TextBox txtM 
      Height          =   375
      Left            =   2640
      TabIndex        =   11
      Top             =   1320
      Width           =   975
   End
   Begin VB.TextBox txtD 
      Height          =   375
      Left            =   3720
      TabIndex        =   13
      Top             =   1320
      Width           =   975
   End
   Begin VB.TextBox txtCost 
      Height          =   375
      Left            =   6960
      TabIndex        =   39
      Top             =   600
      Visible         =   0   'False
      Width           =   255
   End
   Begin btButtonEx.ButtonEx bttnSettle 
      Height          =   375
      Left            =   8880
      TabIndex        =   33
      Top             =   7920
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Settle"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnDelete 
      Height          =   375
      Left            =   6840
      TabIndex        =   20
      Top             =   7920
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Delete"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnAdd 
      Height          =   375
      Left            =   6960
      TabIndex        =   18
      Top             =   2400
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Add"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CheckBox chkPrint 
      Caption         =   "&Print"
      Height          =   255
      Left            =   8880
      TabIndex        =   32
      Top             =   7560
      Value           =   1  'Checked
      Width           =   1095
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   10320
      TabIndex        =   34
      Top             =   7920
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo dtcIx 
      Height          =   1860
      Left            =   1560
      TabIndex        =   17
      Top             =   2400
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   3281
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   1
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid GridIx 
      Height          =   3495
      Left            =   120
      TabIndex        =   19
      Top             =   4320
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   6165
      _Version        =   393216
      SelectionMode   =   1
   End
   Begin MSDataListLib.DataCombo cmbDoc 
      Height          =   360
      Left            =   1560
      TabIndex        =   15
      Top             =   1920
      Width           =   5295
      _ExtentX        =   9340
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker dtpDOB 
      Height          =   375
      Left            =   4800
      TabIndex        =   38
      Top             =   1320
      Visible         =   0   'False
      Width           =   2055
      _ExtentX        =   3625
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   69271555
      CurrentDate     =   39773
   End
   Begin MSDataListLib.DataCombo cmbTitle 
      Height          =   360
      Left            =   1560
      TabIndex        =   1
      Top             =   120
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbSex 
      Height          =   360
      Left            =   1560
      TabIndex        =   4
      Top             =   600
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx btnReprint 
      Height          =   375
      Left            =   120
      TabIndex        =   35
      Top             =   7920
      Visible         =   0   'False
      Width           =   2295
      _ExtentX        =   4048
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Reprint Last Bill"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      Caption         =   "Todays Patients"
      Height          =   240
      Left            =   8040
      TabIndex        =   36
      Top             =   720
      Width           =   1350
   End
   Begin VB.Label Label19 
      AutoSize        =   -1  'True
      Caption         =   "Phone"
      Height          =   240
      Left            =   3240
      TabIndex        =   5
      Top             =   600
      Width           =   525
   End
   Begin VB.Label Label14 
      AutoSize        =   -1  'True
      Caption         =   "&Age"
      Height          =   240
      Left            =   120
      TabIndex        =   7
      Top             =   1320
      Width           =   330
   End
   Begin VB.Label Label13 
      AutoSize        =   -1  'True
      Caption         =   "&Patient Name"
      Height          =   240
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1140
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      Caption         =   "&Sex"
      Height          =   240
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   315
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      Caption         =   "&Years"
      Height          =   240
      Left            =   1560
      TabIndex        =   8
      Top             =   1080
      Width           =   480
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      Caption         =   "&Months"
      Height          =   240
      Left            =   2640
      TabIndex        =   10
      Top             =   1080
      Width           =   615
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      Caption         =   "&Days"
      Height          =   240
      Left            =   3720
      TabIndex        =   12
      Top             =   1080
      Width           =   405
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "Re&ffered By"
      Height          =   240
      Left            =   120
      TabIndex        =   14
      Top             =   1920
      Width           =   990
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "&Investigation"
      Height          =   240
      Left            =   120
      TabIndex        =   16
      Top             =   2400
      Width           =   1080
   End
End
Attribute VB_Name = "frmPatientIxNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'    Dim rsViewPatient As New ADODB.Recordset
    Dim rsViewIx As New ADODB.Recordset
    Dim rsPatientIxBill As New ADODB.Recordset
    Dim rsPatientIx As New ADODB.Recordset
    Dim rsPatient As New ADODB.Recordset
    Dim rsIx As New ADODB.Recordset
    Dim rsBHT As New ADODB.Recordset
    Dim rsTitle As New ADODB.Recordset
    
    Dim temSQL As String
    Dim temPatientIxBillID As Long
    Dim cSetDfltPrinter As New cSetDfltPrinter
    Dim temtubeID() As Long
    Dim temTubeVol() As Double
    Dim temTube() As String
    Dim temTubeCount As Long


    Dim PatientList As New clsFillCombos

Dim pPatientID As Long
Dim pDoc As String
Dim pPatientIxBillID As Long
Dim psstab1tab As Integer
Dim ptest() As String
Dim pAmount() As String
Dim pTotal As Double
Dim pDiscount As Double
Dim pNetTotal As Double
Dim pBHT As String
Dim pCompany As String
Dim pRoom As String
Dim pCardNo As Long

Private Sub btnReprint_Click()
    printLabBillImpact pPatientIxBillID, BillPrinterName, BillPaperName, Me.hdc
'    printLabBillImpact pPatientIxBillID, BillPrinterName, BillPaperName, Me.hdc, Me, 2
'    printLabBillImpact pPatientIxBillID, BillPrinterName, BillPaperName, Me.hdc, Me, 3
End Sub

Private Sub bttnAdd_Click()
'    On Error GoTo eh
    
    
    Dim tr As Integer
    Dim temPatient As String
    
    Dim temHospitalCharge As Double
    Dim temStaffCharge As Double
    Dim temOtherCharge As Double
    Dim temCharge As Double
    
    Dim temMaterialCost As Double
    Dim temExpenceCost As Double
    Dim temOtherCost As Double
    
    
    Dim temValue As Double
    Dim temCost As Double
    Dim ItemTubeID As Long
    Dim ItemTubeVol As Double
    
    If IsNumeric(dtcIx.BoundText) = False Then
        MsgBox "Please select a Test?"
        dtcIx.SetFocus
        Exit Sub
    End If
    
    
    If IxAlreadyAdded(dtcIx.BoundText) = True Then
        tr = MsgBox("Test Already Added. You want to add it again?", vbYesNo)
        If tr = vbNo Then
            dtcIx.SetFocus
            Exit Sub
        End If
    End If
    
    
    With gridIx
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .col = 0
        .text = .Row
        
        .col = 1
        .text = dtcIx.text
        
        With rsIx
            If .State = 1 Then .Close
            temSQL = "SELECT * FROM tblIX WHERE IxID = " & Val(dtcIx.BoundText)
            .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
            
            If .RecordCount > 0 Then
            
            
                If UCase(!IxDisplay) <> UCase(dtcIx.text) Then
                    MsgBox "Error Adding. Please re-enter"
                    dtcIx.SetFocus
                    SendKeys "{Esc}"
                    Call FormatGrid
                    
                    Exit Sub
                End If
            
            
                If IsNull(!Value) = False Then
                    temValue = !Value
                End If
                If IsNull(!Cost) = False Then
                    temCost = !Cost
                End If
                If IsNull(!TubeID) = False Then
                    ItemTubeID = !TubeID
                End If
                If IsNull(!TubeVolume) = False Then
                    ItemTubeVol = !TubeVolume
                End If
            
            
            
                If IsNull(!StaffCharge) = False Then
                    temStaffCharge = !StaffCharge
                End If
                If IsNull(!HospitalCharge) = False Then
                    temHospitalCharge = !HospitalCharge
                End If
                If IsNull(!OtherCharge) = False Then
                    temOtherCharge = !OtherCharge
                End If
                If IsNull(!Charge) = False Then
                    temCharge = !Charge
                End If
            
                If IsNull(!MaterialCost) = False Then
                    temMaterialCost = !MaterialCost
                End If
                If IsNull(!ExpenceCost) = False Then
                    temExpenceCost = !ExpenceCost
                End If
                If IsNull(!OtherCost) = False Then
                    temOtherCost = !OtherCost
                End If
                
                If IsNull(!Cost) = False Then
                    temCost = !Cost
                End If
            
            
            
            
            
            
            End If
            .Close
        End With
        .col = 2
        .text = Format(temValue, "0.00")
        .col = 3
        .text = temCost
        .col = 4
        .text = dtcIx.BoundText
        .col = 5
        .text = ItemTubeID
        .col = 6
        .text = ItemTubeVol
        .col = 7
        .text = temHospitalCharge
        .col = 8
        .text = temStaffCharge
        .col = 9
        .text = temOtherCharge
        .col = 10
        .text = temCharge
        .col = 11
        .text = temMaterialCost
        .col = 12
        .text = temExpenceCost
        .col = 13
        .text = temOtherCost
        .col = 14
        .text = temCost
        
         
    End With
'    With GridIx
'        If .Rows > 9 Then
'            .TopRow = .Rows - 8
'        End If
'    End With
    Call ClearAddValues
    Call CalculateGrossTotal
    Call CalculateNetTotal
    
    DoEvents
    
    dtcIx.SetFocus
    
    Exit Sub
    
eh:
    MsgBox "Please select Investigation"
    dtcIx.SetFocus
    Exit Sub
    
End Sub

Private Function IxAlreadyAdded(ByVal IxID As Long) As Boolean
    IxAlreadyAdded = False
    Dim i As Integer
    With gridIx
        For i = 1 To .Rows - 1
            If IxID = Val(.TextMatrix(i, 4)) Then
                IxAlreadyAdded = True
            End If
        Next
    End With
End Function

Private Sub CalculateGrossTotal()
    Dim i As Integer
    Dim temTotalValue  As Double
    Dim temTotalCost As Double
    
    
    Dim temHospitalCharge As Double
    Dim temStaffCharge As Double
    Dim temOtherCharge As Double
    Dim temCharge As Double
    
    Dim temMaterialCost As Double
    Dim temExpenceCost As Double
    Dim temOtherCost As Double
    
    
    With gridIx
        For i = 1 To .Rows - 1
            temTotalValue = temTotalValue + Val(.TextMatrix(i, 2))
            temTotalCost = temTotalCost + Val(.TextMatrix(i, 3))
            
            temHospitalCharge = temHospitalCharge + Val(.TextMatrix(i, 7))
            temStaffCharge = temStaffCharge + Val(.TextMatrix(i, 8))
            temOtherCharge = temOtherCharge + Val(.TextMatrix(i, 9))
            
            
            temMaterialCost = temMaterialCost + Val(.TextMatrix(i, 11))
            temExpenceCost = temExpenceCost + Val(.TextMatrix(i, 12))
            temTotalCost = temTotalCost + Val(.TextMatrix(i, 13))
        
        Next
    End With

    
    txtGTotal.text = Format(temTotalValue, "0.00")
    txtCost.text = temTotalCost
    
    txtExpenceCost.text = temExpenceCost
    txtMaterialCost.text = temMaterialCost
    txtOtherCOst.text = temOtherCost
    txtCost.text = temTotalCost
    
    txtHospitalCharge.text = temHospitalCharge
    txtStaffCharge.text = temStaffCharge
    txtOtherCharge.text = temOtherCharge
    txtTotalCharge.text = temTotalValue
    
    
End Sub

Private Sub CalculateNetTotal()
    Dim NetTotal As Long
    NetTotal = Val(txtGTotal.text) - Val(txtDiscount.text)
    txtNTotal.text = Format(NetTotal, "0.00")
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnDelete_Click()
    Dim i As Integer
    With gridIx
        If .Rows <= 1 Then
            Exit Sub
        ElseIf .Row = 1 Then
            FormatGrid
        Else
            .RemoveItem (.Row)
            For i = 1 To .Rows - 1
                .TextMatrix(i, 0) = i
            Next
        End If
    End With
    Call CalculateGrossTotal
    Call CalculateNetTotal
    Call ClearAddValues
    dtcIx.SetFocus
End Sub

Private Sub bttnSettle_Click()
    Dim temPatientID As Long
    Dim temBHTID As Long
    Select Case SSTab1.Tab
        Case 0:
            If cmbName.text = "" Then
                MsgBox "Pleas enter a name"
                Exit Sub
            End If
            If IsNumeric(cmbName.BoundText) = True Then
                temPatientID = Val(cmbName.BoundText)
                updatePatient
            Else
                temPatientID = savePatient
            End If
        Case 1:
            If cmbName.text = "" Then
                MsgBox "Pleas enter a name"
                cmbName.SetFocus
                Exit Sub
            End If
            If IsNumeric(cmbCompany.BoundText) = False Then
                MsgBox "Pelase select a company"
                cmbCompany.SetFocus
                Exit Sub
            End If
            If IsNumeric(cmbName.BoundText) = True Then
                temPatientID = Val(cmbName.BoundText)
                updatePatient
            Else
                temPatientID = savePatient
            End If
        Case 2:
            If IsNumeric(cmbRoom.BoundText) = False Then
                MsgBox "Room?"
                cmbRoom.SetFocus
                Exit Sub
            End If
            If IsNumeric(txtBHT.text) = False Then
                If IsNumeric(cmbBHT.BoundText) = False Then
                    MsgBox "BHT?"
                    cmbBHT.SetFocus
                    Exit Sub
                End If
                temBHTID = Val(cmbBHT.BoundText)
                temPatientID = PatientIDFromBHTID(Val(cmbBHT.BoundText))
            Else
                If cmbName.text = "" Then
                    MsgBox "Pleas enter a name"
                    cmbName.SetFocus
                    Exit Sub
                End If
                
                If IsNumeric(cmbName.BoundText) = True Then
                    temPatientID = Val(cmbName.BoundText)
                    savePatient
                Else
                    temPatientID = savePatient
                End If
                
                temBHTID = saveBHT(temPatientID, txtBHT.text, Val(cmbRoom.BoundText))
                Call FillBHTCombo
                cmbBHT.BoundText = temBHTID
            End If
        End Select
        
        If gridIx.Rows <= 1 Then
            MsgBox "You have not added any test to the bill"
            dtcIx.SetFocus
            Exit Sub
        End If
        
    
    If temPatientID = 0 Then
        Exit Sub
    End If
    
    Call WritePatientIxBill(temPatientID, temBHTID)
    Call WritePatientIx(temPatientID)
    Call CalculateTubeVol
    
    
    
    pPatientIxBillID = temPatientIxBillID
    'If chkPrint.Value = 1 Then Call printLabBillImpact(temPatientIxBillID, BillPrinterName, BillPaperName, Me.hdc)
    
    If chkPrint.Value = 1 Then Call printLabBillPos(temPatientIxBillID, BillPrinterName, BillPaperName, Me.hdc, Me, 1)
'    If chkPrint.Value = 1 Then Call printLabBillImpact(temPatientIxBillID, BillPrinterName, BillPaperName, Me.hdc, Me, 2)
'    If chkPrint.Value = 1 Then Call printLabBillImpact(temPatientIxBillID, BillPrinterName, BillPaperName, Me.hdc, Me, 3)
    
    Call FormatGrid
    Call ClearAddValues
    
    PatientList.FillSpecificField cmbName, "Patient", "Name", True
    
    
    btnReprint.Visible = True
    Dim temString
    Select Case SSTab1.Tab
        Case 0:
            temString = "Cash"
        Case 1:
            temString = "Company"
        Case 2:
            temString = "Inward"
    End Select

    Dim i As Integer
    i = MsgBox("Bill No : " & temPatientIxBillID & vbNewLine & "Payment Method : " & temString & vbNewLine & "Total" & vbTab & txtNTotal.text)
    
    Call ClearSettleValues
    Call FillBHTCombo
    
    Call FormatPatientGrid
    Call FillPatientGrid
    
    DoEvents
    
'    cmbName.SetFocus
    cmbTitle.SetFocus

End Sub

Private Function PatientIDFromBHTID(BHTID As Long)
    PatientIDFromBHTID = 0
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "Select * from tblBHT where BHTID = " & BHTID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            PatientIDFromBHTID = !PatientID
        End If
        .Close
    End With
End Function

Private Sub CalculateTubeVol()
    Dim i As Integer
    Dim MyTubeID As Long
    For i = 0 To temTubeCount - 1
        temTubeVol(i) = 0
    Next

    With gridIx
        For i = 1 To .Rows - 1
            For MyTubeID = 0 To temTubeCount - 1
                If temtubeID(MyTubeID) = Val(.TextMatrix(i, 5)) Then
                    temTubeVol(MyTubeID) = temTubeVol(MyTubeID) + Val(.TextMatrix(i, 6))
                End If
            Next
        Next
    End With
End Sub

Private Sub WritePatientIxBill(PatientID As Long, BHTID As Long)
    With rsPatientIxBill
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblPatientIxBill where PatientIxBillID = 0 "
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !Date = Date
        !Time = Time
        !UserID = UserID
        
        !HospitalCharge = Val(txtHospitalCharge.text)
        !StaffCharge = Val(txtStaffCharge.text)
        !OtherCharge = Val(txtOtherCharge.text)
        !Charge = Val(txtTotalCharge.text)
        
        !Value = Val(txtGTotal.text)
        !Discount = Val(txtDiscount.text)
        !NetValue = Val(txtNTotal.text)
        
        !MaterialCost = Val(txtMaterialCost.text)
        !ExpenceCost = Val(txtExpenceCost.text)
        !OtherCost = Val(txtOtherCOst.text)
        !Cost = Val(txtCost.text)
        
        !Cancelled = False
        !PatientID = PatientID
        !DoctorID = Val(cmbDoc.BoundText)
'        !InstitutionID = Val(cmbIns.BoundText)
        
        Select Case SSTab1.Tab
            Case 0:
                !CompanyID = 0
                !BHTID = 0
                !RoomID = 0
                !BillType = "Cash"
                !CardNo = 0
            Case 1:
                !CompanyID = Val(cmbCompany.BoundText)
                !BHTID = 0
                !RoomID = 0
                !BillType = "Company"
                !CardNo = Val(txtCardNo.text)
            Case 2:
                !BHTID = BHTID
                !RoomID = Val(cmbRoom.BoundText)
                !CompanyID = 0
                !BillType = "Inward"
                !CardNo = 0
        End Select
        
        .Update
        temPatientIxBillID = !PatientIxBillID
        .Close
    End With
End Sub

Private Sub WritePatientIx(PatientID As Long)
    Dim i As Integer
    With rsPatientIx
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblPatientIX where PatientIxBillID = 0"
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
    End With
    With gridIx
        For i = 1 To .Rows - 1
            With rsPatientIx
                .AddNew
                !PatientIxBillID = temPatientIxBillID
                
                If IsNumeric(gridIx.TextMatrix(i, 4)) = False Or Val(gridIx.TextMatrix(i, 4)) = 0 Then
                    MsgBox "Please cancel this Bill. There was an error of entering investigation. Please cancel the bill and reenter"
                    Exit Sub
                End If
                
                !IxID = Val(gridIx.TextMatrix(i, 4))
                !Value = Val(gridIx.TextMatrix(i, 2))
                !Cost = Val(gridIx.TextMatrix(i, 3))
                
                !HospitalFee = Val(gridIx.TextMatrix(i, 7))
                !StaffFee = Val(gridIx.TextMatrix(i, 8))
                !OtherFee = Val(gridIx.TextMatrix(i, 9))
                
                
                !Printed = False
                !Issued = False
                !Deleted = False
                !PatientID = PatientID
                !Billed = True
                !BilledDate = Date
                !BilledTime = Time
                !BilledUserID = UserID
                !DoctorID = Val(cmbDoc.BoundText)
'                !InstitutionID = Val(cmbIns.BoundText)
            .Update
            End With
        Next
    End With
End Sub

Private Sub ClearSettleValues()
    cmbDoc.text = Empty
'    cmbIns.Text = Empty
    txtCost.text = Empty
    txtDiscount.text = Empty
    txtGTotal.text = Empty
    txtNTotal.text = Empty
    cmbName.text = Empty
    txtY.text = Empty
    txtM.text = Empty
    txtD.text = Empty
    dtpDOB.Value = Date
    cmbSex.text = Empty
    cmbTitle.text = Empty
    txtPhone.text = Empty
    txtCardNo.text = Empty
    
    txtExpenceCost.text = 0
    txtStaffCharge.text = 0
    txtOtherCharge.text = 0
    txtTotalCharge.text = 0
    
    txtHospitalCharge.text = 0
    txtStaffCharge.text = 0
    txtOtherCharge.text = 0
    txtTotalCharge.text = 0
    
    cmbCompany.text = Empty
    cmbBHT.text = Empty
    cmbRoom.text = Empty
    txtBHT.text = Empty
    
    SSTab1.Tab = 0
    
End Sub

Private Sub ClearAddValues()
    dtcIx.text = Empty
End Sub

Private Sub cmbBHT_Change()
    If IsNumeric(cmbBHT.BoundText) = True Then
        DisplayPatientDetails PatientIDFromBHTID(Val(cmbBHT.BoundText))
    End If
End Sub

Private Sub DisplayPatientDetails(PatientID As Long)
    Dim rsTem As New ADODB.Recordset
    Dim temDOB As Date
    Dim temAge As Long
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblPatient where PatientID = " & PatientID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            cmbName.text = !Name
            cmbSex.BoundText = !SexID
            cmbTitle.BoundText = !TitleID
            temDOB = !DateOfBirth
            temAge = DateDiff("D", temDOB, Date)
            If temAge < 30 Then
                txtD.text = DateDiff("D", temDOB, Date)
            ElseIf temAge < 12 * 30 Then
                txtM.text = DateDiff("M", temDOB, Date)
            Else
                txtY.text = DateDiff("M", temDOB, Date) / 12
            End If
        End If
        .Close
    End With
    
    
End Sub

Private Sub cmbBHT_Click(Area As Integer)
    cmbBHT_Change
End Sub

Private Sub cmbDoc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        dtcIx.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        cmbDoc.text = Empty
    End If
End Sub

Private Sub cmbName_Change()
    If IsNumeric(cmbName.BoundText) = True Then
        Dim temPt As New clsPatient
        temPt.ID = Val(cmbName.BoundText)
        
        txtY.text = temPt.AgeInYears
        Set temPt = Nothing
    End If
End Sub

'Private Sub cmbIns_Change()
'    Dim rsTem As New ADODB.Recordset
'    With rsTem
'        If .State = 1 Then .Close
'        temSQL = "Select * from tblInstitution where Deleted = False and InstitutionID = " & Val(cmbIns.BoundText)
'        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
'        If .RecordCount > 0 Then
'            If IsNull(!DiscountPercent) = False Then
'                txtDiscountPercent.Text = !DiscountPercent
'            Else
'                txtDiscountPercent.Text = 0
'            End If
'        Else
'            txtDiscountPercent.Text = 0
'        End If
'    End With
'End Sub

'Private Sub cmbIns_KeyDown(KeyCode As Integer, Shift As Integer)
'    If KeyCode = vbKeyEscape Then
'        cmbIns.Text = Empty
'    ElseIf KeyCode = vbKeyReturn Then
'        KeyCode = Empty
'        dtcIx.SetFocus
'    End If
'End Sub

Private Sub dtcIx_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        bttnAdd_Click
    ElseIf KeyCode = vbKeyEscape Then
        dtcIx.text = Empty
    End If
End Sub

Private Sub Form_Load()
    Call FillCombos
    Call FormatGrid
    Call Tubes
    bttnDelete.Enabled = False
    Call GetSettings
    
    Call FormatPatientGrid
    Call FillPatientGrid
    
    dtpDOB.Value = Date
End Sub

Private Sub GetSettings()
    chkPrint.Value = GetSetting(App.EXEName, Me.Name, chkPrint.Name, 0)
    SSTab1.Tab = 0
End Sub

Private Sub SaveSettings()
    SaveSetting App.EXEName, Me.Name, chkPrint.Name, chkPrint.Value
End Sub

Private Sub Tubes()
    Dim i As Integer
    Dim rsTube As New ADODB.Recordset
    With rsTube
        If .State = 1 Then .Close
        temSQL = "Select * from tblTube where Deleted = false"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            .MoveLast
            ReDim temtubeID(.RecordCount)
            ReDim temTubeVol(.RecordCount)
            ReDim temTube(.RecordCount)
            temTubeCount = .RecordCount
            .MoveFirst
            For i = 0 To .RecordCount - 1
                temtubeID(i) = !TubeID
                temTube(i) = !Tube
                .MoveNext
            Next
        End If
    End With
End Sub

Private Sub FormatGrid()
    With gridIx
        .Rows = 1
        .Cols = 15
        .Row = 0
        .col = 0
        .CellAlignment = 4
        .text = "No."
        .col = 1
        .CellAlignment = 4
        .text = "Investigation"
        .col = 2
        .CellAlignment = 4
        .text = "Value"
        .col = 3
        .text = "Cost"
        .col = 4
        .text = "IxID"
        .ColWidth(0) = 600
        .ColWidth(2) = 1100
        .ColWidth(3) = 1
        .ColWidth(4) = 1
        .ColWidth(1) = .Width - (.ColWidth(0) + .ColWidth(2) + 100)
        .ColWidth(5) = 0
        .ColWidth(6) = 0
        .ColWidth(7) = 0
        .ColWidth(8) = 0
        .ColWidth(9) = 0
        .ColWidth(10) = 0
        .ColWidth(11) = 0
        .ColWidth(12) = 0
        .ColWidth(13) = 0
        .ColWidth(14) = 0

        .col = 5
        .text = "TubeID"

        .col = 6
        .text = "Tube Vol"
        
        .col = 7
        .text = "Hospital Charge"
        .col = 8
        .text = "Staff Charge"
        .col = 9
        .text = "Other Charge"
        .col = 10
        .text = "Total Charge"
        
        .col = 11
        .text = "Material Cost"
        .col = 12
        .text = "Expence Cost"
        .col = 13
        .text = "Other Cost"
        .col = 14
        .text = "Total Cost"

    End With

End Sub

Public Sub FillCombos()
    With rsViewIx
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblIx  WHERE Deleted = False ORDER BY IxDisplay"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcIx
        Set .RowSource = rsViewIx
        .ListField = "IxDisplay"
        .BoundColumn = "IxID"
    End With

    
    With rsTitle
        If .State = 1 Then .Close
        temSQL = "SELECT * FROM tblTitle  ORDER BY TitleID"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With cmbTitle
        Set .RowSource = rsTitle
        .ListField = "Title"
        .BoundColumn = "TitleID"
    End With
    

    
    
    Dim RDoc As New clsFillCombos
    Dim RIns As New clsFillCombos
    Dim Sex As New clsFillCombos
   ' Dim Title As New clsFillCombos
    
    Dim Room As New clsFillCombos
    Dim Company As New clsFillCombos
    
    Company.FillAnyCombo cmbCompany, "Company", True
    
    Room.FillAnyCombo cmbRoom, "Room", True
    
    PatientList.FillSpecificField cmbName, "Patient", "Name", True
    
    
    
    Sex.FillAnyCombo cmbSex, "Sex", False
'    Title.FillAnyCombo cmbTitle, "Title", False
    
    RDoc.FillAnyCombo cmbDoc, "Doctor", True
'    RIns.FillAnyCombo cmbIns, "Institution", True

    FillBHTCombo
End Sub

Private Sub FillBHTCombo()
    With rsBHT
        temSQL = "Select * from tblBHT where Discharged = false AND Admitted = true order by BHT"
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With cmbBHT
        Set .RowSource = rsBHT
        .ListField = "BHT"
        .BoundColumn = "BHTID"
        .text = Empty
    End With
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSettings
End Sub

Private Sub GridIx_Click()
    If gridIx.Row > 0 Then
        bttnDelete.Enabled = True
    Else
        bttnDelete.Enabled = False
    End If
End Sub

Private Sub gridPt_DblClick()
    Dim temMyPtID As Long
    Dim temPt As New clsPatient
    
    Dim temBillID As Long
    Dim temBill As New clsBill
    
    temMyPtID = Val(gridPt.TextMatrix(gridPt.Row, 0))
    temBillID = Val(gridPt.TextMatrix(gridPt.Row, 1))
    
    temPt.ID = temMyPtID
    temBill.BillID = temBillID
    
    cmbTitle.text = temPt.Title
    cmbName.text = temPt.Name
    cmbSex.text = temPt.Sex
    txtPhone.text = temPt.Phone
    txtY.text = temPt.AgeInYears
    dtpDOB.Value = temPt.DateOfBirth
    
    Select Case temBill.BillType
        Case "Cash":
            SSTab1.Tab = 0
        
        Case "Inward":
            SSTab1.Tab = 2
            cmbBHT.BoundText = temBill.BHTID
            cmbRoom.BoundText = temBill.RoomID
        
        Case "Company":
            SSTab1.Tab = 1
            If temBill.CardNo <> 0 Then txtCardNo.text = temBill.CardNo
            cmbCompany.BoundText = temBill.CompanyID
    End Select
    
    
End Sub

Private Sub txtDiscount_Change()
    Call CalculateNetTotal
End Sub












Private Sub eLabPrintPos(PatientID As Long)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyPatient As New clsPatient
    MyPatient.ID = PatientID
    
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    If SelectForm(BillPaperName, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer

pPatientID = 0
pDoc = Empty
pPatientIxBillID = 0
psstab1tab = 0
ReDim ptest(0)
ReDim pAmount(0)
pTotal = 0
pDiscount = 0
pNetTotal = 0
pBHT = Empty
pCompany = Empty
pRoom = Empty
pCardNo = 0

    Printer.Font = "Tw Cen MT Condensed Extra Bold"
    Printer.FontSize = 12
    Printer.ForeColor = vbRed
    Printer.Print HospitalName
    Printer.FontName = "Tw Cen MT Condensed"
    Printer.FontSize = 11
    Printer.Print HospitalDescreption
    Printer.ForeColor = vbBlack
    Printer.FontSize = 11
    Printer.Print HospitalAddress
    
    Printer.Print Telephone1
    Dim displacement As Integer
    displacement = -7
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement

    Printer.FontSize = 11
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & MyPatient.Title & " " & MyPatient.Name;  'Tab(Tab3); ' "Date : " & Format(Date, "yyyy MM dd")
    Printer.Print Tab(Tab1); "Patient Age  : " & MyPatient.AgeInWords '; Tab(Tab3); Format(Date, "yyyy MM dd") & " " & Format(time, "hh:mm AMPM")
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone '; Tab(Tab3); "Payment : Cash"
    If (cmbDoc.text <> "") Then Printer.Print Tab(Tab1); "Referral  : " & cmbDoc.text
    
    
    pDoc = cmbDoc.text
    pPatientID = PatientID

    
    
    If SSTab1.Tab = 0 Then
        Printer.Print Tab(Tab1); "Payment : Cash"
        
    ElseIf SSTab1.Tab = 1 Then
        Printer.Print Tab(Tab1); "Payment : Credit(Company)"
        pCompany = cmbCompany.text
        pCardNo = Val(txtCardNo.text)
    ElseIf SSTab1.Tab = 2 Then
        Printer.Print Tab(Tab1); "Payment : Credit(Inward)"
        pBHT = cmbBHT.text
        pRoom = cmbRoom.text
    End If
    
    Printer.FontBold = False
    Printer.Print Tab(Tab1); "Bill No.     : " & temPatientIxBillID
    
    pPatientIxBillID = temPatientIxBillID
       
    Printer.Print
       
    Printer.FontSize = 10
    Printer.FontBold = False
    
    Dim MyLine As String
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 54 + displacement
    Tab4 = 36 + displacement
    
    
    Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    Printer.Print Tab(Tab1); MyLine

    ReDim ptest(gridIx.Rows - 2)
    ReDim pAmount(gridIx.Rows - 2)
    With gridIx
        For i = 1 To .Rows - 1
            ptest(i - 1) = Left(.TextMatrix(i, 1), 22)
            pAmount(i - 1) = Right((Space(10)) & .TextMatrix(i, 2), 10)
'            Printer.Print Tab(Tab1); .TextMatrix(i, 0); Tab(Tab2); Left(.TextMatrix(i, 1), 22); Tab(Tab3); Right((Space(10)) & .TextMatrix(i, 2), 10)
            Printer.Print Tab(Tab1); Left(.TextMatrix(i, 1), 22); Tab(Tab3); Right((Space(10)) & .TextMatrix(i, 2), 10)
        Next i
    End With
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 54 + displacement
    Tab4 = 46 + displacement
'    Printer.Print
    With Printer
        Printer.Print Tab(Tab1); MyLine
        
        
    'Printer.FontName = "Courier"
    'Printer.FontSize = 10
    Printer.FontBold = False
        
        
        
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & (txtGTotal.text), 10)
        
   ' Printer.FontName = "Courier"
   ' Printer.FontSize = 9
    Printer.FontBold = False
        
        
        pTotal = Val(txtGTotal.text)
        pDiscount = Val(txtDiscount.text)
        pNetTotal = Val(txtNTotal.text)
        
        If Val(txtDiscount.text) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (txtDiscount.text), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (txtNTotal.text), 10)
        End If
        Printer.Print Tab(Tab1); MyLine

        Printer.Print
'        Printer.Print
        Printer.Print Tab(Tab1); UserName
        Printer.Print Tab(Tab1); Format(Time, "hh:mm AMPM")
        Printer.Print Tab(Tab1); Format(Date, "dd MMM yyyy")
        Printer.Print
        Printer.Print
'        DrawBorder
        
        .EndDoc
    End With
End Sub





Private Sub eLabRePrintPos(PatientID As Long)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyPatient As New clsPatient
    MyPatient.ID = PatientID
    csetPrinter.SetPrinterAsDefault BillPrinterName
    If SelectForm(BillPaperName, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer



    Printer.Font = "Tw Cen MT Condensed Extra Bold"
    Printer.FontSize = 12
    Printer.ForeColor = vbRed
    Printer.Print HospitalName
    Printer.FontName = "Tw Cen MT Condensed"
    Printer.FontSize = 11
    Printer.Print HospitalDescreption
    Printer.ForeColor = vbBlack
    Printer.FontSize = 11
    Printer.Print HospitalAddress
    
    Dim displacement As Integer
    displacement = -7
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement

    Printer.FontSize = 11
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & MyPatient.Title & " " & MyPatient.Name; Tab(Tab3); "Date : " & Format(Date, "yyyy MM dd")
    Printer.Print Tab(Tab1); "Patient Age  : " & MyPatient.AgeInWords; Tab(Tab3); "Time : " & Format(Time, "hh:mm AMPM")
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone;
    Printer.Print Tab(Tab1); "Referral  : " & pDoc;
    
    If psstab1tab = 0 Then
        Printer.Print Tab(Tab1); "Payment : Cash"
    ElseIf psstab1tab = 1 Then
        Printer.Print Tab(Tab1); "Payment : Credit(Company)"
    ElseIf psstab1tab = 2 Then
        Printer.Print Tab(Tab1); "Payment : Credit(Inward)"
    End If

    Printer.Print Tab(Tab1); "Bill No.     : " & pPatientIxBillID

    
    Dim MyLine As String
    MyLine = "" ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 54 + displacement
    Tab4 = 36 + displacement
    
    Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    Printer.Print Tab(Tab1); MyLine

    
        For i = 0 To UBound(ptest)
            Printer.Print Tab(Tab1); ptest(i); Tab(Tab3); pAmount(i)
        Next i
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 54 + displacement
    Tab4 = 46 + displacement
'    Printer.Print
    With Printer
        Printer.Print Tab(Tab1); MyLine
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & Format(pTotal, "0.00"), 10)
        
        
        If Val(txtDiscount.text) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & Format(pDiscount, "0.00"), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & Format(pNetTotal, "0.00"), 10)
        End If
        Printer.Print Tab(Tab1); MyLine
        
        Printer.Print
'        Printer.Print
        Printer.Print Tab(Tab1); UserName
        Printer.Print Tab(Tab1); Format(Time, "hh:mm AMPM")
        Printer.Print Tab(Tab1); Format(Date, "dd MMM yyyy")
        Printer.Print
        Printer.Print
'        DrawBorder
        
        
        .EndDoc
    End With
End Sub






















Private Sub eLabPrint(PatientID As Long)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyPatient As New clsPatient
    MyPatient.ID = PatientID
    
    
    csetPrinter.SetPrinterAsDefault BillPrinterName
    If SelectForm(BillPaperName, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    
 
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer

pPatientID = 0
pDoc = Empty
pPatientIxBillID = 0
psstab1tab = 0
ReDim ptest(0)
ReDim pAmount(0)
pTotal = 0
pDiscount = 0
pNetTotal = 0
pBHT = Empty
pCompany = Empty
pRoom = Empty
pCardNo = 0

    Printer.Font = "Tw Cen MT Condensed Extra Bold"
    Printer.Font = "Times New Roman"
    Printer.FontSize = 15
    Printer.ForeColor = vbRed
    Printer.Print HospitalName


    Printer.FontName = "Tw Cen MT Condensed"
    Printer.Font = "Times New Roman"
    
'    Printer.FontSize = 11
'    Printer.Print HospitalDescreption
    
    Printer.ForeColor = vbBlack
    Printer.FontSize = 12
    Printer.Print HospitalAddress
    Printer.Print Telephone1
    
    Dim displacement As Integer
    displacement = 5
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement

    Printer.FontSize = 11
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & MyPatient.Title & " " & MyPatient.Name;  'Tab(Tab3); ' "Date : " & Format(Date, "yyyy MM dd")
    Printer.Print Tab(Tab1); "Patient Age  : " & MyPatient.AgeInWords '; Tab(Tab3); Format(Date, "yyyy MM dd") & " " & Format(time, "hh:mm AMPM")
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone '; Tab(Tab3); "Payment : Cash"
    If (cmbDoc.text <> "") Then Printer.Print Tab(Tab1); "Referral  : " & cmbDoc.text
    
    
    pDoc = cmbDoc.text
    pPatientID = PatientID

    
    
    If SSTab1.Tab = 0 Then
        Printer.Print Tab(Tab1); "Payment : Cash"
        
    ElseIf SSTab1.Tab = 1 Then
        Printer.Print Tab(Tab1); "Payment : Credit(Company)"
        pCompany = cmbCompany.text
        pCardNo = Val(txtCardNo.text)
    ElseIf SSTab1.Tab = 2 Then
        Printer.Print Tab(Tab1); "Payment : Credit(Inward)"
        pBHT = cmbBHT.text
        pRoom = cmbRoom.text
    End If
    
    Printer.FontBold = False
    Printer.Print Tab(Tab1); "Bill No.     : " & temPatientIxBillID
    
    pPatientIxBillID = temPatientIxBillID
       
    Printer.Print
       
    Printer.FontSize = 10
    Printer.FontBold = False
    
    Dim MyLine As String
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 54 + displacement
    Tab4 = 36 + displacement
    
    
    Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    Printer.Print Tab(Tab1); MyLine

    ReDim ptest(gridIx.Rows - 2)
    ReDim pAmount(gridIx.Rows - 2)
    With gridIx
        For i = 1 To .Rows - 1
            ptest(i - 1) = Left(.TextMatrix(i, 1), 22)
            pAmount(i - 1) = Right((Space(10)) & .TextMatrix(i, 2), 10)
'            Printer.Print Tab(Tab1); .TextMatrix(i, 0); Tab(Tab2); Left(.TextMatrix(i, 1), 22); Tab(Tab3); Right((Space(10)) & .TextMatrix(i, 2), 10)
            Printer.Print Tab(Tab1); Left(.TextMatrix(i, 1), 22); Tab(Tab3); Right((Space(10)) & .TextMatrix(i, 2), 10)
        Next i
    End With
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 54 + displacement
    Tab4 = 46 + displacement
'    Printer.Print
    With Printer
        Printer.Print Tab(Tab1); MyLine
        
        
    'Printer.FontName = "Courier"
    'Printer.FontSize = 10
    Printer.FontBold = False
        
        
        
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & (txtGTotal.text), 10)
        
   ' Printer.FontName = "Courier"
   ' Printer.FontSize = 9
    Printer.FontBold = False
        
        
        pTotal = Val(txtGTotal.text)
        pDiscount = Val(txtDiscount.text)
        pNetTotal = Val(txtNTotal.text)
        
        If Val(txtDiscount.text) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (txtDiscount.text), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (txtNTotal.text), 10)
        End If
        Printer.Print Tab(Tab1); MyLine

        Printer.Print
'        Printer.Print
        Printer.Print Tab(Tab1); UserName
        Printer.Print Tab(Tab1); Format(Time, "hh:mm AMPM")
        Printer.Print Tab(Tab1); Format(Date, "dd MMM yyyy")
        Printer.Print
        Printer.Print
'        DrawBorder
        
        .EndDoc
    End With
End Sub





Private Sub eLabRePrint(PatientID As Long)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyPatient As New clsPatient
    MyPatient.ID = PatientID
    csetPrinter.SetPrinterAsDefault BillPrinterName
    If SelectForm(BillPaperName, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer



    Printer.Font = "Tw Cen MT Condensed Extra Bold"
    Printer.FontSize = 14
    Printer.ForeColor = vbRed
    Printer.Print HospitalName
    Printer.FontName = "Tw Cen MT Condensed"
'    Printer.FontSize = 11
'    Printer.Print HospitalDescreption
    Printer.ForeColor = vbBlack
    Printer.FontSize = 12
    Printer.Print HospitalAddress
    Printer.Print Telephone1
        
    Dim displacement As Integer
    displacement = -7
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement

    Printer.FontSize = 11
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & MyPatient.Title & " " & MyPatient.Name; Tab(Tab3); "Date : " & Format(Date, "yyyy MM dd")
    Printer.Print Tab(Tab1); "Patient Age  : " & MyPatient.AgeInWords; Tab(Tab3); "Time : " & Format(Time, "hh:mm AMPM")
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone;
    Printer.Print Tab(Tab1); "Referral  : " & pDoc;
    
    If psstab1tab = 0 Then
        Printer.Print Tab(Tab1); "Payment : Cash"
    ElseIf psstab1tab = 1 Then
        Printer.Print Tab(Tab1); "Payment : Credit(Company)"
    ElseIf psstab1tab = 2 Then
        Printer.Print Tab(Tab1); "Payment : Credit(Inward)"
    End If

    Printer.Print Tab(Tab1); "Bill No.     : " & pPatientIxBillID

    
    Dim MyLine As String
    MyLine = "" ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 54 + displacement
    Tab4 = 36 + displacement
    
    Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    Printer.Print Tab(Tab1); MyLine

    
        For i = 0 To UBound(ptest)
            Printer.Print Tab(Tab1); ptest(i); Tab(Tab3); pAmount(i)
        Next i
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 54 + displacement
    Tab4 = 46 + displacement
'    Printer.Print
    With Printer
        Printer.Print Tab(Tab1); MyLine
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & Format(pTotal, "0.00"), 10)
        
        
        If Val(txtDiscount.text) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & Format(pDiscount, "0.00"), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & Format(pNetTotal, "0.00"), 10)
        End If
        Printer.Print Tab(Tab1); MyLine
        
        Printer.Print
'        Printer.Print
        Printer.Print Tab(Tab1); UserName
        Printer.Print Tab(Tab1); Format(Time, "hh:mm AMPM")
        Printer.Print Tab(Tab1); Format(Date, "dd MMM yyyy")
        Printer.Print
        Printer.Print
'        DrawBorder
        
        
        .EndDoc
    End With
End Sub




Private Sub ClearBillValues()
    Call ClearAddValues
    Call FormatGrid
    txtGTotal.text = "0.00"
    txtNTotal.text = "0.00"
    txtDiscount.text = "0.00"
    txtPhone.text = ""
End Sub

Private Sub txtDiscountPercent_Change()
    If Val(txtDiscountPercent.text) = 0 Then
        txtDiscount.text = 0
    Else
        txtDiscount.text = (Val(txtGTotal.text) * Val(txtDiscountPercent.text) / 100)
    End If
End Sub

Private Sub cmbSex_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtPhone.SetFocus
    Else
    
    End If
End Sub

Private Sub cmbTitle_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        cmbName.SetFocus
    End If
End Sub

Private Function savePatient() As Long
    savePatient = 0
    Dim TemResponce As Integer
    If Trim(cmbName.text) = "" Then
        MsgBox "Please enter a name"
        cmbName.SetFocus
        Exit Function
    End If
    Dim rstemPatient As New ADODB.Recordset
    With rstemPatient
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        temSQL = "Select * From tblPatient where PatientID = 0"
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !Name = Trim(cmbName.text)
        !Phone = Trim(txtPhone.text)
        !DateOfBirth = dtpDOB.Value
        !SexID = Val(cmbSex.BoundText)
        !TitleID = Val(cmbTitle.BoundText)
        .Update
        savePatient = !PatientID
        If .State = 1 Then .Close
        Exit Function
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        If .State = 1 Then .Close
    End With
End Function

Private Sub updatePatient()
    Dim TemResponce As Integer
    Dim rstemPatient As New ADODB.Recordset
    With rstemPatient
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        temSQL = "Select * From tblPatient where PatientID = " & Val(cmbName.BoundText)
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Name = Trim(cmbName.text)
            !Phone = Trim(txtPhone.text)
            !DateOfBirth = dtpDOB.Value
            !SexID = Val(cmbSex.BoundText)
            !TitleID = Val(cmbTitle.BoundText)
            .Update
        End If
        If .State = 1 Then .Close
        Exit Sub
ErrorHandler:
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        If .State = 1 Then .Close
    End With
End Sub


Private Function saveBHT(PatientID As Long, BHT As String, RoomID As Long) As Long
    saveBHT = 0
    If PatientID = 0 Then Exit Function
    Dim rstemPatient As New ADODB.Recordset
    With rstemPatient
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        temSQL = "Select * From tblBHT"
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !PatientID = PatientID
        !BHT = BHT
        !RoomID = RoomID
        !Admitted = True
        !AdmittedTime = Time
        !AdmittedDate = Date
        !AdmittedUserID = UserID
        !AdmittedDateTime = Now
        .Update
        saveBHT = !BHTID
        If .State = 1 Then .Close
        Exit Function
ErrorHandler:
        Dim TemResponce As Long
        TemResponce = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        If .State = 1 Then .Close
    End With
End Function


Private Sub CalculateAge()
        dtpDOB.Value = DateSerial(Year(Date) - Val(txtY.text), Month(Date) - Val(txtM.text), Day(Date) - Val(txtD.text))
End Sub

Private Sub txtD_Change()
    CalculateAge
End Sub

Private Sub txtD_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        cmbDoc.SetFocus
    End If
End Sub

Private Sub txtM_Change()
    CalculateAge
End Sub

Private Sub txtM_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        
        KeyCode = Empty
        If IsNumeric(txtM.text) = True Then
            dtcIx.SetFocus
        Else
            txtD.SetFocus
        End If
    End If
End Sub

Private Sub cmbName_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        cmbSex.SetFocus
    End If
End Sub

Private Sub cmbName_LostFocus()
'    cmbName.Text = UCase(cmbName.Text)
End Sub

Private Sub txtPhone_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        txtY.SetFocus
    Else
    
    End If

End Sub

Private Sub txtY_Change()
    CalculateAge
End Sub

Private Sub txtY_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        If IsNumeric(txtY.text) = True Then
            cmbDoc.SetFocus
        Else
            txtM.SetFocus
        End If
    End If
End Sub

Private Sub FormatPatientGrid()
    With gridPt
        .Clear
        .Rows = 1
        .Cols = 3
        
        .Row = 0
        
        .col = 0
        .ColWidth(0) = 0
        .text = "PatientID"
        
        .col = 1
        .ColWidth(1) = 800
        .text = "Bill No"
        
        .col = 2
        .ColWidth(2) = .Width - .ColWidth(1) - 450
        .text = "Patient"
    End With
End Sub

Private Sub FillPatientGrid()
    Dim rsTemPt As New ADODB.Recordset
    With rsTemPt
        If .State = 1 Then .Close
        temSQL = "SELECT tblPatient.PatientID, tblPatientIxBill.PatientIxBillID, tblPatient.Name " & _
                    "FROM tblPatient RIGHT JOIN tblPatientIxBill ON tblPatient.PatientID = tblPatientIxBill.PatientID " & _
                    "Where(((tblPatientIxBill.Date) = #" & Format(Date, "dd MMMM yyyy") & "#)) " & _
                    "ORDER BY tblPatientIxBill.PatientIxBillID DESC"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            gridPt.Rows = gridPt.Rows + 1
            gridPt.Row = gridPt.Rows - 1
            gridPt.col = 0
            gridPt.CellAlignment = 1
            gridPt.text = !PatientID
            gridPt.col = 1
            gridPt.CellAlignment = 1
            gridPt.text = !PatientIxBillID
            gridPt.col = 2
            gridPt.CellAlignment = 1
            gridPt.text = !Name
            
            .MoveNext
        Wend
        .Close
    End With
End Sub
