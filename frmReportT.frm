VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmReportT 
   Caption         =   "Reports"
   ClientHeight    =   10545
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11940
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   10545
   ScaleWidth      =   11940
   Begin VB.TextBox txtSpecimanNo 
      Height          =   360
      Left            =   9480
      TabIndex        =   25
      Top             =   1680
      Width           =   2295
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   22
      Top             =   10080
      Width           =   3255
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   1800
      Style           =   2  'Dropdown List
      TabIndex        =   21
      Top             =   9720
      Width           =   3255
   End
   Begin MSDataListLib.DataCombo ListTitle 
      Height          =   360
      Left            =   1800
      TabIndex        =   20
      Top             =   9240
      Visible         =   0   'False
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   635
      _Version        =   393216
      Text            =   "DataCombo1"
   End
   Begin VB.Frame FrameDetails 
      Height          =   1575
      Left            =   120
      TabIndex        =   8
      Top             =   0
      Width           =   11655
      Begin MSComCtl2.DTPicker dtpTime 
         Height          =   375
         Left            =   9720
         TabIndex        =   19
         Top             =   240
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   661
         _Version        =   393216
         Format          =   16580610
         CurrentDate     =   39812
      End
      Begin MSComCtl2.DTPicker dtpDate 
         Height          =   360
         Left            =   6600
         TabIndex        =   18
         Top             =   240
         Width           =   2415
         _ExtentX        =   4260
         _ExtentY        =   635
         _Version        =   393216
         CustomFormat    =   "dd MMMM yyyy"
         Format          =   16580611
         CurrentDate     =   39812
      End
      Begin MSDataListLib.DataCombo DataComboPatient 
         Height          =   360
         Left            =   2160
         TabIndex        =   0
         Top             =   240
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   635
         _Version        =   393216
         Enabled         =   0   'False
         MatchEntry      =   -1  'True
         Style           =   2
         ListField       =   ""
         BoundColumn     =   ""
         Text            =   ""
         Object.DataMember      =   ""
      End
      Begin MSDataListLib.DataCombo DataComboDoctor 
         Height          =   360
         Left            =   2160
         TabIndex        =   1
         Top             =   660
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   635
         _Version        =   393216
         Enabled         =   0   'False
         MatchEntry      =   -1  'True
         Style           =   2
         ListField       =   ""
         BoundColumn     =   ""
         Text            =   ""
         Object.DataMember      =   ""
      End
      Begin MSDataListLib.DataCombo DataComboInstitute 
         Height          =   360
         Left            =   2160
         TabIndex        =   2
         Top             =   1080
         Width           =   3735
         _ExtentX        =   6588
         _ExtentY        =   635
         _Version        =   393216
         Enabled         =   0   'False
         MatchEntry      =   -1  'True
         Style           =   2
         ListField       =   ""
         BoundColumn     =   ""
         Text            =   ""
         Object.DataMember      =   ""
      End
      Begin VB.Label lblID 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   9720
         TabIndex        =   9
         Top             =   660
         Width           =   1695
      End
      Begin VB.Label Label11 
         Caption         =   "Sex"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6000
         TabIndex        =   17
         Top             =   1080
         Width           =   495
      End
      Begin VB.Label Label10 
         Caption         =   "ID"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   9120
         TabIndex        =   16
         Top             =   660
         Width           =   495
      End
      Begin VB.Label Label8 
         Caption         =   "Time"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   9120
         TabIndex        =   15
         Top             =   240
         Width           =   495
      End
      Begin VB.Label Label7 
         Caption         =   "Age"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6000
         TabIndex        =   14
         Top             =   660
         Width           =   495
      End
      Begin VB.Label Label3 
         Caption         =   "Date"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   6000
         TabIndex        =   13
         Top             =   240
         Width           =   495
      End
      Begin VB.Label lblAge 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6600
         TabIndex        =   4
         Top             =   660
         Width           =   2415
      End
      Begin VB.Label Label4 
         Caption         =   "Referring Doctor"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   660
         Width           =   1815
      End
      Begin VB.Label Label5 
         Caption         =   "Referring Institute"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1080
         Width           =   2055
      End
      Begin VB.Label Label6 
         Caption         =   "Patient"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label lblSex 
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   360
         Left            =   6600
         TabIndex        =   3
         Top             =   1080
         Width           =   2415
      End
   End
   Begin VB.Timer Timer1 
      Interval        =   1000
      Left            =   7320
      Top             =   1680
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   10080
      TabIndex        =   7
      Top             =   9960
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttPrint 
      Height          =   375
      Left            =   8520
      TabIndex        =   6
      Top             =   9720
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnChange 
      Height          =   375
      Left            =   6960
      TabIndex        =   5
      Top             =   9720
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Save"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DataComboIx 
      Height          =   360
      Left            =   2160
      TabIndex        =   26
      Top             =   1680
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   635
      _Version        =   393216
      Enabled         =   0   'False
      MatchEntry      =   -1  'True
      Style           =   2
      ListField       =   ""
      BoundColumn     =   ""
      Text            =   ""
      Object.DataMember      =   ""
   End
   Begin VB.Label Label1 
      Caption         =   "Investigation Name"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   28
      Top             =   1680
      Width           =   2055
   End
   Begin VB.Label Label9 
      Caption         =   "Speciman No."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   7440
      TabIndex        =   27
      Top             =   1680
      Width           =   2055
   End
   Begin VB.Label Label13 
      Caption         =   "Paper"
      Height          =   255
      Left            =   120
      TabIndex        =   24
      Top             =   10080
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "Printer"
      Height          =   375
      Left            =   120
      TabIndex        =   23
      Top             =   9720
      Width           =   1575
   End
End
Attribute VB_Name = "frmReportT"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    
    Dim MyPatient As New clsPatient
    Dim MyIx As New clsIx
    Dim MyBill As New clsBill
    
    Dim NumForms As Long, i As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim SuppliedWord As String
    Dim FSys As New Scripting.FileSystemObject
    Private csetPrinter As New cSetDfltPrinter
    
    
    Dim LeftX As Long
    Dim RightX As Long
    Dim TopY As Long
    Dim BottomY As Long
    Dim InbetweenY As Long
    Dim NowY As Long
    Dim NowX As Long
    Dim PatientDOB As Date
    Dim Line1X As Long
    Dim Line2X As Long
    Dim Line3X As Long
    Dim TemResponce As Byte
    Dim temPatientID As Long
    Dim Frows As Long
    Dim NowROw As Long
    
    Dim TextList As String
    Dim ResultsList As String
    Dim UnitsList As String
    Dim ReferancesList As String
    
    Dim temPatientIxID As Long
    Dim temPatientIxBillID As Long

    
    Dim rsViewPatient As New ADODB.Recordset
    Dim rsAuthority As New ADODB.Recordset
    Dim rsStaff As New ADODB.Recordset

    Dim rsIx As New ADODB.Recordset
    Dim temSQL As String


Private Sub bttnChange_Click()
    If Not IsNumeric(DataComboIx.BoundText) Then
        TemResponce = MsgBox("You have not selected an Investigation from the list. You can't add a new investigation here. To add investigations, you have to go to INVESTIGATIONS in the main menu", vbCritical, "Select Ix")
        DataComboIx.SetFocus
        Exit Sub
    End If
    If Not IsNumeric(DataComboPatient.BoundText) Then
        TemResponce = MsgBox("You have not selected a patient from the list. You can't add a new patient here. To add patients, you have to go to Patients in the main menu", vbCritical, "Select Ix")
        DataComboIx.SetFocus
        Exit Sub
    End If
    Call ChangeData
    bttPrint.Enabled = True
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub
Private Sub bttPrint_Click()
    Dim MyPrinter As Printer
    
    
    csetPrinter.SetPrinterAsDefault ("")
    
    For Each MyPrinter In VB.Printers
        If MyPrinter.DeviceName = cmbPrinter.Text Then
            Set Printer = MyPrinter
        End If
    Next
    
    csetPrinter.SetPrinterAsDefault (cmbPrinter.Text)

    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    csetPrinter.SetPrinterAsDefault (cmbPrinter.Text)
    
    
    
    
    Dim rsPatietnIX As New ADODB.Recordset
    With rsPatietnIX
        If .State = 1 Then .Close
        temSQL = "Select * from tblPatientIX where PatientIxID = " & temPatientIxID
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Printed = True
            !PrintedDate = Date
            !PrintedTime = time
            !PrintedUserID = UserID
            .Update
            .Close
        Else
            .Close
            MsgBox "Error"
            Exit Sub
        End If
    End With

    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = cmbPrinter.Text Then
            Set MyPrinter = Printer
        End If
    Next
    If SelectForm(cmbPaper.Text, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    csetPrinter.SetPrinterAsDefault (cmbPrinter.Text)
    Printer.PrintQuality = vbPRPQHigh
    Printer.Print
    
    Call PrintReportFormat
    Call PrintIxFormat
    
    
    
    Printer.EndDoc
End Sub

Private Sub PrintIxFormat()
    Dim rstemReportItems As New ADODB.Recordset
    Dim TemX1 As Long
    Dim TemY1 As Long
    Dim TemX2 As Long
    Dim TemY2 As Long
    Dim temRadius As Long
    
    Dim temText As String
    
    Printer.FillStyle = vbFSTransparent
    
    With rstemReportItems
        If .State = 1 Then .Close
        temSQL = "Select * from tblIxItem  where Deleted = false  AND IxID = " & Val(DataComboIx.BoundText)
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            TemX1 = Printer.ScaleWidth * !X1
            TemX2 = Printer.ScaleWidth * !X2
            TemY1 = Printer.ScaleHeight * !Y1
            TemY2 = Printer.ScaleHeight * !Y2
            If IsNull(!CircleRadius) = False Then
                temRadius = Printer.ScaleWidth * !CircleRadius
            End If
            If !IsLine = True Then
                Printer.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour
            ElseIf !IsRectangle = True Then
                Printer.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour, B
            ElseIf !IsCircle = True Then
                Printer.Circle (TemX1, TemY1), temRadius, !ForeColour
            ElseIf !IsLabel = True Then
                temText = !IxItem
                PrintReportText temText, TemX1, TemX2, TemY1, TemY2, !FontName, !FontSize, !FontBold, !FontItalic, !FontUnderline, !FONTSTRIKETHROUGH, !TextAlignment
            ElseIf !IsValue = True Then
                temText = FindIxValue(!IxItemID)
                PrintReportText temText, TemX1, TemX2, TemY1, TemY2, !FontName, !FontSize, !FontBold, !FontItalic, !FontUnderline, !FONTSTRIKETHROUGH, !TextAlignment
            ElseIf !IsCalc = True Then
                temText = FindCalcValue(!IxItemID)
                PrintReportText temText, TemX1, TemX2, TemY1, TemY2, !FontName, !FontSize, !FontBold, !FontItalic, !FontUnderline, !FONTSTRIKETHROUGH, !TextAlignment
            End If
            .MoveNext
        Wend
        .Close
    End With
'    temText = "Software by Lankan Medical Programmers (LakMediPro) - 071 5812399"
'            TemX1 = Printer.ScaleWidth * 0.05
'            TemX2 = Printer.ScaleWidth * 0.99
'            TemY1 = Printer.ScaleHeight * 0.0025
'            TemY2 = Printer.ScaleHeight * 0.99
'    PrintReportText temText, TemX1, TemX2, TemY1, TemY2, "Arial", 8, False, True, False, False, TextAlignment.LeftAlign
    
    temText = "Software by Lankan Medical Programmers (LakMediPro) - 071 5812399"
            TemX1 = Printer.ScaleWidth * 0.05
            TemX2 = Printer.ScaleWidth * 0.99
            TemY1 = Printer.ScaleHeight * 0.97
            TemY2 = Printer.ScaleHeight * 0.99
    PrintReportText temText, TemX1, TemX2, TemY1, TemY2, "Arial", 8, False, True, False, False, TextAlignment.LeftAlign
End Sub

Private Function FindCalcValue(ByVal temIxItemID As Long) As String
    Dim rsTem As New ADODB.Recordset
    Dim temCalc As Double
    Dim temFunction As String
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblIxItemCalc Where IxItemID = " & temIxItemID & " AND Deleted = false Order by ItemOrderNo"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            If !ItemOrderNo = 1 Then
                If !FunctionType = "Value" Then
                    If IsNumeric(FindIxValue(!ValueIxItemID)) = True Then
                        temCalc = CDbl(FindIxValue(!ValueIxItemID))
                    Else
                        temCalc = 0
                    End If
                ElseIf !FunctionType = "Constant" Then
                    If IsNumeric(!ConstantValue) = True Then
                        temCalc = CDbl(!ConstantValue)
                    Else
                        temCalc = 0
                    End If
                End If
            Else
                Select Case !FunctionType
                    Case "Constant":
                        If IsNumeric(!ConstantValue) = True Then
                            temCalc = CalculateValue(temFunction, temCalc, !ConstantValue)
                        Else
                            temCalc = 0
                        End If
                        
                    Case "Value":
                        If IsNumeric(FindIxValue(!ValueIxItemID)) = True Then
                            temCalc = CalculateValue(temFunction, temCalc, FindIxValue(!ValueIxItemID))
                        Else
                            temCalc = 0
                        End If
                    Case Else:
                        temFunction = !FunctionType
                End Select
            End If
            .MoveNext
        Wend
        FindCalcValue = temCalc
        If .State = 1 Then .Close
        temSQL = "Select * from tblIxItem where IxItemID = " & temIxItemID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!ItemFormat) = False Then
                If !ItemFormat <> "" Then
                    FindCalcValue = Format(FindCalcValue, !ItemFormat)
                End If
            End If
            If IsNull(!ItemPrefix) = False Then
                If !ItemPrefix <> "" Then
                    FindCalcValue = !ItemPrefix & FindCalcValue
                End If
            End If
            If IsNull(!ItemSuffix) = False Then
                If !ItemSuffix <> "" Then
                    FindCalcValue = FindCalcValue & !ItemSuffix
                End If
            End If
        End If
        .Close
    End With
End Function

Private Function CalculateValue(ByVal FunctionType As String, ByVal StartingValue As Double, ByVal NewValue As Double) As Double
    Select Case FunctionType
        Case "Add": CalculateValue = StartingValue + NewValue
        Case "Subtract": CalculateValue = StartingValue - NewValue
        Case "Multiply": CalculateValue = StartingValue * NewValue
        Case "Divide": CalculateValue = StartingValue / NewValue
        Case Else:
    End Select
End Function


Private Function FindIxValue(ByVal temIxItemID As Long) As String
    With rsIx
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblPatientIxItem where PatientIxId = " & temPatientIxID & " AND IxItemID = " & temIxItemID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            FindIxValue = !Value
        Else
            FindIxValue = Empty
        End If
    End With
End Function

Private Sub PrintReportFormat()
    Dim rstemReportItems As New ADODB.Recordset
    Dim TemX1 As Long
    Dim TemY1 As Long
    Dim TemX2 As Long
    Dim TemY2 As Long
    Dim temRadius As Long
    Dim temText As String
    
    Printer.FillStyle = vbFSTransparent
    
    With rstemReportItems
        If .State = 1 Then .Close
        temSQL = "Select * from tblReportItem  where Deleted = false"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            TemX1 = Printer.ScaleWidth * !X1
            TemX2 = Printer.ScaleWidth * !X2
            TemY1 = Printer.ScaleHeight * !Y1
            TemY2 = Printer.ScaleHeight * !Y2
            If IsNull(!CircleRadius) = False Then
                temRadius = Printer.ScaleWidth * !CircleRadius
            End If
            If !IsLine = True Then
                Printer.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour
            ElseIf !IsRectangle = True Then
                Printer.Line (TemX1, TemY1)-(TemX2, TemY2), !ForeColour, B
            ElseIf !IsCircle = True Then
                Printer.Circle (TemX1, TemY1), temRadius, !ForeColour
            ElseIf !IsLabel = True Then
                temText = !LabelText
                PrintReportText temText, TemX1, TemX2, TemY1, TemY2, !FontName, !FontSize, !FontBold, !FontItalic, !FontUnderline, !FONTSTRIKETHROUGH, !TextAlignment
            ElseIf !IsText = True Then
                Select Case !TextText
                    Case "Patient Name": temText = MyPatient.NameWithTitle
                    Case "Patient ID": temText = MyPatient.ID
                    Case "Patient Age in Words": temText = MyPatient.AgeInWords
                    Case "Patient Data of Birth": temText = MyPatient.DateOfBirth
                    Case "Patient Sex": temText = MyPatient.Sex
                    Case "Bill No": temText = temPatientIxBillID
                    Case "Patient Civil Status": temText = ""
                    Case "Patient Address": temText = MyPatient.Address
                    Case "Patient Telephone": temText = MyPatient.Phone
                    Case "Investigation": temText = MyIx.Ix
                    Case "Investigation ID": temText = MyIx.IxID
                    Case "Investigation Comments": temText = MyIx.Comments
                    Case "Department": temText = MyIx.Department
                    Case "Speciman": temText = MyIx.Speciman
                    Case "Speciman No.": temText = txtSpecimanNo.Text
                    Case "Speciman Comments": temText = ""
                    Case "Referring Doctor": temText = DataComboDoctor.Text
                    Case "Referring Institution": temText = DataComboInstitute.Text
                    Case "Date": temText = Format(dtpDate.Value, "dd MMMM yyyy")

                    Case "Room No.": temText = MyBill.Room
                    Case "Company":
                    
                        If MyBill.CardNo = 0 Then
                            temText = MyBill.Company
                        Else
                            temText = MyBill.Company & " (" & MyBill.CardNo & ")"
                        End If
                    
                    Case "BHT": temText = MyBill.BHT
                    
                    Case Else
                        temText = ""
                End Select
                PrintReportText temText, TemX1, TemX2, TemY1, TemY2, !FontName, !FontSize, !FontBold, !FontItalic, !FontUnderline, !FONTSTRIKETHROUGH, !TextAlignment
            End If
            .MoveNext
        Wend
        .Close
    End With
End Sub

Private Sub ChangeData()
'On Error GoTo ErrorHandler

    Dim MyCombo As Control
    Dim temIxItemID As Long

    For Each MyCombo In Controls
        If Left(MyCombo.Name, 5) = "Ixcmb" Then
            temIxItemID = Val(Right(MyCombo.Name, Len(MyCombo.Name) - 5))
            With rsIx
                If .State = 1 Then .Close
                temSQL = "SELECT * from tblPatientIxItem where PatientIxId = " & temPatientIxID & " AND IxItemID = " & temIxItemID
                .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
                If .RecordCount > 0 Then
                     !Value = MyCombo.Text
                Else
                    .AddNew
                    !PatientID = Val(DataComboPatient.BoundText)
                    !IxID = Val(DataComboIx.BoundText)
                    !PatientIxID = temPatientIxID
                    !IxItemID = temIxItemID
                    !Value = MyCombo.Text
                End If
                .Update
            End With
        End If
    Next

    With rsIx
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblPatientIx where PatientIxId = " & temPatientIxID
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
'            !PatientID = temPatientID
            If IsNumeric(DataComboDoctor.BoundText) Then !DoctorID = DataComboDoctor.BoundText
            If IsNumeric(DataComboInstitute.BoundText) Then !InstitutionID = DataComboInstitute.BoundText
'            !IxID = DataComboIx.BoundText
            
            !SavedDate = dtpDate.Value
            !SavedTime = dtpTime.Value
            !SavedUserID = UserID
            !Saved = True
            !specimanno = txtSpecimanNo.Text
            .Update
        End If
        .Close
    Exit Sub

ErrorHandler:
 TemResponce = MsgBox("An Unknown Error Occured, Please contact lakmedipro", vbCritical, "Error")
 .CancelUpdate
 .Close
End With
End Sub




Private Sub LocatePatient()
    With rsIx
        If .State = 1 Then .Close
        temSQL = "SELECT tblPatient.*, tblSex.Sex, tblTitle.Title " & _
                    "FROM (tblPatient LEFT JOIN tblSex ON tblPatient.SexID = tblSex.SexID) LEFT JOIN tblTitle ON tblPatient.TitleID = tblTitle.TitleID " & _
                    "WHERE (((tblPatient.PatientID)=" & MyPatient.ID & "))"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then .Close: Exit Sub
        If IsNull(!Sex) = False Then
            lblSex.Caption = !Sex
        End If
        If IsNull(!DateOfBirth) = False Then
            PatientDOB = !DateOfBirth
        End If
        lblAge.Caption = CalculateAgeInWords
        temPatientID = !PatientID
        lblID.Caption = temPatientID
        If Not IsNull(!Title) Then
            ListTitle.BoundText = !Title
        Else
            ListTitle.Text = Empty
        End If
    End With
End Sub


Private Function CalculateAgeInWords() As String
    Dim Age As Long
    Age = DateDiff("yyyy", PatientDOB, Now)
    If Age >= 5 Then
        CalculateAgeInWords = Age & " Years"
        Exit Function
    Else
        Age = DateDiff("m", PatientDOB, Now)
        If Age > 48 Then CalculateAgeInWords = "4" & " Years and " & Age - 48 & " months": Exit Function
        If Age = 48 Then CalculateAgeInWords = "4" & " Years": Exit Function
        If Age > 36 Then CalculateAgeInWords = "3" & " Years and " & Age - 36 & " months": Exit Function
        If Age = 36 Then CalculateAgeInWords = "3" & " Years": Exit Function
        If Age > 24 Then CalculateAgeInWords = "2" & " Years and " & Age - 24 & " months": Exit Function
        If Age = 24 Then CalculateAgeInWords = "2" & " Years": Exit Function
        If Age > 12 Then CalculateAgeInWords = "1" & " Years and " & Age - 12 & " months": Exit Function
        If Age = 12 Then CalculateAgeInWords = "1" & " Year": Exit Function
        If Age >= 1 Then CalculateAgeInWords = Age & " Months": Exit Function
        Age = DateDiff("d", PatientDOB, Now)
        CalculateAgeInWords = Age & " Days": Exit Function
        Exit Function
    End If
End Function

Private Sub FillCombos()
    Dim Ix As New clsFillCombos
    Dim Doc As New clsFillCombos
    Dim Ins As New clsFillCombos
    Dim Title As New clsFillCombos
    Title.FillAnyCombo ListTitle, "Title", False
    Ix.FillAnyCombo DataComboIx, "Ix", True
    Doc.FillAnyCombo DataComboDoctor, "Doctor", True
    Ins.FillAnyCombo DataComboInstitute, "Institution", True
    With rsViewPatient
        If .State = 1 Then .Close
        temSQL = "Select * from tblPatient where Deleted = false order by Name"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With DataComboPatient
        Set .RowSource = rsViewPatient
        .ListField = "Name"
        .BoundColumn = "PatientID"
    End With
End Sub

Private Sub FillPrinters()
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        cmbPrinter.AddItem MyPrinter.DeviceName
    Next
End Sub

Private Sub cmbPrinter_Change()
    cmbPrinter_Click
End Sub

Private Sub cmbPrinter_Click()
    cmbPaper.Clear
    csetPrinter.SetPrinterAsDefault (cmbPrinter.Text)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        With FormSize
            .cx = BillPaperHeight
            .cy = BillPaperWidth
        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                cmbPaper.AddItem PtrCtoVbString(.pName)
            End With
        Next i
        ClosePrinter (PrinterHandle)
    End If
End Sub

Private Sub Form_Load()
    Call FillCombos
    bttnChange.Enabled = True
    bttPrint.Enabled = False
    temPatientIxID = frmTraceIx.temPatientIxID
    temPatientIxBillID = 0
    With rsIx
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblPatientIx where PatientIxID = " & temPatientIxID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        DataComboPatient.BoundText = !PatientID
        MyPatient.ID = !PatientID
        MyIx.ID = !IxID
        temPatientIxBillID = !PatientIxBillID
        DataComboIx.BoundText = !IxID
        dtpDate.Value = Date
        dtpTime.Value = time
        If Not IsNull(!DoctorID) Then
            DataComboDoctor.BoundText = !DoctorID
        End If
        If Not IsNull(!InstitutionID) Then
            DataComboInstitute.BoundText = !InstitutionID
        End If
        If Not IsNull(!specimanno) Then
            txtSpecimanNo.Text = !specimanno
        End If
    End With
    Call LocatePatient
    MyBill.BillID = temPatientIxBillID
    Call FillPrinters
    Call AddCombos
    Call FillComboValues
    On Error Resume Next
    cmbPrinter.Text = GetSetting(App.EXEName, Me.Name, "Printer", "")
    cmbPrinter_Click
    cmbPaper.Text = GetSetting(App.EXEName, Me.Name, "Paper", "")
End Sub



Private Sub FillComboValues()
    Dim MyCombo As Control
    Dim temIxItemID As Long
    For Each MyCombo In Controls
        If Left(MyCombo.Name, 5) = "Ixcmb" Then
            temIxItemID = Val(Right(MyCombo.Name, Len(MyCombo.Name) - 5))
            With rsIx
                If .State = 1 Then .Close
                temSQL = "SELECT * from tblPatientIxItem where PatientIxId = " & temPatientIxID & " AND IxItemID = " & temIxItemID
                .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
                If .RecordCount > 0 Then
                    MyCombo.Text = !Value
                Else
                    MyCombo.Text = FindDefaultValue(temIxItemID)
                End If
            End With
        End If
    Next
End Sub

Private Function FindDefaultValue(IxItemID As Long) As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT tblIxValue.IxValue " & _
                    "FROM tblIxItem LEFT JOIN tblIxValue ON tblIxItem.IxValueDefaultID = tblIxValue.IxValueID " & _
                    "WHERE (((tblIxItem.IxItemID)=" & IxItemID & "))"

        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!IxValue) = False Then
                FindDefaultValue = !IxValue
            Else
                FindDefaultValue = Empty
            End If
        Else
            FindDefaultValue = Empty
        End If
    End With
End Function

Private Sub AddCombos()
    Dim MyCombo As Control
    Dim MyLabel As Label
    Dim rsTemIxItem As New ADODB.Recordset
    
    Dim LeftMargin As Long
    Dim TopMargin As Long
    Dim BottomMargin As Long
    Dim ComboHeight As Long
    Dim LabelWidth As Long
    Dim ComboWidth As Long
    Dim VerticalSpace As Long
    Dim HorizentalSpace As Long
    
    BottomMargin = 9250
    LeftMargin = 120
    TopMargin = 2160
    ComboHeight = 360
    LabelWidth = 2055
    ComboWidth = 3000
    VerticalSpace = 120
    HorizentalSpace = 400
    
    
    Dim IxItemHCount As Long
    Dim IxItemVCount As Long
    
    IxItemHCount = 1
    IxItemVCount = 0
    
    
    With rsTemIxItem
        If .State = 1 Then .Close
        temSQL = "Select * from tblIxItem where Deleted = false AND IsValue = true AND IxID = " & Val(DataComboIx.BoundText)
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                IxItemVCount = IxItemVCount + 1
                
                If TopMargin + ((IxItemVCount - 1) * (ComboHeight + VerticalSpace)) > BottomMargin Then
                    IxItemHCount = IxItemHCount + 1
                    IxItemVCount = 1
                End If
                
                Set MyCombo = Me.Controls.Add("VB.ComboBox", "Ixcmb" & !IxItemID, Me)
                MyCombo.Visible = True
                MyCombo.Top = TopMargin + ((IxItemVCount - 1) * (ComboHeight + VerticalSpace))
                MyCombo.Left = LeftMargin + LabelWidth + ((IxItemHCount - 1) * (LeftMargin + LabelWidth + ComboWidth + HorizentalSpace))
                MyCombo.Width = ComboWidth
'                MyCombo.Height = ComboHeight
                Call FillNewCombos(MyCombo, !IxItemID)
                
                Set MyLabel = Me.Controls.Add("VB.label", "lbl" & !IxItemID, Me)
                MyLabel.Visible = True
                MyLabel.Top = TopMargin + ((IxItemVCount - 1) * (ComboHeight + VerticalSpace))
                MyLabel.Left = LeftMargin + ((IxItemHCount - 1) * (LeftMargin + LabelWidth + ComboWidth + HorizentalSpace))
                MyLabel.Width = LabelWidth
                MyLabel.Height = ComboHeight
                MyLabel.Caption = !IxItem
                .MoveNext
            Wend
        End If
        .Close
    
    End With
End Sub


Private Sub FillNewCombos(NewCombo As ComboBox, IxItemID As Long)
    Dim rsTem As New ADODB.Recordset
    Dim temIxValueCategoryID As Long
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblIxItem where IxItemID = " & IxItemID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            temIxValueCategoryID = !IxValueCategoryID
        Else
            temIxValueCategoryID = 0
        End If
        If .State = 1 Then .Close
        temSQL = "Select * from tblIxValue where Deleted = False AND IxValueCategoryID = " & temIxValueCategoryID & " order By IxValue"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            NewCombo.AddItem !IxValue
            .MoveNext
        Wend
    End With
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSetting App.EXEName, Me.Name, "Printer", cmbPrinter.Text
    SaveSetting App.EXEName, Me.Name, "Paper", cmbPaper.Text
End Sub

