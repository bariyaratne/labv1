VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsDept"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarDeptID As Long 'local copy
Private mvarHospitalFee As Double 'local copy
Private mvarReagentFee As Double 'local copy
Private mvarMLTFee As Double 'local copy
Private mvarDept As String 'local copy
Public Property Let Dept(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Dept = 5
    mvarDept = vData
End Property


Public Property Get Dept() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Dept
    Dept = mvarDept
End Property



Public Property Let MLTFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.MLTFee = 5
    mvarMLTFee = vData
End Property


Public Property Get MLTFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.MLTFee
    MLTFee = mvarMLTFee
End Property



Public Property Let ReagentFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ReagentFee = 5
    mvarReagentFee = vData
End Property


Public Property Get ReagentFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ReagentFee
    ReagentFee = mvarReagentFee
End Property



Public Property Let HospitalFee(ByVal vData As Double)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.HospitalFee = 5
    mvarHospitalFee = vData
End Property


Public Property Get HospitalFee() As Double
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.HospitalFee
    HospitalFee = mvarHospitalFee
End Property



Public Property Let DeptID(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.DeptID = 5
    mvarDeptID = vData
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblDept where DeptID = " & mvarDeptID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            mvarDept = !Dept
            If Not IsNull(!HospitalFee) Then mvarHospitalFee = !HospitalFee
            If Not IsNull(!MLTFee) Then mvarMLTFee = !MLTFee
            If Not IsNull(!ReagentFee) Then mvarReagentFee = !ReagentFee
        End If
        .Close
    End With
End Property


Public Property Get DeptID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.DeptID
    DeptID = mvarDeptID
End Property



