VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmIx 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Investigations"
   ClientHeight    =   8160
   ClientLeft      =   2130
   ClientTop       =   1635
   ClientWidth     =   12975
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8160
   ScaleWidth      =   12975
   Begin VB.Frame Frame2 
      Height          =   7815
      Left            =   120
      TabIndex        =   8
      Top             =   120
      Width           =   5175
      Begin MSDataListLib.DataCombo dtcIx 
         Height          =   6900
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   4935
         _ExtentX        =   8705
         _ExtentY        =   12171
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   1
         Text            =   ""
      End
      Begin btButtonEx.ButtonEx bttnAdd 
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   7320
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnEdit 
         Height          =   375
         Left            =   1800
         TabIndex        =   2
         Top             =   7320
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Edit"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnDelete 
         Height          =   375
         Left            =   3480
         TabIndex        =   3
         Top             =   7320
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Delete"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Frame1 
      Height          =   7335
      Left            =   5520
      TabIndex        =   7
      Top             =   240
      Width           =   7335
      Begin VB.TextBox txtDisplay 
         Height          =   360
         Left            =   1920
         TabIndex        =   30
         Top             =   756
         Width           =   4575
      End
      Begin VB.TextBox txtcomment 
         Height          =   1215
         Left            =   1920
         MultiLine       =   -1  'True
         TabIndex        =   17
         Top             =   5400
         Width           =   5295
      End
      Begin VB.TextBox txtName 
         Height          =   360
         Left            =   1920
         TabIndex        =   16
         Top             =   240
         Width           =   4575
      End
      Begin VB.TextBox txtVolume 
         Height          =   360
         Left            =   1920
         TabIndex        =   14
         Top             =   2820
         Width           =   5295
      End
      Begin VB.TextBox txtHospitalFee 
         Alignment       =   1  'Right Justify
         Height          =   360
         Left            =   1920
         TabIndex        =   13
         Top             =   3336
         Width           =   5295
      End
      Begin VB.TextBox txtMLTFee 
         Alignment       =   1  'Right Justify
         Height          =   360
         Left            =   1920
         TabIndex        =   12
         Top             =   3852
         Width           =   5295
      End
      Begin VB.TextBox txtReagentFee 
         Alignment       =   1  'Right Justify
         Height          =   360
         Left            =   1920
         TabIndex        =   11
         Top             =   4368
         Width           =   5295
      End
      Begin VB.TextBox txtTotalFee 
         Alignment       =   1  'Right Justify
         Height          =   360
         Left            =   1920
         TabIndex        =   10
         Top             =   4884
         Width           =   5295
      End
      Begin VB.TextBox txtID 
         Enabled         =   0   'False
         Height          =   360
         Left            =   6600
         TabIndex        =   9
         Top             =   240
         Width           =   615
      End
      Begin btButtonEx.ButtonEx bttnSave 
         Height          =   375
         Left            =   3000
         TabIndex        =   4
         Top             =   6840
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Save"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin btButtonEx.ButtonEx bttnCancel 
         Height          =   375
         Left            =   4080
         TabIndex        =   5
         Top             =   6840
         Width           =   975
         _ExtentX        =   1720
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "&Cancel"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo dtcCategory 
         Height          =   360
         Left            =   1920
         TabIndex        =   15
         Top             =   1272
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin MSDataListLib.DataCombo dtcDept 
         Height          =   360
         Left            =   1920
         TabIndex        =   18
         Top             =   1788
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin MSDataListLib.DataCombo cmbTube 
         Height          =   360
         Left            =   1920
         TabIndex        =   19
         Top             =   2304
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin VB.Label Label11 
         Caption         =   "&Listed Name"
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   720
         Width           =   2055
      End
      Begin VB.Label Label2 
         Caption         =   "C&omments"
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   5400
         Width           =   2055
      End
      Begin VB.Label Label1 
         Caption         =   "&Investigation"
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label3 
         Caption         =   "Categ&ory"
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   1320
         Width           =   2055
      End
      Begin VB.Label Label4 
         Caption         =   "&Department"
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   1800
         Width           =   2055
      End
      Begin VB.Label Label5 
         Caption         =   "&Volume"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   2760
         Width           =   2055
      End
      Begin VB.Label Label6 
         Caption         =   "&Tube"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   2280
         Width           =   2055
      End
      Begin VB.Label Label7 
         Caption         =   "&Hospital Fee"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   3360
         Width           =   2055
      End
      Begin VB.Label Label8 
         Caption         =   "&MLT Fee"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   3840
         Width           =   2055
      End
      Begin VB.Label Label9 
         Caption         =   "&Reagent Fee"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   4440
         Width           =   2055
      End
      Begin VB.Label Label10 
         Caption         =   "&Total Fee"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   4920
         Width           =   2055
      End
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   11880
      TabIndex        =   6
      Top             =   7680
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmIx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Dim rsViewIx As New ADODB.Recordset
    Dim rsViewCategory As New ADODB.Recordset
    Dim rsViewDept As New ADODB.Recordset
    Dim rsIx As New ADODB.Recordset

Private Sub bttnCancel_Click()
    Call SelectMode
    Call Clearvalues
    dtcIx.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnDelete_Click()
    Dim tr  As Long
    tr = MsgBox("Are you sure you want to delete this investigation?", vbYesNo)
    If tr = vbNo Then Exit Sub
    tr = MsgBox("If you delete an investigation, you will never be able to get its format, previous investigations, values. Are you Still want to delete this investigation?", vbYesNo)
    If tr = vbNo Then Exit Sub
    
    With rsIx
        If .State = 1 Then .Close
        temSql = "Select * from tblIx where IxID = " & Val(dtcIx.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Deleted = True
            !DeletedDate = Date
            !DeletedTime = Time
            !DeletedUserID = UserID
            .Update
        End If
        If .State = 1 Then .Close
    End With
    Call Clearvalues
    Call FillCombos
    dtcIx.text = Empty
    dtcIx.SetFocus
End Sub

Private Sub dtcIx_Change()
    If IsNumeric(dtcIx.BoundText) = True Then
        Call DisplaySelected
        bttnEdit.Enabled = True
        bttnAdd.Enabled = False
        bttnDelete.Enabled = True
    Else
        Clearvalues
        bttnAdd.Enabled = True
        bttnEdit.Enabled = False
        bttnDelete.Enabled = False
    End If
End Sub

Private Sub Form_Load()
    FillCombos
    Call SelectMode
    Clearvalues
End Sub

Private Sub bttnAdd_Click()
    Clearvalues
    Call EditMode
    txtName.SetFocus
    txtName.text = dtcIx.text
    txtID.Visible = False
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnEdit_Click()
    Call EditMode
    txtName.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub ChangeDate()
    Dim i As Integer
    With rsIx
        On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        temSql = "Select * From tblix Where ixID = " & Val(dtcIx.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Ix = Trim(txtName.text)
            !IxCategoryID = Val(dtcCategory.BoundText)
            !IxDisplay = txtDisplay.text
            !DeptID = Val(dtcDept.BoundText)
            !TubeID = Val(cmbTube.BoundText)
            !TubeVolume = Val(txtVolume.text)
            !Comments = txtcomment.text
            !StaffCharge = Val(txtMLTFee.text)
            !HospitalCharge = Val(txtHospitalFee.text)
            !OtherCharge = Val(txtReagentFee.text)
            !Charge = Val(txtTotalFee.text)
            !Value = Val(txtTotalFee.text)
            .Update
        Else
            MsgBox "Error"
        End If
        If .State = 1 Then .Close
        Exit Sub
ErrorHandler:
        i = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        If .State = 1 Then .Close
    End With
End Sub

Private Sub bttnSave_Click()
    If Trim(txtName.text) = "" Then
        MsgBox "Please enter a Name"
        txtName.SetFocus
        Exit Sub
    End If
    If IsNumeric(dtcIx.BoundText) = True Then
        Call ChangeDate
    Else
        Call saveData
    End If
    FillCombos
    Call SelectMode
    Call Clearvalues
    dtcIx.text = Empty
    dtcIx.SetFocus
End Sub

Private Sub saveData()
    Dim i As Integer
    With rsIx
    On Error GoTo ErrorHandler
        If .State = 1 Then .Close
        .Open "Select * From tblix", cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !Ix = Trim(txtName.text)
        !IxCategoryID = Val(dtcCategory.BoundText)
        !IxDisplay = txtDisplay.text
        !DeptID = Val(dtcDept.BoundText)
        !Comments = txtcomment.text
        !TubeID = Val(cmbTube.BoundText)
        !TubeVolume = Val(txtVolume.text)
        !StaffCharge = Val(txtMLTFee.text)
        !HospitalCharge = Val(txtHospitalFee.text)
        !OtherCharge = Val(txtReagentFee.text)
        !Charge = Val(txtTotalFee.text)
        !Value = Val(txtTotalFee.text)
        
        .Update
        If .State = 1 Then .Close
        Exit Sub
    
ErrorHandler:
        i = MsgBox(Err.Number & vbNewLine & Err.Description & Me.Caption, vbCritical + vbOKOnly, "Save Error")
        If .State = 1 Then .CancelUpdate
        If .State = 1 Then .Close
    End With
End Sub

Private Sub FillCombos()
    Dim Tube As New clsFillCombos
    Tube.FillAnyCombo cmbTube, "Tube", True
    With rsViewCategory
        If .State = 1 Then .Close
        temSql = "SELECT * from tblIxCategory  WHERE Deleted = False order by IxCategory"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcCategory
        Set .RowSource = rsViewCategory
        .ListField = "IxCategory"
        .BoundColumn = "IxCategoryID"
    End With
    With rsViewIx
        If .State = 1 Then .Close
        temSql = "SELECT * from tblIx  WHERE Deleted = False order by Ix"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcIx
        Set .RowSource = rsViewIx
        .ListField = "Ix"
        .BoundColumn = "IXID"
    End With
    With rsViewDept
        If .State = 1 Then .Close
        temSql = "SELECT * from tblDept  WHERE Deleted = False order by Dept"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcDept
        Set .RowSource = rsViewDept
        .ListField = "Dept"
        .BoundColumn = "DeptID"
    End With
End Sub

Private Sub EditMode()
    
    bttnAdd.Enabled = False
    bttnEdit.Enabled = False
    dtcIx.Enabled = False
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = True
    bttnCancel.Enabled = True
    txtcomment.Enabled = True
    txtName.Enabled = True
    txtVolume.Enabled = True
    txtMLTFee.Enabled = True
    txtDisplay.Enabled = True
    txtHospitalFee.Enabled = True
    txtReagentFee.Enabled = True
    txtTotalFee.Enabled = True
    cmbTube.Enabled = True
    
    dtcCategory.Enabled = True
    dtcDept.Enabled = True
End Sub

Private Sub SelectMode()
    
    bttnAdd.Enabled = True
    bttnEdit.Enabled = False
    dtcIx.Enabled = True
    bttnDelete.Enabled = False
    
    bttnSave.Enabled = False
    bttnCancel.Enabled = False
    txtcomment.Enabled = False
    txtName.Enabled = False
    txtDisplay.Enabled = False
    txtVolume.Enabled = False
    txtMLTFee.Enabled = False
    txtHospitalFee.Enabled = False
    txtReagentFee.Enabled = False
    txtTotalFee.Enabled = False
    cmbTube.Enabled = False
    
    dtcCategory.Enabled = False
    dtcDept.Enabled = False
End Sub

Private Sub Clearvalues()
    txtName.text = Empty
    txtcomment.text = Empty
    dtcCategory.text = Empty
    dtcDept.text = Empty
    txtVolume.text = Empty
    cmbTube.text = Empty
    txtID.text = Empty
    txtMLTFee.text = Empty
    txtHospitalFee.text = Empty
    txtReagentFee.text = Empty
    txtTotalFee.text = Empty
    txtDisplay.text = Empty
End Sub

Private Sub DisplaySelected()
    If Not IsNumeric(dtcIx.BoundText) Then Exit Sub
    With rsIx
        If .State = 1 Then .Close
        .Open "Select * From tblIx Where (IxID = " & dtcIx.BoundText & ")", cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount = 0 Then Exit Sub
        Call Clearvalues
        txtName.text = !Ix
        If IsNull(!IxDisplay) = False Then
            txtDisplay.text = !IxDisplay
        Else
            txtDisplay.text = Empty
        End If
        If IsNull(!IxCategoryID) = False Then
            dtcCategory.BoundText = !IxCategoryID
        Else
            dtcCategory.text = Empty
        End If
        If IsNull(!DeptID) = False Then
            dtcDept.BoundText = !DeptID
        Else
            dtcDept.text = Empty
        End If
        If Not IsNull(!Comments) Then txtcomment.text = !Comments
        If IsNull(!TubeID) = False Then
            cmbTube.BoundText = !TubeID
        Else
            cmbTube.text = Empty
        End If
        If IsNull(!TubeVolume) = False Then
            txtVolume.text = !TubeVolume
        Else
            txtVolume.text = Empty
        End If
        
        txtHospitalFee.text = Format(!HospitalCharge, "0.00")
        txtMLTFee.text = Format(!StaffCharge, "0.00")
        txtReagentFee.text = Format(!OtherCharge, "0.00")
        txtTotalFee.text = Format(!Value, "0.00")
        
        
        
        txtID.Visible = True
        txtID.text = !IxID
        If .State = 1 Then .Close
    End With
End Sub

Private Sub txtHospitalFee_LostFocus()
    txtHospitalFee.text = Format(txtHospitalFee.text, "0.00")
    Call CalculateTotal
End Sub


Private Sub txtMLTFee_LostFocus()
    txtMLTFee.text = Format(txtMLTFee.text, "0.00")
    Call CalculateTotal
End Sub


Private Sub txtReagentFee_LostFocus()
    txtReagentFee.text = Format(txtReagentFee.text, "0.00")
    Call CalculateTotal
End Sub


Private Sub txtTotalFee_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn And IsNumeric(dtcDept.BoundText) Then
        Dim temDept As New clsDept
        Dim temTotal As Double
        temTotal = Val(txtTotalFee.text)
        temDept.DeptID = Val(dtcDept.BoundText)
        
        txtMLTFee.text = temTotal * temDept.MLTFee / 100
        txtReagentFee.text = temTotal * temDept.ReagentFee / 100
        txtHospitalFee.text = temTotal * temDept.HospitalFee / 100
        
    End If
End Sub

Private Sub txtTotalFee_LostFocus()
    txtTotalFee.text = Format(txtTotalFee.text, "0.00")
    Call CalculateTotal
End Sub

Private Sub CalculateTotal()
    txtTotalFee.text = Format(Val(txtHospitalFee.text) + Val(txtMLTFee.text) + Val(txtReagentFee.text), "0.00")
End Sub
