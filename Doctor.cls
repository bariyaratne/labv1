VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Doctor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varDoctorID As Long
    Private varDoctor As String
    Private varDoctorAddress As String
    Private varDoctorTelephone As String
    Private varDoctorFax As String
    Private varDoctorEmail As String
    Private varDoctorPaymentMethod As String
    Private varDoctorBank As String
    Private varDoctorAccount As String
    Private varDoctorCredit As Double
    Private varDoctorComments As String
    Private varDeleted As Boolean
    Private varDeletedUserID As Long
    Private varDeletedTime As Boolean
    Private varDeletedDate As Date

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblDoctor Where DoctorID = " & varDoctorID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !Doctor = varDoctor
        !DoctorAddress = varDoctorAddress
        !DoctorTelephone = varDoctorTelephone
        !DoctorFax = varDoctorFax
        !DoctorEmail = varDoctorEmail
        !DoctorPaymentMethod = varDoctorPaymentMethod
        !DoctorBank = varDoctorBank
        !DoctorAccount = varDoctorAccount
        !DoctorCredit = varDoctorCredit
        !DoctorComments = varDoctorComments
        !Deleted = varDeleted
        !DeletedUserID = varDeletedUserID
        !DeletedTime = varDeletedTime
        If varDeletedDate = 0 Then
            !DeletedDate = Null
        Else
            !DeletedDate = varDeletedDate
        End If
        .Update
        varDoctorID = !DoctorID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblDoctor WHERE DoctorID = " & varDoctorID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!DoctorID) Then
               varDoctorID = !DoctorID
            End If
            If Not IsNull(!Doctor) Then
               varDoctor = !Doctor
            End If
            If Not IsNull(!DoctorAddress) Then
               varDoctorAddress = !DoctorAddress
            End If
            If Not IsNull(!DoctorTelephone) Then
               varDoctorTelephone = !DoctorTelephone
            End If
            If Not IsNull(!DoctorFax) Then
               varDoctorFax = !DoctorFax
            End If
            If Not IsNull(!DoctorEmail) Then
               varDoctorEmail = !DoctorEmail
            End If
            If Not IsNull(!DoctorPaymentMethod) Then
               varDoctorPaymentMethod = !DoctorPaymentMethod
            End If
            If Not IsNull(!DoctorBank) Then
               varDoctorBank = !DoctorBank
            End If
            If Not IsNull(!DoctorAccount) Then
               varDoctorAccount = !DoctorAccount
            End If
            If Not IsNull(!DoctorCredit) Then
               varDoctorCredit = !DoctorCredit
            End If
            If Not IsNull(!DoctorComments) Then
               varDoctorComments = !DoctorComments
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varDoctorID = 0
    varDoctor = Empty
    varDoctorAddress = Empty
    varDoctorTelephone = Empty
    varDoctorFax = Empty
    varDoctorEmail = Empty
    varDoctorPaymentMethod = Empty
    varDoctorBank = Empty
    varDoctorAccount = Empty
    varDoctorCredit = 0
    varDoctorComments = Empty
    varDeleted = False
    varDeletedUserID = 0
    varDeletedTime = False
    varDeletedDate = Empty
End Sub

Public Property Let DoctorID(ByVal vDoctorID As Long)
    Call clearData
    varDoctorID = vDoctorID
    Call loadData
End Property

Public Property Get DoctorID() As Long
    DoctorID = varDoctorID
End Property

Public Property Let Doctor(ByVal vDoctor As String)
    varDoctor = vDoctor
End Property

Public Property Get Doctor() As String
    Doctor = varDoctor
End Property

Public Property Let DoctorAddress(ByVal vDoctorAddress As String)
    varDoctorAddress = vDoctorAddress
End Property

Public Property Get DoctorAddress() As String
    DoctorAddress = varDoctorAddress
End Property

Public Property Let DoctorTelephone(ByVal vDoctorTelephone As String)
    varDoctorTelephone = vDoctorTelephone
End Property

Public Property Get DoctorTelephone() As String
    DoctorTelephone = varDoctorTelephone
End Property

Public Property Let DoctorFax(ByVal vDoctorFax As String)
    varDoctorFax = vDoctorFax
End Property

Public Property Get DoctorFax() As String
    DoctorFax = varDoctorFax
End Property

Public Property Let DoctorEmail(ByVal vDoctorEmail As String)
    varDoctorEmail = vDoctorEmail
End Property

Public Property Get DoctorEmail() As String
    DoctorEmail = varDoctorEmail
End Property

Public Property Let DoctorPaymentMethod(ByVal vDoctorPaymentMethod As String)
    varDoctorPaymentMethod = vDoctorPaymentMethod
End Property

Public Property Get DoctorPaymentMethod() As String
    DoctorPaymentMethod = varDoctorPaymentMethod
End Property

Public Property Let DoctorBank(ByVal vDoctorBank As String)
    varDoctorBank = vDoctorBank
End Property

Public Property Get DoctorBank() As String
    DoctorBank = varDoctorBank
End Property

Public Property Let DoctorAccount(ByVal vDoctorAccount As String)
    varDoctorAccount = vDoctorAccount
End Property

Public Property Get DoctorAccount() As String
    DoctorAccount = varDoctorAccount
End Property

Public Property Let DoctorCredit(ByVal vDoctorCredit As Double)
    varDoctorCredit = vDoctorCredit
End Property

Public Property Get DoctorCredit() As Double
    DoctorCredit = varDoctorCredit
End Property

Public Property Let DoctorComments(ByVal vDoctorComments As String)
    varDoctorComments = vDoctorComments
End Property

Public Property Get DoctorComments() As String
    DoctorComments = varDoctorComments
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let DeletedTime(ByVal vDeletedTime As Boolean)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime() As Boolean
    DeletedTime = varDeletedTime
End Property

Public Property Let DeletedDate(ByVal vDeletedDate As Date)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate() As Date
    DeletedDate = varDeletedDate
End Property


