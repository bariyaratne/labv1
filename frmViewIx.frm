VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmViewIx 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "View Investigation"
   ClientHeight    =   6210
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6600
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6210
   ScaleWidth      =   6600
   Begin VB.TextBox txtBillId 
      Height          =   375
      Left            =   5400
      TabIndex        =   19
      Text            =   "Text1"
      Top             =   4560
      Width           =   1095
   End
   Begin VB.Frame Frame4 
      Height          =   1695
      Left            =   120
      TabIndex        =   11
      Top             =   4440
      Width           =   3495
      Begin VB.TextBox txtNTotal 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   375
         Left            =   1800
         TabIndex        =   17
         Top             =   1200
         Width           =   1575
      End
      Begin VB.TextBox txtDiscount 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   375
         Left            =   1800
         TabIndex        =   16
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox txtTotal 
         Alignment       =   1  'Right Justify
         Enabled         =   0   'False
         Height          =   375
         Left            =   1800
         TabIndex        =   15
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label7 
         Caption         =   "Total"
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label9 
         Caption         =   "Discount"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   720
         Width           =   1095
      End
      Begin VB.Label Label11 
         Caption         =   "Net Total"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   1200
         Width           =   1095
      End
   End
   Begin VB.Frame Frame3 
      Height          =   3015
      Left            =   120
      TabIndex        =   9
      Top             =   1440
      Width           =   6375
      Begin MSFlexGridLib.MSFlexGrid GridIx 
         Height          =   2655
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   6135
         _ExtentX        =   10821
         _ExtentY        =   4683
         _Version        =   393216
      End
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   120
      TabIndex        =   5
      Top             =   720
      Width           =   6375
      Begin VB.TextBox txtDate 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   375
         Left            =   840
         TabIndex        =   18
         Top             =   240
         Width           =   2175
      End
      Begin VB.TextBox txtTime 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         Height          =   375
         Left            =   4080
         TabIndex        =   8
         Top             =   240
         Width           =   2175
      End
      Begin VB.Label Label3 
         Caption         =   "Date"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label5 
         Caption         =   "Time"
         Height          =   255
         Left            =   3240
         TabIndex        =   6
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   120
      TabIndex        =   2
      Top             =   0
      Width           =   6375
      Begin MSDataListLib.DataCombo dtcPatient 
         Height          =   360
         Left            =   1920
         TabIndex        =   3
         Top             =   240
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   635
         _Version        =   393216
         Style           =   2
         Text            =   ""
      End
      Begin VB.Label Label1 
         Caption         =   "Patient Name"
         Height          =   375
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   1935
      End
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   5280
      TabIndex        =   1
      Top             =   5640
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnPrint 
      Height          =   375
      Left            =   3960
      TabIndex        =   0
      Top             =   5640
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmViewIx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsPatientIxBill As New ADODB.Recordset
    Dim rsPatientIx As New ADODB.Recordset
    Dim temSql As String
    Dim rsViewIx As New ADODB.Recordset
    Dim rsViewPatient As New ADODB.Recordset
    Dim temPatientIxBillID As Long
    Dim cSetDfltPrinter As New cSetDfltPrinter

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnPrint_Click()
    Call eLabPrint
End Sub

Private Sub Form_Load()
    Call FormatGrid
    Call FillCombos
    Call FillGrid
End Sub

Private Sub FillGrid()
    With rsPatientIxBill
        If .State = 1 Then .Close
        temSql = "SELECT * FROM tblPatientIxBill Where PatientIxBillID = " & TxPatientIxBillID
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            txtDate.text = Format(!Date, "dd MMMM yyyy")
            txtTime.text = !Time
            txtTotal.text = !Value
            txtDiscount.text = !Discount
            txtNTotal.text = !NetValue
            dtcPatient.BoundText = !PatientID
        End If
        .Close
    End With
    With rsPatientIx
        If .State = 1 Then .Close
        temSql = "SELECT tblPatientIx.*, tblIx.Ix FROM tblIx RIGHT JOIN tblPatientIx ON tblIx.IxID = tblPatientIx.IxID where PatientIxBillID = " & TxPatientIxBillID
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                gridIx.Rows = gridIx.Rows + 1
                gridIx.Row = gridIx.Rows - 1
                gridIx.col = 0
                gridIx.text = gridIx.Row
                gridIx.col = 1
                gridIx.text = !Ix
                gridIx.col = 2
                gridIx.text = Format(!Value, "0.00")
'                GridIx.Col = 3
'                GridIx.Text = Format(!Cost, "0.00")
                .MoveNext
            Wend
'            txtTotal.Text = Format(!Value, "0.00")
        End If
        .Close
    End With
    With gridIx
        If .Rows > 7 Then
            .TopRow = .Rows - 7
        End If
    End With
End Sub
 
 Private Sub FormatGrid()
     With gridIx
        .Rows = 1
        .Cols = 3
        .Row = 0
        .col = 0
        .CellAlignment = 4
        .text = "No."
        .col = 1
        .CellAlignment = 4
        .text = "Investigation"
        .col = 2
        .CellAlignment = 4
        .text = "Value"
        .ColWidth(0) = 700
        .ColWidth(2) = 1100
        .ColWidth(1) = .Width - (.ColWidth(0) + .ColWidth(2) + 375)
    End With
 End Sub
 Private Sub FillCombos()
    With rsViewPatient
        If .State = 1 Then .Close
        temSql = "SELECT tblPatient.* FROM tblPatient"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcPatient
        Set .RowSource = rsViewPatient
        .ListField = "Name"
        .BoundColumn = "PatientID"
    End With
 End Sub
Private Sub eLabPrint()
    
        Dim csetPrinter     As New cSetDfltPrinter
    Dim MyPatient As New clsPatient
    MyPatient.ID = Val(dtcPatient.BoundText)
    csetPrinter.SetPrinterAsDefault BillPrinterName
    If SelectForm(BillPaperName, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer



        Printer.Font = "Arial Black"
'        Printer.Print
'        Printer.Print
'        Printer.Print
'
        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(HospitalName) / 2)
        Printer.Print HospitalName
        
        Printer.FontSize = 11
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(HospitalAddress) / 2)
        Printer.Print HospitalAddress
        
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(HospitalDescreption) / 2)
        Printer.Print HospitalDescreption
        
    Tab1 = 8
    Tab2 = 13
    Tab3 = 48
    Tab4 = 46
    Tab5 = 46
    tab6 = 7
    tab7 = 9
    tab8 = 6
    tab9 = 43

    Printer.FontName = "Courier"
    Printer.FontSize = 10
    Printer.Print
'    Printer.Print
'    Printer.Print
'    Printer.Print
    Printer.Print Tab(Tab1); "Patient Name : " & MyPatient.Name
    Printer.Print Tab(Tab1); "Patient Age  : " & MyPatient.AgeInWords
    Printer.Print Tab(Tab1); "Patient Sex  : " & MyPatient.Sex
    Printer.Print Tab(Tab1); "Bill No.     : " & TxPatientIxBillID
    Printer.Print

'    Printer.Print
'    Printer.Print
'    Printer.Print
'    Printer.Print
'    Printer.Print
'    Printer.Print

    ' 7.0

    Tab1 = 8
    Tab2 = 13
    Tab3 = 48
    Tab4 = 46

    With gridIx
        For i = 1 To .Rows - 1
            Printer.Print Tab(Tab1); .TextMatrix(i, 0); Tab(Tab2); Left(.TextMatrix(i, 1), 22); Tab(Tab3); Right((Space(10)) & .TextMatrix(i, 2), 10)
        Next i
    End With
    
       
    
    Tab1 = 8
    Tab2 = 13
    Tab3 = 48
    Tab4 = 46
    Printer.Print
    With Printer
        Printer.Print Tab(Tab1); "----------------------------------------------------------------------"
        Printer.Print Tab(Tab1); "Gross Total"; Tab(Tab3); Right((Space(10)) & (txtTotal.text), 10)
        If Val(txtDiscount.text) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (txtDiscount.text), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (txtNTotal.text), 10)
        End If
        Printer.Print
        Printer.Print
        Printer.Print Tab(Tab1); UserName
        Printer.Print Tab(Tab1); Format(Time, "hh:mm AMPM")
        Printer.Print Tab(Tab1); Format(Date, "dd MMM yyyy")
        
        DrawBorder
        
        .EndDoc
    End With

    
   
End Sub
