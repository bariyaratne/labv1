VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmMaterialUsage 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Material Usage"
   ClientHeight    =   6120
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5985
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6120
   ScaleWidth      =   5985
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   4800
      TabIndex        =   3
      Top             =   5640
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnPrint 
      Height          =   375
      Left            =   3600
      TabIndex        =   2
      Top             =   5640
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame2 
      Height          =   4095
      Left            =   120
      TabIndex        =   6
      Top             =   1440
      Width           =   5775
      Begin MSFlexGridLib.MSFlexGrid GridMaterial 
         Height          =   3735
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   5535
         _ExtentX        =   9763
         _ExtentY        =   6588
         _Version        =   393216
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1335
      Left            =   120
      TabIndex        =   4
      Top             =   0
      Width           =   5775
      Begin MSComCtl2.DTPicker dtpFrom 
         Height          =   375
         Left            =   1440
         TabIndex        =   7
         Top             =   240
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd MMMM yyyy"
         Format          =   20905987
         CurrentDate     =   39773
      End
      Begin MSComCtl2.DTPicker dtpTo 
         Height          =   375
         Left            =   1440
         TabIndex        =   8
         Top             =   720
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   661
         _Version        =   393216
         CustomFormat    =   "dd MMMM yyyy"
         Format          =   20905987
         CurrentDate     =   39773
      End
      Begin VB.Label Label1 
         Caption         =   "&From"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label4 
         Caption         =   "&To"
         Height          =   255
         Left            =   240
         TabIndex        =   0
         Top             =   720
         Width           =   1575
      End
   End
End
Attribute VB_Name = "frmMaterialUsage"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Dim rsMaterial As New ADODB.Recordset
    Dim cSetDfltPrinter As New cSetDfltPrinter
    
Private Sub FormatGrid()
    With GridMaterial
        .Rows = 1
        
        .Cols = 3
        
        .Row = 0
        
        .Col = 0
        .CellAlignment = 4
        .Text = "No."
        
        .Col = 1
        .CellAlignment = 4
        .Text = "Material"
        
        .Col = 2
        .CellAlignment = 4
        .Text = "Quentity"
        
        .ColWidth(0) = 600
        .ColWidth(2) = 1600
        .ColWidth(1) = .Width - (.ColWidth(0) + .ColWidth(2) + 400)
        
    End With
End Sub

Private Sub FillGrid()
    With rsMaterial
        If .State = 1 Then .Close
        temSql = "SELECT Sum(tblIxMaterial.Quentity) AS TotalMaterialUsage, tblMaterial.Material " & _
                    "FROM ((tblPatientIx RIGHT JOIN tblPatientIxBill ON tblPatientIx.PatientIxBillID = tblPatientIxBill.PatientIxBillID) LEFT JOIN tblIxMaterial ON tblPatientIx.IxID = tblIxMaterial.IxID) LEFT JOIN tblMaterial ON tblIxMaterial.MaterialID = tblMaterial.MaterialID " & _
                    "WHERE (((tblPatientIxBill.Date) Between #" & Format(dtpFrom.Value) & "# And #" & Format(dtpTo.Value) & "#)) " & _
                    "GROUP BY tblMaterial.Material"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            While .EOF = False
                GridMaterial.Rows = GridMaterial.Rows + 1
                GridMaterial.Row = GridMaterial.Rows - 1
                GridMaterial.Col = 0
                GridMaterial.Text = GridMaterial.Row
                If Not IsNull(!Material) Then
                    GridMaterial.Col = 1
                    GridMaterial.CellAlignment = 1
                    GridMaterial.Text = !Material
                End If
                If Not IsNull(!TotalMaterialUsage) Then
                    GridMaterial.Col = 2
                    GridMaterial.Text = !TotalMaterialUsage
                End If
                .MoveNext
            Wend
        End If
    End With
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnPrint_Click()
    Call eLabPrint
End Sub

Private Sub dtpFrom_Change()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub dtpTo_Change()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub Form_Load()
    dtpFrom.Value = Date
    dtpTo.Value = Date
    Call FormatGrid
    Call FillGrid
End Sub
Private Sub eLabPrint()
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    
    Tab1 = 1
    Tab2 = 4
    Tab3 = 20
    Tab4 = 30
    
    With Printer
        .FontSize = 12
        .Font = "Times New Roman"
        Printer.Print
        Printer.Print Tab(Tab1); HospitalName
        .FontSize = 9
        .Font = "Times New Roman"
        Printer.Print Tab(Tab1); HospitalDescreption
        Printer.Print Tab(Tab1); HospitalAddress
        Printer.Print
        Printer.Print Tab(Tab1); "Date : "; Format(Date, "dd MM yy"); Tab(Tab1 + 25); "Time : "; Time;
        Printer.Print Tab(Tab1); "Material Usage"; 'Tab(Tab1 + 25); "Bill No." & TxPatientIxBillID
        Printer.Print Tab(Tab1); "--------------------------------------"
        .FontSize = 10
        .Font = "Lucida Console"
    End With
    
    Tab1 = 1
    Tab2 = 4
    Tab3 = 26
    Tab4 = 30
    
    With GridMaterial
        For i = 1 To .Rows - 1
            Printer.Print Tab(Tab1); .TextMatrix(i, 0); Tab(Tab2); Left(.TextMatrix(i, 1), 22); Tab(Tab3); Right((Space(10)) & .TextMatrix(i, 2), 10)
        Next i
    End With
    Tab1 = 1
    Tab2 = 4
    Tab3 = 42
    Tab4 = 40
    With Printer
        .Font = 12
        Printer.Print Tab(Tab1); "--------------------------------------"
'        Printer.Print Tab(Tab1); "Total Income"; Tab(Tab4); Right((Space(10)) & (txtTIncome.Text), 10)
'        If Val(txtDiscount.Text) > 0 Then
'            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (txtDiscount.Text), 10)
'            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab4); Right((Space(10)) & (txtNTotal.Text), 10)
'        End If
        Printer.Print Tab(Tab1); "--------------------------------------"
        Printer.Print Tab(Tab1); "THANK YOU"
        Printer.Print Tab(Tab1); "--------------------------------------"
        .EndDoc
    End With
End Sub
