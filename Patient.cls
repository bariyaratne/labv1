VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Patient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varPatientID As Long
    Private varPatient As String
    Private varName As String
    Private varSurName As String
    Private varTitleID As Long
    Private varSexID As Long
    Private varRaceID As Long
    Private varMaritalID As Long
    Private varNICNo As String
    Private varBirthCertificateNo As String
    Private varAddress As String
    Private varPhone As String
    Private varFax As String
    Private varEmail As String
    Private varDateOfBirth As Date
    Private varAgeInWords As String
    Private varNotes As String
    Private varPhoto As String
    Private varCredit As Double
    Private varMaxCredit As Double
    Private varBlackListedPatient As Boolean
    Private varRegisteredDate As Date
    Private varDeleted As Boolean
    Private varDeletedUserID As Long
    Private varDeletedDate As Date
    Private varDeletedTime As Date
    Private varComments As String

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatient Where PatientID = " & varPatientID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !Patient = varPatient
        !Name = varName
        !SurName = varSurName
        !TitleID = varTitleID
        !SexID = varSexID
        !RaceID = varRaceID
        !MaritalID = varMaritalID
        !NICNo = varNICNo
        !BirthCertificateNo = varBirthCertificateNo
        !Address = varAddress
        !Phone = varPhone
        !Fax = varFax
        !Email = varEmail
        !DateOfBirth = varDateOfBirth
        !AgeInWords = varAgeInWords
        !Notes = varNotes
        !Photo = varPhoto
        !Credit = varCredit
        !MaxCredit = varMaxCredit
        !BlackListedPatient = varBlackListedPatient
        !RegisteredDate = varRegisteredDate
        !Deleted = varDeleted
        !DeletedUserID = varDeletedUserID
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !Comments = varComments
        .Update
        varPatientID = !PatientID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatient WHERE PatientID = " & varPatientID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!PatientID) Then
               varPatientID = !PatientID
            End If
            If Not IsNull(!Patient) Then
               varPatient = !Patient
            End If
            If Not IsNull(!Name) Then
               varName = !Name
            End If
            If Not IsNull(!SurName) Then
               varSurName = !SurName
            End If
            If Not IsNull(!TitleID) Then
               varTitleID = !TitleID
            End If
            If Not IsNull(!SexID) Then
               varSexID = !SexID
            End If
            If Not IsNull(!RaceID) Then
               varRaceID = !RaceID
            End If
            If Not IsNull(!MaritalID) Then
               varMaritalID = !MaritalID
            End If
            If Not IsNull(!NICNo) Then
               varNICNo = !NICNo
            End If
            If Not IsNull(!BirthCertificateNo) Then
               varBirthCertificateNo = !BirthCertificateNo
            End If
            If Not IsNull(!Address) Then
               varAddress = !Address
            End If
            If Not IsNull(!Phone) Then
               varPhone = !Phone
            End If
            If Not IsNull(!Fax) Then
               varFax = !Fax
            End If
            If Not IsNull(!Email) Then
               varEmail = !Email
            End If
            If Not IsNull(!DateOfBirth) Then
               varDateOfBirth = !DateOfBirth
            End If
            If Not IsNull(!AgeInWords) Then
               varAgeInWords = !AgeInWords
            End If
            If Not IsNull(!Notes) Then
               varNotes = !Notes
            End If
            If Not IsNull(!Photo) Then
               varPhoto = !Photo
            End If
            If Not IsNull(!Credit) Then
               varCredit = !Credit
            End If
            If Not IsNull(!MaxCredit) Then
               varMaxCredit = !MaxCredit
            End If
            If Not IsNull(!BlackListedPatient) Then
               varBlackListedPatient = !BlackListedPatient
            End If
            If Not IsNull(!RegisteredDate) Then
               varRegisteredDate = !RegisteredDate
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varPatientID = 0
    varPatient = Empty
    varName = Empty
    varSurName = Empty
    varTitleID = 0
    varSexID = 0
    varRaceID = 0
    varMaritalID = 0
    varNICNo = Empty
    varBirthCertificateNo = Empty
    varAddress = Empty
    varPhone = Empty
    varFax = Empty
    varEmail = Empty
    varDateOfBirth = Empty
    varAgeInWords = Empty
    varNotes = Empty
    varPhoto = Empty
    varCredit = 0
    varMaxCredit = 0
    varBlackListedPatient = False
    varRegisteredDate = Empty
    varDeleted = False
    varDeletedUserID = 0
    varDeletedDate = Empty
    varDeletedTime = Empty
    varComments = Empty
End Sub

Public Property Let PatientID(ByVal vPatientID As Long)
    Call clearData
    varPatientID = vPatientID
    Call loadData
End Property

Public Property Get PatientID() As Long
    PatientID = varPatientID
End Property

Public Property Let Patient(ByVal vPatient As String)
    varPatient = vPatient
End Property

Public Property Get Patient() As String
    Patient = varPatient
End Property

Public Property Let Name(ByVal vName As String)
    varName = vName
End Property

Public Property Get Name() As String
    Name = varName
End Property

Public Property Let SurName(ByVal vSurName As String)
    varSurName = vSurName
End Property

Public Property Get SurName() As String
    SurName = varSurName
End Property

Public Property Let TitleID(ByVal vTitleID As Long)
    varTitleID = vTitleID
End Property

Public Property Get TitleID() As Long
    TitleID = varTitleID
End Property

Public Property Let SexID(ByVal vSexID As Long)
    varSexID = vSexID
End Property

Public Property Get SexID() As Long
    SexID = varSexID
End Property

Public Property Let RaceID(ByVal vRaceID As Long)
    varRaceID = vRaceID
End Property

Public Property Get RaceID() As Long
    RaceID = varRaceID
End Property

Public Property Let MaritalID(ByVal vMaritalID As Long)
    varMaritalID = vMaritalID
End Property

Public Property Get MaritalID() As Long
    MaritalID = varMaritalID
End Property

Public Property Let NICNo(ByVal vNICNo As String)
    varNICNo = vNICNo
End Property

Public Property Get NICNo() As String
    NICNo = varNICNo
End Property

Public Property Let BirthCertificateNo(ByVal vBirthCertificateNo As String)
    varBirthCertificateNo = vBirthCertificateNo
End Property

Public Property Get BirthCertificateNo() As String
    BirthCertificateNo = varBirthCertificateNo
End Property

Public Property Let Address(ByVal vAddress As String)
    varAddress = vAddress
End Property

Public Property Get Address() As String
    Address = varAddress
End Property

Public Property Let Phone(ByVal vPhone As String)
    varPhone = vPhone
End Property

Public Property Get Phone() As String
    Phone = varPhone
End Property

Public Property Let Fax(ByVal vFax As String)
    varFax = vFax
End Property

Public Property Get Fax() As String
    Fax = varFax
End Property

Public Property Let Email(ByVal vEmail As String)
    varEmail = vEmail
End Property

Public Property Get Email() As String
    Email = varEmail
End Property

Public Property Let DateOfBirth(ByVal vDateOfBirth As Date)
    varDateOfBirth = vDateOfBirth
End Property

Public Property Get DateOfBirth() As Date
    DateOfBirth = varDateOfBirth
End Property

Public Property Let AgeInWords(ByVal vAgeInWords As String)
    varAgeInWords = vAgeInWords
End Property

Public Property Get AgeInWords() As String
    AgeInWords = varAgeInWords
End Property

Public Property Let Notes(ByVal vNotes As String)
    varNotes = vNotes
End Property

Public Property Get Notes() As String
    Notes = varNotes
End Property

Public Property Let Photo(ByVal vPhoto As String)
    varPhoto = vPhoto
End Property

Public Property Get Photo() As String
    Photo = varPhoto
End Property

Public Property Let Credit(ByVal vCredit As Double)
    varCredit = vCredit
End Property

Public Property Get Credit() As Double
    Credit = varCredit
End Property

Public Property Let MaxCredit(ByVal vMaxCredit As Double)
    varMaxCredit = vMaxCredit
End Property

Public Property Get MaxCredit() As Double
    MaxCredit = varMaxCredit
End Property

Public Property Let BlackListedPatient(ByVal vBlackListedPatient As Boolean)
    varBlackListedPatient = vBlackListedPatient
End Property

Public Property Get BlackListedPatient() As Boolean
    BlackListedPatient = varBlackListedPatient
End Property

Public Property Let RegisteredDate(ByVal vRegisteredDate As Date)
    varRegisteredDate = vRegisteredDate
End Property

Public Property Get RegisteredDate() As Date
    RegisteredDate = varRegisteredDate
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let DeletedDate(ByVal vDeletedDate As Date)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate() As Date
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime As Date)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime() As Date
    DeletedTime = varDeletedTime
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property


