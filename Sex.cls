VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Sex"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varSexID As Long
    Private varSex As String

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblSex Where SexID = " & varSexID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !Sex = varSex
        .Update
        varSexID = !SexID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblSex WHERE SexID = " & varSexID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!SexID) Then
               varSexID = !SexID
            End If
            If Not IsNull(!Sex) Then
               varSex = !Sex
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varSexID = 0
    varSex = Empty
End Sub

Public Property Let SexID(ByVal vSexID As Long)
    Call clearData
    varSexID = vSexID
    Call loadData
End Property

Public Property Get SexID() As Long
    SexID = varSexID
End Property

Public Property Let Sex(ByVal vSex As String)
    varSex = vSex
End Property

Public Property Get Sex() As String
    Sex = varSex
End Property


