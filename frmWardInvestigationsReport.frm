VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmWardInvestigationsReport 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ward Investigations Report"
   ClientHeight    =   8835
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13755
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8835
   ScaleWidth      =   13755
   Begin btButtonEx.ButtonEx btnCancel 
      Height          =   495
      Left            =   12360
      TabIndex        =   7
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid gridIx 
      Height          =   5895
      Left            =   120
      TabIndex        =   6
      Top             =   2280
      Width           =   13455
      _ExtentX        =   23733
      _ExtentY        =   10398
      _Version        =   393216
   End
   Begin MSDataListLib.DataCombo cmbUser 
      Height          =   360
      Left            =   1440
      TabIndex        =   5
      Top             =   1200
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1440
      TabIndex        =   3
      Top             =   240
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "yyyy MMMM dd"
      Format          =   102957059
      CurrentDate     =   40447
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   1440
      TabIndex        =   4
      Top             =   720
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "yyyy MMMM dd"
      Format          =   102957059
      CurrentDate     =   40447
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   495
      Left            =   120
      TabIndex        =   8
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   1440
      TabIndex        =   9
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnProcess 
      Height          =   375
      Left            =   6480
      TabIndex        =   10
      Top             =   1680
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "P&rocess"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmbCompany 
      Height          =   360
      Left            =   1440
      TabIndex        =   11
      Top             =   1680
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      Caption         =   "BHT"
      Height          =   240
      Left            =   240
      TabIndex        =   12
      Top             =   1800
      Width           =   345
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "User"
      Height          =   240
      Left            =   240
      TabIndex        =   2
      Top             =   1320
      Width           =   390
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "From"
      Height          =   240
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   450
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "To"
      Height          =   240
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   225
   End
End
Attribute VB_Name = "frmWardInvestigationsReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temTopic As String
    Dim temSubTopic As String
    Dim temSelect As String
    Dim temFrom As String
    Dim temWhere As String
    Dim temOrderBy As String
    Dim temSql As String
    

Private Sub SaveSettings()
    Call SaveCommonSettings(Me)
End Sub

Private Sub GetSettings()
    dtpFrom.Value = Date
    dtpTo.Value = Date
    Call GetCommonSettings(Me)
End Sub

Private Sub btnExcel_Click()
    GridToExcel GridIx, temTopic, temSubTopic
End Sub

Private Sub btnPrint_Click()
    Dim myPR As PrintReport
    
    GetPrintDefaults myPR
    
    GridPrint GridIx, myPR, temTopic, temSubTopic

End Sub

Private Sub btnProcess_Click()
    Dim d(4) As Integer
    Dim p(1) As Integer
    
    temTopic = "Ward Investigations"
    If dtpFrom.Value = dtpTo.Value Then
        temSubTopic = "On " & Format(dtpFrom.Value, "dd MMMM yyyy")
    Else
        temSubTopic = "From " & Format(dtpFrom.Value, "dd MMMM yyyy") & " To " & Format(dtpTo.Value, "dd MMMM yyyy")
    End If
    temSelect = "SELECT tblPatientIxBill.PatientIxBillID AS [Bill No], "
    p(0) = 0
    If dtpFrom.Value <> dtpTo.Value Then
        temSelect = temSelect & " format(tblPatientIxBill.Date, 'yyyy MMM dd') as [Bill Date], "
        d(0) = 3: d(1) = 4: d(2) = 5: d(3) = 6
    Else
        d(0) = 3: d(1) = 3: d(2) = 4: d(3) = 5
    End If
    
    temSelect = temSelect & " tblIx.Ix AS Investigation, Format(tblPatientIx.HospitalFee,'Fixed') AS [Hospital Fee], Format(tblPatientIx.StaffFee,'Fixed') AS [MLT Fee], Format(tblPatientIx.OtherFee,'Fixed') AS [Reagent Fee], Format(tblPatientIx.Value,'Fixed') AS Total "
    
    temFrom = "FROM (tblPatientIxBill LEFT JOIN tblPatientIx ON tblPatientIxBill.PatientIxBillID = tblPatientIx.PatientIxBillID) LEFT JOIN tblIx ON tblPatientIx.IxID = tblIx.IxID "
    temWhere = "WHERE (((tblPatientIxBill.Cancelled)=False) AND ((tblPatientIxBill.Date) Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# And #" & Format(dtpTo.Value, "dd MMMM yyyy") & "#) "
    If IsNumeric(cmbUser.BoundText) = True Then
        temWhere = temWhere & " AND ((tblPatientIxBill.UserID)=" & Val(cmbUser.BoundText) & ") "
        temSubTopic = temSubTopic & " User - " & cmbUser.Text
    End If
    If IsNumeric(cmbCompany.BoundText) = True Then
        temWhere = temWhere & " AND ((tblPatientIxBill.BHTID)=" & Val(cmbCompany.BoundText) & ") "
        temSubTopic = temSubTopic & " BHT - " & cmbCompany.Text
    End If
    
    temWhere = temWhere & " AND ((tblPatientIxBill.CompanyID)= 0) AND ((tblPatientIxBill.BHTID)<> 0) AND ((tblPatientIx.Deleted)=False)) "
    temOrderBy = "ORDER BY tblPatientIxBill.PatientIxBillID, tblPatientIx.PatientIxID "
    temSql = temSelect & temFrom & temWhere & temOrderBy
    FillAnyGrid temSql, GridIx, 1, d, p
End Sub

Private Sub cmbUser_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbUser.Text = Empty
    End If
End Sub

Private Sub cmbCompany_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbCompany.Text = Empty
    End If
End Sub



Private Sub Form_Load()
    Call FillCombos
    
    Call GetSettings
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call SaveSettings
End Sub

Private Sub FillCombos()
    Dim rsStaff As New clsFillCombos
    rsStaff.FillSpecificField cmbUser, "Staff", "Name", False
    Dim rsCompany As New clsFillCombos
    rsCompany.FillAnyCombo cmbCompany, "BHT", False
End Sub
