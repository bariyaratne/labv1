Attribute VB_Name = "modPrintBill"
Option Explicit
Dim cSetDfltPrinter As New cSetDfltPrinter


'cdlPDAllPages             &H0         Returns or sets the state of the All
'                                      Pages option button.
'
'cdlPDCollate              &H10        Returns or sets the state of the
'                                      Collate check box.
'
'cdlPDDisablePrintToFile   &H80000     Disables the Print To File check box.
'
'cdlPDHelpButton           &H800       Causes the dialog box to display the
'                                      Help button.
'
'cdlPDHidePrintToFile      &H100000    Hides the Print To File check box.
'
'cdlPDNoPageNums           &H8         Disables the Pages option button and
'                                      the associated edit control.
'
'cdlPDNoSelection          &H4         Disables the Selection option button.
'
'cdlPDNoWarning            &H80        Prevents a warning message from being
'                                      displayed when there is no default
'                                      printer.
'
'cdlPDPageNums             &H2         Returns or sets the state of the
'                                      Pages option button.
'
'cdlPDPrintSetup           &H40        Causes the system to display the
'                                      Print Setup; dialog; box; instead; of
'                                      the Print dialog box.
'
'cdlPDPrintToFile          &H20        Returns or sets the state of the
'                                      Print To File check box.
'
'cdlPDReturnDC             &H100       Returns a device context for the
'                                      printer selection made in the dialog
'                                      box. The device context is returned
'                                      in the hDC property of the dialog
'                                      box.
'
'cdlPDReturnDefault        &H400       Returns the default printer name.
'
'cdlPDReturnIC             &H200       Returns an information context for
'                                      the printer selection made in the
'                                      dialog box. An information context
'                                      provides a fast way to get
'                                      information about the device without
'                                      creating a device context. The
'                                      information context is returned in
'                                      the hDC property of the dialog box.
'
'cdlPDSelection            &H1         Returns or sets the state of the
'                                      Selection option button. If neither
'                                      cdlPDPageNums nor cdlPDSelection is
'                                      specified, the All option button is
'                                      in the selected state.
'
'cdlPDUseDevModeCopies     &H40000     If a printer driver does not support
'                                      multiple copies, setting this flag
'                                      disables the Number of Copies spinner
'                                      control in the Print dialog box. If a
'                                      driver does support multiple copies,
'                                      setting this flag indicates that the
'                                      dialog box stores the requested
'                                      number of copies in the Copies
'                                      property.



Private Const CCHDEVICENAME As Long = 32
Private Const CCHFORMNAME As Long = 32
Private Const GMEM_MOVEABLE As Long = &H2
Private Const GMEM_ZEROINIT As Long = &H40
Private Const DM_DUPLEX As Long = &H1000&
Private Const DM_ORIENTATION As Long = &H1&

Private Type PRINTDLG_TYPE
    lStructSize As Long
    hWndOwner As Long
    hDevMode As Long
    hDevNames As Long
    hdc As Long
    Flags As Long
    nFromPage As Integer
    nToPage As Integer
    nMinPage As Integer
    nMaxPage As Integer
    nCopies As Integer
    hInstance As Long
    lCustData As Long
    lpfnPrintHook As Long
    lpfnSetupHook As Long
    lpPrintTemplateName As String
    lpSetupTemplateName As String
    hPrintTemplate As Long
    hSetupTemplate As Long
End Type

Private Type DEVNAMES_TYPE
    wDriverOffset As Integer
    wDeviceOffset As Integer
    wOutputOffset As Integer
    wDefault As Integer
    extra As String * 100
End Type

Private Type DEVMODE_TYPE
    dmDeviceName As String * CCHDEVICENAME
    dmSpecVersion As Integer
    dmDriverVersion As Integer
    dmSize As Integer
    dmDriverExtra As Integer
    dmFields As Long
    dmOrientation As Integer
    dmPaperSize As Integer
    dmPaperLength As Integer
    dmPaperWidth As Integer
    dmScale As Integer
    dmCopies As Integer
    dmDefaultSource As Integer
    dmPrintQuality As Integer
    dmColor As Integer
    dmDuplex As Integer
    dmYResolution As Integer
    dmTTOption As Integer
    dmCollate As Integer
    dmFormName As String * CCHFORMNAME
    dmUnusedPadding As Integer
    dmBitsPerPel As Integer
    dmPelsWidth As Long
    dmPelsHeight As Long
    dmDisplayFlags As Long
    dmDisplayFrequency As Long
End Type

Private Declare Function PrintDialog Lib "comdlg32.dll" Alias "PrintDlgA" (pPrintdlg As PRINTDLG_TYPE) As Long
Private Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (hpvDest As Any, hpvSource As Any, ByVal cbCopy As Long)
Private Declare Function GlobalLock Lib "kernel32" (ByVal hMem As Long) As Long
Private Declare Function GlobalUnlock Lib "kernel32" (ByVal hMem As Long) As Long
Private Declare Function GlobalAlloc Lib "kernel32" (ByVal wFlags As Long, ByVal dwBytes As Long) As Long
Private Declare Function GlobalFree Lib "kernel32" (ByVal hMem As Long) As Long

Public Sub ShowPrinter(frmOwner As Form, Optional PrintFlags As Long)

Dim PrintDlg As PRINTDLG_TYPE
Dim DEVMODE As DEVMODE_TYPE
Dim DevName As DEVNAMES_TYPE

Dim lpDevMode As Long
Dim lpDevName As Long
Dim bReturn As Integer
Dim objPrinter As Printer
Dim NewPrinterName As String

' Use PrintDialog to get the handle to a memory
' block with a DevMode and DevName structures

PrintDlg.lStructSize = Len(PrintDlg)
PrintDlg.hWndOwner = frmOwner.hwnd

PrintDlg.Flags = PrintFlags

On Error Resume Next
'Set the current orientation and duplex setting
DEVMODE.dmDeviceName = Printer.DeviceName
DEVMODE.dmSize = Len(DEVMODE)
DEVMODE.dmFields = DM_ORIENTATION Or DM_DUPLEX
DEVMODE.dmPaperWidth = Printer.Width
DEVMODE.dmOrientation = Printer.Orientation
DEVMODE.dmPaperSize = Printer.PaperSize
DEVMODE.dmDuplex = Printer.Duplex

On Error GoTo ErrorHandler

'Allocate memory for the initialization hDevMode structure
'and copy the settings gathered above into this memory
PrintDlg.hDevMode = GlobalAlloc(GMEM_MOVEABLE Or GMEM_ZEROINIT, Len(DEVMODE))
lpDevMode = GlobalLock(PrintDlg.hDevMode)

If lpDevMode > 0 Then
    CopyMemory ByVal lpDevMode, DEVMODE, Len(DEVMODE)
    bReturn = GlobalUnlock(PrintDlg.hDevMode)
End If

'Set the current driver, device, and port name strings
With DevName
    .wDriverOffset = 8
    .wDeviceOffset = .wDriverOffset + 1 + Len(Printer.DriverName)
    .wOutputOffset = .wDeviceOffset + 1 + Len(Printer.Port)
    .wDefault = 0
End With

With Printer
    DevName.extra = .DriverName & vbNullChar & .DeviceName & vbNullChar & .Port & vbNullChar
End With

'Allocate memory for the initial hDevName structure
'and copy the settings gathered above into this memory
PrintDlg.hDevNames = GlobalAlloc(GMEM_MOVEABLE Or GMEM_ZEROINIT, Len(DevName))
lpDevName = GlobalLock(PrintDlg.hDevNames)

If lpDevName > 0 Then
    CopyMemory ByVal lpDevName, DevName, Len(DevName)
    bReturn = GlobalUnlock(lpDevName)
End If

If PrintDialog(PrintDlg) <> 0 Then
    lpDevName = GlobalLock(PrintDlg.hDevNames)
    CopyMemory DevName, ByVal lpDevName, 45
    bReturn = GlobalUnlock(lpDevName)
    GlobalFree PrintDlg.hDevNames
    
    'Next get the DevMode structure and set the printer
    'properties appropriately
    lpDevMode = GlobalLock(PrintDlg.hDevMode)
    CopyMemory DEVMODE, ByVal lpDevMode, Len(DEVMODE)
    bReturn = GlobalUnlock(PrintDlg.hDevMode)
    GlobalFree PrintDlg.hDevMode
    NewPrinterName = UCase$(Left(DEVMODE.dmDeviceName, InStr(DEVMODE.dmDeviceName, vbNullChar) - 1))
    
    If Printer.DeviceName <> NewPrinterName Then
        For Each objPrinter In Printers
            If UCase$(objPrinter.DeviceName) = NewPrinterName Then
                Set Printer = objPrinter
            End If
        Next objPrinter
    End If

    On Error Resume Next
    Printer.Copies = DEVMODE.dmCopies
    Printer.Duplex = DEVMODE.dmDuplex
    Printer.Orientation = DEVMODE.dmOrientation
    Printer.PaperSize = DEVMODE.dmPaperSize
    Printer.PrintQuality = DEVMODE.dmPrintQuality
    Printer.ColorMode = DEVMODE.dmColor
    Printer.PaperBin = DEVMODE.dmDefaultSource
End If

Exit Sub
ErrorHandler:

End Sub


Public Sub printTextInCentre(PringintWord As String, FontSize As Integer, LeftXDis As Long)
    Printer.Font.Size = FontSize
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(PringintWord) / 2) + LeftXDis
    Printer.Print PringintWord
End Sub

Public Sub printLabBillPos(BillID As Long, PrinterName As String, PaperName As String, formHdc, MyForm As Form, Optional copyNo As Integer)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyBill As New PatientIxBill
    Dim MyPatient As New Patient
    Dim myPtIx As New PatientIx
    Dim myBillItem As New PatientIxItem
    Dim MyIx As New Ix
    Dim myTitle As New Title
    Dim myStaff As New Staff
    Dim myLab As New LabDetails
    Dim myDoc As New Doctor
    Dim mySex As New Sex
    Dim MyPt As New clsPatient
    
    
    
    MyBill.PatientIxBillID = BillID
    MyPatient.PatientID = MyBill.PatientID
    myTitle.TitleID = MyPatient.TitleID
    myLab.LabID = 1
    myStaff.StaffID = MyBill.UserID
    myDoc.DoctorID = MyBill.DoctorID
    mySex.SexID = MyPatient.SexID
    MyPt.ID = MyBill.PatientID
    
    
    
    csetPrinter.SetPrinterAsDefault PrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = PrinterName Then
            Set Printer = myP
        End If
    Next
    
    csetPrinter.SetPrinterAsDefault PrinterName
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer

    
    Printer.Font = "Courier New"
    
    Dim temStrCopy As String
    
    Printer.ForeColor = vbRed
    
    
    Dim LeftXDis As Long
    LeftXDis = -500
    LeftXDis = 0
    
    Printer.Font.Size = 14
    Printer.Font.Bold = True
    Printer.ForeColor = vbRed
    
    
    Dim pos As Integer
    Dim headings() As String
    Dim sizes() As String
    
    Printer.Font = DecreptedWord(myLab.RegNo)

    headings = Split(DecreptedWord(myLab.Description), vbCrLf, , vbTextCompare)
    sizes = Split(DecreptedWord(myLab.Name), vbCrLf, , vbTextCompare)
    
    pos = 0

    Do While pos < UBound(headings)
        printTextInCentre headings(pos), Val(sizes(pos)), LeftXDis
        pos = pos + 1
    Loop
    
    
    
    
    
    Printer.ForeColor = vbBlack
'    Printer.Print
    
    Printer.FontSize = 11
    Dim line As String
    line = "----------------------------------"
    
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(line) / 2) + LeftXDis
    Printer.Print line
    
    
    Printer.FontSize = 12
    Printer.Font.Bold = True
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Laboratory Receipt") / 2) + LeftXDis
    Printer.Print "Laboratory Receipt"
    Printer.Font.Bold = False
    
    
    Printer.FontSize = 10
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(line) / 2) + LeftXDis
    Printer.Print line
    
    
    
    Dim displacement As Integer
    
    displacement = -5
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 10
    
'    Printer.Print
    'Printer.FontName = "Lucida Console"
    
    Printer.Print Tab(Tab1); "Name      : " & myTitle.Title & " " & MyPatient.Name
    Printer.Print Tab(Tab1); "Age / Sex : " & MyPt.AgeInWords & " / " & MyPt.Sex
    Printer.Print Tab(Tab1); "Ref. By   : " & myDoc.Doctor
    'Printer.Print Tab(Tab1); "Phone No   : " & MyPatient.Phone
    Printer.Print Tab(Tab1); "Date/Time : " & Format(MyBill.billDate, "dd MMM yyyy") & " - " & Format(MyBill.Time, "hh:mm AMPM")
    Printer.Print Tab(Tab1); "Ref. No.  : " & MyBill.PatientIxBillID
    
    
 '   Printer.Print
       
    Printer.FontSize = 11
    Printer.FontBold = False
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(line) / 2) + LeftXDis
    Printer.Print line
    
    displacement = -8
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 44 + displacement
    Tab4 = 34 + displacement
    
    
  
    Printer.FontName = DecreptedWord(myLab.Telephone1)
    Printer.Font.Size = 10
    
    Printer.Print Tab(Tab1); Left("Test", 210); Tab(Tab3);
    Printer.Print Right((Space(7)) & "Amount", 7)
    
    
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "Select * from tblPatientIx where deleted=false and patientIxBillId = " & MyBill.PatientIxBillID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            myPtIx.PatientIxID = !PatientIxID
            MyIx.IxID = !IxID
            Printer.Print Tab(Tab1); Left(MyIx.Ix, 210); Tab(Tab3);
            Printer.Print Right((Space(7)) & Format(myPtIx.Value, "0.00"), 7)
            .MoveNext
        Wend
        .Close
    End With
    
    'Tab1 = 8 + displacement
    'Tab2 = 13 + displacement
    'Tab3 = 30 + displacement
    'Tab4 = 44 + displacement
    With Printer
        
        'Printer.Print Tab(Tab1); MyLine
        Printer.Font.Size = 10
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(line) / 2) + LeftXDis
        Printer.Print line
    
        
        Printer.Font.Size = 12
        
        Printer.FontBold = False
        If copyNo <> 2 Then
            Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3 - 8); Right((Space(10)) & (Format(MyBill.Value, "0.00")), 10)
        Else
            Printer.Print
        End If
        
        Printer.FontBold = False
        
        
        If Val(MyBill.Discount) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Discount, "0.00")), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.NetValue)), 10)
        End If
        
    Printer.FontSize = 10
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(line) / 2) + LeftXDis
    Printer.Print line
        
        Printer.Font.Size = 9
    
        Printer.Print Tab(Tab1); "User : " & myStaff.Name

        Printer.Print

        Printer.FontSize = 12
        Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Thank you") / 2) + LeftXDis
        Printer.Print "Thank you"

        
       ' Printer.Print
        
        .EndDoc
    End With
End Sub



Public Sub printLabBillDouble(BillID As Long, PrinterName As String, PaperName As String, formHdc)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyBill As New PatientIxBill
    Dim MyPatient As New Patient
    Dim myPtIx As New PatientIx
    Dim myBillItem As New PatientIxItem
    Dim MyIx As New Ix
    Dim myTitle As New Title
    Dim myStaff As New Staff
    Dim myLab As New LabDetails
    
    MyBill.PatientIxBillID = BillID
    MyPatient.PatientID = MyBill.PatientID
    myTitle.TitleID = MyPatient.TitleID
    myLab.LabID = 1
    myStaff.StaffID = MyBill.UserID
    
    csetPrinter.SetPrinterAsDefault PrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = PrinterName Then
            Set Printer = myP
        End If
    Next
    
    If SelectForm(PaperName, formHdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer


    Printer.Font = "Courier New"
    
    Printer.FontSize = 15
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Name)) / 2)
    Printer.Print DecreptedWord(myLab.Name)

    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.RegNo)) / 2)
    Printer.Print DecreptedWord(myLab.RegNo)

    
    Printer.FontSize = 13
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Address)) / 2)
    Printer.Print DecreptedWord(myLab.Address)
    
    Printer.FontSize = 12
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Telephone1)) / 2)
    Printer.Print DecreptedWord(myLab.Telephone1)
    
    Printer.FontSize = 11
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Email)) / 2)
    Printer.Print DecreptedWord(myLab.Email)
    
    
    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Laboratory Receipt") / 2)
    Printer.Print "Laboratory Receipt"

    Dim displacement As Integer
    
    displacement = -6
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 11
    
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & myTitle.Title & " " & MyPatient.Name
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.Time, "hh:mm AMPM")) + 740)
    Printer.Print Format(MyBill.Time, "hh:mm AMPM")
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Bill No.     : " & MyBill.PatientIxBillID
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.billDate, "dd MMM yyyy")) + 740)
    Printer.Print Format(MyBill.billDate, "dd MMM yyyy")
    
    
    
    
    Printer.Print
       
    Printer.FontSize = 10
    Printer.FontBold = False
    
    Dim MyLine As String
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 50 + displacement
    Tab4 = 36 + displacement
    
    
    'Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    'Printer.Print Tab(Tab1); MyLine

    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "Select * from tblPatientIx where deleted=false and patientIxBillId = " & MyBill.PatientIxBillID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            myPtIx.PatientIxID = !PatientIxID
            MyIx.IxID = !IxID
            Printer.Print Tab(Tab1); Left(MyIx.IxDisplay, 38); Tab(Tab3); Right((Space(10)) & Format(myPtIx.Value, "0.00"), 10)
            Printer.Print
            .MoveNext
        Wend
        .Close
    End With
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 40 + displacement
    Tab4 = 46 + displacement
    With Printer
        Printer.Print Tab(Tab1); MyLine
        Printer.FontBold = False
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Value, "0.00")), 10)
        Printer.FontBold = False
        
        
        If Val(MyBill.Discount) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Discount, "0.00")), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.NetValue)), 10)
        End If
        Printer.Print
        
        Printer.Print Tab(Tab1); DecreptedWord(myStaff.UserName)
        
        
        Printer.Print
        Printer.Print
        
        Printer.Font = "Courier New"
    
    Printer.FontSize = 15
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Name)) / 2)
    Printer.Print DecreptedWord(myLab.Name)

    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.RegNo)) / 2)
    Printer.Print DecreptedWord(myLab.RegNo)

    
    Printer.FontSize = 13
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Address)) / 2)
    Printer.Print DecreptedWord(myLab.Address)
    
    Printer.FontSize = 12
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Telephone1)) / 2)
    Printer.Print DecreptedWord(myLab.Telephone1)
    
    Printer.FontSize = 11
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Email)) / 2)
    Printer.Print DecreptedWord(myLab.Email)
    
    
    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Laboratory Receipt") / 2)
    Printer.Print "Laboratory Receipt"

    
    displacement = -6
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    

    Printer.FontSize = 11
    
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & myTitle.Title & " " & MyPatient.Name
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Phone No  : " & MyPatient.Phone
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.Time, "hh:mm AMPM")) + 740)
    Printer.Print Format(MyBill.Time, "hh:mm AMPM")
    
    temY = Printer.CurrentY
    Printer.Print Tab(Tab1); "Bill No.     : " & MyBill.PatientIxBillID
    Printer.CurrentY = temY
    Printer.CurrentX = (Printer.Width) - (Printer.TextWidth(Format(MyBill.billDate, "dd MMM yyyy")) + 740)
    Printer.Print Format(MyBill.billDate, "dd MMM yyyy")
    
    
    
    
    Printer.Print
       
    Printer.FontSize = 10
    Printer.FontBold = False
    
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 50 + displacement
    Tab4 = 36 + displacement
    
    End With
    
    
    
    'Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    'Printer.Print Tab(Tab1); MyLine

    With rsTem
        temSQL = "Select * from tblPatientIx where deleted=false and patientIxBillId = " & MyBill.PatientIxBillID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            myPtIx.PatientIxID = !PatientIxID
            MyIx.IxID = !IxID
            Printer.Print Tab(Tab1); Left(MyIx.IxDisplay, 38); Tab(Tab3); Right((Space(10)) & Format(myPtIx.Value, "0.00"), 10)
            Printer.Print
            .MoveNext
        Wend
        .Close
    End With
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 40 + displacement
    Tab4 = 46 + displacement
    With Printer
        Printer.Print Tab(Tab1); MyLine
        Printer.FontBold = False
        Printer.Print Tab(Tab1); "Total Amount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Value, "0.00")), 10)
        Printer.FontBold = False
        
        
        If Val(MyBill.Discount) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.Discount, "0.00")), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (Format(MyBill.NetValue)), 10)
        End If
        Printer.Print
        
        Printer.Print Tab(Tab1); DecreptedWord(myStaff.UserName)
        
        
        
        
        
        .EndDoc
    End With
    
    
End Sub




Public Sub printLabBillImpact(BillID As Long, PrinterName As String, PaperName As String, formHdc)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyBill As New PatientIxBill
    Dim MyPatient As New Patient
    Dim temPt As New clsPatient
    Dim myPtIx As New PatientIx
    Dim myBillItem As New PatientIxItem
    Dim MyIx As New Ix
    Dim myTitle As New Title
    Dim myStaff As New Staff
    Dim myLab As New LabDetails
    
    MyBill.PatientIxBillID = BillID
    MyPatient.PatientID = MyBill.PatientID
    temPt.ID = MyBill.PatientID
    
    myTitle.TitleID = MyPatient.TitleID
    myLab.LabID = 1
    myStaff.StaffID = MyBill.UserID
    
    csetPrinter.SetPrinterAsDefault PrinterName
    
    Dim myP As Printer
    
    For Each myP In Printers
        If myP.DeviceName = PrinterName Then
            Set Printer = myP
        End If
    Next
    
    If SelectForm(PaperName, formHdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer


    Printer.Font = "Courier New"
    
 
    Dim temTxt As String
    
   
    
    temTxt = DecreptedWord(myLab.Name)
    

    Printer.FontSize = 15
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(temTxt) / 2)
    Printer.Print 'temTxt
    
    Printer.FontSize = 13
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.RegNo)) / 2)
    Printer.Print 'DecreptedWord(myLab.RegNo)
    
    Printer.FontSize = 13
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Address)) / 2)
    Printer.Print 'DecreptedWord(myLab.Address)
    
'    Printer.FontSize = 12
'    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Telephone1)) / 2)
'    Printer.Print 'DecreptedWord(myLab.Telephone1)
'
'    Printer.FontSize = 11
'    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth(DecreptedWord(myLab.Email)) / 2)
'    Printer.Print  'DecreptedWord(myLab.Email)
    
    
    Printer.FontSize = 14
    Printer.CurrentX = (Printer.Width / 2) - (Printer.TextWidth("Laboratory Receipt") / 2) - (1440 / 2)
    Printer.Print "Laboratory Receipt"
    
    Dim displacement As Integer
    displacement = -8
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 52 + displacement
    Tab4 = 46 + displacement
    Tab5 = 46 + displacement
    tab6 = 7 + displacement
    tab7 = 9 + displacement
    tab8 = 6 + displacement
    tab9 = 43 + displacement
    
    Dim temY As Double

    Printer.FontSize = 11
    
    Printer.Print
    
    Printer.Print Tab(Tab1); "Patient Name : " & myTitle.Title & " " & MyPatient.Name
    Printer.Print Tab(Tab1); "Age          : " & temPt.AgeInWords
    Printer.Print Tab(Tab1); "Bill No.     : " & MyBill.PatientIxBillID
    Printer.Print Tab(Tab1); "Billed At    : " & Format(MyBill.billDate, "dd MMM yyyy") & " / " & Format(MyBill.Time, "hh:mm AMPM")
    
    
    
    
    Printer.Print
       
    Printer.FontSize = 10
    Printer.FontBold = False
    
    Dim MyLine As String
    MyLine = ""   ' "__________________________________________________"
    Printer.Print Tab(Tab1); MyLine
    
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 44 + displacement
    Tab4 = 36 + displacement
    
    
    'Printer.Print Tab(Tab1); "Test Type"; Tab(Tab3); Right(Space(10) & "Amount", 10)
    'Printer.Print Tab(Tab1); MyLine

    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "Select * from tblPatientIx where deleted=false and patientIxBillId = " & MyBill.PatientIxBillID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            myPtIx.PatientIxID = !PatientIxID
            MyIx.IxID = !IxID
            Printer.Print Tab(Tab1); Left(MyIx.IxDisplay, 38); Tab(Tab3); Right((Space(10)) & Format(myPtIx.Value, "0.00"), 10)
            'Printer.Print
            .MoveNext
        Wend
        .Close
    End With
    
    Tab1 = 8 + displacement
    Tab2 = 13 + displacement
    Tab3 = 64 + displacement
    Tab4 = 46 + displacement
    
    With Printer
    
        Printer.Print Tab(Tab1); MyLine
        Printer.FontBold = False
        Printer.Print Tab(Tab1); "Total Amount   : "; Right(Space(10) & Format(MyBill.Value, "0.00"), 10)
        Printer.FontBold = False
        
        
        If Val(MyBill.Discount) > 0 Then
        
            Printer.Print Tab(Tab1); "Discount       : "; Right(Space(10) & Format(MyBill.Discount, "0.00"), 10)
            Printer.Print Tab(Tab1); "Net Total      : "; Right(Space(10) & Format(MyBill.NetValue, "0.00"), 10)
            
        End If
        
        Printer.Print
        
        Printer.Print Tab(Tab1); DecreptedWord(myStaff.UserName)
        
        .EndDoc
    End With
End Sub

