Attribute VB_Name = "ModuleVariables"
Option Explicit
    Public ProgramVariable As New clsVariables
    
    'API
    Public Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lparam As Any) As Long
    Public Const LB_SETTABSTOPS = &H192
    
    
    ' database Variables
    Public Database As String
    Public DatabaseLocation As String
    Public cnnLab As New ADODB.Connection
    
    
    ' User Variables
    Public UserName As String
    Public UserID As Long
    Public UserPositionID As Long
    Public UserPosition As String
    Public UserDeptID As Long
    Public UserDept As String
    Public UserStaffID As Long
    
    
    ' Data Exchange Between forms
    Public OrderBillID As Long
    Public TxSaleBillID As Long
    Public TxPatientIxBillID As Long
    
    ' Store Variables
    Public HospitalName As String
    Public HospitalDescreption As String
    Public HospitalAddress As String
    Public Telephone1 As String
    Public Telephone2 As String
    Public Fax As Long
    Public Email As String
    Public Web As String
    Public LongAd As String
    Public ShortAd As String
    
    ' Printing Preferances
    Public BillPrinterName As String
    Public BillPaperName As String
    Public ReportPrinterName As String
    Public ReportPaperName As String
    Public ReportPaperHeight As Long
    Public ReportPaperWidth As Long
    Public BillPaperHeight As Long
    Public BillPaperWidth As Long
    
    
    ' Program Preferances
    Public HighRate As Integer
    Public LongDateFormat As String
    Public ShortDateFormat As String
    
    ' ***************** Printing
    Public MyPrintObject As Object
