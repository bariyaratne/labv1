VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmIxListsTime 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Investigation Lists By Time"
   ClientHeight    =   8520
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   14340
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8520
   ScaleWidth      =   14340
   Begin MSComCtl2.DTPicker dtpTime 
      Height          =   375
      Left            =   1440
      TabIndex        =   34
      Top             =   600
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   661
      _Version        =   393216
      Format          =   103153666
      CurrentDate     =   41545
   End
   Begin VB.CheckBox chkPrint 
      Caption         =   "Print"
      Height          =   195
      Left            =   120
      TabIndex        =   21
      Top             =   7560
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   1815
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   4440
      Style           =   2  'Dropdown List
      TabIndex        =   20
      Top             =   7320
      Width           =   3255
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   4440
      Style           =   2  'Dropdown List
      TabIndex        =   19
      Top             =   6960
      Width           =   3255
   End
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   2760
      TabIndex        =   16
      Top             =   7680
      Width           =   4935
      Begin VB.OptionButton optLandscape 
         Caption         =   "Landscape"
         Height          =   240
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.OptionButton optPotrate 
         Caption         =   "Potrate"
         Height          =   240
         Left            =   2400
         TabIndex        =   17
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.OptionButton optByID 
      Caption         =   "By Time"
      Height          =   255
      Left            =   9000
      TabIndex        =   15
      Top             =   840
      Width           =   1815
   End
   Begin VB.OptionButton optByIx 
      Caption         =   "By Investigation"
      Height          =   255
      Left            =   9000
      TabIndex        =   14
      Top             =   480
      Width           =   1695
   End
   Begin VB.OptionButton optByPatient 
      Caption         =   "By Patient"
      Height          =   255
      Left            =   9000
      TabIndex        =   13
      Top             =   120
      Value           =   -1  'True
      Width           =   1695
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   375
      Left            =   12960
      TabIndex        =   10
      Top             =   8040
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid gridIx 
      Height          =   4815
      Left            =   120
      TabIndex        =   9
      Top             =   2040
      Width           =   14055
      _ExtentX        =   24791
      _ExtentY        =   8493
      _Version        =   393216
      WordWrap        =   -1  'True
      FocusRect       =   0
      SelectionMode   =   1
      AllowUserResizing=   1
   End
   Begin MSDataListLib.DataCombo cmbDept 
      Height          =   360
      Left            =   1440
      TabIndex        =   8
      Top             =   1080
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1440
      TabIndex        =   6
      Top             =   120
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   102432771
      CurrentDate     =   39812
   End
   Begin VB.CheckBox chkToHadnover 
      Caption         =   "Handed-over to patient"
      Height          =   255
      Left            =   6000
      TabIndex        =   5
      Top             =   840
      Width           =   3015
   End
   Begin VB.CheckBox chkPrinted 
      Caption         =   "Printouts taken"
      Height          =   255
      Left            =   6000
      TabIndex        =   4
      Top             =   480
      Width           =   1815
   End
   Begin VB.CheckBox chkToPrint 
      Caption         =   "To Take Printouts"
      Height          =   255
      Left            =   6000
      TabIndex        =   3
      Top             =   120
      Value           =   1  'Checked
      Width           =   1815
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   10920
      TabIndex        =   7
      Top             =   120
      Visible         =   0   'False
      Width           =   2775
      _ExtentX        =   4895
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "dd MMMM yyyy"
      Format          =   98369539
      CurrentDate     =   39812
   End
   Begin btButtonEx.ButtonEx btnPrintList 
      Height          =   375
      Left            =   6360
      TabIndex        =   11
      Top             =   1560
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Print List"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnIx 
      Height          =   375
      Left            =   12840
      TabIndex        =   12
      Top             =   1560
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Report"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo cmbStaff 
      Height          =   360
      Left            =   1440
      TabIndex        =   32
      Tag             =   "SS"
      Top             =   1560
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   375
      Left            =   5040
      TabIndex        =   35
      Top             =   1560
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label5 
      Caption         =   "MLT:"
      Height          =   255
      Left            =   120
      TabIndex        =   33
      Top             =   1560
      Width           =   1215
   End
   Begin VB.Label Label10 
      Caption         =   "Reagent Fee"
      Height          =   255
      Left            =   7800
      TabIndex        =   31
      Top             =   7680
      Width           =   1575
   End
   Begin VB.Label lblReagentFee 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      Height          =   255
      Left            =   9840
      TabIndex        =   30
      Top             =   7680
      Width           =   1575
   End
   Begin VB.Label Label8 
      Caption         =   "MLT Fee"
      Height          =   255
      Left            =   7800
      TabIndex        =   29
      Top             =   7320
      Width           =   1575
   End
   Begin VB.Label lblMLTFee 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      Height          =   255
      Left            =   9840
      TabIndex        =   28
      Top             =   7320
      Width           =   1575
   End
   Begin VB.Label Label6 
      Caption         =   "Hospital Fee"
      Height          =   255
      Left            =   7800
      TabIndex        =   27
      Top             =   6960
      Width           =   1575
   End
   Begin VB.Label lblHospitalFee 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      Height          =   255
      Left            =   9840
      TabIndex        =   26
      Top             =   6960
      Width           =   1575
   End
   Begin VB.Label lblTotal 
      Alignment       =   1  'Right Justify
      Caption         =   "0.00"
      Height          =   255
      Left            =   9840
      TabIndex        =   25
      Top             =   8160
      Width           =   1575
   End
   Begin VB.Label Label4 
      Caption         =   "Total Value"
      Height          =   255
      Left            =   7800
      TabIndex        =   24
      Top             =   8160
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "Paper"
      Height          =   255
      Left            =   2760
      TabIndex        =   23
      Top             =   7320
      Width           =   1575
   End
   Begin VB.Label Label12 
      Caption         =   "Printer"
      Height          =   375
      Left            =   2760
      TabIndex        =   22
      Top             =   6960
      Width           =   1575
   End
   Begin VB.Label Label3 
      Caption         =   "Department :"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Time :"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "From :"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "frmIxListsTime"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim NumForms As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim SuppliedWord As String
    Dim FSys As New Scripting.FileSystemObject
    Private csetPrinter As New cSetDfltPrinter
    
    Dim rsStaff As New ADODB.Recordset
    
    Dim temSQL As String
    Dim rsIxList As New ADODB.Recordset
    Dim i As Integer
    Public temReportID As Long
    Public temPatientIxID As Long
    
Private Sub Process()
    Call FormatGrid
    Call FillGrid
End Sub

Private Sub FillCombos()
    Dim Dept As New clsFillCombos
    Dept.FillAnyCombo cmbDept, "Dept", True
    With rsStaff
        If .State = 1 Then .Close
        temSQL = "Select * From tblStaff Order By Name"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With cmbStaff
        Set .RowSource = rsStaff
        .ListField = "Name"
        .BoundColumn = "StaffID"
    End With
   
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnExcel_Click()
        GridToExcel gridIx, "Summery -  " & cmbDept.text, Format(dtpFrom.Value, "dd MMMM yyyy")
End Sub

Private Sub btnIx_Click()
    Dim temRow As Integer
    Dim temPatientID As Long
    Dim temIxID As Long
    Dim temDoctorID As Long
    Dim temInstitutionID As Long
    
    Unload frmReport
    
    With gridIx
        If .Rows <= 1 Then Exit Sub
        If .Row < 1 Then Exit Sub
        temRow = .Row
        If IsNumeric(.TextMatrix(temRow, 6)) = False Then Exit Sub
        temPatientIxID = Val(.TextMatrix(temRow, 6))
    End With
    With rsIxList
        If .State = 1 Then .Close
        temSQL = "Select * from tblPatientIx where PatientIxID = " & temPatientIxID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            temPatientID = !PatientID
            temIxID = !IxID
            If IsNull(!DoctorID) = False Then
                temDoctorID = !DoctorID
            Else
                temDoctorID = 0
            End If
            If IsNull(!InstitutionID) = False Then
                temInstitutionID = !InstitutionID
            Else
                temInstitutionID = 0
            End If
        Else
            MsgBox "Error"
            Exit Sub
        End If
    End With
    
    frmReportFromList.Show
    frmReportFromList.WindowState = 2
    On Error Resume Next
    
    Call Process
    
End Sub


Private Sub btnPrintList_Click()
    
    Dim tabLabName As Integer
    Dim tabLabDescreption As Integer
    Dim tabLabAddress As Integer
    Dim tabNo As Integer
    Dim tabPatient As Integer
    Dim tabIX As Integer
    Dim tabBilled As Integer
    Dim tabPrinted As Integer
    Dim tabIssued As Integer
    Dim tabSex As Integer
    Dim tabAge As Integer
    Dim tabDept As Integer
    Dim tabDate As Integer
    Dim tabPrice As Integer
    Dim tabAge1 As Integer
    Dim tabIX1 As Integer
    
    
    Dim temText As String
    
     tabLabName = 27
     tabLabDescreption = 18
     tabLabAddress = 18
     tabNo = 5
     tabPatient = 10
     tabIX = 60
     tabBilled = 110
     tabPrinted = 85
     tabIssued = 105
     tabAge = 47
     tabDept = 5
     tabDate = 5
     tabPrice = 96
     tabAge1 = 44
     tabIX1 = 56
    
    csetPrinter.SetPrinterAsDefault (cmbPrinter.text)
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = cmbPrinter.text Then
            Set MyPrinter = Printer
        End If
    Next
    If SelectForm(cmbPaper.text, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    
    
    On Error Resume Next

    If optLandscape.Value = True Then
        Printer.Orientation = vbPRORLandscape
    End If
    Printer.Font.Name = "Verdana"
    Printer.Font.Size = 8
    'Printer.Print Tab(tabNo); "Software by Lankan Medical Programmers (LakMediPro) - 0715812399"
    Printer.Font.Name = "Verdana"
    Printer.Font.Size = 14
    Printer.FontBold = True
    
    
    Printer.Print
    Printer.Print Tab(tabLabName); HospitalName
    Printer.Font.Name = "Verdana"
    Printer.Font.Size = 12
    Printer.Print Tab(tabLabDescreption); HospitalDescreption
    Printer.Print Tab(tabLabAddress); HospitalAddress
    Printer.Print
    If IsNumeric(cmbDept.BoundText) = True Then
        Printer.Print Tab(tabDept); "Department : " & cmbDept.text
    Else
        Printer.Print Tab(tabDept); "All Departments"
    End If
    If dtpFrom.Value = dtpTo.Value Then
        Printer.Print Tab(tabDate); "On " & Format(dtpTo.Value, "dd MMMM yyyy")
    Else
        Printer.Print Tab(tabDate); "From " & Format(dtpFrom.Value, "dd MMMM yyyy") & " To " & Format(dtpTo.Value, "dd MMMM yyyy")
    End If
    Printer.Print
    Printer.Font.Name = "Verdana"
    Printer.Font.Size = 10
'    Printer.FontBold = False
    
    
    With gridIx
    Dim PrintPatient As New clsPatient
    
    Printer.Print Tab(tabNo); "Bill No";
    Printer.Print Tab(tabPatient); "Patient";
    Printer.Print Tab(tabAge1); "Age";
    Printer.Print Tab(tabIX1); "Investigation";
    Printer.Print Tab(tabPrice); "Price";
    Printer.Print
    Printer.Print
    
    Printer.Font.Name = "Verdana"
    Printer.Font.Size = 10
    Printer.FontBold = False
    
        For i = 1 To .Rows - 1
            PrintPatient.ID = Val(gridIx.TextMatrix(i, 8))
            Printer.Print Tab(tabNo); .TextMatrix(i, 0);

            If temText <> .TextMatrix(i, 1) Then
                temText = .TextMatrix(i, 1)
                Printer.Print Tab(tabPatient); PrintPatient.NameWithTitle;
                Printer.Print Tab(tabAge); PrintPatient.AgeInWords;
            End If

            Printer.Print Tab(tabIX); .TextMatrix(i, 2);
            If UserPositionID = 9 Then
                Printer.Print Tab(tabBilled - Len(.TextMatrix(i, 7))); .TextMatrix(i, 7);
            End If
            
'            Printer.Print Tab(tabPrinted); .TextMatrix(i, 4);
'            Printer.Print Tab(tabIssued); .TextMatrix(i, 5);
            Printer.Print
        Next
        
    End With
    
    
    
    Dim temDiscount As Double
    Dim ToOut As Double
    
    temDiscount = Val(lblTotal.Caption) / 10
    ToOut = Val(lblTotal.Caption) - temDiscount
    
    Dim temDis As String
    Dim temOut As String
    
    temDis = Format(temDiscount, "0.00")
    temOut = Format(ToOut, "0.00")
    
    Printer.Print
    
    If UserPositionID <> 9 Then
        Printer.Print Tab(tabNo); "MLT Fee"; Tab(tabBilled - Format(lblMLTFee.Caption, "0.00")); Format(lblMLTFee.Caption, "0.00")
        Printer.Print Tab(tabNo); "Hospital Fee"; Tab(tabBilled - Format(lblHospitalFee.Caption, "0.00")); Format(lblHospitalFee.Caption, "0.00")
        Printer.Print Tab(tabNo); "Reagent Fee"; Tab(tabBilled - Len(Format(lblReagentFee.Caption, "0.00"))); Format(lblReagentFee.Caption, "0.00")
        Printer.Print Tab(tabNo); "Total Amount"; Tab(tabBilled - Len(Format(lblTotal.Caption, "0.00"))); Format(lblTotal.Caption, "0.00")
    End If
    
   
    
    Printer.EndDoc
    
End Sub

Private Sub cmbPrinter_Change()
    cmbPrinter_Click
End Sub

Private Sub cmbPrinter_Click()
    cmbPaper.Clear
    csetPrinter.SetPrinterAsDefault (cmbPrinter.text)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        With FormSize
            .cx = BillPaperHeight
            .cy = BillPaperWidth
        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                cmbPaper.AddItem PtrCtoVbString(.pName)
            End With
        Next i
        ClosePrinter (PrinterHandle)
    End If
End Sub


Private Sub chkPrinted_Click()
    Call Process
End Sub

Private Sub chkToHadnover_Click()
    Call Process
End Sub

Private Sub chkToPrint_Click()
    Call Process
End Sub

Private Sub cmbDept_Change()
    Call Process
End Sub

Private Sub cmbDept_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbDept.text = Empty
    End If
End Sub

Private Sub cmbStaff_Change()
    Call Process
End Sub

Private Sub cmbStaff_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbStaff.text = Empty
    End If

End Sub

Private Sub dtpFrom_Change()
    dtpTo.Value = dtpFrom.Value + 1
    Call Process
End Sub

Private Sub dtpTime_Change()
    Call Process
End Sub

Private Sub dtpTo_Change()
    Call Process
End Sub

Private Sub FillPrinters()
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        cmbPrinter.AddItem MyPrinter.DeviceName
    Next
End Sub


Private Sub Form_Load()
    
'    If UserPositionID = 9 Then
'        Label4.Visible = True
'        lblTotal.Visible = True
'    Else
'        Label4.Visible = False
'        lblTotal.Visible = False
'    End If
    
    Call FillCombos
    Call FormatGrid
    Call FillPrinters
    dtpFrom.Value = Date
    dtpTo.Value = Date + 1
    dtpTime.Value = #12:00:00 PM#
    Call FillGrid
    On Error Resume Next
    cmbPrinter.text = GetSetting(App.EXEName, Me.Name, "Printer", "")
    cmbPrinter_Click
    cmbPaper.text = GetSetting(App.EXEName, Me.Name, "Paper", "")
End Sub


Private Sub FormatGrid()
    With gridIx
        .Clear
        .Rows = 1
        .Cols = 13
        For i = 0 To .Cols - 1
            Select Case i
                Case 0:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 600
                    .text = "No."
                Case 1:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 3000
                    .text = "Patient"
                Case 2:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 3000
                    .text = "Investigation"
                Case 3:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 2600
                    .text = "Billing"
                Case 4:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 2600
                    .text = "Printed"
                Case 5:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 2600
                    .text = "Issued"
                Case 6:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 0
                    .text = "ID"
                Case 7:
                    
                    If UserPositionID = 9 Then
                        .col = i
                        .CellAlignment = 4
                        .ColWidth(i) = 1000
                        .text = "Price"
                    Else
                        .ColWidth(i) = 0
                    End If
                    
                Case 8:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 0
                    .text = ""
                
                Case 9:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 1000
                    .text = "Hospital Fee"
                    
                Case 10:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 1000
                    .text = "MLT Fee"
                    
                Case 11:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 1000
                    .text = "Reagent Fee"
                    
                Case 12:
                    .col = i
                    .CellAlignment = 4
                    .ColWidth(i) = 1000
                    .text = "Total Fee"
                
                Case Else:
                    .ColWidth(i) = 0
            End Select
        Next
    End With
End Sub

Private Sub FillGrid()
    Dim TotalValue As Double
    Dim HospitalValue As Double
    Dim MLTValue As Double
    Dim ReagentValue As Double
    Dim AllValue As Double
    
    
    With rsIxList
    
        If .State = 1 Then .Close
        temSQL = "SELECT  tblPatientIx.PatientIxBillID, tblPatient.Name, tblIx.Ix, tblIx.Value, tblPatientIx.* , tblPatient.PatientID "
        temSQL = temSQL & "FROM (((tblPatient RIGHT JOIN tblPatientIx ON tblPatient.PatientID = tblPatientIx.PatientID) LEFT JOIN tblTitle ON tblPatient.TitleID = tblTitle.TitleID) LEFT JOIN tblIx ON tblPatientIx.IxID = tblIx.IxID) LEFT JOIN tblPatientIxBill ON tblPatientIx.PatientIxBillID = tblPatientIxBill.PatientIxBillID "
        temSQL = temSQL & "WHERE (((tblPatientIx.Deleted)=False)  AND ((tblPatientIxBill.Cancelled)=False) AND ( ((tblPatientIx.Billed)=False) "
        If chkPrinted.Value = 1 Then
            temSQL = temSQL & " OR ((tblPatientIx.Printed)=True) "
        End If
        If chkToHadnover.Value = 1 Then
            temSQL = temSQL & " OR ((tblPatientIx.Issued)=True) "
        End If
        If chkToPrint.Value = 1 Then
            temSQL = temSQL & " OR ((tblPatientIx.Printed)=False) "
        End If
        temSQL = temSQL & " ) "
        
        temSQL = temSQL & " AND ( (  tblPatientIx.BilledDate = #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# AND tblPatientIx.BilledTime >= #" & dtpTime.Value & "#)  or  (  tblPatientIx.BilledDate = #" & Format(dtpTo.Value, "dd MMMM yyyy") & "# AND tblPatientIx.BilledTime <= #" & dtpTime.Value & "#)   ) "
        If IsNumeric(cmbStaff.BoundText) = True Then
            temSQL = temSQL & " AND ((tblPatientIx.StaffID)=" & Val(cmbStaff.BoundText) & ")"
        Else
            temSQL = temSQL & ""
        End If
        
        If IsNumeric(cmbDept.BoundText) = True Then
            temSQL = temSQL & " AND ((tblIx.DeptID)=" & Val(cmbDept.BoundText) & "))"
        Else
            temSQL = temSQL & ")"
        End If

        
        If optByPatient.Value = True Then
            temSQL = temSQL & "ORDER BY tblPatient.Name"
        ElseIf optByIx.Value = True Then
            temSQL = temSQL & "ORDER BY tblIx.Ix"
        ElseIf optByID.Value = True Then
            temSQL = temSQL & "ORDER BY tblPatientIx.PatientIxID"
        End If
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            .MoveLast
            .MoveFirst
            gridIx.Rows = .RecordCount + 1
            For i = 1 To .RecordCount
                gridIx.TextMatrix(i, 0) = ![tblPatientIx.PatientIxBillID]
                gridIx.TextMatrix(i, 1) = !Name
                gridIx.TextMatrix(i, 2) = !Ix
                gridIx.TextMatrix(i, 3) = Format(!BilledDate, "dd MM yy") & " - " & Format(!BilledTime, "HH:MM")
                gridIx.TextMatrix(i, 4) = Format(!PrintedDate, "dd MM yy") & " - " & Format(!PrintedTime, "HH:MM")
                gridIx.TextMatrix(i, 5) = Format(!IssuedDate, "dd MM yy") & " - " & Format(!IssuedTime, "HH:MM")
                gridIx.TextMatrix(i, 6) = !PatientIxID
                gridIx.TextMatrix(i, 8) = ![tblPatient.PatientID]
                If IsNull(![tblIx.Value]) = False Then
                    gridIx.TextMatrix(i, 7) = Format(![tblIx.Value], "0.00")
                    TotalValue = TotalValue + ![tblIx.Value]
                End If
                
                Dim temH As Double
                Dim temM As Double
                Dim temR As Double
                Dim temT As Double
                
                temH = Val(Format(![HospitalFee], "0.00"))
                temM = Val(Format(![StaffFee], "0.00"))
                temR = Val(Format(![OtherFee], "0.00"))
                temT = Val(Format(![tblPatientIx.Value], "0.00"))
                
                HospitalValue = HospitalValue + temH
                MLTValue = MLTValue + temM
                ReagentValue = ReagentValue + temR
                AllValue = AllValue + temT
                
                gridIx.TextMatrix(i, 9) = Format(temH, "0.00")
                gridIx.TextMatrix(i, 10) = Format(temM, "0.00")
                gridIx.TextMatrix(i, 11) = Format(temR, "0.00")
                gridIx.TextMatrix(i, 12) = Format(temT, "0.00")
                
                .MoveNext
            Next
        End If
        .Close
        
    End With
    
    With gridIx
        .Rows = .Rows + 1
        .Row = .Rows - 1
        
        .col = 9
        .text = Format(HospitalValue, "0.00")
        
        .col = 10
        .text = Format(MLTValue, "0.00")
        
        .col = 11
        .text = Format(ReagentValue, "0.00")
        
        .col = 12
        .text = Format(TotalValue, "0.00")
        
        
    End With
    
    lblTotal.Caption = Format(TotalValue, "0.00")
    
    
    lblHospitalFee.Caption = Format(HospitalValue, "0.00")
    lblMLTFee.Caption = Format(MLTValue, "0.00")
    lblReagentFee.Caption = Format(ReagentValue, "0.00")
    
    
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSetting App.EXEName, Me.Name, "Printer", cmbPrinter.text
    SaveSetting App.EXEName, Me.Name, "Paper", cmbPaper.text
    
End Sub

Private Sub gridIx_DblClick()
    btnIx_Click
End Sub

Private Sub optByID_Click()
    Call Process

End Sub

Private Sub optByIx_Click()
    Call Process

End Sub

Private Sub optByPatient_Click()
    Call Process

End Sub
