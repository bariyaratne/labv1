VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsActivation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim Pro As String

Public Sub activate()
    If databaseExists = False Then createActivationTable
    Dim mySec As New clsSecurity
    Dim temSql As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * From tblSequence where SequenceID=0"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !TableId = Month(Date)
        !LastID = Year(Date)
        !SessionID = Day(Date)
        !Comments = mySec.Hash(registrationString)
        .Update
        If .State = 1 Then .Close
    End With
End Sub

Private Sub createActivationTable()
    'On Error Resume Next
    Dim temSql As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "CREATE TABLE tblSequence ([SequenceID] AUTOINCREMENT, [TableID] INTEGER, [LastID] DOUBLE, [SessionID] DOUBLE, [Comments] Memo, [Deleted] YesNo,  CONSTRAINT [PK_tblSequence] PRIMARY KEY ([SequenceID]))"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Update
        If .State = 1 Then .Close
    End With
    
End Sub

Private Function databaseExists() As Boolean
    On Error GoTo eh
    Dim temSql As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select* From tblSequence where SequenceID=1"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .State = 1 Then .Close
    End With
    databaseExists = True
    Exit Function
eh:
    databaseExists = False
    
End Function



Public Function isActivated() As Boolean
    isActivated = False
    If databaseExists = False Then
        Exit Function
    End If
    Dim mySec As New clsSecurity
    Dim temSql As String
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * From tblSequence"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .State = 0 Then .Open
        While .EOF = False
            If !Comments = mySec.Hash(registrationString) Then
                isActivated = True
                Exit Function
            End If
            .MoveNext
        Wend
        If .State = 1 Then .Close
    End With
End Function

Public Function registrationKey() As String
    Dim temStr As String
    Dim mySerial As New clsSerial
    Dim mySec As New clsSecurity
    Pro = "Lab"
    mySerial.CurrentDrive = 0
    If mySerial.GetSerialNumber = "" Then
        mySerial.CurrentDrive = 1
        If mySerial.GetSerialNumber = "" Then
            mySerial.CurrentDrive = 2
            If mySerial.GetSerialNumber = "" Then
                mySerial.CurrentDrive = 3
            End If
        End If
    End If
    temStr = mySerial.GetSerialNumber & Pro & mySerial.GetModelNumber & Pro & mySerial.GetFirmwareRevision
    registrationKey = mySec.Encode(temStr, ProgramVariable.SecurityKey)
    
    
    
End Function

Public Function registrationString() As String
    Dim temStr As String
    Dim mySerial As New clsSerial
    Dim mySec As New clsSecurity
    Pro = "Lab"
    mySerial.CurrentDrive = 0
    If mySerial.GetSerialNumber = "" Then
        mySerial.CurrentDrive = 1
        If mySerial.GetSerialNumber = "" Then
            mySerial.CurrentDrive = 2
            If mySerial.GetSerialNumber = "" Then
                mySerial.CurrentDrive = 3
            End If
        End If
    End If
    temStr = mySerial.GetSerialNumber & Pro & mySerial.GetModelNumber & Pro & mySerial.GetFirmwareRevision
    registrationString = temStr
    
End Function


Public Function getActivationKey(regKey As String) As String
    Dim temStr As String
    Dim serialNo As String
    Dim afterSerialNo As String
    Dim ModalNo As String
    Dim afterModalNo As String
    Dim FirmNo As String
    Dim Pro As String
    getActivationKey = ""
    Dim mySec As New clsSecurity
    temStr = mySec.Decode(regKey, ProgramVariable.SecurityKey)
    
    If InStr(temStr, "Lab") > 0 Then
'        txtProgram.text = "Lab"
        Pro = "Lab"
    ElseIf InStr(temStr, "Cha") > 0 Then
'        txtProgram.text = "Channelling"
        Pro = "Cha"
    ElseIf InStr(temStr, "Pha") > 0 Then
'        txtProgram.text = "Pharmacy"
        Pro = "Pha"
    ElseIf InStr(temStr, "Cas") > 0 Then
'        txtProgram.text = "Cashier"
        Pro = "Cas"
        
    ElseIf InStr(temStr, "Ema") > 0 Then
'        txtProgram.text = "EMA"
        Pro = "Ema"
    ElseIf InStr(temStr, "Mil") > 0 Then
'        txtProgram.text = "Milk"
        Pro = "Mil"
    ElseIf InStr(temStr, "Che") > 0 Then
'        txtProgram.text = "Cheque"
        Pro = "Che"
    ElseIf InStr(temStr, "Oth") > 0 Then
'        txtProgram.text = "Other"
        Pro = "Oth"
    Else
'        txtProgram.text = "Nothing"
'        txtA.text = "Error. No Program"
        Exit Function
    End If
    
    serialNo = Left(temStr, InStr(temStr, Pro) - 1)
    afterSerialNo = Right(temStr, Len(temStr) - InStr(temStr, Pro) - 2)
    ModalNo = Left(afterSerialNo, InStr(afterSerialNo, Pro) - 1)
    FirmNo = Right(afterSerialNo, Len(afterSerialNo) - InStr(afterSerialNo, Pro) - 2)
    getActivationKey = mySec.Encode(Pro & ModalNo & Pro & serialNo & Pro & FirmNo & Pro, ProgramVariable.SecurityKey)
End Function



