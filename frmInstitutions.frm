VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmInstitutions 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Institution Details"
   ClientHeight    =   8940
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10305
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8940
   ScaleWidth      =   10305
   Begin MSDataListLib.DataCombo cmbInstitution 
      Height          =   6900
      Left            =   240
      TabIndex        =   27
      Top             =   120
      Width           =   3855
      _ExtentX        =   6800
      _ExtentY        =   12171
      _Version        =   393216
      Style           =   1
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx bttnCancel 
      Height          =   495
      Left            =   7680
      TabIndex        =   12
      Top             =   7680
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "Ca&ncel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   495
      Left            =   8760
      TabIndex        =   13
      Top             =   8280
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnSave 
      Height          =   495
      Left            =   6240
      TabIndex        =   11
      Top             =   7680
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "Sa&ve"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame framInstitution 
      Height          =   7575
      Left            =   4320
      TabIndex        =   14
      Top             =   0
      Width           =   5775
      Begin VB.TextBox txtEmail 
         Height          =   360
         Left            =   2040
         TabIndex        =   25
         Top             =   3720
         Width           =   3495
      End
      Begin VB.TextBox txtCredit 
         Height          =   360
         Left            =   2040
         TabIndex        =   10
         Top             =   6720
         Width           =   3495
      End
      Begin VB.TextBox txtAccount 
         Height          =   375
         Left            =   2040
         TabIndex        =   9
         Top             =   6120
         Width           =   3495
      End
      Begin VB.TextBox txtBank 
         Height          =   360
         Left            =   2040
         TabIndex        =   8
         Top             =   5640
         Width           =   3495
      End
      Begin VB.TextBox txtPaymode 
         Height          =   360
         Left            =   2040
         TabIndex        =   7
         Top             =   5160
         Width           =   3495
      End
      Begin VB.TextBox txtComment 
         Height          =   735
         Left            =   2040
         TabIndex        =   6
         Top             =   4320
         Width           =   3495
      End
      Begin VB.TextBox txtFax 
         Height          =   375
         Left            =   2040
         TabIndex        =   5
         Top             =   3240
         Width           =   3495
      End
      Begin VB.TextBox txtTel 
         Height          =   360
         Left            =   2040
         TabIndex        =   4
         Top             =   2760
         Width           =   3495
      End
      Begin VB.TextBox txtAddress 
         Height          =   1575
         Left            =   2040
         MultiLine       =   -1  'True
         TabIndex        =   3
         Top             =   1080
         Width           =   3495
      End
      Begin VB.TextBox txtName 
         Height          =   360
         Left            =   2040
         TabIndex        =   2
         Top             =   480
         Width           =   3495
      End
      Begin VB.Label Label10 
         Caption         =   "E - Mail"
         Height          =   375
         Left            =   120
         TabIndex        =   24
         Top             =   3720
         Width           =   1575
      End
      Begin VB.Label Label9 
         Caption         =   "Discount %"
         Height          =   495
         Left            =   120
         TabIndex        =   23
         Top             =   6720
         Width           =   1575
      End
      Begin VB.Label Label8 
         Caption         =   "Institution Account"
         Height          =   495
         Left            =   120
         TabIndex        =   22
         Top             =   6240
         Width           =   2055
      End
      Begin VB.Label Label7 
         Caption         =   "Institution  Bank"
         Height          =   495
         Left            =   120
         TabIndex        =   21
         Top             =   5760
         Width           =   1815
      End
      Begin VB.Label Label6 
         Caption         =   "Payment Method"
         Height          =   495
         Left            =   120
         TabIndex        =   20
         Top             =   5160
         Width           =   2415
      End
      Begin VB.Label Label5 
         Caption         =   "Institution Comments"
         Height          =   495
         Left            =   120
         TabIndex        =   19
         Top             =   4320
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Institution Fax"
         Height          =   495
         Left            =   120
         TabIndex        =   18
         Top             =   3240
         Width           =   1335
      End
      Begin VB.Label Label3 
         Caption         =   "Institution Tel:"
         Height          =   495
         Left            =   120
         TabIndex        =   17
         Top             =   2880
         Width           =   1335
      End
      Begin VB.Label Label2 
         Caption         =   "Institution Address"
         Height          =   375
         Left            =   120
         TabIndex        =   16
         Top             =   1080
         Width           =   2175
      End
      Begin VB.Label Label1 
         Caption         =   "Institution Name"
         Height          =   375
         Left            =   120
         TabIndex        =   15
         Top             =   480
         Width           =   2055
      End
   End
   Begin btButtonEx.ButtonEx bttnEdit 
      Height          =   495
      Left            =   1560
      TabIndex        =   1
      Top             =   7080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Edit"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnAdd 
      Height          =   495
      Left            =   240
      TabIndex        =   0
      Top             =   7080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Add"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnDelete 
      Height          =   495
      Left            =   2880
      TabIndex        =   26
      Top             =   7080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Delete"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmInstitutions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Dim rsInstitution As New ADODB.Recordset
    

Private Sub btnDelete_Click()
    Dim i As Integer
    If IsNumeric(cmbInstitution.BoundText) = False Then
        MsgBox "Please select a Institution to delete"
        cmbInstitution.SetFocus
        Exit Sub
    End If
    i = MsgBox("Are you sure?", vbYesNo)
    If i = vbNo Then Exit Sub
    With rsInstitution
        If .State = 1 Then .Close
        temSql = "Select * from tblInstitution where InstitutionID = " & Val(cmbInstitution.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Deleted = True
            !DeletedUserID = UserID
            !DeletedDate = Date
            !DeletedTime = Time
            .Update
            MsgBox "Deleted"
            .Close
        End If
    End With
    Call Clearvalues
    Call FillCOmbos
    cmbInstitution.Text = Empty
    cmbInstitution.SetFocus
End Sub

Private Sub bttnAdd_Click()
    Dim temString As String
    If IsNumeric(cmbInstitution.BoundText) = False Then
        temString = cmbInstitution.Text
    Else
        temString = Empty
    End If
    cmbInstitution.Text = Empty

    Call Clearvalues
    Call EditMode
        
    txtName.Text = temString
    txtName.SetFocus
    
On Error Resume Next:     SendKeys "{home}+{end}"
        
End Sub

Private Sub bttnCancel_Click()
    Call Clearvalues
    Call SelectMode
    cmbInstitution.Text = Empty
    cmbInstitution.SetFocus
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnEdit_Click()
    Call EditMode
    txtName.SetFocus
On Error Resume Next:     SendKeys "{home}+{end}"
End Sub

Private Sub bttnSave_Click()
    If IsNumeric(cmbInstitution.BoundText) = True Then
        Call ChangeData
    Else
        Call SaveData
    End If
    Call Clearvalues
    Call SelectMode
    Call FillCOmbos
    cmbInstitution.Text = Empty
    cmbInstitution.SetFocus
End Sub

Private Sub FillCOmbos()
    Dim clsDoc As New clsFillCombos
    clsDoc.FillAnyCombo cmbInstitution, "Institution", True
End Sub

Private Sub cmbInstitution_Change()
    Call Clearvalues
    If IsNumeric(cmbInstitution.BoundText) = True Then
        Call DisplayDetails
    End If
End Sub

Private Sub Form_Load()
    Call FillCOmbos
    Call SelectMode
End Sub

Private Sub SelectMode()
    bttnEdit.Enabled = True
    bttnAdd.Enabled = True
    btnDelete.Enabled = True
    
    bttnSave.Enabled = False
    bttnCancel.Enabled = False
    
    framInstitution.Enabled = False
End Sub

Private Sub EditMode()
    bttnEdit.Enabled = False
    bttnAdd.Enabled = False
    btnDelete.Enabled = False
    
    bttnSave.Enabled = True
    bttnCancel.Enabled = True
    
    framInstitution.Enabled = True
End Sub

Private Sub SaveData()
    Dim A
    If Trim(txtName.Text) = "" Then
        A = MsgBox("Name Empty,Enter ", vbCritical + vbOKOnly, "Error")
        txtName.SetFocus
        Exit Sub
    End If
    
    On Error GoTo ErrorHandler
    
    With rsInstitution
        If .State = 1 Then .Close
        temSql = "Select * from tblInstitution"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        
        !Institution = Trim(txtName.Text)
        !InstitutionAddress = Trim(txtAddress.Text)
        !InstitutionTelephone = Trim(txtTel.Text)
        !InstitutionFax = Trim(txtFax.Text)
        !InstitutionEmail = Trim(txtEmail.Text)
        !InstitutionComments = Trim(txtcomment.Text)
        !InstitutionPaymentMethod = Trim(txtPaymode.Text)
        !InstitutionBank = Trim(txtBank.Text)
        !InstitutionAccount = Trim(txtAccount.Text)
        !DiscountPercent = Val(txtCredit.Text)
        
        
        
        .Update
        .Close
    End With
    
Exit Sub
    
ErrorHandler:
    MsgBox Error, vbOKOnly, "Update Error"
End Sub

Private Sub ChangeData()
    Dim A
    If Trim(txtName.Text) = "" Then
        A = MsgBox("Name Empty,Enter ", vbCritical + vbOKOnly, "Error")
        txtName.SetFocus
        Exit Sub
    End If
    
    On Error GoTo ErrorHandler
    
    With rsInstitution
        If .State = 1 Then .Close
        temSql = "Select * from tblInstitution where InstitutionID = " & Val(cmbInstitution.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !Institution = Trim(txtName.Text)
            !InstitutionAddress = Trim(txtAddress.Text)
            !InstitutionTelephone = Trim(txtTel.Text)
            !InstitutionFax = Trim(txtFax.Text)
            !InstitutionEmail = Trim(txtEmail.Text)
            !InstitutionComments = Trim(txtcomment.Text)
            !InstitutionPaymentMethod = Trim(txtPaymode.Text)
            !InstitutionBank = Trim(txtBank.Text)
            !InstitutionAccount = Trim(txtAccount.Text)
            !DiscountPercent = Val(txtCredit.Text)
            .Update
        End If
        .Close
    End With
    
Exit Sub
    
ErrorHandler:
    MsgBox Error, vbOKOnly, "Update Error"
End Sub


Private Sub Clearvalues()
    txtName.Text = Empty
    txtAddress.Text = Empty
    txtTel.Text = Empty
    txtFax.Text = Empty
    txtPaymode.Text = Empty
    txtBank.Text = Empty
    txtEmail.Text = Empty
    txtAccount.Text = Empty
    txtcomment.Text = Empty
    txtAccount.Text = Empty
    txtCredit.Text = Empty
End Sub

Private Sub DisplayDetails()
    If IsNumeric(cmbInstitution.BoundText) = False Then Exit Sub
    With rsInstitution
        If .State = 1 Then .Close
        temSql = "SELECT * from tblInstitution where InstitutionID = " & Val(cmbInstitution.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If Not IsNull(!Institution) Then
                txtName.Text = !Institution
            End If
            If Not IsNull(!InstitutionAddress) Then
                txtAddress.Text = !InstitutionAddress
            End If
            If Not IsNull(!InstitutionTelephone) Then
                txtTel.Text = !InstitutionTelephone
            End If
            If Not IsNull(!InstitutionFax) Then
                txtFax.Text = !InstitutionFax
            End If
            If Not IsNull(!InstitutionEmail) Then
                txtEmail.Text = !InstitutionEmail
            End If
            If Not IsNull(!InstitutionComments) Then
                txtcomment.Text = !InstitutionComments
            End If
            If Not IsNull(!InstitutionPaymentMethod) Then
                txtPaymode.Text = !InstitutionPaymentMethod
            End If
            If Not IsNull(!InstitutionBank) Then
                txtBank.Text = !InstitutionBank
            End If
            If Not IsNull(!InstitutionAccount) Then
                txtAccount.Text = !InstitutionAccount
            End If
            If Not IsNull(!DiscountPercent) Then
                txtCredit.Text = !DiscountPercent
            End If
        End If
    End With
End Sub
