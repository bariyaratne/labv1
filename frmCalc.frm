VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmCalc 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Calculated Fields"
   ClientHeight    =   7140
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8805
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7140
   ScaleWidth      =   8805
   Begin VB.Frame frameFormat 
      Caption         =   "Format"
      Height          =   1815
      Left            =   3840
      TabIndex        =   15
      Top             =   3960
      Width           =   4815
      Begin VB.TextBox txtSuffix 
         Height          =   375
         Left            =   1080
         TabIndex        =   20
         Top             =   1320
         Width           =   3495
      End
      Begin VB.TextBox txtPrefix 
         Height          =   375
         Left            =   1080
         TabIndex        =   19
         Top             =   840
         Width           =   3495
      End
      Begin VB.TextBox txtFormat 
         Height          =   375
         Left            =   1080
         TabIndex        =   18
         Top             =   360
         Width           =   3495
      End
      Begin VB.Label Label5 
         Caption         =   "Suffix"
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label Label4 
         Caption         =   "Prefix"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label Label3 
         Caption         =   "Format"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.Frame frameFunctions 
      Caption         =   "Add Functions"
      Height          =   855
      Left            =   3840
      TabIndex        =   12
      Top             =   3000
      Width           =   4815
      Begin VB.ComboBox cmbFunction 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   360
         Width           =   3495
      End
      Begin btButtonEx.ButtonEx btnAddFunctions 
         Height          =   375
         Left            =   3840
         TabIndex        =   14
         Top             =   360
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame frameValues 
      Caption         =   "Add Values"
      Height          =   855
      Left            =   3840
      TabIndex        =   9
      Top             =   2040
      Width           =   4815
      Begin MSDataListLib.DataCombo cmbIxValue 
         Height          =   315
         Left            =   240
         TabIndex        =   10
         Top             =   360
         Width           =   3495
         _ExtentX        =   6165
         _ExtentY        =   556
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
      End
      Begin btButtonEx.ButtonEx btnAddValues 
         Height          =   375
         Left            =   3840
         TabIndex        =   11
         Top             =   360
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin btButtonEx.ButtonEx btnDelete 
      Height          =   375
      Left            =   3720
      TabIndex        =   7
      Top             =   6240
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "Remove Last"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame frameConstants 
      Caption         =   "Add Constants"
      Height          =   855
      Left            =   3840
      TabIndex        =   5
      Top             =   1080
      Width           =   4815
      Begin VB.TextBox txtConstant 
         Height          =   375
         Left            =   240
         TabIndex        =   6
         Top             =   240
         Width           =   3495
      End
      Begin btButtonEx.ButtonEx btnAddConstant 
         Height          =   375
         Left            =   3840
         TabIndex        =   8
         Top             =   240
         Width           =   855
         _ExtentX        =   1508
         _ExtentY        =   661
         Appearance      =   3
         Caption         =   "Add"
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin MSFlexGridLib.MSFlexGrid gridCalc 
      Height          =   5535
      Left            =   240
      TabIndex        =   4
      Top             =   1080
      Width           =   3375
      _ExtentX        =   5953
      _ExtentY        =   9763
      _Version        =   393216
   End
   Begin MSDataListLib.DataCombo cmbIx 
      Height          =   360
      Left            =   1800
      TabIndex        =   0
      Top             =   120
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   556
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin MSDataListLib.DataCombo cmbIxItem 
      Height          =   360
      Left            =   1800
      TabIndex        =   1
      Top             =   600
      Width           =   5055
      _ExtentX        =   8916
      _ExtentY        =   556
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
   End
   Begin btButtonEx.ButtonEx btnClose 
      Height          =   495
      Left            =   7320
      TabIndex        =   16
      Top             =   6600
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnSave 
      Height          =   495
      Left            =   5880
      TabIndex        =   17
      Top             =   6600
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "Save"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      Caption         =   "Investigation"
      Height          =   255
      Left            =   240
      TabIndex        =   3
      Top             =   120
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Value Field"
      Height          =   255
      Left            =   240
      TabIndex        =   2
      Top             =   600
      Width           =   1215
   End
End
Attribute VB_Name = "frmCalc"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temSql As String
    Dim rsViewIxItem As New ADODB.Recordset
    Dim rsViewValue As New ADODB.Recordset
    
Private Sub btnAddConstant_Click()
    If IsNumeric(txtConstant.Text) = False Then
        MsgBox "Please enter a value"
        txtConstant.SetFocus
    On Error Resume Next:     SendKeys "{home}+{end}"
        Exit Sub
    End If
    With gridCalc
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 0
        .Text = .Row
        .Col = 1
        .Text = "Constant - " & txtConstant.Text
        .Col = 2
        .Text = "Constant"
        .Col = 3
        .Text = txtConstant.Text
    End With
    Call SelectMode
    txtConstant.Text = Empty
End Sub

Private Sub btnAddFunctions_Click()
    With gridCalc
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 0
        .Text = .Row
        .Col = 1
        .Text = "Function - " & cmbFunction.Text
        .Col = 2
        .Text = cmbFunction.Text
        .Col = 3
        .Text = cmbFunction.Text
    End With
    Call SelectMode
End Sub

Private Sub btnAddValues_Click()
    If IsNumeric(cmbIxValue.BoundText) = False Then
        MsgBox "Please select a value"
        cmbIxValue.SetFocus
        Exit Sub
    End If
    With gridCalc
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 0
        .Text = .Row
        .Col = 1
        .Text = "Value - " & cmbIxValue.Text
        .Col = 2
        .Text = "Value"
        .Col = 3
        .Text = cmbIxValue.BoundText
    End With
    Call SelectMode
    cmbIxValue.Text = Empty
End Sub

Private Sub btnClose_Click()
    Unload Me
End Sub

Private Sub btnDelete_Click()
    If gridCalc.Rows > 2 Then
        gridCalc.RemoveItem (gridCalc.Rows - 1)
    Else
        Call FormatGrid
    End If
    Call SelectMode
End Sub

Private Sub btnSave_Click()
    Dim rsTem As New ADODB.Recordset
    Dim i As Integer
    If IsNumeric(cmbIxItem.BoundText) = False Then
        cmbIxItem.SetFocus
        Exit Sub
    End If
    With gridCalc
        If .Rows < 1 Then
            MsgBox "You have no adequate calculations to save"
            Exit Sub
        Else
            If .Rows Mod 2 = 1 Then
                MsgBox "You have to end tha calculation with a value or a constant"
                Exit Sub
            End If
        End If
    End With
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItemCalc where IxItemID = " & Val(cmbIxItem.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        While .EOF = False
            !Deleted = True
            !DeletedDate = Date
            !DeletedTime = Time
            !DeletedUserID = UserID
            .Update
            .MoveNext
        Wend
        .Close
        temSql = "Select * from tblIxItemCalc"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        For i = 1 To gridCalc.Rows - 1
            gridCalc.Row = i
            .AddNew
            !IxItemID = Val(cmbIxItem.BoundText)
            !ItemOrderNo = gridCalc.Row
            gridCalc.Col = 1
            !IxItemCalc = gridCalc.Text
            If gridCalc.TextMatrix(i, 2) = "Value" Then
                !ValueIxItemID = Val(gridCalc.TextMatrix(i, 3))
                !FunctionType = gridCalc.TextMatrix(i, 2)
            ElseIf gridCalc.TextMatrix(i, 2) = "Constant" Then
                !ConstantValue = gridCalc.TextMatrix(i, 3)
                !FunctionType = gridCalc.TextMatrix(i, 2)
            Else
                !FunctionType = gridCalc.TextMatrix(i, 2)
            End If
            .Update
        Next
        .Close
        temSql = "Select * from tblIxItem where IxItemID = " & Val(cmbIxItem.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            !ItemFormat = txtFormat.Text
            !ItemPrefix = txtPrefix.Text
            !ItemSuffix = txtSuffix.Text
            .Update
        End If
        .Close
    End With
    MsgBox "Saved"
    Call FormatGrid
    cmbIx.Text = Empty
    cmbIxItem.Text = Empty
    cmbIxValue.Text = Empty
    txtFormat.Text = Empty
    txtPrefix.Text = Empty
    txtSuffix.Text = Empty
    Call SelectMode
End Sub

Private Sub cmbIx_Change()
    With rsViewIxItem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where IxID = " & Val(cmbIx.BoundText) & " AND Deleted = False AND IsCalc = true Order by IxItem"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With cmbIxItem
        Set .RowSource = rsViewIxItem
        .ListField = "IxItem"
        .BoundColumn = "IxItemID"
    End With
    With rsViewValue
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where IxID = " & Val(cmbIx.BoundText) & " AND Deleted = False AND IsValue = true Order by IxItem"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With cmbIxValue
        Set .RowSource = rsViewValue
        .ListField = "IxItem"
        .BoundColumn = "IxItemID"
    End With
    cmbIxItem.Text = Empty
    cmbIxValue.Text = Empty
End Sub

Private Sub cmbIxItem_Change()
    Call FormatGrid
    Call FillGrid
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItem where IxItemID = " & Val(cmbIxItem.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!ItemFormat) = False Then
                If !ItemFormat <> "" Then
                    txtFormat.Text = !ItemFormat
                End If
            End If
            If IsNull(!ItemPrefix) = False Then
                If !ItemPrefix <> "" Then
                    txtPrefix.Text = !ItemPrefix
                End If
            End If
            If IsNull(!ItemSuffix) = False Then
                If !ItemSuffix <> "" Then
                    txtSuffix.Text = !ItemSuffix
                End If
            End If
        End If
    End With
    Call SelectMode
End Sub

Private Sub Form_Load()
    Dim Ix As New clsFillCombos
    Ix.FillAnyCombo cmbIx, "Ix", True
    Call FormatGrid
    Call SelectMode
    
    cmbFunction.AddItem "Add"
    cmbFunction.AddItem "Subtract"
    cmbFunction.AddItem "Multiply"
    cmbFunction.AddItem "Divide"
'    cmbFunction.AddItem "Power"
'    cmbFunction.AddItem "Square root"
    
End Sub

Private Sub SelectMode()
    frameConstants.Enabled = False
    frameFunctions.Enabled = False
    frameValues.Enabled = False
    
    If gridCalc.Rows = 1 Then
        frameConstants.Enabled = True
        frameValues.Enabled = True
    Else
        If gridCalc.Rows Mod 2 = 0 Then
            frameFunctions.Enabled = True
        Else
            frameConstants.Enabled = True
            frameValues.Enabled = True
        End If
    End If
End Sub

Private Sub FillGrid()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblIxItemCalc Where IxItemID = " & Val(cmbIxItem.BoundText) & " AND Deleted = false Order by ItemOrderNo"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        While .EOF = False
            gridCalc.Rows = gridCalc.Rows + 1
            gridCalc.Row = gridCalc.Rows - 1
            
            gridCalc.Col = 1
            gridCalc.CellAlignment = 1
            gridCalc.Text = gridCalc.Row
            
            gridCalc.Col = 1
            gridCalc.CellAlignment = 1
            gridCalc.Text = !IxItemCalc
            
            
            gridCalc.Col = 2
            gridCalc.CellAlignment = 1
            gridCalc.Text = !FunctionType
            
            If !ValueIxItemID <> 0 Then
                gridCalc.Col = 3
                gridCalc.CellAlignment = 1
                gridCalc.Text = !ValueIxItemID
            ElseIf !ConstantValue <> 0 Then
                gridCalc.Col = 3
                gridCalc.CellAlignment = 1
                gridCalc.Text = !ConstantValue
            Else
                gridCalc.Col = 3
                gridCalc.CellAlignment = 1
                gridCalc.Text = !FunctionType
            End If
            
            .MoveNext
        Wend
    End With
End Sub

Private Sub FormatGrid()
    With gridCalc
        .Clear
        
        .Rows = 1
        .Cols = 4
        
        .Col = 0
        .Text = "Order"
        
        .Col = 1
        .Text = "Discreption"
        
        .Col = 2
        .Text = "Type"
        
        .Col = 3
        .Text = "Value"
        
        .ColWidth(0) = 600
        .ColWidth(1) = 3500
        .ColWidth(2) = 0
        .ColWidth(3) = 0
        
        
    End With
End Sub

