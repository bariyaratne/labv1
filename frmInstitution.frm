VERSION 5.00
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmInstitution 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lab Profile"
   ClientHeight    =   10260
   ClientLeft      =   3390
   ClientTop       =   3390
   ClientWidth     =   11025
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmInstitution.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   10260
   ScaleWidth      =   11025
   Begin VB.Frame framInstutions 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   9375
      Left            =   120
      TabIndex        =   11
      Top             =   120
      Width           =   9495
      Begin VB.TextBox txtInsname 
         Height          =   4815
         Left            =   7800
         MultiLine       =   -1  'True
         TabIndex        =   0
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox txtDiscription 
         Height          =   4815
         Left            =   360
         MultiLine       =   -1  'True
         TabIndex        =   1
         Top             =   600
         Width           =   7335
      End
      Begin VB.TextBox txtRegistration 
         Height          =   375
         Left            =   1560
         TabIndex        =   2
         Top             =   5520
         Width           =   3255
      End
      Begin VB.TextBox txtAddress01 
         Height          =   975
         Left            =   2040
         MultiLine       =   -1  'True
         TabIndex        =   3
         Top             =   6000
         Width           =   6375
      End
      Begin VB.TextBox txtTelephone01 
         Height          =   375
         Left            =   6000
         TabIndex        =   4
         Top             =   5520
         Width           =   3135
      End
      Begin VB.TextBox txtTelephone02 
         Height          =   375
         Left            =   2040
         TabIndex        =   5
         Top             =   7080
         Width           =   6375
      End
      Begin VB.TextBox txtEmail01 
         Height          =   375
         Left            =   2040
         TabIndex        =   7
         Top             =   8040
         Width           =   6375
      End
      Begin VB.TextBox txtwbsite01 
         Height          =   375
         Left            =   2040
         TabIndex        =   8
         Top             =   8520
         Width           =   6375
      End
      Begin VB.TextBox txtFax 
         Height          =   375
         Left            =   2040
         TabIndex        =   6
         Top             =   7560
         Width           =   6375
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "Font Size"
         Height          =   255
         Left            =   7080
         TabIndex        =   19
         Top             =   240
         Width           =   2295
      End
      Begin VB.Label Label3 
         BackStyle       =   0  'Transparent
         Caption         =   "Bill Heading"
         Height          =   255
         Left            =   360
         TabIndex        =   18
         Top             =   240
         Width           =   2295
      End
      Begin VB.Label Label4 
         BackStyle       =   0  'Transparent
         Caption         =   "Header Font"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   5520
         Width           =   2295
      End
      Begin VB.Label Label6 
         BackStyle       =   0  'Transparent
         Caption         =   "&Address"
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   6000
         Width           =   2295
      End
      Begin VB.Label Label10 
         BackStyle       =   0  'Transparent
         Caption         =   "Item Font"
         Height          =   255
         Left            =   4920
         TabIndex        =   15
         Top             =   5520
         Width           =   1095
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "&Email "
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   8040
         Width           =   2295
      End
      Begin VB.Label Label14 
         BackStyle       =   0  'Transparent
         Caption         =   "&Website"
         Height          =   255
         Left            =   240
         TabIndex        =   13
         Top             =   8520
         Width           =   2295
      End
      Begin VB.Label Label11 
         BackStyle       =   0  'Transparent
         Caption         =   "&Fax"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   7560
         Width           =   2295
      End
   End
   Begin btButtonEx.ButtonEx bttnOK 
      Height          =   495
      Left            =   9120
      TabIndex        =   10
      Top             =   9600
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Ok"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnCancel 
      Height          =   495
      Left            =   7560
      TabIndex        =   9
      Top             =   9600
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Cancel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnEdit 
      Height          =   495
      Left            =   6000
      TabIndex        =   20
      Top             =   9600
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Edit"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
End
Attribute VB_Name = "frmInstitution"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim TemResponce  As Integer
    Dim rsIns As New ADODB.Recordset
    Dim a As String
    Dim temSQL As String

Private Sub bttnEdit_Click()
    Call AfterEdit
End Sub

Private Sub bttnOk_Click()
'    If Date > #12/18/2009# Then
'        TemResponce = MsgBox("Please contact Lakmedipro for Assistant", vbCritical, "Expired")
'        End
'    End If
    If Trim(txtInsname.text) = "" Then
        TemResponce = MsgBox("You have not entered the name of the institution", vbCritical, "Institution Name?")
        txtInsname.SetFocus
        Exit Sub
    End If
    Unload Me
End Sub

Private Sub Form_Load()
    Call BeforeEdit
    With rsIns
        If .State = 1 Then .Close
        temSQL = "SELECT tblLabDetails.* FROM tblLabDetails"
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount < 1 Then
           TemResponce = MsgBox("Someone had altered the database outside the program. Please contact Lakmedipro for assistant" & vbNewLine & Me.Caption & vbNewLine & Err.Description, vbCritical, "Altered Database")
            Exit Sub
        Else
            .MoveFirst
        End If
        txtInsname.text = DecreptedWord(!Name)
        txtDiscription.text = DecreptedWord(!Description)
        txtRegistration.text = DecreptedWord(!RegNo)
        txtAddress01.text = DecreptedWord(!Address)
        txtTelephone01.text = DecreptedWord(!Telephone1)
        txtTelephone02.text = DecreptedWord(!Telephone2)
        txtFax.text = DecreptedWord(!Fax)
        txtEmail01.text = DecreptedWord(!Email)
        txtwbsite01.text = DecreptedWord(!Web)
        .Close
    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If txtInsname.text = "" Then
        TemResponce = MsgBox("You have not entered an Institution Name", vbCritical, "? Institution Name")
        txtInsname.SetFocus
    End If
    With rsIns
        If .State = 1 Then .Close
        temSQL = "SELECT tblLabDetails.* FROM tblLabDetails"
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount < 1 Then
            TemResponce = MsgBox("Someone had altered the database outside the program. Please contact Lakmedipro for assistant" & vbNewLine & Me.Caption & vbNewLine & Err.Description, vbCritical, "Altered Database")
            .AddNew
        End If
        !Name = EncreptedWord(txtInsname.text)
        !Description = EncreptedWord(txtDiscription.text)
        !RegNo = EncreptedWord(txtRegistration.text)
        !Address = EncreptedWord(txtAddress01.text)
        !Telephone1 = EncreptedWord(txtTelephone01.text)
        !Telephone2 = EncreptedWord(txtTelephone02.text)
        !Fax = EncreptedWord(txtFax.text)
        !Email = EncreptedWord(txtEmail01.text)
        !Web = EncreptedWord(txtwbsite01.text)
        .Update
        .Close
    End With
End Sub


Private Sub bttnCancel_Click()
    Unload Me
End Sub

Private Sub BeforeEdit()
    txtInsname.Enabled = False
    txtDiscription.Enabled = False
    txtRegistration.Enabled = False
    txtAddress01.Enabled = False
    txtTelephone01.Enabled = False
    txtTelephone02.Enabled = False
    txtFax.Enabled = False
    txtEmail01.Enabled = False
    txtwbsite01.Enabled = False
    
    bttnOK.Visible = False
End Sub

Private Sub AfterEdit()
    txtInsname.Enabled = True
    txtDiscription.Enabled = True
    txtRegistration.Enabled = True
    txtAddress01.Enabled = True
    txtTelephone01.Enabled = True
    txtTelephone02.Enabled = True
    txtFax.Enabled = True
    txtEmail01.Enabled = True
    txtwbsite01.Enabled = True
    
    bttnOK.Visible = True
    bttnEdit.Visible = False
End Sub

