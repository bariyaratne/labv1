VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PatientIx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varPatientIxID As Long
    Private varPatientIxBillID As Long
    Private varIxID As Long
    Private varPatientID As Long
    Private varDoctorID As Long
    Private varInstitutionID As Long
    Private varSpecimanNo As String
    Private varSpecimanComments As String
    Private varValue As Double
    Private varHospitalFee As Double
    Private varStaffFee As Double
    Private varOtherFee As Double
    Private varCost As Double
    Private varBilled As Boolean
    Private varBilledDate As Date
    Private varBilledTime As Date
    Private varBilledUserID As Date
    Private varSaved As Boolean
    Private varSavedDate As Date
    Private varSavedTime As Date
    Private varSavedUserID As Long
    Private varPrinted As Boolean
    Private varPrintedDate As Date
    Private varPrintedTime As Date
    Private varPrintedUserID As Long
    Private varIssued As Boolean
    Private varIssuedDate As Date
    Private varIssuedTime As Date
    Private varIssuedUserID As Long
    Private varDeleted As Boolean
    Private varDeletedUserID As Long
    Private varDeletedDate As Date
    Private varDeletedTime As Date
    Private varPaymentMethodID As Date
    Private varCancelled As Boolean
    Private varStaffID As Long

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientIx Where PatientIxID = " & varPatientIxID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !PatientIxBillID = varPatientIxBillID
        !IxID = varIxID
        !PatientID = varPatientID
        !DoctorID = varDoctorID
        !InstitutionID = varInstitutionID
        !SpecimanNo = varSpecimanNo
        !SpecimanComments = varSpecimanComments
        !Value = varValue
        !HospitalFee = varHospitalFee
        !StaffFee = varStaffFee
        !OtherFee = varOtherFee
        !Cost = varCost
        !Billed = varBilled
        !BilledDate = varBilledDate
        !BilledTime = varBilledTime
        !BilledUserID = varBilledUserID
        !Saved = varSaved
        !SavedDate = varSavedDate
        !SavedTime = varSavedTime
        !SavedUserID = varSavedUserID
        !Printed = varPrinted
        !PrintedDate = varPrintedDate
        !PrintedTime = varPrintedTime
        !PrintedUserID = varPrintedUserID
        !Issued = varIssued
        !IssuedDate = varIssuedDate
        !IssuedTime = varIssuedTime
        !IssuedUserID = varIssuedUserID
        !Deleted = varDeleted
        !DeletedUserID = varDeletedUserID
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !PaymentMethodID = varPaymentMethodID
        !Cancelled = varCancelled
        !StaffID = varStaffID
        .Update
        varPatientIxID = !PatientIxID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblPatientIx WHERE PatientIxID = " & varPatientIxID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!PatientIxID) Then
               varPatientIxID = !PatientIxID
            End If
            If Not IsNull(!PatientIxBillID) Then
               varPatientIxBillID = !PatientIxBillID
            End If
            If Not IsNull(!IxID) Then
               varIxID = !IxID
            End If
            If Not IsNull(!PatientID) Then
               varPatientID = !PatientID
            End If
            If Not IsNull(!DoctorID) Then
               varDoctorID = !DoctorID
            End If
            If Not IsNull(!InstitutionID) Then
               varInstitutionID = !InstitutionID
            End If
            If Not IsNull(!SpecimanNo) Then
               varSpecimanNo = !SpecimanNo
            End If
            If Not IsNull(!SpecimanComments) Then
               varSpecimanComments = !SpecimanComments
            End If
            If Not IsNull(!Value) Then
               varValue = !Value
            End If
            If Not IsNull(!HospitalFee) Then
               varHospitalFee = !HospitalFee
            End If
            If Not IsNull(!StaffFee) Then
               varStaffFee = !StaffFee
            End If
            If Not IsNull(!OtherFee) Then
               varOtherFee = !OtherFee
            End If
            If Not IsNull(!Cost) Then
               varCost = !Cost
            End If
            If Not IsNull(!Billed) Then
               varBilled = !Billed
            End If
            If Not IsNull(!BilledDate) Then
               varBilledDate = !BilledDate
            End If
            If Not IsNull(!BilledTime) Then
               varBilledTime = !BilledTime
            End If
            If Not IsNull(!BilledUserID) Then
               varBilledUserID = !BilledUserID
            End If
            If Not IsNull(!Saved) Then
               varSaved = !Saved
            End If
            If Not IsNull(!SavedDate) Then
               varSavedDate = !SavedDate
            End If
            If Not IsNull(!SavedTime) Then
               varSavedTime = !SavedTime
            End If
            If Not IsNull(!SavedUserID) Then
               varSavedUserID = !SavedUserID
            End If
            If Not IsNull(!Printed) Then
               varPrinted = !Printed
            End If
            If Not IsNull(!PrintedDate) Then
               varPrintedDate = !PrintedDate
            End If
            If Not IsNull(!PrintedTime) Then
               varPrintedTime = !PrintedTime
            End If
            If Not IsNull(!PrintedUserID) Then
               varPrintedUserID = !PrintedUserID
            End If
            If Not IsNull(!Issued) Then
               varIssued = !Issued
            End If
            If Not IsNull(!IssuedDate) Then
               varIssuedDate = !IssuedDate
            End If
            If Not IsNull(!IssuedTime) Then
               varIssuedTime = !IssuedTime
            End If
            If Not IsNull(!IssuedUserID) Then
               varIssuedUserID = !IssuedUserID
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!PaymentMethodID) Then
               varPaymentMethodID = !PaymentMethodID
            End If
            If Not IsNull(!Cancelled) Then
               varCancelled = !Cancelled
            End If
            If Not IsNull(!StaffID) Then
               varStaffID = !StaffID
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varPatientIxID = 0
    varPatientIxBillID = 0
    varIxID = 0
    varPatientID = 0
    varDoctorID = 0
    varInstitutionID = 0
    varSpecimanNo = Empty
    varSpecimanComments = Empty
    varValue = 0
    varHospitalFee = 0
    varStaffFee = 0
    varOtherFee = 0
    varCost = 0
    varBilled = False
    varBilledDate = Empty
    varBilledTime = Empty
    varBilledUserID = Empty
    varSaved = False
    varSavedDate = Empty
    varSavedTime = Empty
    varSavedUserID = 0
    varPrinted = False
    varPrintedDate = Empty
    varPrintedTime = Empty
    varPrintedUserID = 0
    varIssued = False
    varIssuedDate = Empty
    varIssuedTime = Empty
    varIssuedUserID = 0
    varDeleted = False
    varDeletedUserID = 0
    varDeletedDate = Empty
    varDeletedTime = Empty
    varPaymentMethodID = Empty
    varCancelled = False
    varStaffID = 0
End Sub

Public Property Let PatientIxID(ByVal vPatientIxID As Long)
    Call clearData
    varPatientIxID = vPatientIxID
    Call loadData
End Property

Public Property Get PatientIxID() As Long
    PatientIxID = varPatientIxID
End Property

Public Property Let PatientIxBillID(ByVal vPatientIxBillID As Long)
    varPatientIxBillID = vPatientIxBillID
End Property

Public Property Get PatientIxBillID() As Long
    PatientIxBillID = varPatientIxBillID
End Property

Public Property Let IxID(ByVal vIxID As Long)
    varIxID = vIxID
End Property

Public Property Get IxID() As Long
    IxID = varIxID
End Property

Public Property Let PatientID(ByVal vPatientID As Long)
    varPatientID = vPatientID
End Property

Public Property Get PatientID() As Long
    PatientID = varPatientID
End Property

Public Property Let DoctorID(ByVal vDoctorID As Long)
    varDoctorID = vDoctorID
End Property

Public Property Get DoctorID() As Long
    DoctorID = varDoctorID
End Property

Public Property Let InstitutionID(ByVal vInstitutionID As Long)
    varInstitutionID = vInstitutionID
End Property

Public Property Get InstitutionID() As Long
    InstitutionID = varInstitutionID
End Property

Public Property Let SpecimanNo(ByVal vSpecimanNo As String)
    varSpecimanNo = vSpecimanNo
End Property

Public Property Get SpecimanNo() As String
    SpecimanNo = varSpecimanNo
End Property

Public Property Let SpecimanComments(ByVal vSpecimanComments As String)
    varSpecimanComments = vSpecimanComments
End Property

Public Property Get SpecimanComments() As String
    SpecimanComments = varSpecimanComments
End Property

Public Property Let Value(ByVal vValue As Double)
    varValue = vValue
End Property

Public Property Get Value() As Double
    Value = varValue
End Property

Public Property Let HospitalFee(ByVal vHospitalFee As Double)
    varHospitalFee = vHospitalFee
End Property

Public Property Get HospitalFee() As Double
    HospitalFee = varHospitalFee
End Property

Public Property Let StaffFee(ByVal vStaffFee As Double)
    varStaffFee = vStaffFee
End Property

Public Property Get StaffFee() As Double
    StaffFee = varStaffFee
End Property

Public Property Let OtherFee(ByVal vOtherFee As Double)
    varOtherFee = vOtherFee
End Property

Public Property Get OtherFee() As Double
    OtherFee = varOtherFee
End Property

Public Property Let Cost(ByVal vCost As Double)
    varCost = vCost
End Property

Public Property Get Cost() As Double
    Cost = varCost
End Property

Public Property Let Billed(ByVal vBilled As Boolean)
    varBilled = vBilled
End Property

Public Property Get Billed() As Boolean
    Billed = varBilled
End Property

Public Property Let BilledDate(ByVal vBilledDate As Date)
    varBilledDate = vBilledDate
End Property

Public Property Get BilledDate() As Date
    BilledDate = varBilledDate
End Property

Public Property Let BilledTime(ByVal vBilledTime As Date)
    varBilledTime = vBilledTime
End Property

Public Property Get BilledTime() As Date
    BilledTime = varBilledTime
End Property

Public Property Let BilledUserID(ByVal vBilledUserID As Date)
    varBilledUserID = vBilledUserID
End Property

Public Property Get BilledUserID() As Date
    BilledUserID = varBilledUserID
End Property

Public Property Let Saved(ByVal vSaved As Boolean)
    varSaved = vSaved
End Property

Public Property Get Saved() As Boolean
    Saved = varSaved
End Property

Public Property Let SavedDate(ByVal vSavedDate As Date)
    varSavedDate = vSavedDate
End Property

Public Property Get SavedDate() As Date
    SavedDate = varSavedDate
End Property

Public Property Let SavedTime(ByVal vSavedTime As Date)
    varSavedTime = vSavedTime
End Property

Public Property Get SavedTime() As Date
    SavedTime = varSavedTime
End Property

Public Property Let SavedUserID(ByVal vSavedUserID As Long)
    varSavedUserID = vSavedUserID
End Property

Public Property Get SavedUserID() As Long
    SavedUserID = varSavedUserID
End Property

Public Property Let Printed(ByVal vPrinted As Boolean)
    varPrinted = vPrinted
End Property

Public Property Get Printed() As Boolean
    Printed = varPrinted
End Property

Public Property Let PrintedDate(ByVal vPrintedDate As Date)
    varPrintedDate = vPrintedDate
End Property

Public Property Get PrintedDate() As Date
    PrintedDate = varPrintedDate
End Property

Public Property Let PrintedTime(ByVal vPrintedTime As Date)
    varPrintedTime = vPrintedTime
End Property

Public Property Get PrintedTime() As Date
    PrintedTime = varPrintedTime
End Property

Public Property Let PrintedUserID(ByVal vPrintedUserID As Long)
    varPrintedUserID = vPrintedUserID
End Property

Public Property Get PrintedUserID() As Long
    PrintedUserID = varPrintedUserID
End Property

Public Property Let Issued(ByVal vIssued As Boolean)
    varIssued = vIssued
End Property

Public Property Get Issued() As Boolean
    Issued = varIssued
End Property

Public Property Let IssuedDate(ByVal vIssuedDate As Date)
    varIssuedDate = vIssuedDate
End Property

Public Property Get IssuedDate() As Date
    IssuedDate = varIssuedDate
End Property

Public Property Let IssuedTime(ByVal vIssuedTime As Date)
    varIssuedTime = vIssuedTime
End Property

Public Property Get IssuedTime() As Date
    IssuedTime = varIssuedTime
End Property

Public Property Let IssuedUserID(ByVal vIssuedUserID As Long)
    varIssuedUserID = vIssuedUserID
End Property

Public Property Get IssuedUserID() As Long
    IssuedUserID = varIssuedUserID
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let DeletedDate(ByVal vDeletedDate As Date)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate() As Date
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime As Date)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime() As Date
    DeletedTime = varDeletedTime
End Property

Public Property Let PaymentMethodID(ByVal vPaymentMethodID As Date)
    varPaymentMethodID = vPaymentMethodID
End Property

Public Property Get PaymentMethodID() As Date
    PaymentMethodID = varPaymentMethodID
End Property

Public Property Let Cancelled(ByVal vCancelled As Boolean)
    varCancelled = vCancelled
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = varCancelled
End Property

Public Property Let StaffID(ByVal vStaffID As Long)
    varStaffID = vStaffID
End Property

Public Property Get StaffID() As Long
    StaffID = varStaffID
End Property


