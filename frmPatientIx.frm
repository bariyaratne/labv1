VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmPatientIx 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Adding Investigations"
   ClientHeight    =   8370
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8220
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8370
   ScaleWidth      =   8220
   Begin VB.TextBox txtDiscountPercent 
      Height          =   375
      Left            =   7560
      TabIndex        =   27
      Top             =   3840
      Width           =   495
   End
   Begin btButtonEx.ButtonEx bttnSettle 
      Height          =   375
      Left            =   7080
      TabIndex        =   18
      Top             =   6600
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Settle"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnDelete 
      Height          =   375
      Left            =   7080
      TabIndex        =   10
      Top             =   3360
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Delete"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx bttnAdd 
      Height          =   375
      Left            =   7080
      TabIndex        =   8
      Top             =   2880
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Add"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Frame Frame3 
      Height          =   1695
      Left            =   120
      TabIndex        =   22
      Top             =   6600
      Width           =   6855
      Begin VB.TextBox txtGTotal 
         Height          =   375
         Left            =   1800
         TabIndex        =   12
         Top             =   240
         Width           =   3975
      End
      Begin VB.TextBox txtDiscount 
         Height          =   360
         Left            =   1800
         TabIndex        =   14
         Top             =   735
         Width           =   3975
      End
      Begin VB.TextBox txtNTotal 
         Height          =   375
         Left            =   1800
         TabIndex        =   16
         Top             =   1200
         Width           =   3975
      End
      Begin VB.TextBox txtCost 
         Height          =   375
         Left            =   5880
         TabIndex        =   23
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "&Total"
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   1815
      End
      Begin VB.Label Label4 
         Caption         =   "Dis&count"
         Height          =   375
         Left            =   120
         TabIndex        =   13
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label5 
         Caption         =   "N&et Total"
         Height          =   375
         Left            =   120
         TabIndex        =   15
         Top             =   1200
         Width           =   1935
      End
   End
   Begin VB.Frame Frame2 
      Height          =   3375
      Left            =   120
      TabIndex        =   21
      Top             =   3240
      Width           =   6855
      Begin MSFlexGridLib.MSFlexGrid GridIx 
         Height          =   3015
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   6615
         _ExtentX        =   11668
         _ExtentY        =   5318
         _Version        =   393216
         SelectionMode   =   1
      End
   End
   Begin VB.Frame Frame1 
      Height          =   2775
      Left            =   120
      TabIndex        =   20
      Top             =   0
      Width           =   7935
      Begin VB.TextBox txtID 
         Height          =   375
         Left            =   6480
         TabIndex        =   26
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox txtPatientDetails 
         Height          =   975
         Left            =   2520
         MultiLine       =   -1  'True
         TabIndex        =   25
         Top             =   720
         Width           =   5295
      End
      Begin MSDataListLib.DataCombo dtcpatientName 
         Height          =   360
         Left            =   2520
         TabIndex        =   1
         Top             =   240
         Width           =   3855
         _ExtentX        =   6800
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo cmbDoc 
         Height          =   360
         Left            =   2520
         TabIndex        =   3
         Top             =   1800
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo cmbIns 
         Height          =   360
         Left            =   2520
         TabIndex        =   5
         Top             =   2280
         Width           =   5295
         _ExtentX        =   9340
         _ExtentY        =   635
         _Version        =   393216
         MatchEntry      =   -1  'True
         Style           =   2
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label8 
         Caption         =   "Patient Details"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label Label7 
         Caption         =   "Referring Institution"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   2280
         Width           =   2535
      End
      Begin VB.Label Label3 
         Caption         =   "Referring Doctor"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   1800
         Width           =   1575
      End
      Begin VB.Label Label1 
         Caption         =   "Patient &Name"
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   1335
      End
   End
   Begin VB.CheckBox chkPrint 
      Caption         =   "&Print"
      Height          =   375
      Left            =   7200
      TabIndex        =   17
      Top             =   6120
      Value           =   1  'Checked
      Width           =   855
   End
   Begin btButtonEx.ButtonEx bttnClose 
      Height          =   375
      Left            =   7080
      TabIndex        =   19
      Top             =   7080
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Close"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo dtcIx 
      Height          =   360
      Left            =   1440
      TabIndex        =   7
      Top             =   2880
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   635
      _Version        =   393216
      MatchEntry      =   -1  'True
      Style           =   2
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label6 
      Caption         =   "&Investigation"
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   2880
      Width           =   1215
   End
End
Attribute VB_Name = "frmPatientIx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim rsViewPatient As New ADODB.Recordset
    Dim rsViewIx As New ADODB.Recordset
    Dim rsPatientIxBill As New ADODB.Recordset
    Dim rsPatientIx As New ADODB.Recordset
    Dim rsPatient As New ADODB.Recordset
    Dim rsIx As New ADODB.Recordset
    Dim temSql As String
    Dim temPatientIxBillID As Long
    Dim cSetDfltPrinter As New cSetDfltPrinter
    Dim temtubeID() As Long
    Dim temTubeVol() As Double
    Dim temTube() As String
    Dim temTubeCount As Long
    
Private Sub bttnAdd_Click()
    Dim tr As Integer
    Dim temPatient As String
    Dim temValue As Double
    Dim temCost As Double
    Dim ItemTubeID As Long
    Dim ItemTubeVol As Double
    If IsNumeric(dtcIx.BoundText) = False Then
        MsgBox "Ix?"
        dtcIx.SetFocus
        Exit Sub
    End If
    With GridIx
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 0
        .Text = .Row
        .Col = 1
        .Text = dtcIx.Text
        With rsIx
            If .State = 1 Then .Close
            temSql = "SELECT * FROM tblIX WHERE IxID = " & Val(dtcIx.BoundText)
            .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
            If .RecordCount > 0 Then
                If IsNull(!Value) = False Then
                    temValue = !Value
                End If
                If IsNull(!Cost) = False Then
                    temCost = !Cost
                End If
                If IsNull(!TubeID) = False Then
                    ItemTubeID = !TubeID
                End If
                If IsNull(!TubeVolume) = False Then
                    ItemTubeVol = !TubeVolume
                End If
            End If
            .Close
        End With
        .Col = 2
        .Text = Format(temValue, "0.00")
        .Col = 3
        .Text = temCost
        .Col = 4
        .Text = dtcIx.BoundText
        .Col = 5
        .Text = ItemTubeID
        .Col = 6
        .Text = ItemTubeVol
    End With
    With GridIx
        If .Rows > 9 Then
            .TopRow = .Rows - 8
        End If
    End With
    Call ClearAddValues
    Call CalculateGrossTotal
    Call CalculateNetTotal
    dtcIx.SetFocus
End Sub

Private Sub CalculateGrossTotal()
    Dim i As Integer
    Dim temTotalValue  As Double
    Dim temTotalCost As Double
    With GridIx
        For i = 1 To .Rows - 1
            temTotalValue = temTotalValue + Val(.TextMatrix(i, 2))
            temTotalCost = temTotalCost + Val(.TextMatrix(i, 3))
        Next
    End With
    txtGTotal.Text = Format(temTotalValue, "0.00")
    txtCost.Text = temTotalCost
End Sub

Private Sub CalculateNetTotal()
    txtNTotal.Text = Format((Val(txtGTotal.Text) - Val(txtDiscount.Text)), "0.00")
End Sub

Private Sub bttnClose_Click()
    Unload Me
End Sub

Private Sub bttnDelete_Click()
    Dim i As Integer
    With GridIx
        If .Rows <= 1 Then
            Exit Sub
        ElseIf .Row = 1 Then
            FormatGrid
        Else
            .RemoveItem (.Row)
            For i = 1 To .Rows - 1
                .TextMatrix(i, 0) = i
            Next
        End If
    End With
    Call CalculateGrossTotal
    Call CalculateNetTotal
    Call ClearAddValues
    dtcIx.SetFocus
End Sub

Private Sub bttnSettle_Click()
    Dim temPatientID As Long
    If dtcpatientName.Text = Empty Then
        MsgBox "Name?"
        dtcpatientName.SetFocus
        Exit Sub
    End If
    If IsNumeric(dtcpatientName.BoundText) = False Then
        With rsPatient
            If .State = 1 Then .Close
            temSql = "SELECT * FROM tblPatient"
            .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
            .AddNew
            !Name = dtcpatientName.Text
            .Update
            temPatientID = !PatientID
            .Close
        End With
        Call FillCOmbos
        dtcpatientName.BoundText = temPatientID
    Else
        temPatientID = dtcpatientName.BoundText
    End If
    
    
    Call WritePatientIxBill
    Call WritePatientIx
    
    Call CalculateTubeVol
    
    If chkPrint.Value = 1 Then Call eLabPrint(temPatientID)
    Call FormatGrid
    Call ClearAddValues
    Call ClearSettleValues
    dtcpatientName.SetFocus
    
    
    
    
End Sub

Private Sub CalculateTubeVol()
    Dim i As Integer
    Dim MyTubeID As Long
    For i = 0 To temTubeCount - 1
        temTubeVol(i) = 0
    Next
    
    With GridIx
        For i = 1 To .Rows - 1
            For MyTubeID = 0 To temTubeCount - 1
                If temtubeID(MyTubeID) = Val(.TextMatrix(i, 5)) Then
                    temTubeVol(MyTubeID) = temTubeVol(MyTubeID) + Val(.TextMatrix(i, 6))
                End If
            Next
        Next
    End With
End Sub

Private Sub WritePatientIxBill()
    With rsPatientIxBill
        If .State = 1 Then .Close
        temSql = "SELECT * FROM tblPatientIxBill"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
        .AddNew
        !Date = Date
        !Time = Time
        !UserID = UserID
        !Value = Val(txtGTotal.Text)
        !Discount = Val(txtDiscount.Text)
        !NetValue = Val(txtNTotal.Text)
        !Cost = Val(txtCost.Text)
        !Cancelled = False
        !PatientID = dtcpatientName.BoundText
        !DoctorID = Val(cmbDoc.BoundText)
        !InstitutionID = Val(cmbIns.BoundText)
        .Update
        temPatientIxBillID = !PatientIxBillID
        .Close
    End With
End Sub

Private Sub WritePatientIx()
    Dim i As Integer
    With rsPatientIx
        If .State = 1 Then .Close
        temSql = "SELECT * from tblPatientIX"
        .Open temSql, cnnLab, adOpenStatic, adLockOptimistic
    End With
    With GridIx
        For i = 1 To .Rows - 1
            With rsPatientIx
                .AddNew
                !PatientIxBillID = temPatientIxBillID
                !IxID = Val(GridIx.TextMatrix(i, 4))
                !Value = Val(GridIx.TextMatrix(i, 2))
                !Cost = Val(GridIx.TextMatrix(i, 3))
                !Printed = False
                !Issued = False
                !Deleted = False
                !PatientID = dtcpatientName.BoundText
                !Billed = True
                !BilledDate = Date
                !BilledTime = Time
                !BilledUserID = UserID
                !DoctorID = Val(cmbDoc.BoundText)
                !InstitutionID = Val(cmbIns.BoundText)
            .Update
            End With
        Next
    End With
End Sub

Private Sub ClearSettleValues()
    dtcpatientName.Text = Empty
    cmbDoc.Text = Empty
    cmbIns.Text = Empty
    txtCost.Text = Empty
    txtDiscount.Text = Empty
    txtGTotal.Text = Empty
    txtNTotal.Text = Empty
End Sub

Private Sub ClearAddValues()
    dtcIx.Text = Empty
End Sub



Private Sub cmbDoc_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        dtcIx.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        cmbDoc.Text = Empty
    End If
End Sub

Private Sub cmbIns_Change()
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSql = "Select * from tblInstitution where Deleted = False and InstitutionID = " & Val(cmbIns.BoundText)
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If IsNull(!DiscountPercent) = False Then
                txtDiscountPercent.Text = !DiscountPercent
            Else
                txtDiscountPercent.Text = 0
            End If
        Else
            txtDiscountPercent.Text = 0
        End If
    End With
End Sub

Private Sub cmbIns_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbIns.Text = Empty
    ElseIf KeyCode = vbKeyReturn Then
        KeyCode = Empty
        dtcIx.SetFocus
    End If
End Sub

Private Sub dtcIx_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        bttnAdd_Click
    ElseIf KeyCode = vbKeyEscape Then
        dtcIx.Text = Empty
    End If
End Sub

Private Sub dtcpatientName_Change()

    Dim MyPt As New clsPatient
    MyPt.ID = Val(dtcpatientName.BoundText)
    txtPatientDetails.Text = "Name:" & vbTab & MyPt.NameWithTitle & vbNewLine
    txtPatientDetails.Text = txtPatientDetails.Text & "Age:" & vbTab & MyPt.AgeInWords & vbNewLine
    txtPatientDetails.Text = txtPatientDetails.Text & "Sex:" & vbTab & MyPt.Sex & vbNewLine
    
End Sub

Private Sub dtcpatientName_Click(Area As Integer)

    Dim MyPt As New clsPatient
    MyPt.ID = Val(dtcpatientName.BoundText)
    txtPatientDetails.Text = "Name:" & vbTab & MyPt.NameWithTitle & vbNewLine
    txtPatientDetails.Text = txtPatientDetails.Text & "Age:" & vbTab & MyPt.AgeInWords & vbNewLine
    txtPatientDetails.Text = txtPatientDetails.Text & "Sex:" & vbTab & MyPt.Sex & vbNewLine
    
End Sub

Private Sub dtcpatientName_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyReturn Then
        KeyCode = Empty
        cmbDoc.SetFocus
    ElseIf KeyCode = vbKeyEscape Then
        dtcpatientName.Text = Empty
    End If
End Sub

Private Sub Form_Load()
    Call FillCOmbos
    Call FormatGrid
    Call Tubes
    bttnDelete.Enabled = False
    Call GetSettings
End Sub

Private Sub GetSettings()
    chkPrint.Value = GetSetting(App.EXEName, Me.Name, chkPrint.Name, 0)
End Sub

Private Sub SaveSettings()
    SaveSetting App.EXEName, Me.Name, chkPrint.Name, chkPrint.Value
End Sub

Private Sub Tubes()
    Dim i As Integer
    Dim rsTube As New ADODB.Recordset
    With rsTube
        If .State = 1 Then .Close
        temSql = "Select * from tblTube where Deleted = false"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            .MoveLast
            ReDim temtubeID(.RecordCount)
            ReDim temTubeVol(.RecordCount)
            ReDim temTube(.RecordCount)
            temTubeCount = .RecordCount
            .MoveFirst
            For i = 0 To .RecordCount - 1
                temtubeID(i) = !TubeID
                temTube(i) = !Tube
                .MoveNext
            Next
        End If
    End With
End Sub

Private Sub FormatGrid()
    With GridIx
        .Rows = 1
        .Cols = 7
        .Row = 0
        .Col = 0
        .CellAlignment = 4
        .Text = "No."
        .Col = 1
        .CellAlignment = 4
        .Text = "Investigation"
        .Col = 2
        .CellAlignment = 4
        .Text = "Value"
        .Col = 3
        .Text = "Cost"
        .Col = 4
        .Text = "IxID"
        .ColWidth(0) = 600
        .ColWidth(2) = 1100
        .ColWidth(3) = 1
        .ColWidth(4) = 1
        .ColWidth(1) = .Width - (.ColWidth(0) + .ColWidth(2) + 100)
        .ColWidth(5) = 0
        .ColWidth(6) = 0
        
        .Col = 5
        .Text = "TubeID"
        
        .Col = 6
        .Text = "Tube Vol"
        
    End With

End Sub

Public Sub FillCOmbos()
    With rsViewIx
        If .State = 1 Then .Close
        temSql = "SELECT * FROM tblIx  WHERE Deleted = False ORDER BY Ix"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcIx
        Set .RowSource = rsViewIx
        .ListField = "Ix"
        .BoundColumn = "IxID"
    End With
    With rsViewPatient
        If .State = 1 Then .Close
        temSql = "SELECT * FROM tblPatient WHERE Deleted = False ORDER BY Name"
        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcpatientName
        Set .RowSource = rsViewPatient
        .ListField = "Name"
        .BoundColumn = "PatientID"
    End With
    Dim RDoc As New clsFillCombos
    Dim RIns As New clsFillCombos
    RDoc.FillAnyCombo cmbDoc, "Doctor", True
    RIns.FillAnyCombo cmbIns, "Institution", True
End Sub


Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    SaveSettings
End Sub

Private Sub GridIx_Click()
    If GridIx.Row > 0 Then
        bttnDelete.Enabled = True
    Else
        bttnDelete.Enabled = False
    End If
End Sub

Private Sub txtDiscount_Change()
    Call CalculateNetTotal
End Sub

Private Sub eLabPrint(PatientID As Long)
    Dim csetPrinter     As New cSetDfltPrinter
    Dim MyPatient As New clsPatient
    MyPatient.ID = PatientID
    csetPrinter.SetPrinterAsDefault BillPrinterName
    If SelectForm(BillPaperName, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    Dim i As Integer
    Dim Tab1 As Integer
    Dim Tab2 As Integer
    Dim Tab3 As Integer
    Dim Tab4 As Integer
    Dim Tab5 As Integer
    Dim tab6 As Integer
    Dim tab7 As Integer
    Dim tab8 As Integer
    Dim tab9 As Integer
    
    Tab1 = 5
    Tab2 = 10
    Tab3 = 45
    Tab4 = 43
    Tab5 = 43
    tab6 = 4
    tab7 = 6
    tab8 = 3
    tab9 = 40
    
    Printer.FontName = "Courier"
    Printer.FontSize = 12
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print

    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print

    ' 7.0
    
    Tab1 = 5
    Tab2 = 10
    Tab3 = 45
    Tab4 = 43

    With GridIx
        For i = 1 To .Rows - 1
            Printer.Print Tab(Tab1); .TextMatrix(i, 0); Tab(Tab2); Left(.TextMatrix(i, 1), 22); Tab(Tab3); Right((Space(10)) & .TextMatrix(i, 2), 10)
        Next i
    End With
    Tab1 = 5
    Tab2 = 10
    Tab3 = 45
    Tab4 = 43
    With Printer
        Printer.Print Tab(Tab1); "----------------------------------------------------------"
        Printer.Print Tab(Tab1); "Gross Total"; Tab(Tab3); Right((Space(10)) & (txtGTotal.Text), 10)
        If Val(txtDiscount.Text) > 0 Then
            Printer.Print Tab(Tab1); "Discount"; Tab(Tab3); Right((Space(10)) & (txtDiscount.Text), 10)
            Printer.Print Tab(Tab1); "Net Total"; Tab(Tab3); Right((Space(10)) & (txtNTotal.Text), 10)
        End If
        
        Printer.Print
        Printer.Print Tab(Tab1); UserName
        .EndDoc
    End With
End Sub

Private Sub ClearBillValues()
    Call ClearAddValues
    Call FormatGrid
    txtGTotal.Text = "0.00"
    txtNTotal.Text = "0.00"
    txtDiscount.Text = "0.00"
End Sub

Private Sub txtDiscountPercent_Change()
    If Val(txtDiscountPercent.Text) = 0 Then
        txtDiscount.Text = 0
    Else
        txtDiscount.Text = Val(txtGTotal.Text) - (Val(txtGTotal.Text) * 100 / Val(txtDiscountPercent.Text))
    End If
End Sub

Private Sub txtID_Change()
    dtcpatientName.BoundText = Val(txtID.Text)
End Sub
