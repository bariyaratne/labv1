VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPrintItem"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarPrintText As String 'local copy
Private mvarByCoordinates As Boolean 'local copy
Private mvarByTabs As Boolean 'local copy
Private mvarX1 As Integer 'local copy
Private mvarX2 As Integer 'local copy
Private mvarY1 As Integer 'local copy
Private mvarY2 As Integer 'local copy
Private mvarTab1 As Integer 'local copy
Private mvarTab2 As Integer 'local copy
Public Property Let Tab2(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Tab2 = 5
    mvarTab2 = vData
End Property


Public Property Get Tab2() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Tab2
    Tab2 = mvarTab2
End Property



Public Property Let Tab1(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Tab1 = 5
    mvarTab1 = vData
End Property


Public Property Get Tab1() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Tab1
    Tab1 = mvarTab1
End Property



Public Property Let Y2(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Y2 = 5
    mvarY2 = vData
End Property


Public Property Get Y2() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Y2
    Y2 = mvarY2
End Property



Public Property Let Y1(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Y1 = 5
    mvarY1 = vData
End Property


Public Property Get Y1() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Y1
    Y1 = mvarY1
End Property



Public Property Let X2(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.X2 = 5
    mvarX2 = vData
End Property


Public Property Get X2() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.X2
    X2 = mvarX2
End Property



Public Property Let X1(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.X1 = 5
    mvarX1 = vData
End Property


Public Property Get X1() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.X1
    X1 = mvarX1
End Property



Public Property Let ByTabs(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ByTabs = 5
    mvarByTabs = vData
End Property


Public Property Get ByTabs() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ByTabs
    ByTabs = mvarByTabs
End Property



Public Property Let ByCoordinates(ByVal vData As Boolean)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ByCoordinates = 5
    mvarByCoordinates = vData
End Property


Public Property Get ByCoordinates() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ByCoordinates
    ByCoordinates = mvarByCoordinates
End Property



Public Property Let PrintText(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PrintText = 5
    mvarPrintText = vData
End Property


Public Property Get PrintText() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PrintText
    PrintText = mvarPrintText
End Property



