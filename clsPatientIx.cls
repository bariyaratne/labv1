VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPatientIx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarPatientIxID As Long 'local copy
Private mvarPatientID As Long 'local copy
Private mvarIxID As Long 'local copy
Private mvarSaved As Boolean 'local copy
Private mvarSavedTime As Date 'local copy
Private mvarSavedDate As Date 'local copy
Private mvarPrinted As Boolean 'local copy
Private mvarPrintedDate As Date 'local copy
Private mvarPrintedTime As Date 'local copy
Private mvarIssued As Boolean 'local copy
Private mvarIssuedDate As Date 'local copy
Private mvarIssuedTime As Date 'local copy


Public Property Get IssuedTime() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IssuedTime
    IssuedTime = mvarIssuedTime
End Property


Public Property Get IssuedDate() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IssuedDate
    IssuedDate = mvarIssuedDate
End Property


Public Property Get Issued() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Issued
    Issued = mvarIssued
End Property

Public Property Get PrintedTime() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PrintedTime
    PrintedTime = mvarPrintedTime
End Property

Public Property Get PrintedDate() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PrintedDate
    PrintedDate = mvarPrintedDate
End Property

Public Property Get Printed() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Printed
    Printed = mvarPrinted
End Property

Public Property Get SavedDate() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SavedDate
    SavedDate = mvarSavedDate
End Property

Public Property Get SavedTime() As Date
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.SavedTime
    SavedTime = mvarSavedTime
End Property

Public Property Get Saved() As Boolean
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Saved
    Saved = mvarSaved
End Property

Public Property Get IxID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.IxID
    IxID = mvarIxID
End Property

Public Property Get PatientID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PatientID
    PatientID = mvarPatientID
End Property

Public Property Let PatientIxID(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PatientIxID = 5
    Call ClearData
    mvarPatientIxID = vData
    Dim rsTem As New ADODB.Recordset
    Dim temSQL As String
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblPatientIx where PatientIxID = " & mvarPatientIxID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            mvarPatientID = !PatientID
            mvarIxID = !IxID
            mvarSaved = !Saved
            If !Saved = True Then
                mvarSavedTime = !SavedTime
                mvarSavedDate = !SavedDate
            End If
            mvarPrinted = !Printed
            If !Printed = True Then
                mvarPrintedDate = !PrintedDate
                mvarPrintedTime = !PrintedTime
            End If
            mvarIssued = !Issued
            If !Issued = True Then
                mvarIssuedDate = !IssuedDate
                mvarIssuedTime = !IssuedTime
            End If
        End If
        .Close
    End With
End Property


Public Property Get PatientIxID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PatientIxID
    PatientIxID = mvarPatientIxID
End Property

Private Sub ClearData()
 mvarPatientID = 0
 mvarIxID = 0
 mvarSaved = False
 mvarSavedTime = Empty
 mvarSavedDate = Empty
 mvarPrinted = False
 mvarPrintedDate = Empty
 mvarPrintedTime = Empty
 mvarIssued = False
 mvarIssuedDate = Empty
 mvarIssuedTime = Empty

End Sub

