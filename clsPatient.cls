VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPatient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit
    Dim NameValue As String
    Dim IDValue As Long
    Dim AgeInWordsValue As String
    Dim AgeInDaysValue As Long
    Dim AgeInMonthsValue As Long
    Dim AgeInYearsValue As Long
    Dim SexValueValue As String
    Dim AddressValue As String
    Dim DateOfBirthValue As Date
    Dim PhoneValue As String
    Dim TitleValue As String
    Dim NameWithTitleValue As String
    
    
    Dim temSQL As String
    Dim rsTem As New ADODB.Recordset


Public Property Let ID(PatientID As Long)
    IDValue = PatientID
    With rsTem
        If .State = 1 Then .Close
        temSQL = "SELECT tblTitle.Title, tblSex.Sex, tblPatient.* " & _
                    "FROM tblSex RIGHT JOIN (tblTitle RIGHT JOIN tblPatient ON tblTitle.TitleID = tblPatient.TitleID) ON tblSex.SexID = tblPatient.SexID " & _
                    "WHERE (((tblPatient.PatientID)=" & IDValue & "))"

        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            If Not IsNull(!Name) Then NameValue = !Name
            If Not IsNull(!Address) Then AddressValue = !Address
            If Not IsNull(!DateOfBirth) Then DateOfBirthValue = !DateOfBirth
            If Not IsNull(!Phone) Then PhoneValue = !Phone
            If Not IsNull(!Sex) Then SexValueValue = !Sex
            If Not IsNull(!Title) Then TitleValue = !Title
            NameWithTitleValue = TitleValue & " " & NameValue
            AgeInWordsValue = CalculateAgeInWords(DateOfBirthValue)
            AgeInYearsValue = DateDiff("yyyy", DateOfBirthValue, Date)
            AgeInMonthsValue = DateDiff("m", DateOfBirthValue, Date)
            AgeInDaysValue = DateDiff("d", DateOfBirthValue, Date)
        Else
            AddressValue = Empty
            DateOfBirthValue = Empty
            PhoneValue = Empty
            SexValueValue = Empty
            TitleValue = Empty
            NameWithTitleValue = Empty
            AgeInWordsValue = Empty
            AgeInYearsValue = Empty
            AgeInMonthsValue = Empty
            AgeInDaysValue = Empty
        End If
        .Close
    End With
End Property

Private Function CalculateAgeInWords(PatientDOB As Date) As String
    Dim Age As Long
    Age = DateDiff("yyyy", PatientDOB, Now)
    If Age >= 5 Then
        CalculateAgeInWords = Age & " Years"
        Exit Function
    Else
        Age = DateDiff("m", PatientDOB, Now)
        If Age > 48 Then CalculateAgeInWords = "4" & " Years and " & Age - 48 & " months": Exit Function
        If Age = 48 Then CalculateAgeInWords = "4" & " Years": Exit Function
        If Age > 36 Then CalculateAgeInWords = "3" & " Years and " & Age - 36 & " months": Exit Function
        If Age = 36 Then CalculateAgeInWords = "3" & " Years": Exit Function
        If Age > 24 Then CalculateAgeInWords = "2" & " Years and " & Age - 24 & " months": Exit Function
        If Age = 24 Then CalculateAgeInWords = "2" & " Years": Exit Function
        If Age > 12 Then CalculateAgeInWords = "1" & " Years and " & Age - 12 & " months": Exit Function
        If Age = 12 Then CalculateAgeInWords = "1" & " Year": Exit Function
        If Age >= 1 Then CalculateAgeInWords = Age & " Months": Exit Function
'        Age = DateDiff("d", PatientDOB, Now)
'        CalculateAgeInWords = Age & " Days": Exit Function
            CalculateAgeInWords = ""
        Exit Function
    End If
End Function

Public Property Get ID() As Long
    ID = IDValue
End Property

Public Property Get Name() As String
    Name = NameValue
End Property

Public Property Get AgeInWords() As String
    AgeInWords = AgeInWordsValue
End Property

Public Property Get AgeInDays() As Long
    AgeInDays = AgeInDaysValue
End Property

Public Property Get AgeInMonths() As Long
    AgeInMonths = AgeInMonthsValue
End Property

Public Property Get AgeInYears() As Long
    AgeInYears = AgeInYearsValue
End Property

Public Property Get DateOfBirth() As Date
    DateOfBirth = DateOfBirthValue
End Property

Public Property Get Sex() As String
    Sex = SexValueValue
End Property

Public Property Get Title() As String
    Title = TitleValue
End Property

Public Property Get NameWithTitle() As String
    NameWithTitle = NameWithTitleValue
End Property

Public Property Get Address() As String
    Address = AddressValue
End Property

Public Property Get Phone() As String
    Phone = PhoneValue
End Property
