VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Begin VB.Form frmReportFormatNew 
   Caption         =   "Form1"
   ClientHeight    =   8565
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   12000
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   8565
   ScaleWidth      =   12000
   Begin VB.CommandButton Command3 
      Caption         =   "Command3"
      Height          =   495
      Left            =   4920
      TabIndex        =   12
      Top             =   3240
      Width           =   1215
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   495
      Left            =   4920
      TabIndex        =   11
      Top             =   3840
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   2880
      TabIndex        =   10
      Top             =   3360
      Width           =   1215
   End
   Begin VB.TextBox txtY 
      Height          =   495
      Left            =   0
      TabIndex        =   9
      Top             =   1440
      Width           =   6015
   End
   Begin VB.TextBox txtX 
      Height          =   495
      Left            =   0
      TabIndex        =   8
      Text            =   "Text1"
      Top             =   720
      Width           =   6015
   End
   Begin MSComCtl2.FlatScrollBar scbHorizontal 
      Height          =   375
      Left            =   6240
      TabIndex        =   6
      Top             =   8160
      Width           =   5175
      _ExtentX        =   9128
      _ExtentY        =   661
      _Version        =   393216
      Appearance      =   0
      Arrows          =   65536
      LargeChange     =   5
      Max             =   100
      Orientation     =   1179649
      Value           =   50
   End
   Begin VB.ComboBox cmbPrinter 
      Height          =   360
      Left            =   1680
      Style           =   2  'Dropdown List
      TabIndex        =   3
      Tag             =   "SS"
      Top             =   7800
      Width           =   3255
   End
   Begin VB.ComboBox cmbPaper 
      Height          =   360
      Left            =   1680
      Style           =   2  'Dropdown List
      TabIndex        =   2
      Tag             =   "SS"
      Top             =   8160
      Width           =   3255
   End
   Begin VB.PictureBox picBox 
      AutoRedraw      =   -1  'True
      BackColor       =   &H0080FFFF&
      Height          =   7935
      Left            =   6240
      ScaleHeight     =   7875
      ScaleWidth      =   5115
      TabIndex        =   0
      Top             =   120
      Width           =   5175
      Begin VB.PictureBox picPage 
         AutoRedraw      =   -1  'True
         BackColor       =   &H00FFFFFF&
         Height          =   3735
         Left            =   960
         ScaleHeight     =   3675
         ScaleWidth      =   3195
         TabIndex        =   1
         Top             =   1200
         Width           =   3255
      End
   End
   Begin MSComCtl2.FlatScrollBar scbVertical 
      Height          =   7935
      Left            =   11520
      TabIndex        =   7
      Top             =   120
      Width           =   375
      _ExtentX        =   661
      _ExtentY        =   13996
      _Version        =   393216
      Appearance      =   0
      LargeChange     =   5
      Max             =   100
      Orientation     =   1179648
      Value           =   50
   End
   Begin VB.Label Label12 
      Caption         =   "Printer"
      Height          =   375
      Left            =   0
      TabIndex        =   5
      Top             =   7800
      Width           =   1575
   End
   Begin VB.Label Label13 
      Caption         =   "Paper"
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   8160
      Width           =   1575
   End
End
Attribute VB_Name = "frmReportFormatNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    ' Form Settings
    Dim BoxToWindowMargin As Long

    ' Printer Settings
    Dim NumForms As Long, i As Long
    Dim FI1 As FORM_INFO_1
    Dim aFI1() As FORM_INFO_1
    Dim Temp() As Byte
    Dim BytesNeeded As Long
    Dim PrinterName As String
    Dim PrinterHandle As Long
    Dim FormItem As String
    Dim RetVal As Long
    Dim FormSize As SIZEL
    Dim SetPrinter As Boolean
    Dim SuppliedWord As String
    Dim FSys As New Scripting.FileSystemObject
    Private csetPrinter As New cSetDfltPrinter
    
    'Page Settings
    Dim PageScale As Double
    Dim VerticalLocation As Double
    Dim HorizontalLocation As Double
    Dim PageTop As Double
    Dim PageLeft As Double
    Dim UppermostTop As Double
    Dim LowermostTop As Double
    Dim LeftmostLeft As Double
    Dim RightmostLeft As Double
    Dim PageToBoxMargin As Double
    
    
    'Mouse Scroll Settings
    Private Const PM_REMOVE = &H1
    
    Private Type POINTAPI
        x As Long
        Y As Long
    End Type
    
    Private Type Msg
        hwnd As Long
        Message As Long
        wParam As Long
        lparam As Long
        time As Long
        pt As POINTAPI
    End Type
    
    Private Declare Function PeekMessage Lib "user32" Alias "PeekMessageA" (lpMsg As Msg, ByVal hwnd As Long, ByVal wMsgFilterMin As Long, ByVal wMsgFilterMax As Long, ByVal wRemoveMsg As Long) As Long
    Private Declare Function WaitMessage Lib "user32" () As Long
'    Private bCancel As Boolean
    Private Const WM_MOUSEWHEEL = 522
    
    
    
Private Sub FillPrinters()
    Dim MyPrinter As Printer
    For Each MyPrinter In Printers
        cmbPrinter.AddItem MyPrinter.DeviceName
    Next
End Sub
    
Private Sub cmbPaper_Change()
    If SelectForm(cmbPaper.Text, Me.hdc) <> 1 Then
        MsgBox "Printer Error"
        Exit Sub
    End If
    Call ResizePage
End Sub

Private Sub cmbPaper_Click()
    Call cmbPaper_Change
End Sub

Private Sub cmbPrinter_Change()
    cmbPrinter_Click
End Sub

Private Sub cmbPrinter_Click()
    Dim MyPrinter As Printer
    csetPrinter.SetPrinterAsDefault ("")
    For Each MyPrinter In VB.Printers
        If MyPrinter.DeviceName = cmbPrinter.Text Then
            Set Printer = MyPrinter
        End If
    Next
    csetPrinter.SetPrinterAsDefault (cmbPrinter.Text)
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        ClosePrinter (PrinterHandle)
    End If
    csetPrinter.SetPrinterAsDefault (cmbPrinter.Text)
    cmbPaper.Clear
    PrinterName = Printer.DeviceName
    If OpenPrinter(PrinterName, PrinterHandle, 0&) Then
        With FormSize
            .cx = BillPaperHeight
            .cy = BillPaperWidth
        End With
        ReDim aFI1(1)
        RetVal = EnumForms(PrinterHandle, 1, aFI1(0), 0&, BytesNeeded, NumForms)
        ReDim Temp(BytesNeeded)
        ReDim aFI1(BytesNeeded / Len(FI1))
        RetVal = EnumForms(PrinterHandle, 1, Temp(0), BytesNeeded, BytesNeeded, NumForms)
        Call CopyMemory(aFI1(0), Temp(0), BytesNeeded)
        For i = 0 To NumForms - 1
            With aFI1(i)
                cmbPaper.AddItem PtrCtoVbString(.pName)
            End With
        Next i
        ClosePrinter (PrinterHandle)
    End If
End Sub


Private Sub GetSettings()
    BoxToWindowMargin = 250 + ((scbHorizontal.Height + scbVertical.Width) / 4)
    PageToBoxMargin = 150
    PageScale = 0
    Call GetCommonSettings(Me)
End Sub

Private Sub Command1_Click()
    txtX.Text = Empty
    txtY.Text = Empty
End Sub

Private Sub ProcessMessages()
'    Dim Message As Msg
'    Do While Not bCancel
'        WaitMessage 'Wait For message and...
'        If PeekMessage(Message, Me.hwnd, WM_MOUSEWHEEL, WM_MOUSEWHEEL, PM_REMOVE) Then '...when the mousewheel is used...
'            If Message.wParam < 0 Then '...scroll up...
'                PageScale = Round(PageScale * 1.1, 1)
'            Else '... or scroll down
'                PageScale = Round(PageScale * 0.9, 1)
'            End If
'        End If
'        DoEvents
'    Loop
'    Call ResizePage
End Sub

Private Sub Command2_Click()
    PageScale = Round(PageScale * 0.9, 1)
    Call ResizePage
End Sub

Private Sub Command3_Click()
    PageScale = Round(PageScale * 1.1, 1)
    Call ResizePage
End Sub

Private Sub Form_Load()
    Call FillPrinters
    Call GetSettings
    'bCancel = False
End Sub

Private Sub SaveSettings()
    SaveCommonSettings Me
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call SaveSettings
End Sub

Private Sub Form_Resize()
    With picBox
        .Left = Me.Width / 2
        .Top = BoxToWindowMargin
        .Height = Me.Height - (4 * BoxToWindowMargin)
        .Width = (Me.Width / 2) - (2 * BoxToWindowMargin)
    End With
    With scbHorizontal
        .Left = picBox.Left
        .Top = picBox.Top + picBox.Height
        .Width = picBox.Width
    End With
    With scbVertical
        .Left = picBox.Left + picBox.Width
        .Top = picBox.Top
        .Height = picBox.Height
    End With
    ResizePage
End Sub

Private Sub ResizePage()
    If cmbPaper.Text = "" Or cmbPrinter.Text = "" Then
        PageScale = 1
    End If
    
    If PageScale = 0 Then PageScale = 1
    If PageScale < 0.1 Then PageScale = 0.1
    If PageScale > 10 Then PageScale = 10
    
    
    With picPage
    
        
        txtY.Text = "Scale Width " & Printer.ScaleWidth / 1440 & " AND Scale Height " & Printer.ScaleHeight / 1440
        txtX.Text = "Width " & Printer.Width / 1440 & " AND Height " & Printer.Height / 1440
        
        If Printer.ScaleWidth > Printer.ScaleHeight Then
            .Width = picBox.Width * PageScale * 0.9
            .Height = (picBox.Width / Printer.Width) * picBox.Height * PageScale * 0.9
        Else
            .Height = picBox.Height * PageScale * 0.9
            .Width = (picBox.Height / Printer.Height) * picBox.Width * PageScale * 0.9
        End If
        

        
        
        If picBox.Width > (picPage.Width + (PageToBoxMargin * 2)) Then
            LeftmostLeft = PageToBoxMargin
            RightmostLeft = picBox.Width - picPage.Width - PageToBoxMargin
        Else
            LeftmostLeft = 0 - (picPage.Width - picBox.Width + PageToBoxMargin)
            RightmostLeft = PageToBoxMargin
        End If
        
        
        If picBox.Height > picPage.Height Then
        
        Else
        
        End If
        
        MoveHorizontally
        
        
        DoEvents
        .Refresh
        DoEvents
        picBox.Refresh
        DoEvents
    End With
    
End Sub


Private Sub MoveHorizontally()
        picPage.Left = LeftmostLeft + Abs((RightmostLeft - LeftmostLeft) * (scbHorizontal.Value / 100)) + PageToBoxMargin
End Sub


Private Sub MoveVerticall()

End Sub


Private Sub picBox_MouseMove(Button As Integer, Shift As Integer, x As Single, Y As Single)
    Dim Message As Msg
    WaitMessage 'Wait For message and...
    If PeekMessage(Message, Me.hwnd, WM_MOUSEWHEEL, WM_MOUSEWHEEL, PM_REMOVE) Then '...when the mousewheel is used...
        If Message.wParam < 0 Then '...scroll up...
            PageScale = Round(PageScale * 0.8, 2)
        Else '... or scroll down
            PageScale = Round(PageScale * 1.2, 2)
        End If
        Call ResizePage
        DoEvents
        DoEvents
    End If
End Sub

Private Sub scbHorizontal_Change()
        MoveHorizontally

End Sub
