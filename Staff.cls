VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Staff"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varName As String
    Private varStaffID As Long
    Private varAddress1 As String
    Private varTelephone As String
    Private varMobile As String
    Private varPositionID As Long
    Private varUserName As String
    Private varPassword As String
    Private varDeleted As Boolean
    Private varDeletedUserID As Long
    Private varDeletedDate As Date
    Private varDeletedTime As Date
    Private varComments As String

Public Property Let Name(ByVal vName As String)
    varName = vName
End Property

Public Property Get Name() As String
    Name = varName
End Property

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblStaff Where StaffID = " & varStaffID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !Name = varName
        !Address1 = varAddress1
        !Telephone = varTelephone
        !Mobile = varMobile
        !PositionID = varPositionID
        !UserName = varUserName
        !Password = varPassword
        !Deleted = varDeleted
        !DeletedUserID = varDeletedUserID
        !DeletedDate = varDeletedDate
        !DeletedTime = varDeletedTime
        !Comments = varComments
        .Update
        varStaffID = !StaffID
        If .State = 1 Then .Close
    End With
    
End Sub



Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblStaff WHERE StaffID = " & varStaffID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!Name) Then
               varName = !Name
            End If
            If Not IsNull(!StaffID) Then
               varStaffID = !StaffID
            End If
            If Not IsNull(!Address1) Then
               varAddress1 = !Address1
            End If
            If Not IsNull(!Telephone) Then
               varTelephone = !Telephone
            End If
            If Not IsNull(!Mobile) Then
               varMobile = !Mobile
            End If
            If Not IsNull(!PositionID) Then
               varPositionID = !PositionID
            End If
            If Not IsNull(!UserName) Then
               varUserName = !UserName
            End If
            If Not IsNull(!Password) Then
               varPassword = !Password
            End If
            If Not IsNull(!Deleted) Then
               varDeleted = !Deleted
            End If
            If Not IsNull(!DeletedUserID) Then
               varDeletedUserID = !DeletedUserID
            End If
            If Not IsNull(!DeletedDate) Then
               varDeletedDate = !DeletedDate
            End If
            If Not IsNull(!DeletedTime) Then
               varDeletedTime = !DeletedTime
            End If
            If Not IsNull(!Comments) Then
               varComments = !Comments
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varName = Empty
    varStaffID = 0
    varAddress1 = Empty
    varTelephone = Empty
    varMobile = Empty
    varPositionID = 0
    varUserName = Empty
    varPassword = Empty
    varDeleted = False
    varDeletedUserID = 0
    varDeletedDate = Empty
    varDeletedTime = Empty
    varComments = Empty
End Sub

Public Property Let StaffID(ByVal vStaffID As Long)
    Call clearData
    varStaffID = vStaffID
    Call loadData
End Property

Public Property Get StaffID() As Long
    StaffID = varStaffID
End Property

Public Property Let Address1(ByVal vAddress1 As String)
    varAddress1 = vAddress1
End Property

Public Property Get Address1() As String
    Address1 = varAddress1
End Property

Public Property Let Telephone(ByVal vTelephone As String)
    varTelephone = vTelephone
End Property

Public Property Get Telephone() As String
    Telephone = varTelephone
End Property

Public Property Let Mobile(ByVal vMobile As String)
    varMobile = vMobile
End Property

Public Property Get Mobile() As String
    Mobile = varMobile
End Property

Public Property Let PositionID(ByVal vPositionID As Long)
    varPositionID = vPositionID
End Property

Public Property Get PositionID() As Long
    PositionID = varPositionID
End Property

Public Property Let UserName(ByVal vUserName As String)
    varUserName = vUserName
End Property

Public Property Get UserName() As String
    UserName = varUserName
End Property

Public Property Let Password(ByVal vPassword As String)
    varPassword = vPassword
End Property

Public Property Get Password() As String
    Password = varPassword
End Property

Public Property Let Deleted(ByVal vDeleted As Boolean)
    varDeleted = vDeleted
End Property

Public Property Get Deleted() As Boolean
    Deleted = varDeleted
End Property

Public Property Let DeletedUserID(ByVal vDeletedUserID As Long)
    varDeletedUserID = vDeletedUserID
End Property

Public Property Get DeletedUserID() As Long
    DeletedUserID = varDeletedUserID
End Property

Public Property Let DeletedDate(ByVal vDeletedDate As Date)
    varDeletedDate = vDeletedDate
End Property

Public Property Get DeletedDate() As Date
    DeletedDate = varDeletedDate
End Property

Public Property Let DeletedTime(ByVal vDeletedTime As Date)
    varDeletedTime = vDeletedTime
End Property

Public Property Get DeletedTime() As Date
    DeletedTime = varDeletedTime
End Property

Public Property Let Comments(ByVal vComments As String)
    varComments = vComments
End Property

Public Property Get Comments() As String
    Comments = varComments
End Property


