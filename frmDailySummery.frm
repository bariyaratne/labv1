VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmDailySummery 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Daily Summery"
   ClientHeight    =   8835
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   13755
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8835
   ScaleWidth      =   13755
   Begin VB.Frame Frame1 
      Height          =   615
      Left            =   8640
      TabIndex        =   11
      Top             =   600
      Width           =   4935
      Begin VB.OptionButton optAll 
         Caption         =   "&All"
         Height          =   240
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Value           =   -1  'True
         Width           =   1215
      End
      Begin VB.OptionButton optWard 
         Caption         =   "&Ward"
         Height          =   240
         Left            =   3720
         TabIndex        =   14
         Top             =   240
         Width           =   975
      End
      Begin VB.OptionButton optCredit 
         Caption         =   "C&redit"
         Height          =   240
         Left            =   2520
         TabIndex        =   13
         Top             =   240
         Width           =   1215
      End
      Begin VB.OptionButton optCash 
         Caption         =   "&Cash"
         Height          =   240
         Left            =   1320
         TabIndex        =   12
         Top             =   240
         Width           =   1215
      End
   End
   Begin btButtonEx.ButtonEx btnCancel 
      Height          =   495
      Left            =   12360
      TabIndex        =   7
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "C&lose"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid gridIx 
      Height          =   6375
      Left            =   120
      TabIndex        =   6
      Top             =   1800
      Width           =   13455
      _ExtentX        =   23733
      _ExtentY        =   11245
      _Version        =   393216
   End
   Begin MSDataListLib.DataCombo cmbUser 
      Height          =   360
      Left            =   8640
      TabIndex        =   5
      Top             =   240
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   1440
      TabIndex        =   3
      Top             =   240
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "yyyy MMMM dd"
      Format          =   120455171
      CurrentDate     =   40447
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   1440
      TabIndex        =   4
      Top             =   720
      Width           =   4935
      _ExtentX        =   8705
      _ExtentY        =   661
      _Version        =   393216
      CustomFormat    =   "yyyy MMMM dd"
      Format          =   120455171
      CurrentDate     =   40447
   End
   Begin btButtonEx.ButtonEx btnPrint 
      Height          =   495
      Left            =   120
      TabIndex        =   8
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Print"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnExcel 
      Height          =   495
      Left            =   1440
      TabIndex        =   9
      Top             =   8280
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   873
      Appearance      =   3
      Caption         =   "&Excel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx btnProcess 
      Height          =   375
      Left            =   240
      TabIndex        =   10
      Top             =   1320
      Width           =   13335
      _ExtentX        =   23521
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "P&rocess"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      Caption         =   "Bill Type"
      Height          =   240
      Left            =   7440
      TabIndex        =   16
      Top             =   720
      Width           =   720
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      Caption         =   "User"
      Height          =   240
      Left            =   7440
      TabIndex        =   2
      Top             =   360
      Width           =   390
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "From"
      Height          =   240
      Left            =   240
      TabIndex        =   1
      Top             =   240
      Width           =   450
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "To"
      Height          =   240
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   225
   End
End
Attribute VB_Name = "frmDailySummery"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim temTopic As String
    Dim temSubTopic As String
    Dim temSelect As String
    Dim temFrom As String
    Dim temWhere As String
    Dim temGroupBy As String
    Dim temOrderBy As String
    Dim temSQL As String
    

Private Sub SaveSettings()
    Call SaveCommonSettings(Me)
End Sub

Private Sub GetSettings()
    dtpFrom.Value = DateSerial(Year(Date), Month(Date), 1)
    dtpTo.Value = Date
    Call GetCommonSettings(Me)
End Sub

Private Sub btnExcel_Click()
    GridToExcel gridIx, temTopic, temSubTopic
End Sub

Private Sub btnPrint_Click()

    Dim setDF As cSetDfltPrinter
    setDF ""
    
    Dim MyPrinter As Printer
    
    For Each MyPrinter In Printers
        If MyPrinter.DeviceName = ReportPrinterName Then
            Set Printer = MyPrinter
        End If
    Next
    
    DoEvents
    
    
    setDF ReportPrinterName

    Dim myPR As PrintReport
    
    GetPrintDefaults myPR
    
    GridPrint gridIx, myPR, temTopic, temSubTopic

End Sub

Private Sub btnProcess_Click()
    Dim D(5) As Integer
    Dim p(0) As Integer
    temTopic = "Daily Summery"
    If optAll.Value = True Then
        temTopic = temTopic & " - All Bills"
    ElseIf optCash.Value = True Then
        temTopic = temTopic & " - Cash Bills"
    ElseIf optCredit.Value = True Then
        temTopic = temTopic & " - Company Bills"
    ElseIf optWard.Value = True Then
        temTopic = temTopic & " - Inward Bills"
    End If
    
    If dtpFrom.Value = dtpTo.Value Then
        temSubTopic = "On " & Format(dtpFrom.Value, "dd MMMM yyyy")
    Else
        temSubTopic = "From " & Format(dtpFrom.Value, "dd MMMM yyyy") & " To " & Format(dtpTo.Value, "dd MMMM yyyy")
    End If
    temSelect = "SELECT format(tblPatientIxBill.Date, 'yyyy MMM dd')  AS [Bill Date], Count(tblPatientIxBill.PatientIxBillID) as [Number of Tests], Format(sum(tblPatientIx.HospitalFee),'Fixed') AS [Hospital Fee], Format(sum(tblPatientIx.StaffFee),'Fixed') AS [MLT Fee], Format(sum(tblPatientIx.OtherFee),'Fixed') AS [Reagent Fee], Format(sum(tblPatientIx.Value),'Fixed') AS Total "
    temFrom = "FROM (tblPatientIxBill LEFT JOIN tblPatientIx ON tblPatientIxBill.PatientIxBillID = tblPatientIx.PatientIxBillID) LEFT JOIN tblIx ON tblPatientIx.IxID = tblIx.IxID "
    temWhere = "WHERE (((tblPatientIxBill.Cancelled)=False) AND ((tblPatientIxBill.Date) Between #" & Format(dtpFrom.Value, "dd MMMM yyyy") & "# And #" & Format(dtpTo.Value, "dd MMMM yyyy") & "#) "
    If IsNumeric(cmbUser.BoundText) = True Then
        temWhere = temWhere & " AND ((tblPatientIxBill.UserID)=" & Val(cmbUser.BoundText) & ") "
        temSubTopic = temSubTopic & " User - " & cmbUser.Text
    End If
    
    If optCash.Value = True Then
        temWhere = temWhere & " AND ((tblPatientIxBill.BillType)= 'Cash') "
    ElseIf optWard.Value = True Then
        temWhere = temWhere & " AND ((tblPatientIxBill.BillType)= 'Inward') "
    ElseIf optCredit.Value = True Then
        temWhere = temWhere & " AND ((tblPatientIxBill.BillType)= 'Company') "
    ElseIf optAll.Value = True Then
    
    End If
    
    temWhere = temWhere & " AND ((tblPatientIx.Deleted)=False)) "
    
    
    temGroupBy = "GROUP BY tblPatientIxBill.Date  "
    
    temOrderBy = "ORDER BY tblPatientIxBill.Date  "
    
    D(0) = 1: D(1) = 2: D(2) = 3: D(3) = 4: D(4) = 5
    
    temSQL = temSelect & temFrom & temWhere & temGroupBy & temOrderBy
    FillAnyGrid temSQL, gridIx, 0, D, p
End Sub

Private Sub cmbUser_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        cmbUser.Text = Empty
    End If
End Sub


Private Sub Form_Load()
    Call FillCombos
    
    Call GetSettings
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Call SaveSettings
End Sub

Private Sub FillCombos()
    Dim Staff As New clsFillCombos
    Staff.FillSpecificField cmbUser, "Staff", "Name", False
End Sub
