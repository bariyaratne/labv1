VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "LabDetails"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
    Dim temSQL As String
    Private varLabID As Long
    Private varName As String
    Private varAddress As String
    Private varTelephone1 As String
    Private varTelephone2 As String
    Private varFax As String
    Private varRegNo As String
    Private varEmail As String
    Private varWeb As String
    Private varDescription As String

Public Sub saveData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblLabDetails Where LabID = " & varLabID
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount <= 0 Then .AddNew
        !Name = varName
        !Address = varAddress
        !Telephone1 = varTelephone1
        !Telephone2 = varTelephone2
        !Fax = varFax
        !RegNo = varRegNo
        !Email = varEmail
        !Web = varWeb
        !Description = varDescription
        .Update
        varLabID = !LabID
        If .State = 1 Then .Close
    End With
    
End Sub
Public Sub loadData()
 
    Dim rsTem As New ADODB.Recordset
    With rsTem
        temSQL = "SELECT * FROM tblLabDetails"
        If .State = 1 Then .Close
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        If .RecordCount > 0 Then
            If Not IsNull(!LabID) Then
               varLabID = !LabID
            End If
            If Not IsNull(!Name) Then
               varName = !Name
            End If
            If Not IsNull(!Address) Then
               varAddress = !Address
            End If
            If Not IsNull(!Telephone1) Then
               varTelephone1 = !Telephone1
            End If
            If Not IsNull(!Telephone2) Then
               varTelephone2 = !Telephone2
            End If
            If Not IsNull(!Fax) Then
               varFax = !Fax
            End If
            If Not IsNull(!RegNo) Then
               varRegNo = !RegNo
            End If
            If Not IsNull(!Email) Then
               varEmail = !Email
            End If
            If Not IsNull(!Web) Then
               varWeb = !Web
            End If
            If Not IsNull(!Description) Then
               varDescription = !Description
            End If
        End If
    If .State = 1 Then .Close
    End With
    
End Sub
Public Sub clearData()
    varLabID = 0
    varName = Empty
    varAddress = Empty
    varTelephone1 = Empty
    varTelephone2 = Empty
    varFax = Empty
    varRegNo = Empty
    varEmail = Empty
    varWeb = Empty
    varDescription = Empty
End Sub

Public Property Let LabID(ByVal vLabID As Long)
    Call clearData
    varLabID = vLabID
    Call loadData
End Property

Public Property Get LabID() As Long
    LabID = varLabID
End Property

Public Property Let Name(ByVal vName As String)
    varName = vName
End Property

Public Property Get Name() As String
    Name = varName
End Property

Public Property Let Address(ByVal vAddress As String)
    varAddress = vAddress
End Property

Public Property Get Address() As String
    Address = varAddress
End Property

Public Property Let Telephone1(ByVal vTelephone1 As String)
    varTelephone1 = vTelephone1
End Property

Public Property Get Telephone1() As String
    Telephone1 = varTelephone1
End Property

Public Property Let Telephone2(ByVal vTelephone2 As String)
    varTelephone2 = vTelephone2
End Property

Public Property Get Telephone2() As String
    Telephone2 = varTelephone2
End Property

Public Property Let Fax(ByVal vFax As String)
    varFax = vFax
End Property

Public Property Get Fax() As String
    Fax = varFax
End Property

Public Property Let RegNo(ByVal vRegNo As String)
    varRegNo = vRegNo
End Property

Public Property Get RegNo() As String
    RegNo = varRegNo
End Property

Public Property Let Email(ByVal vEmail As String)
    varEmail = vEmail
End Property

Public Property Get Email() As String
    Email = varEmail
End Property

Public Property Let Web(ByVal vWeb As String)
    varWeb = vWeb
End Property

Public Property Get Web() As String
    Web = varWeb
End Property

Public Property Let Description(ByVal vDescription As String)
    varDescription = vDescription
End Property

Public Property Get Description() As String
    Description = varDescription
End Property


