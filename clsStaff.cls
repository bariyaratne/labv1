VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsStaff"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarStaffID As Long 'local copy
Private mvarStaffName As String 'local copy
Private mvarPositionID As Long 'local copy
Public Property Let PositionID(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.PositionID = 5
    mvarPositionID = vData
End Property


Public Property Get PositionID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.PositionID
    PositionID = mvarPositionID
End Property



Public Property Let StaffName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.StaffName = 5
    mvarStaffName = vData
End Property


Public Property Get StaffName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.StaffName
    StaffName = mvarStaffName
End Property



Public Property Let StaffID(ByVal vData As Long)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.StaffID = 5
    mvarStaffID = vData
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "Select * from tblStaff where StaffID = " & mvarStaffID
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount > 0 Then
            mvarPositionID = !PositionID
            mvarStaffName = !Name
        End If
        If .State = 1 Then .Close
    End With

End Property


Public Property Get StaffID() As Long
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.StaffID
    StaffID = mvarStaffID
End Property



