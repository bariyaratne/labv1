VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{575E4548-F564-4A9D-8667-6EE848F77EB8}#1.0#0"; "ButtonEx.ocx"
Begin VB.Form frmLogin 
   BorderStyle     =   0  'None
   ClientHeight    =   6375
   ClientLeft      =   0
   ClientTop       =   60
   ClientWidth     =   8595
   ClipControls    =   0   'False
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmLogin.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmLogin.frx":29C12
   ScaleHeight     =   6375
   ScaleWidth      =   8595
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSDataListLib.DataCombo dtcDepartment 
      Height          =   360
      Left            =   4800
      TabIndex        =   4
      Top             =   4440
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Style           =   2
      Text            =   ""
   End
   Begin VB.TextBox txtTemUsername 
      Height          =   360
      Left            =   360
      TabIndex        =   7
      Top             =   1920
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox txtUserName 
      Height          =   375
      Left            =   4800
      TabIndex        =   0
      Top             =   4920
      Width           =   3615
   End
   Begin VB.TextBox txtPassword 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   345
      IMEMode         =   3  'DISABLE
      Left            =   4800
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   5400
      Width           =   3645
   End
   Begin btButtonEx.ButtonEx cmdCancel 
      Height          =   375
      Left            =   6720
      TabIndex        =   3
      Top             =   5880
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Cancel"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin btButtonEx.ButtonEx cmdOK 
      Height          =   375
      Left            =   4800
      TabIndex        =   2
      Top             =   5880
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      Appearance      =   3
      Caption         =   "&Login"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "&Department"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2880
      TabIndex        =   8
      Top             =   4440
      Width           =   1815
   End
   Begin VB.Label Label2 
      BackStyle       =   0  'Transparent
      Caption         =   "&Password"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2880
      TabIndex        =   6
      Top             =   5520
      Width           =   1815
   End
   Begin VB.Label Label1 
      BackStyle       =   0  'Transparent
      Caption         =   "&User Name"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   255
      Left            =   2880
      TabIndex        =   5
      Top             =   5040
      Width           =   1815
   End
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
    Dim FSys As New Scripting.FileSystemObject
    Dim rsStaff As New ADODB.Recordset
    Dim rsDept As New ADODB.Recordset
    Dim rsAds As New ADODB.Recordset
    Dim rsLab As New ADODB.Recordset
    Dim temSQL As String
    Dim constr As String
    Dim TemUserPassward As String
    
Private Sub cmdCancel_Click()
    End
End Sub

Private Sub cmdOK_Click()
    Dim TemResponce As Byte
    Dim UserNameFound As Boolean
    UserNameFound = False
    
    TemUserPassward = txtPassword.text
    If Trim(txtUserName.text) = "" Then
        TemResponce = MsgBox("You have not entered a username", vbCritical, "Username")
        txtUserName.SetFocus
        Exit Sub
    End If
    If Trim(txtPassword.text) = "" Then
        TemResponce = MsgBox("You have not entered a password", vbCritical, "Password")
        txtPassword.SetFocus
        Exit Sub
    End If
    If Not IsNumeric(dtcDepartment.BoundText) Then
        TemResponce = MsgBox("You have not selected a department", vbCritical, "Department")
        dtcDepartment.SetFocus
        Exit Sub
    End If
    With rsStaff
        If .State = 1 Then .Close
        temSQL = "Select tblstaff.* from tblstaff where Deleted = False"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount < 1 Then Exit Sub
        .MoveFirst
        Do While Not .EOF
            txtTemUsername.text = DecreptedWord(!UserName)
            If UCase(txtUserName.text) = UCase(txtTemUsername.text) Then
                UserNameFound = True
                If txtPassword.text = DecreptedWord(!Password) Then
                    UserName = UCase(txtUserName.text)
                    UserID = !StaffID
                    If Not IsNull(!PositionID) Then
                        UserPositionID = !PositionID
                    Else
                        UserPositionID = 0
                    End If
                    Exit Do
                Else
                    TemResponce = MsgBox("The username and password you entered are not matching. Please try again", vbCritical, "Wrong Username and Password")
                    txtUserName.SetFocus
                On Error Resume Next:     SendKeys "{home}+{end}"
                    Exit Sub
                End If
            Else
            End If
            .MoveNext
        Loop
        .Close
        If UserNameFound = False Then
            TemResponce = MsgBox("There is no such  a username, Please try again", vbCritical, "Username")
            txtUserName.SetFocus
        On Error Resume Next:     SendKeys "{home}+{end}"
            Exit Sub
        End If
        End With
        
        UserDept = dtcDepartment.text
        UserDeptID = dtcDepartment.BoundText
        
        Unload Me
        MDIMain.Show
        MDIMain.Caption = MDIMain.Caption & " - " & UserDept & " - " & UserName

End Sub


Private Sub setSecKey()
    Dim mySec As New clsSecurity
    ProgramVariable.SecurityKey = mySec.Decode(")?C{.�", "Buddhika")
End Sub

Private Sub Form_Load()

    setSecKey


    Dim TemResponce As Byte
    Dim WillExpire As Boolean
    Dim ExpiaryDate As Date
    
    Dim myAct As New clsActivation
    
    
    
    WillExpire = True
    ExpiaryDate = #12/15/2035#
    
    If WillExpire = True And ExpiaryDate < Date Then
        TemResponce = MsgBox("The Program has expiared. Please contact Lakmedipro for Assistant", vbCritical, "Expired")
        End
    End If
    
    Dim tempath As String
    Call LoadPreferances
    If Database = "" Then
        TemResponce = MsgBox("The path of the database you have selected does not exist. Please select the database", vbCritical, "Wrong database path")
        frmInitialPreferances.Show 1
    End If
    If FSys.FileExists(Database) = False Then
        TemResponce = MsgBox("The path of the database you have selected does not exist. Please select the database", vbCritical, "Wrong database path")
        frmInitialPreferances.Show 1
    End If
    
'    DatabasePath = FSys.GetParentFolderName(Database)
    
    constr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source= " & Database & " ;Mode=ReadWrite|Share Deny None;Persist Security Info=False"
    cnnLab.Open constr
    DataEnvironment1.Connection1.ConnectionString = "data source=" & constr  'GetSetting(App.EXEName, "Options", "DatabaseLocation", App.Path & "\hospital.mdb")
    Call LoadInstitutionDetails
    Call updateDatabases
    Call FillCombo
    Call GetAds
    
    dtcDepartment.text = GetSetting(App.EXEName, "Options", "dtcDepartment", "")
    
    If myAct.isActivated = False Then
        frmActivate.Show
        frmActivate.ZOrder 0
        Unload Me
        Exit Sub
    End If

End Sub


Private Sub updateDatabases()
    addStaffToIxTable
    addFeeToDeptTable
    addOrderNoToControlsTable
End Sub

Private Sub addStaffToIxTable()
    On Error Resume Next
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "ALTER TABLE tblPatientIx ADD COLUMN StaffID INTEGER;"
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        .Update
        If .State = 1 Then .Close
    End With
End Sub

Private Sub addOrderNoToControlsTable()
    On Error Resume Next
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "ALTER TABLE tblControl ADD COLUMN OrderNo INTEGER;"
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        .Update
        If .State = 1 Then .Close
    End With
End Sub

Private Sub addFeeToDeptTable()
    On Error Resume Next
    Dim rsTem As New ADODB.Recordset
    With rsTem
        If .State = 1 Then .Close
        temSQL = "ALTER TABLE tblDept ADD COLUMN HospitalFee DOUBLE, MLTFee DOUBLE, ReagentFee Double;"
        .Open temSQL, cnnLab, adOpenStatic, adLockOptimistic
        .Update
        If .State = 1 Then .Close
    End With
End Sub


Private Sub FillCombo()
    With rsDept
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblDept where deleted = false order by Dept"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
    End With
    With dtcDepartment
        Set .RowSource = rsDept
        .ListField = "Dept"
        .BoundColumn = "DeptID"
    End With
End Sub

Private Sub GetAds()
'With rsAds
'    If .State = 1 Then .Close
'    temSql = "select * from tblads"
'    .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
'    If .RecordCount > 0 Then
'        Dim temnum As Long
'        Randomize
'        temnum = Round((Rnd * .RecordCount - 1), 0) + 1
'        If temnum < 1 Then temnum = 0
'        If temnum > .RecordCount Then temnum = .RecordCount
'        If .State = 1 Then .Close
'        temSql = "select * from tblads where id = " & temnum
'        .Open temSql, cnnLab, adOpenStatic, adLockReadOnly
'        If .RecordCount > 1 Then
'            LongAd = !AdLong
'            ShortAd = !AdShort
'        End If
'    End If
'    .Close
'End With
End Sub


Private Sub Form_Unload(Cancel As Integer)
    SaveSetting App.EXEName, "Options", "dtcDepartment", dtcDepartment.text
End Sub

Private Sub txtPassword_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 And txtUserName.text <> "" Then cmdOK_Click: Exit Sub
    If KeyAscii = 13 And txtUserName.text = "" Then txtUserName.SetFocus: Exit Sub
End Sub

Private Sub txtUserName_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then txtPassword.SetFocus
End Sub

Private Sub LoadPreferances()
    Database = GetSetting(App.EXEName, "Options", "Database", "")
    DatabaseLocation = GetSetting(App.EXEName, "Options", "DatabasePath")
    LongDateFormat = GetSetting(App.EXEName, "Options", "LongDateFormat", "dddd, dd MMMM yyyy")
    ShortDateFormat = GetSetting(App.EXEName, "Options", "ShortDateFormat", "dd MM yy")
    BillPrinterName = GetSetting(App.EXEName, "Options", "BillPrinterName", "")
    BillPaperName = GetSetting(App.EXEName, "Options", "BillPaperName", "")
    ReportPrinterName = GetSetting(App.EXEName, "Options", "ReportPrinterName", "")
    ReportPaperName = GetSetting(App.EXEName, "Options", "ReportPaperName", "")
End Sub

Private Sub LoadInstitutionDetails()
    Dim rsHospital As New ADODB.Recordset
    With rsHospital
        If .State = 1 Then .Close
        temSQL = "SELECT * from tblLabDetails"
        .Open temSQL, cnnLab, adOpenStatic, adLockReadOnly
        If .RecordCount < 1 Then Exit Sub
        HospitalName = DecreptedWord(!Name)
        HospitalDescreption = DecreptedWord(!Description)
        HospitalAddress = DecreptedWord(!Address)
        Telephone1 = DecreptedWord(!Telephone1)
        If .State = 1 Then .Close
    End With
End Sub
